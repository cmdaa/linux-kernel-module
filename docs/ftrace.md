[ftrace](https://www.kernel.org/doc/Documentation/trace/ftrace.txt) is a Linux
Kernel utility to allow hooking Kernel functions
- [Hooking Linux Kernel Functions with ftrace](https://www.apriorit.com/dev-blog/546-hooking-linux-functions-2);
- [What are the main Pros and Cons of ftrace](https://www.apriorit.com/dev-blog/547-hooking-linux-functions-3);

On a fresh install of Debian 9 and CentOS 7 (via the installation process
described in the [README](README.md)), `ftrace` **is** enabled:
``` bash
sudo sysctl kernel.ftrace_enabled
kernel.ftrace_enabled = 1
```


While `ftrace` is typically used to trace kernel functions, it can be leveraged
to hook function calls. Since it does not use breakpoints, it has a lower
overhead than `kprobes`, but is more fragile.

One [unexpected surprise of using
`ftrace`](https://www.apriorit.com/dev-blog/547-hooking-linux-functions-3#A3)
stems from the dynamic register modification combined with compiler
optimizations. In certain compilation conditions, the compiler will perform
*tail optimization* relying on the `parent_ip` for the return instead of
allocating a full stack frame. Since this method of hooking results in an
incorrect `parent_ip`, which actually points to the injected function, this
optimization results in an infinite recursive loop.

There are two ways to avoid this issue:
- ensure that there is code *that will not be optimized out* between the call
  to the real kernel function and the final return;
- use a `gcc` pragma to turn off *tail optimization* for the file with the
  wrapper functions: `#pragma GCC optimize("-fno-optimize-sibling-calls")`;
