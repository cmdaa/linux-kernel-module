KernelBeat metrics

- cpu
  - user      : CPU time spent in user space {u64};
  - nice      : CPU time spent on low-priority processes {u64};
  - sys       : CPU time spent in kernel space {u64};
  - idle      : CPU time spent idle {u64};
  - iowait    : CPU time spent waiting on disk access {u64};
  - irq       : CPU time spent servicing hardware interrupts {u64};
  - softIrq   : CPU time spent servicing software interrupts {u64};
  - steal     : CPU time spent in involuntary wait by the virtual CPU while the
                hypervisor was servicing another processor {u64};
  - guest     : CPU time spent by a virtual CPU in a hypervisor {u64};
  - guestNice : time spent by a virtual CPU in a hypervisor on low-priority
                processes {u64};
  
- cgroup_v1 [ per-cgroup, by cgroup subsystem ]

  All cgroup-related entries will have:
  - id        : unique cgroup id {u32};
  - hid       : cgroup hierarchy id {u32};
  - path      : /sys/fs/cgroup path for the cgroup {string};

  Then, each cgroup subsystem has a specific format:
  - blkio
    - id, hid, path (blkio/)
    - blkio.weight
    - blkio.leaf_weight
    - devices (per-block-device)
      - id            : device id MKDEV(major, minor) {u32};
      - name          : device name '%major%,%minor%' {string};
      - blkio.time
      - blkio.time_recursive
      - blkio.sectors
      - blkio.sectors_recursive
      - blkio.throttle.io_service_bytes          {read,write,sync,async,total}
      - blkio.throttle.io_serviced               {read,write,sync,async,total}
      - blkio.throttle.io_service_time           {read,write,sync,async,total}
      - blkio.throttle.io_wait_time              {read,write,sync,async,total}
      - blkio.throttle.io_merged                 {read,write,sync,async,total}
      - blkio.throttle.io_queued                 {read,write,sync,async,total}
      - blkio.throttle.io_service_bytes_recursive{read,write,sync,async,total}
      - blkio.throttle.io_serviced_recursive     {read,write,sync,async,total}
      - blkio.throttle.io_service_time_recursive {read,write,sync,async,total}
      - blkio.throttle.io_wait_time_recursive    {read,write,sync,async,total}
      - blkio.throttle.io_merged_recursive       {read,write,sync,async,total}
      - blkio.throttle.io_queued_recursive       {read,write,sync,async,total}
  - cpu
    - id, hid, path (cpu,cpuacct/)
    - cpu.shares
    - cpu.cfs_quota_us
    - cpu.cfs_period_us
    - cpu.stat.nr_periods
    - cpu.stat.nr_throttled
    - cpu.stat.throttled_time
  - cpuacct
    - id, hid, path (cpu,cpuacct/)
    - cpuacct.usage
    - cpuacct.usage_user
    - cpuacct.usage_sys
    - cpuacct.usage_percpu      [ 1-per-cpu ]
    - cpuacct.usage_percpu_user [ 1-per-cpu ]
    - cpuacct.usage_percpu_sys  [ 1-per-cpu ]
  - cpuset
    - id, hid, path (cpuset/)
    - cpuset.cpu_exclusive
    - cpuset.mem_exclusive
    - cpuset.mem_hardwalle
    - cpuset.memory_migrate
    - cpuset.memory_pressure
    - cpuset.memory_spread_page
    - cpuset.memory_spread_slab
    - cpuset.memory_pressure_enabled
    - cpuset.sched_load_balance
    - cpuset.sched_relax_domain_level
  - freezer
    - id, hid, path (freezer/)
    - freezer.self_freezing
    - freezer.parent_freezing
  - memory
    - id, hid, path (memory/)
    - memory.failcnt
    - memory.kmem.failcnt
    - memory.kmem.limit_in_bytes
    - memory.kmem.max_usage_in_bytes
    - memory.kmem.usage_in_bytes
    - memory.limit_in_bytes
    - memory.max_usage_in_bytes
    - memory.usage_in_bytes
    - memory.soft_limit_in_bytes
    - memory.move_charge_at_immigrate
    - memory.swappiness
    - memory.use_hierarchy
    - memory.kmem.tcp.failcnt
    - memory.kmem.tcp.limit_in_bytes
    - memory.kmem.tcp.max_usage_in_bytes
    - memory.kmem.tcp.usage_in_bytes
  - net_cls
    - id, hid, path (net_cls,net_prio/)
    - net_cls.classid
  - net_prio
    - id, hid, path (net_cls,net_prio/)
    - net_prio.prioidx
    - interfaces (per-network-interface)
      - name
      - priority
  - pids
    - id, hid, path
    - pids.current
    - pids.max
  
- filesystems [ per-filesystem ]
  - id          : filesystem id (superblock device id) {u32};
  - type        : Filesystem type {string};
  - filesTotal  : file count {u64};
  - filesFree   : file count {u64};
  - sizeTotal   : size (bytes) {u64};
  - sizeFree    : size (bytes) {u64};
  - sizeAvail   : size (bytes) available to unprivileged users {u64};
  - devName     : absolute path of the mounted device {string};
  - mountpoint  : absolute path where the filesystem is mounted {string};

- fsstats (filesytem summary)
  - count       : number of mounted filesystems {u32};
  - filesTotal  : file count for all filesystem {u64};
  - filesFree   : file count {u64};
  - sizeTotal   : size (bytes) {u64};
  - sizeFree    : size (bytes) {u64};
  - sizeAvail   : size (bytes) available to unprivileged users {u64};

- load
  - one         : average (%) for the last 1 minute {u32};
  - five        : average (%) for the last 5 minutes {u32};
  - fifteen     : average (%) for the last 15 minutes {u32};
  - cores       : number of CPU cores {u32};

- mem
  - total             : memory (bytes) {u64};
  - free              : free (bytes) {u64};
  - available         : available memory (bytes) {u64};
  - used              : used memory (bytes) [total - free] {u64};
  - actualUsed        : actual used memory (bytes) [total - available] {u64};
  - buffer            : used as buffers (bytes) {u64};
  - shared            : memory (bytes) {u64};
  - cached            : used as cache {bytes) {u64};
  - swapTotal         : swap memory (bytes) {u64};
  - swapFree          : swap memory (bytes) {u64};
  - swapUsed          : swap memory (bytes) [swapTotal - swapFree] {u64};
  - swapCached        : swap memory (bytes) {u64};
  - highTotal         : high memory (bytes) {u64};
  - highFree          : high memory (bytes) {u64};
  - lowTotal          : low memory (bytes) {u64};
  - lowFree           : low memory (bytes) {u64};
  - commitLimit       : memory commit limit (bytes) {u64};
  - commited          : virtual memory (bytes) {u64};
  - kernStack         : kernel stack size (bytes) {u64};
  - vmallocTotal      : virtual memory (bytes) {u64};

  - active            : memory pages {u32};
  - inactive          : memory pages {u32};
  - activeAnon        : anonymous pages {u32};
  - inactiveAnon      : anonymous pages {u32};
  - activeFile        : file pages {u32};
  - inactiveFile      : file pages {u32};
  - unevictable       : unevictable pages (e.g. RAMfs locked shared memory
                        locked virtual memory) {u32};
  - mlock             : pages {u32};
  - dirty             : writable pages {u32};
  - writeback         : under writeback {u32};
  - anonMapped        : mapped pages {u32};
  - nfsUnstable       : unstable pages {u32};

  - slab              : in a slab {u32};
  - slabReclaimable   : slabs {u32};
  - slabUnreclaimable : slabs {u32};

  - pageTables        : of page tables {u32};
  - bounce            : for bounce buffers {u32};
  - writebackTmp      : writeback pages {u32};

  - unit              : size (bytes) {u32};

  - hugePagesTotal    : number of huge pages {u32};
  - hugePagesFree     : huge pages {u32};
  - hugePagesReserved : huge pages {u32};
  - hugePagesSurplus  : huge pages {u32};
  - hugePageSize      : page size (bytes) {u32};

- networks [ per-interface ]
  - name          : interface name (e.g. "lo") {string};
  - id            : id / index {u32};

  - rxBytes       : (bytes) {u64};
  - rxPackets     : (packets) {u64};
  - rxErrors      : receive errors {u64};
  - rxErrDropped  : errors due to packet drop or missed {u64};
  - rxErrFifo     : errors due to FIFO overruns {u64};
  - rxErrFrame    : errors due to framing issues (general length ring buffer
                    overflow CRC errors frame alignment) {u64};
  - rxCompressed  : packets received {u64};

  - txBytes       : (bytes) {u64};
  - txPackets     : (packets) {u64};
  - txErrors      : transmit errors {u64};
  - txErrDropped  : errors due to packet drop (no space) {u64};
  - txErrFifo     : errors due to FIFO overruns {u64};
  - txErrCarrier  : errors due to carrier issues (general aborted heartbeat
                    window) {u64};
  - txCompressed  : packets transmitted {u64};

  - multicast     : packets received {u64};
  - collisions    : collisions {u64};

- processes [ per-process ]
  - name          : process name {string};
  - state         : process state {string}:
                      R (running)
                      S (sleeping)
                      D (disk sleep)
                      T (stopped)
                      t (tracing stop)
                      X (dead)
                      Z (zombie)
                      x (dead)
                      K (wake kill)
                      W (waking)
                      P (parked)
                      N (noload)
                      n (new)
  - pid           : process id (ALWAYS included if a process has changed in any
                    way) {s32};
  - ppid          : process id {s32};
  - pgrp          : group id {s32};
  - uid           : user id {u32};
  - cgroups       : array of "%cgroup/subsystem-id%" recording the set of
                    cgroups to which this process belongs. These ids reference
                    entries in the most recent top-level 'cgroup_v1' set
                    {array};

  - cpuStart      : start time (seconds since epoch) {u64};
  - cpuUser       : time spent in user space {u32};
  - cpuSys        : time spent in kernel space {u32};

  - fdOpen        : number of currently opened files {u32};
  - fdLimitHard   : limit number of open files {u32};
  - fdLimitSoft   : limit number of open files {u32};

  - memRss        : allocated and in RAM (bytes) (Resident Set Size) {u64};
  - memShare      : {u64};
  - memSize       : Virtual memory size (bytes) all memory accessible by this
                    process including allocated and in-use allocated but not
                    in-use swapped out from shared libraries {u64};

  - memFaultsMinor: number of minor memory faults (no disk access) {u32};
  - memFaultsMajor: number of major memory faults requiring disk access
                    {u32};


  - cmdLine       : full process command line including all
                    arguments (limited to 512characters) {string};

- process_summary
  - total     : total number of processes {u32};
  - sleeping  : sleeping processes (S) {u32};
  - running   : running processes (R) {u32};
  - idle      : idle processes (D) {u32};
  - stopped   : stopped processed (T, t) {u32};
  - zombie    : zombie processes (Z) {u32};
  - dead      : dead processes (X) {u32};
  - unknown   : all others (x, K, W, P, N, n) {u32};

- process_terminated
  - (same as a `process` record with additional fields)
  - exit          : exit code {s64 | s32};

  //=========================================================================
  // The following is include iff process termination was due to an
  // exception/signal
  - exception     : indication of the exception {string};
  - error         : error code {s64 | s32};
  - trapNum       : trap number {s32};
  - sigNum        : signal number {s32};

  // Currently only x86 is supported;
  - regIp         : instruction pointer (register) {u64 | u32};
  - regSp         : stack pointer       (register) {u64 | u32};

  // The following SHOULD BE architecture dependant but is currently limited
  // to x86;
  - regFlags      : register flags {u64 | u32};
  - regAx         : AX register {u64 | u32};
  - regAxOrig     : original AX register {u64 | u32};
  - regBp         : BP register {u64 | u32};
  - regCs         : CS register {u64 | u32};
  - regSs         : SS register {u64 | u32};
  - regCx         : CX register {u64 | u32};
  - regDx         : DX register {u64 | u32};
  - regDi         : DI register {u64 | u32};
  - regSi         : SI register {u64 | u32};


- uptime
  - seconds   : uptime (seconds) {u32};
