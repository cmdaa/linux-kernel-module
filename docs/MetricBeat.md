Tracing [MetricBeat](https://github.com/elastic/beats/tree/6.5/metricbeat) data flow to the kernel source where the data is generated.

The `MetricBeat.system` module ([metricbeat/module/system](https://github.com/elastic/beats/tree/6.5/metricbeat/module/system))
makes use of [libbeat/metrics/system](https://github.com/elastic/beats/tree/6.5/libbeat/metric/system)
which in turn uses [sigar](https://github.com/elastic/beats/tree/6.5/vendor/github.com/elastic/gosigar).


Note that we are limited in what kernel structure a module may access. Only
those symbols that are explicitly exported (`EXPORT_SYMBOL` or
`EXPORT_SYMBOL_GPL`) may be accessed from a module.

---
###### TOC
- [sigar](#sigar)
- [gopsutil](#gopsutil)
- [MetricBeat.system.cpu](#mb-cpu)
  - [LibBeat.metric.system.cpu](#lb-cpu)
  - [sigar.Cpu](#sigar-cpu)
  - [/proc/stat](#proc-stat) : [linux/fs/proc/stat.c](#linux-stat)
- [MetricBeat.system.memory](#mb-mem)
  - [LibBeat.metric.system.mem](#lb-mem)
  - [sigar.Mem](#sigar-mem)
  - [/proc/meminfo](#proc-meminfo) : [linux/fs/proc/meminfo.c](#linux-meminfo)
- [MetricBeat.system.process_summary](#mb-psum)
  - [LibBeat.metric.system.process](#lb-proc)
  - [sigar.Mem](#sigar-proc)
  - [/proc/%pid%/stat](#proc-stat) : [linux/fs/proc/array.c](#linux-stat)
- [MetricBeat.system.network](#mb-net)
  - [gopsutil.Net](#goptsutil-net)
  - [/proc/net/dev](#proc-net) : [linux/fs/proc/net.c](#linux-net)
- [MetricBeat.system.process.cgroup](#mb-cgroup)
---

## <a name="sigar">sigar</a>

[sigar](https://github.com/elastic/beats/tree/6.5/vendor/github.com/elastic/gosigar) is a go-based library that gathers metrics using a standardized
interface to provide cross-platform support.

For `linux` `sigar` makes use of `/proc`.

## <a name="gopsutil">gopsutil</a>

[gopsutil](https://github.com/elastic/beats/tree/6.5/vendor/github.com/shirou/gopsutil)
is a go-based library that gathers network-based metrics using a standardized
interface to provide cross-platform support.

For `linux` `gopsutil` makes use of `/proc/net/dev`.


## <a name="mb-cpu">MetricBeat.system.cpu module</a>

### [metricbeat/module/system/cpu/cpu.go](https://github.com/elastic/beats/blob/6.5/metricbeat/module/system/cpu/cpu.go#L66)


``` go
import "github.com/elastic/beats/libbeat/metric/system/cpu"
type MetricSet struct {
  mb.BaseMetricSet
  config Config
  cpu    *cpu.Monitor
}

func (m *MetricSet) Fetch(r mb.ReporterV2) {
  sample, err := m.cpu.Sample()     // ************** libbeat vvvvv
  /* => Sigar       /proc/stats
   *    User        cpu field 1
   *    Nice        cpu field 2
   *    Sys         cpu field 3
   *    Idle        cpu field 4
   *    Wait        cpu field 5
   *    Irq         cpu field 6
   *    SoftIrq     cpu field 7
   *    Stolen      cpu field 8
   */
  ...
}
```


### <a name="lb-cpu">[libbeat/metric/system/cpu/cpu.go](https://github.com/elastic/beats/blob/6.5/libbeat/metric/system/cpu/cpu.go#L43)</a>

``` go
import sigar "github.com/elastic/gosiger"

type Monitor struct {
  lastSample *sigar.Cpu
}

func (m *Monitor) Sample() (*Metric, error) {
  cpuSample := &sigar.Cpu{}       // ************** sigar vvvvv
  if err := cpuSample.Get(); err != nil {
    return nil, err
  }

  oldLastSample := m.lastSample
  m.lastSample  := cpuSample
  return &Metric(oldLastSample, cpuSample), nil
}
```


### <a name="mb-gosigar">[vendor/github.com/elastic/gosigar](https://github.com/elastic/beats/tree/6.5/vendor/github.com/elastic/gosigar)</a>

#### [sigar_linux.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/elastic/gosigar/sigar_linux.go#L15)
``` go
Procd = "/proc"
```

#### [sigar_interface.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/elastic/gosigar/sigar_interface.go#L207)
``` go
type Sigar interface {
  CollectCpuStats(collectionInterval time.Duration) (<-chan Cpu, chan<- struct{})
  ...
}

type Cpu struct {
  User    uint64
  Nice    uint64
  Sys     uint64
  Idle    uint64
  Wait    uint64
  Irq     uint64
  SoftIrq uint64
  Stolen  uint64
}
```

#### <a name="sigar-cpu">[sigar_linux_common.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/elastic/gosigar/sigar_linux_common.go#L92)</a>
``` go
func (self *Cpu) Get() error {
  // Read from '/proc/stat'
  return readFile( Procd +"/stat", func( line string ) bool {
    /*
     * find and parse the line that begins with 'cpu '
     *  parseCpuStat( self, line )
     *
     * This is an aggregate of all following 'cpu# ' entries
     *
     *  Fields: 1:user,    2:nice, 3:sys,     4:idle,
     *          5:iowait,  6:irq,  7:softIrq, 8:stolen
     *         [9:guest,  10:???]
     */
  })
}
```

### <a name="proc-stat">/proc/stat</a>

Managed by the linux kernel via
<a name="linux-stat">[linux/fs/proc/stat.c](https://github.com/torvalds/linux/blob/v4.9/fs/proc/stat.c#L80)</a>
``` c
int show_stat( sturct seq_file* p, void* v) {
  int i;
  u64 user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice;

  for_each_possible_cpu(i) {
    user      += kcpustat_cpu(i).cpustat[CPUTIME_USER];
    nice      += kcpustat_cpu(i).cpustat[CPUTIME_NICE];
    system    += kcpustat_cpu(i).cpustat[CPUTIME_SYSTEM];
    irq       += kcpustat_cpu(i).cpustat[CPUTIME_IRQ];
    softirq   += kcpustat_cpu(i).cpustat[CPUTIME_SOFTIRQ];
    steal     += kcpustat_cpu(i).cpustat[CPUTIME_SOFTIRQ];
    guest     += kcpustat_cpu(i).cpustat[CPUTIME_GUEST];
    guestnice += kcpustat_cpu(i).cpustat[CPUTIME_GUEST_NICE];
    idle      += get_idle_time(i);
    iowait    += get_iowait_time(i);

    ...
  }

  ... cputime64_to_clock_t( user );
  ... cputime64_to_clock_t( nice );
  ... cputime64_to_clock_t( system );
  ... cputime64_to_clock_t( idle );
  ... cputime64_to_clock_t( iowait );
  ... cputime64_to_clock_t( irq );
  ... cputime64_to_clock_t( softirq );
  ... cputime64_to_clock_t( steal );
  ... cputime64_to_clock_t( guest );
  ... cputime64_to_clock_t( guestnice );

  ...
}

#ifdef arch_idle_time
cputime64_t get_idle_time( int cpu ) {
  cputime64_t idle;
  idle = kcpustat_cpu(cpu).cpustat[CPUTIME_IDLE];
  if (cpu_online(cpu) && !nr_iowait_cpu(cpu)) {
    idle += arch_idle_time(cpu);
  }
  return idle;
}

cputime64_t get_iowait_time( int cpu ) {
  cputime64_t iowait;
  iowait = kcpustat_cpu(cpu).cpustat[CPUTIME_IOWAIT];
  if (cpu_online(cpu) &&  nr_iowait_cpu(cpu)) {
    iowait += arch_idle_time(cpu);
  }
  return idle;
}

#else
u64 get_idle_time( int cpu ) {
  u64 idle, idle_time = -1ULL;

  if (cpu_online(cpu)) { idle_time = get_cpu_idle_time_us(cpu, NULL); }

  if (idle_time == -1ULL) {
    // !NO_HZ or cpu offline so rely on cpustat.idle
    idle = kcpustat_cpu(cpu).cpustat[CPUTIME_IDLE];
  } else {
    idle = usecs_to_cputime64(idle_time);
  }
  return idle;
}

u64 get_iowait_time( int cpu ) {
  u64 iowait, iowait_time = -1ULL;

  if (cpu_online(cpu)) { iowait_time = get_cpu_iowait_time_us(cpu, NULL); }

  if (iowait_time == -1ULL) {
    // !NO_HZ or cpu offline so rely on cpustat.iowait
    iowait = kcpustat_cpu(cpu).cpustat[CPUTIME_IOWAIT];
  } else {
    iowait = usecs_to_cputime64(iowait_time);
  }
  return iowait;
}
#endif

```

---

## <a name="mb-mem">MetricBeat.system.memory module</a>

### [metricbeat/module/system/memory/memory.go](https://github.com/elastic/beats/blob/6.5/metricbeat/module/system/memory/memory.go)

``` go
import mem "github.com/elastic/beats/libbeat/metric/system/memory"

type MetricSet struct {
  mb.BaseMetricSet
}

func (m *MetricSet) Fetch(r mb.ReporterV2) {
  memStat, err := mem.Get()         // ************** libbeat vvvvv
  /* => Sigar       /proc/meminfo
   *    Total       MemTotal
   *    Free        MemFree
   *    ActualFree  MemAvailable
   *                    or
   *                MemFree + Buffers + Cached
   *    Used        MemTotal - MemFree
   *    ActualUsed  MemTotal - sigar[ActualFree]
   */
  ...
  mem.AddMemPercentage(memStat)
  // => ActualUsed / Total

  swapStat, err := mem.GetSwap()
  /* => Sigar       /proc/meminfo
   *    Total       SwapTotal
   *    Free        SwapFree
   *    Used        SwapTotal - SwapFree
   *
   *    if Free > Total || Used > Total
   *        Free = Total
   *        Used = 0
   */
  ...
  mem.AddSwapPercentage(swapStat)
  // => Used / Total
  ...
  hugePageStat, err := mem.GetHugeTLBPages()
  /* => Sigar               /proc/meminfo
   *    Total               HugePages_Total     **NOT accessible**
   *    Free                HugePages_Free      **NOT accessible**
   *    Reserved            HugePages_Rsvd      **NOT accessible**
   *    Surplus             HugePages_Surp      **NOT accessible**
   *    DefaultSize         Hugepagesize        **NOT accessible**
   *    TotalAllocatedSize  Hugetlb             **NOT accessible**
   *                            or
   *                        (HugePages_Total
   *                          - HugePages_Free
   *                          + HugePages_Rsvd) * Hugepagesize
   */

  ...
  mem.AddHugeTLBPagesPercentage(hugePageStat)
  // => (Total != 0 ? (Total - Free + Reserved) / Total)
  ...
}
```

### <a name="lb-mem">[libbeat/metric/system/memory/memory.go](https://github.com/elastic/beats/blob/6.5/libbeat/metric/system/memory/memory.go)</a>

``` go
import sigar "github.com/elastic/gosiger"

type MemStat struct {
  sigar.Mem
  UsedPercent       float64 `json:"used_p"`
  ActualUsedPercent float64 `json:"actual_used_p"`
}

func Get() (*MemStat, error) {
  mem := sigar.Mem{}              // ************** sigar vvvvv
  err := mem.Get()
  ...

  return &MemStat{Mem: mem}, nil
}
```

### [vendor/github.com/elastic/gosigar](https://github.com/elastic/beats/tree/6.5/vendor/github.com/elastic/gosigar)

#### [sigar_linux.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/elastic/gosigar/sigar_linux.go#L15)
``` go
Procd = "/proc"
```

#### [sigar_interface.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/elastic/gosigar/sigar_interface.go#L207)
``` go
type Sigar interface {
  ...
  GetMem() (Mem, error)
  GetSwap() (Swap, error)
  GetHugeTLBPages(HugeTLBPages, error)
  ...
}

...

type Mem struct {
  Total      uint64
  Used       uint64
  Free       uint64
  ActualFree uint64
  ActualUsed uint64
}

type Swap struct {
  Total uint64
  Used  uint64
  Free  uint64
}

type HugeTLBPages struct {
  Total              uint64
  Free               uint64
  Reserved           uint64
  Surplus            uint64
  DefaultSize        uint64
  TotalAllocatedSize uint64
}
```

#### <a name="sigar-mem">[sigar_linux_common.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/elastic/gosigar/sigar_linux_common.go#L54)</a>
``` go
func (self *Mem) Get() error {
  table, err := parseMeminfo()
  ...
}

func parseMeminfo (map[string]uint64, error) {
  table := map[string]uint64{}

  // Read from '/proc/meminfo'
  err := readFile( Procd +"/meminfo", func( line string ) bool {
    /* Example:
     *  MemTotal:        8179720 kB
     *  MemFree:         7714332 kB
     *  MemAvailable:    7851648 kB
     *  Buffers:           27912 kB
     *  Cached:           308748 kB
     *  SwapCached:            0 kB
     *  Active:           143476 kB
     *  Inactive:         222020 kB
     *  Active(anon):      28988 kB
     *  Inactive(anon):     8400 kB
     *  Active(file):     114488 kB
     *  Inactive(file):   213620 kB
     *  Unevictable:           0 kB
     *  Mlocked:               0 kB
     *  SwapTotal:       8386556 kB
     *  SwapFree:        8386556 kB
     *  Dirty:                 0 kB
     *  Writeback:             0 kB
     *  AnonPages:         28796 kB
     *  Mapped:            21404 kB
     *  Shmem:              8564 kB
     *  Slab:              69380 kB
     *  SReclaimable:      57212 kB
     *  SUnreclaim:        12168 kB
     *  KernelStack:        1424 kB
     *  PageTables:         2760 kB
     *  NFS_Unstable:          0 kB
     *  Bounce:                0 kB
     *  WritebackTmp:          0 kB
     *  CommitLimit:    12476416 kB
     *  Committed_AS:      94084 kB
     *  VmallocTotal:   34359738367 kB
     *  VmallocUsed:           0 kB
     *  VmallocChunk:          0 kB
     *  HardwareCorrupted:     0 kB
     *  AnonHugePages:         0 kB
     *  ShmemHugePages:        0 kB
     *  ShmemPmdMapped:        0 kB
     *  HugePages_Total:       0
     *  HugePages_Free:        0
     *  HugePages_Rsvd:        0
     *  HugePages_Surp:        0
     *  Hugepagesize:       2048 kB
     *  DirectMap4k:       53184 kB
     *  DirectMap2M:     8335360 kB
     */
  })
}
```

### <a name="proc-meminfo">/proc/meminfo</a>

Managed by the linux kernel via
<a name="linux-meminfo">[linux/fs/proc/meminfo.c](https://github.com/torvalds/linux/blob/v4.9/fs/proc/meminfo.c#L45)</a>
``` c
int meminfo_proc_show( struct seq_file* m, void* v) {
  struct sysinfo i;                 // include/uapi/linux/sysinfo.h
  long available;
  long cached;

  si_meminfo(&i);                   // include/linux/mm.h
  si_swapinfo(&i);                  // include/linux/swap.h   **NOT exported**

  available = si_mem_available();   // include/linux/mm.h

  /* global_node_page_state            include/linux/vmstat.h
   * NR_FILE_PAGES                     include/linux/mmzone.h
   * total_swapcache_pages             include/linux/swap.h   **NOT exported**
   */
  cached = global_node_page_state(NR_FILE_PAGES) -
              total_swapcache_pages() - i.bufferram;
  if (cached < 0) { cached = 0; }

  ...
  hugetlb_report_meminfo(m);

  /*
   *  MemTotal:     i.totalram
   *  MemFree:      i.freeram
   *  MemAvailable: available
   *  Buffers:      i.bufferram
   *  SwapTotal:    i.totalswp
   *  SwapFree:     i.freeswp
   *  Shmem:        i.sharedram
   *  ...
   */

  return 0;
}
```

`hugetlb_report_meminfo()` is provided by
<a name="linux-hugetlb">[linux/mm/hugetlb.c](https://github.com/torvalds/linux/blob/v4.9/mm/hugetlb.c#L4592)</a>
``` c
void hugetlb_report_meminfo(struct seq_file *m)
{
  struct hstate *h = &default_hstate;   // hstates is **NOT exported**
                                        // making default_hstate unavailable
  if (!hugepages_supported())
    return;
  seq_printf(m,
      "HugePages_Total:   %5lu\n"
      "HugePages_Free:    %5lu\n"
      "HugePages_Rsvd:    %5lu\n"
      "HugePages_Surp:    %5lu\n"
      "Hugepagesize:   %8lu kB\n",
      h->nr_huge_pages,
      h->free_huge_pages,
      h->resv_huge_pages,
      h->surplus_huge_pages,
      1UL << (huge_page_order(h) + PAGE_SHIFT - 10));
}
```

---

## <a name="mb-psum">MetricBeat.system.process_summary module</a>

### [metricbeat/module/system/process_summary/process_summary.go](https://github.com/elastic/beats/blob/6.5/metricbeat/module/system/process_summary/process_summary.go)

``` go
import mem "github.com/elastic/beats/libbeat/metric/system/process"

type MetricSet struct {
  mb.BaseMetricSet
}

func (m *MetricSet) Fetch(r mb.ReporterV2) {
  pids, err := process.Pids()       // ************** libbeat vvvvv
  /* => Sigar       /proc/%pid% ...
   *    total       (count of pids)
   *    sleeping    (count of processes with state & (TASK_INTERRUPTIBLE |
   *                                                  TASK_PARKED)
   *    idle        (count of processes with state & TASK_UNINTERRUPTIBLE)
   *    stopped     (count of processes with state & (__TASK_STOPPED |
   *                                                  __TASK_TRACED)
   *    zombie      (count of processes with state & EXIT_ZOMBIE)
   *    dead        (count of processes with state & EXIT_DEAD)
   *    unknown     (count of processes with any state that doesn't match)
   */
  ...

  var summary struct {
    sleeping  int
    running   int
    idle      int
    stopped   int
    zombie    int
    unknown   int
    dead      int
  }

  for _, pid := range pids {
    state := sigar.ProcState{}
    err = state.Get( pid )
    if err != nil {
      summary.unknown += 1
      continue
    }

    switch byte(state.State) {
      case 'S': summary.sleeping++
      case 'R': summary.running++
      case 'D': summary.idle++
      case 'I': summary.idle++
      case 'T': summary.stopped++
      case 'Z': summary.zombie++
      case 'X': summary.dead++
      default:  summary.unknown++;
    }
  }

  /* => total   : len(pids)
   *    sleeping: summary.sleeping
   *    ...
   */
}
```

### <a name="lb-proc">[libbeat/metric/system/process/process.go](https://github.com/elastic/beats/blob/6.5/libbeat/metric/system/process/process.go)</a>

``` go
import sigar "github.com/elastic/gosiger"

func Pids() ([]int, error) {
  pids:= sigar.ProcList{}         // ************** sigar vvvvv
  err := pids.Get()
  ...

  return pids.List, nil
}
```

### [vendor/github.com/elastic/gosigar](https://github.com/elastic/beats/tree/6.5/vendor/github.com/elastic/gosigar)

#### [sigar_linux.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/elastic/gosigar/sigar_linux.go#L15)
``` go
Procd = "/proc"
```

#### <a name="sigar-proc">[sigar_linux_common.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/elastic/gosigar/sigar_linux_common.go#L125)</a>
``` go
func (self *ProcList) Get() error {
  dir, err := os.Open( Procd )
  ...
  defer dir.Close()

  const readAllDirnames = -1

  names, err := dir.readdirnames( readAllDirnames )
  ...

  capacity := len( names )
  list     := make( []int, 0, capacity )

  for _, name := range names {
    if name[0] < '0' || name[0] > '9' {
      continue
    }
    pid, err := strconv.Atoi(name)
    if err == nil {
      list = append( list, pid )
    }
  }
  self.List = list

  return nil
}

...

func (self *ProcState) Get(pic int) error {
  data, err := readProcFile( pid, "stat" )
  /* pid  name      fields ...
   * 554 (systemd)  S                     // fields[0] : state
   *                1                     // fields[1] : ppid
   *                554                   // fields[2] : pgrp
   *                554                   // fields[3]
   *                0                     // fields[4] : tty_nr
   *                -1                    // fields[5]
   *                4194560               // fields[6]
   *                602                   // fields[7]
   *                28                    // fields[8]
   *                0                     // fields[9]
   *                0                     // fields[10]
   *                0                     // fields[11]
   *                1                     // fields[12]
   *                0                     // fields[13]
   *                0                     // fields[14]
   *                20                    // fields[15] : priority
   *                0                     // fields[16] : nice
   *                1                     // fields[17]
   *                0                     // fields[18]
   *                2857                  // fields[19]
   *                66404352              // fields[20]
   *                1458                  // fields[21]
   *                18446744073709551615  // fields[22]
   *                94044924125184        // fields[23]
   *                94044925094424        // fields[24]
   *                140731495859024       // fields[25]
   *                0                     // fields[26]
   *                0                     // fields[27]
   *                0                     // fields[28]
   *                671173123             // fields[29]
   *                4096                  // fields[30]
   *                0                     // fields[31]
   *                1                     // fields[32]
   *                0                     // fields[33]
   *                0                     // fields[34]
   *                17                    // fields[35]
   *                1                     // fields[36] : processor
   *                0                     // fields[37]
   *                0                     // fields[38]
   *                0                     // fields[39]
   *                0                     // fields[40]
   *                0                     // fields[41]
   *                94044925099616        // fields[42]
   *                94044925247788        // fields[43]
   *                94044950249472        // fields[44]
   *                140731495861932       // fields[45]
   *                140731495861960       // fields[46]
   *                140731495861960       // fields[47]
   *                140731495862243       // fields[48]
   *                0                     // fields[49]
   */
  ...

  lIdx := bytes.Index( data, []byte("(") )
  rIdx := bytes.LastIndex( data, []byte(")") )
  ...
  self.Name = string( data[lIdex+1 : rIdx] )
  fields := bytes.Fields( data[rIdx+2:] ) // > 36

  /* fields[0]  => state
   * fields[1]  => ppid
   * fields[2]  => pgrp
   * fields[4]  => tty_nr
   * fields[15] => priority
   * fields[16] => nice
   * fields[36] => processor (last processor executed on)
   */
  status, err := getProcStatus( pid )
  ...
  uids, err := getUIDs( status )
  ...
  user, err := user.LookupId( uids[0] )
  ...

  return nil
}
```

### <a name="proc-stat">/proc/%pid%/stat</a>

Managed by the linux kernel via
<a name="linux-stat">[linux/fs/proc/array.c](https://github.com/torvalds/linux/blob/v4.9/fs/proc/array.c#L414)</a>
``` c
static int do_task_stat( struct seq_file* m,   struct pid_namespace* ns,
                         struct pid*      pid, struct task_struct*   task,
                         int              while ) {
  ...
  char tcomm[sizeof(task->comm)];
  pid_t ppid      = task_tgid_nr_ns( task->real_parent, ns );
  pid_t pgid      = task_pgrp_nr_ns( task, ns );
  int   tty_nr    = 0;
  int   priority  = task_prio( task );
  int   nice      = task_nice( task );;
  char state      = *get_task_state( task );  // static: fs/proc/array.c
  struct signal_struct* sig = task->signal;

  get_task_comm( tcomm, task );               // linux/sched.h

  if (sig->tty) {
    ...
    tty_nr = new_encode_dev( tty_devnum( sig->tty ) );
  }

  /*
   *  pid_nr_ns( pid, ns ) * => pid
   *  tcomm                * => name
   *  field[0]             * => state
   *  field[1]             * => ppid
   *  field[2]             * => pgid
   *  field[3]               => sid
   *  field[4]             * => tty_nr
   *  field[5]               => tty_pgrp
   *  field[6]               => task->flags
   *  field[7]               => min_flt
   *  field[8]               => cmin_flt
   *  field[9]               => maj_flt
   *  field[10]              => cmaj_flt
   *  field[11]              => cputime_to_clock_t(utime)
   *  field[12]              => cputime_to_clock_t(stime)
   *  field[13]              => cputime_to_clock_t(cutime)
   *  field[14]              => cputime_to_clock_t(cstime)
   *  field[15]            * => priority
   *  field[16]            * => nice
   *  field[17]              => num_threads
   *  field[18]              => 0
   *  field[19]              => start_time
   *  field[20]              => vsize
   *  field[21]              => mm ? get_mm_rss(mm) : 0
   *  field[22]              => rsslim
   *  field[23]              => mm ? (permitted ? mm->start_code : 1) : 0
   *  field[24]              => mm ? (permitted ? mm->end_code   : 1) : 0
   *  field[25]              => mm && permitted ? mm->start_stack : 0
   *  field[26]              => esp
   *  field[27]              => eip
   *  field[28]              => task->pending.signal.sig[0] & 0x7fffffffUL
   *  field[29]              => task->blocked.sig[0]        & 0x7fffffffUL
   *  field[30]              => sigign.sig[0]               & 0x7fffffffUL
   *  field[31]              => sigcatch.sig[0]             & 0x7fffffffUL
   *  field[32]              => wchan ? 1 : 0
   *  field[33]              => 0
   *  field[34]              => 0
   *  field[35]              => task->exit_signal
   *  field[36]            * => task_cpu( task )
   *  field[37]              => task->rt_priority
   *  field[38]              => task->policy
   *  field[39]              => delayacct_blkio_ticks( task )
   *  field[40]              => cputime_to_clock_t( gtime )
   *  field[41]              => cputime_to_clock_t( cgtime )
   *  field[42]              => mm && permitted ? mm->start_data  : 0
   *  field[43]              => mm && permitted ? mm->end_data    : 0
   *  field[44]              => mm && permitted ? mm->start_brk   : 0
   *  field[45]              => mm && permitted ? mm->arg_start   : 0
   *  field[46]              => mm && permitted ? mm->arg_end     : 0
   *  field[47]              => mm && permitted ? mm->env_start   : 0
   *  field[48]              => mm && permitted ? mm->env_end     : 0
   *  field[49]              => permitted       ? task->exit_code : 0
   *  ...
   */

  return 0;
}
```

---

## <a name="mb-net">MetricBeat.system.network module</a>

### [metricbeat/module/system/network/network.go](https://github.com/elastic/beats/blob/6.5/metricbeat/module/system/network/network.go)

``` go
import "github.com/shirou/gopsutil/net"

// MetricSet for fetching system network IO metrics.
type MetricSet struct {
  mb.BaseMetricSet
  interfaces map[string]struct{}
}

func (m *MetricSet) Fetch(r mb.ReporterV2) {
  stats, err := net.IOCounters(true)  // ************ gopsutil vvvvv
  ...
  for _, counters := range stats {
    ... ioCountersToMapStr(counters)
  }
```

### [vendor/github.com/shirou/gopsutil](https://github.com/elastic/beats/tree/6.5/vendor/github.com/shirou/gopsutil)

#### [net/net_linux.go](https://github.com/elastic/beats/blob/6.5/vendor/github.com/chirou/gopsutil/net/net_linux.go#L26)

``` go
func IOCounters(pernic bool) ([]IOCountersStat, error) {
  return IOCountersWithContext(context.Background(), pernic)
}

func IOCountersWithContext(ctx context.Context, pernic bool) ([]IOCountersStat, error) {
  filename := common.HostProc("net/dev")    // ==> /proc/net/dev
  return IOCountersByFile(pernic, filename)
}
```

### <a name="proc-net">/proc/net/dev</a>

Managed by the linux kernel via
<a name="linux-net">[linux/net/core/net-procfs.c](https://github.com/torvalds/linux/blob/v4.9/net/core/net-procfs.c#L77)</a>
``` c
static void dev_seq_printf_stats(struct seq_file *seq, struct net_device *dev)
{
  struct rtnl_link_stats64 temp;
  const struct rtnl_link_stats64 *stats = dev_get_stats(dev, &temp);

  seq_printf(seq, "%6s: %7llu %7llu %4llu %4llu %4llu %5llu %10llu %9llu "
       "%8llu %7llu %4llu %4llu %4llu %5llu %7llu %10llu\n",
       dev->name, stats->rx_bytes, stats->rx_packets,
       stats->rx_errors,
       stats->rx_dropped + stats->rx_missed_errors,
       stats->rx_fifo_errors,
       stats->rx_length_errors + stats->rx_over_errors +
        stats->rx_crc_errors + stats->rx_frame_errors,
       stats->rx_compressed, stats->multicast,
       stats->tx_bytes, stats->tx_packets,
       stats->tx_errors, stats->tx_dropped,
       stats->tx_fifo_errors, stats->collisions,
       stats->tx_carrier_errors +
        stats->tx_aborted_errors +
        stats->tx_window_errors +
        stats->tx_heartbeat_errors,
       stats->tx_compressed);
}

/*
 *  Called from the PROCfs module. This now uses the new arbitrary sized
 *  /proc/net interface to create /proc/net/dev
 */
static int dev_seq_show(struct seq_file *seq, void *v)
{
  if (v == SEQ_START_TOKEN)
    seq_puts(seq, "Inter-|   Receive                            "
            "                    |  Transmit\n"
            " face |bytes    packets errs drop fifo frame "
            "compressed multicast|bytes    packets errs "
            "drop fifo colls carrier compressed\n");
  else
    dev_seq_printf_stats(seq, v);
  return 0;
}

```

## <a name="mb-cgroup">MetricBeat.system.process.cgroup module</a>

MetricBeat only reports cgroup information for a process when the process is
**not** in a root cgroup.

From the [MetricBeat 6.5 documentation](https://www.elastic.co/guide/en/beats/metricbeat/6.5/exported-fields-system.html#_cgroup_fields)
> cgroup fields
> Metrics and limits from the cgroup of which the task is a member. cgroup
> metrics are reported when the process has membership **in a non-root cgroup**.
> These metrics are only available on Linux.


See also: [cgroup-v1 subsystems / metrics](cgroup.md#v1-metrics)
