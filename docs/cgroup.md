###### TOC
- [Overview](#overview)
- [Version 1](#v1)
  - [Changes between Linux Kernel versions](#v1-kernel-changes)
  - [ Available subsystems / metrics](#v1-metrics)
- [Version 2](#v2)
- [References](#refs)
---

### <a name="overview">Overview</a>
The `cgroup` (control group) mechanism in Linux is used "to organize processes
hierarchically and distribute system resources along the hierarchy in a
controlled and configurable manner."

- it is used to limit, throttle, and account for resource usage
  (e.g. memory, cpu, network, io) throughout the system;

- it is managed and accessed via the Virtual Filesystem (VFS) using the
  `cgroup` filesystem type (also referred to as `cgroupfs`);

- on most Linux systems cgroup is mounted at `/sys/fs/cgroup`;

- each resource is handled by a single subsystem and the set of subsystems is
  fixed at kernel-compile-time;

- there is one or more cgroup roots, each identified via `hierarchy_id` and each
  defining a single cgroup hierarchy &mdash; for `cgroupv1` there is *at least*
  one hiearchy per resource, for `cgroupv2` there is a single hierarchy and
  resources apply directly to a cgroup;

- **core**
  - `kernel/cgroup.c` used for both v1 and v2;
  - primarily responsible for hierarchically organizing processes;


- **subsystems** (aka controllers)
  - represented by `cgroup_subsys` objects;
  - typically responsible for distributing a specific type of system resource
    along the hierarchy, although there are utility subsystems which serve
    purposes other than resource distribution:
  - `cpuset`, `cpu,cpuacct`, `blkio`, `memory`, `devices`, `freezer`,
    `net_cls,net_prio`, `perf_event`, `pids`;


- `procfs` entries:
  - for each process: `/proc/%pid%/cgroup`;
  - system-wide     : `/proc/cgroups`;


- currently has two versions;
  - [version 1](#v1) is the default used for most Linux kernels;
  - [version 2](#v2) is avaialble as `cgroup2` for Linux kernels >= v4.5;
  - Documentation:
    - v1 : [Documentation/cgroups/](https://github.com/torvalds/linux/blob/v3.10/Documentation/cgroups/);
    - v2 : [Documentation/cgroup-v2.txt](https://github.com/torvalds/linux/blob/v4.9/Documentation/cgroup-v2.txt);


### <a name="v1">Version 1</a>

`cgroupv1` has a separate hierarchy per resource and each resource contains
cgroup entries for management and accounting.

- a process may be in **exactly** one cgroup per resource, defaulting to the
  root cgroup for the resource if not explicitly assigned;
- a resource is managed by a subsystem `struct cgroup_subsys`;
- each subsystem has a single cgroup root that contains children which may be
  subdirectories extending the hierarchy;
  - &lt; v3.15 : `struct cgroupfs_root`;
  - &gt;= 3.15 : `struct cgroup_root` (built upon `kernfs`);
- resource limits and metrics are stored in the cgroup, indexed by subsystem id
  in the `struct cgroup_subsys_state` array;
- each subsystem has an entry for each managed resource (`struct cftype`) that
  defines how the data from the subsystems `struct cgroup_subsys_state` entry
  is handled;
  - this is an array of resource entries terminated by either `NULL` or an entry
    with an empty `name`;
  - &lt; v3.15  : `subsys->cftsets` &mdash; list of `struct cftype_set` entries,
    where each entry has a `struct cftype* cft` property;
  - &gt;= v3.15 : `subsys->cfts` &mdash; list of `struct cftype` entries;
  - each `struct cftype` entry has a `name` along with a set of read/write
    accessors that manage the state data for the named variable;
  - the full cgroup path to one of these variables, assuming cgroup is mounted
    at `/sys/fs/cgroup`, would be:
    > /sys/fs/cgroup<br />
    > &nbsp;&nbsp;
    >  / %cgroup-root%<br />
    > &nbsp;&nbsp;&nbsp;
    >  / %subsys-name%<br />
    > &nbsp;&nbsp;&nbsp;&nbsp;
    >  [/ %cgroup-path% ] (***if explicitly associated with a non-root group***)<br />
    > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    >  / %subsys-name% . %cftype-name%


Example hierarchy:
```
                    subsys    cgroup  task   resource
                    ======    ======  ====   ==================================
                +-> blkio  ---> bg  ----+---> throttle.write_bps_device=1MiB/s
                |                       A
                |           +-> bg  ----+---> memory.limit_in_bytes=1G
/sys/fs/cgroup -+-> memory -+
                |           +-> ad  ----+---> memory.limit_in_bytes=2G
                |                       B
                +-> pids   ---> ad  ----+---> pids.max=1000
                                        ^
                                        |
                    a task does not appear in the cgroup path but
                    does have an association with a set of cgroups,
                    one per resource;
```
``` bash
% ls -F /sys/fs/cgroup
blkio/                    cpuset/   net_cls@  => net_cls,net_prio   pids/
cpu@     => cpu,cpuacct   devices/  net_cls,net_prio/
cpuacct@ => cpu,cpuacct   freezer/  net_prio@ => net_cls,net_prio
cpu,cpuacct/              memory/   perf_event/

% find /sys/fs/cgroup/blkio -maxdepth 1 -type d
/sys/fs/cgroup/blkio
/sys/fs/cgroup/blkio/bg

% find /sys/fs/cgroup/memory -maxdepth 1 -type d
/sys/fs/cgroup/memory
/sys/fs/cgroup/memory/ad
/sys/fs/cgroup/memory/bg

% find /sys/fs/cgroup/pids -maxdepth 1 -type d
/sys/fs/cgroup/pids
/sys/fs/cgroup/pids/ad

% cat /sys/fs/cgroup/blkio/bg/blkio.throttle.write_bps_device
1048576

% cat /sys/fs/cgroup/memory/bg/memory.limit_in_bytes
1073741824

% cat /sys/fs/cgroup/memory/ad/memory.limit_in_bytes
2147483648

% cat /sys/fs/cgroup/pids/ad/pids.max
1000
```


#### <a name="v1-kernel-changes">Changes between Linux Kernel versions</a>
There are significant changes in the `cgroup` system between versions 3.10,
3.15 and 4.5.

<table>
 <thead><tr><th>version</th><th>changes</th></tr></thead>
 <tbody>
   <tr>
     <td>&lt; 3.15</td>
     <td>
       cgroup root: <code>struct cgroupfs_root</code>;<br />
       cgroup name: <code>cgroup_path( cgroup, buf, buflen )</code>;<br />
       subsys id  : <code>subsys.subsys_id</code>;<br />
       subsys name: <code>subsys.name</code>;
     </td>
   </tr>
   <tr>
     <td>&gt;= 3.15</td>
     <td>
       Switched to using <code>kernfs</code> for the cgroup hiearchy;<br />
       cgroup root: <code>struct cgroup_root</code>;<br />
       cgroup name: <code>cgroup_path( cgroup, buf, buflen )</code>;<br />
       subsys id  : <code>subsys.subsys_id</code>;<br />
       subsys name: <code>subsys.legacy_name</code>;
     </td>
   </tr>
   <tr>
     <td>&gt;= 4.5</td>
     <td>
       Added <code>struct cgroup_namespace* cgroup_ns</code> to <code>struct
       nsproxy</code>, which is used by <code>struct task_struct</code> as
       <code>nsproxy</code>;<br />
       cgroup names are now computed based upon this namespace;<br />
       cgroup name: <code>cgroup_path_ns( cgroup, buf, buflen, ns )</code>;
     </td>
   </tr>
 </tbody>
</table>


#### <a name="v1-metrics">Available subsystems / metrics</a>

cgroup metrics are scattered throughout the kernel, many of the access methods
require the use of a `struct seq_file` often writing a sequence of key/value
pairs, and several in pre-4.5 kernels directly write to a provided user-space
buffer
(e.g.  `cpuset.cpus`, `hugetlb.usage_in_bytes`, `memory.limit_in_bytes`).


Available cgroup metrics:
- blkio / io :
  [linux/block/cfq-iosched.c.c](https://github.com/torvalds/linux/blob/v4.9/block/cfq-iosched.c.c),
  [linux/block/blk-throttle.c](https://github.com/torvalds/linux/blob/v4.9/block/blk-throttle.c)
- cpu :
  [linux/kernel/sched/core.c.c](https://github.com/torvalds/linux/blob/v4.9/kernel/sched/core.c.c)
- cpuacct :
  [linux/kernel/sched/cpuacct.c.c](https://github.com/torvalds/linux/blob/v4.9/kernel/sched/cpuacct.c.c)
- cpuset :
  [linux/kernel/cpuset.c.c](https://github.com/torvalds/linux/blob/v4.9/kernel/cpuset.c.c)
- devices :
  [linux/security/device_cgroup.c.c](https://github.com/torvalds/linux/blob/v4.9/security/device_cgroup.c.c)
- freezer :
  [linux/kernel/cgroup_freezer.c.c](https://github.com/torvalds/linux/blob/v4.9/kernel/cgroup_freezer.c.c)
- hugetlb :
  [linux/mm/hugetlb_cgroup.c.c](https://github.com/torvalds/linux/blob/v4.9/mm/hugetlb_cgroup.c.c)
- memory :
  [linux/mm/memcontrol.c.c](https://github.com/torvalds/linux/blob/v4.9/mm/memcontrol.c.c)
- net_cls :
  [linux/net/core/netclassid_cgroup.c.c](https://github.com/torvalds/linux/blob/v4.9/net/core/netclassid_cgroup.c.c)
- net_prio :
  [linux/net/core/netprio_cgroup.c.c](https://github.com/torvalds/linux/blob/v4.9/net/core/netprio_cgroup.c.c)
- pids :
  [linux/kernel/cgroup_pids.c](https://github.com/torvalds/linux/blob/v4.9/kernel/cgroup_pids.c)


### <a name="v2">Version 2</a>

`cgroupv2` has a single, unified hierarchy and resources apply directly at the
cgroup level. One of the primary redesign goals was simplicity/clarity over
ultimate flexibility.

- a process may be in **exactly one** cgroup;
- the full cgroup path to a variable, assuming cgroup is mounted at
  `/sys/fs/cgroup`, would be:
  > /sys/fs/cgroup<br />
  > &nbsp;&nbsp;
  >   / %cgroup-path%<br />
  > &nbsp;&nbsp;&nbsp;&nbsp;
  >    / %subsys-name% . %cftype-name%


Example hierarchy (from above for version 2):
```
                  cgroup   task    subsystem.resource
                  ======   ====    ============================
                                 +--> io.max="wbps=1MiB/s"
                        +----+---+
                        |    A   +--> memory.high/max=1G
                +-> bg -+
                |       |                           +--> +io
                |       +> cgroup.subtree_control --+
                |                                   +--> +memory
/sys/fs/cgroup -+
                |                +--> memory.high/max=2G
                |       +----+---+
                |       |    B   +--> pids.max=1000
                +-> ad -+
                        |                           +--> +memory
                        +> cgroup.subtree_control --+
                                                    +--> +pids
                             ^
                             |
                    a task does not appear in the cgroup path but
                    does have an association with a single cgroup;
```


### <a name="refs">References</a>
- [cgroups
   v1](https://github.com/torvalds/linux/blob/v3.10/Documentation/cgroups/cgroups.txt),
  Paul Menage, 2004;
- [cgroups
   v2](https://github.com/torvalds/linux/blob/v4.9/Documentation/cgroup-v2.txt),
  Tejun Heo, October 2015;
- [Namespaces and cgroups, the Building Blocks of Linux
   containers](https://events.static.linuxfound.org/sites/events/files/slides/cgroup_and_namespaces.pdf),
  Rami Rosen, Intel, 2014;
- [Resource management: Linux kernel Namespaces and
   cgroups](www.haifuxorg/lectures/299/netLec7.pdf), Rami Rosen, Haifux, May
   2013;
- [cgroupv2: Linux's new unified control group
  system](https://chrisdown.name/talks/cgroupv2/cgroupv2-fosdem.pdf), Chris
  Down, FOSDEM, 2017;
