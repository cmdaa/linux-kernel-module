Timing Anomolies

beat  :      1 @ timestamp: 1555339287 : 2019-04-15 10:41:27
stress:   2240 @ timestamp: 1555341541 : 2019-04-15 11:19:01
stop  :   4875 @ timestamp: 1555344195 : 2019-04-15 12:03:16
beat  :   6598 @ timestamp: 1555345899 : 2019-04-15 12:31:39
beat  :   6603 @ timestamp: 1555345904 : 2019-04-15 12:31:44

---------------- stress on --------------------------   1555341541
Disk stress                                             1555341571 - 1555341601
=== Beat 2305: full time[ 4.535 ] < sum[ 5.5061 ]       1555341592 11:19:52

CPU stress                                              1555342386 - 1555342416
=== Beat 3101: full time[ 3.416 ] < sum[ 4.3881 ]       1555342388 11:33:08

Socket IO                                               1555342476 - 1555342506
=== Beat 3195: full time[ 4.8432 ] < sum[ 5.8131 ]      1555342482 11:34:42

Socket IO                                               1555343200 - 1555343230
=== Beat 3937: full time[ 4.813 ] < sum[ 5.7845 ]       1555343224 11:47:04

---------------- stress off -------------------------   1555344195

=== proc_mon multiples

*** Beat 5740: fs used 18446744073708.566 ms ...        1555345027

=== Beat 5754: proc_mon multiples (19)
=== Beat 5796: proc_mon multiples (18)
=== Beat 6247: proc_mon multiples (18)

