library(ggplot2)
library(grid)
library(stringr)
library(RColorBrewer)

# Disable scientific notation
options(scipen=999)

##
# Generate a chart title using min/max beat counts
# @method makeTitle
# @param  ds    The target data set (with a 'beat' column)
#
# @return The title {String};
makeTitle <- function( ds ) {
  vMin = format( min(ds['beat']), big.mark=',' )
  vMax = format( max(ds['beat']), big.mark=',' )
  return (str_glue( 'Beats: ', vMin, ' - ', vMax ))
}

##################################################################
setwd( '/Users/depeele/src/cmdaa/linux-kernel-module/docs/resources' )

data      <- read.csv( file   = 'time+memory-90-gather.csv',
                       header = TRUE,
                       sep    = ',' )

#########################################################################
# What percentage of time and space do proc and cgroup consume
most <- subset(data, module == 'proc' | module == 'cgroup')

sumMostMs <- function(vec) {
  return ( sum( most$pctMs[ most$beat == as.numeric(vec[1]) ] ) )
}
sumMostB <- function(vec) {
  return ( sum( most$pctB[ most$beat == as.numeric(vec[1]) ] ) )
}

mostPctMs <- apply( most, 1, sumMostMs )
mostPctB  <- apply( most, 1, sumMostB )

summary(mostPctMs)
summary(mostPctB)
#########################################################################

maxMs     <- max( data$beatMs )
maxBytes  <- max( data$beatB )
maxBeat   <- max( data$beat )
maxProcs  <- max( data$procs )

meanMs    <- mean( data$beatMs )
meanBytes <- mean( data$beatB )

# Create a specific ordering of modules:
#   1 : cpu
#   2 : fs
#   3 : fsstat
#   4 : load
#   5 : mem
#   6 : net
#   7 : procSum
#   8 : uptime
#   9 : lkb
#   10: cgroup
#   11: proc
fillLevels = c(
  'proc',
  'cgroup',
  'uptime',
  'procSum',
  'net',
  'mem',
  'load',
  'fsstat',
  'fs',
  'cpu',
  'lkb'
)

#fillColors  <- brewer.pal( 11, 'RdYlBu' )
#fillColors  <- brewer.pal( 11, 'RdBu' )
fillColors  <- brewer.pal( 11, 'Spectral' )

fill.order <- factor( data$module, levels = fillLevels )

#########################
# Stacked line charts
#
ptime1  <- ggplot( data, aes( x = beat, y = timeMs, fill = fill.order ) ) +
  theme_bw() +
  geom_area( alpha=0.8, size=.04, color='black' ) +
  scale_fill_manual( values = fillColors ) +
  ggtitle( makeTitle(data) ) +
  labs( fill = 'module', x = 'beat', y = 'time (ms)' ) +
  coord_cartesian( ylim = c(0, .5) )

pbytes1 <- ggplot( data, aes( x = beat, y = jsonB, fill = fill.order ) ) +
  theme_bw() +
  geom_area( alpha=0.8, size=.04, color='black' ) +
  scale_fill_manual( values = fillColors ) +
  labs( fill = 'module', x = 'beat', y = 'JSON (b)' ) +
  coord_cartesian( ylim = c(0, meanBytes * 1.1) )

grid.newpage()
grid.draw( rbind(ggplotGrob(ptime1),
                 ggplotGrob(pbytes1),
                 size='last') )

dev.copy( png, 'plot-module.png', width=4500, height=4500, res=600 )
dev.off()

#########################
# Stacked bar charts
#
ptime2  <- ggplot( data, aes( x = beat, y = pctMs, fill = fill.order ) ) +
  theme_bw() +
  geom_bar( alpha=0.8, stat='identity', width=1 ) +
  scale_fill_manual( values = fillColors ) +
  ggtitle( makeTitle(data) ) +
  labs( fill = 'module', x = '', y = 'time (% ms)' ) +
  coord_cartesian( ylim = c(0, 15) )

pbytes2 <- ggplot( data, aes( x = beat, y = pctB, fill = fill.order ) ) +
  theme_bw() +
  geom_bar( alpha=0.8, stat='identity', width=1 ) +
  scale_fill_manual( values = fillColors ) +
  labs( fill = 'module', x = 'beat', y = 'JSON (% b)' ) +
  coord_cartesian( ylim = c(0, 88) )

grid.newpage()
grid.draw( rbind(ggplotGrob(ptime2),
                 ggplotGrob(pbytes2),
                 size='last') )

dev.copy( png, 'time+memory-90-module-full.png', width=4500, height=4500, res=600 )
dev.off()

###
sample      <- subset( data, (beat - 1) %% 1000 == 0 )
sample$beat <- floor( sample$beat / 1000 )

sample.fill <- factor( sample$module, levels = fillLevels )

ptime2  <- ggplot( sample, aes( x = beat, y = pctMs, fill = sample.fill ) ) +
  theme_bw() +
  geom_bar( alpha=0.8, stat='identity', width=1 ) +
  scale_fill_manual( values = fillColors ) +
  labs( fill = 'module', x = '', y = 'time (% ms)' ) +
  coord_cartesian( ylim = c(0, 15) )

pbytes2 <- ggplot( sample, aes( x = beat, y = pctB, fill = sample.fill ) ) +
  theme_bw() +
  geom_bar( alpha=0.8, stat='identity', width=1 ) +
  scale_fill_manual( values = fillColors ) +
  labs( fill = 'module', x = 'beat (x 1,000)', y = 'JSON (% b)' ) +
  coord_cartesian( ylim = c(0, 88) )

grid.newpage()
grid.draw( rbind(ggplotGrob(ptime2),
                 ggplotGrob(pbytes2),
                 size='last') )

dev.copy( png, 'time+memory-90-module-sample-1000.png', width=4500, height=4500, res=600 )
dev.off()

###
sample      <- subset( data, (beat - 1) %% 10000 == 0 )
sample$beat <- floor( sample$beat / 10000 )

sample.fill <- factor( sample$module, levels = fillLevels )

ptime2  <- ggplot( sample, aes( x = beat, y = pctMs, fill = sample.fill ) ) +
  theme_bw() +
  geom_bar( alpha=0.8, stat='identity', width=1 ) +
  scale_fill_manual( values = fillColors ) +
  labs( fill = 'module', x = '', y = 'time (% ms)' ) +
  coord_cartesian( ylim = c(0, 15) )

pbytes2 <- ggplot( sample, aes( x = beat, y = pctB, fill = sample.fill ) ) +
  theme_bw() +
  geom_bar( alpha=0.8, stat='identity', width=1 ) +
  scale_fill_manual( values = fillColors ) +
  labs( fill = 'module', x = 'beat (x 10,000)', y = 'JSON (% b)' ) +
  coord_cartesian( ylim = c(0, 88) )

grid.newpage()
grid.draw( rbind(ggplotGrob(ptime2),
                 ggplotGrob(pbytes2),
                 size='last') )

dev.copy( png, 'time+memory-90-module-sample-10000.png', width=4500, height=4500, res=600 )
dev.off()
