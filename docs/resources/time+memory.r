library(ggplot2)
library(grid)
library(stringr)

# Disable scientific notation
options(scipen=999)

srcBase <- 'time+memory-90'
srcDir  <- dirname( sys.frame(1)$ofile )


# Conversion from max y coordinate to line height for text
lineHeight_factor <- 40

# Conversion from max x coordinate to x offset for text
labelx_factor     <- 1.06

##
# Generate a chart title using min/max beat counts
# @method makeTitle
# @param  ds    The target data set (with a 'beat' column)
#
# @return The title {String};
makeTitle <- function( ds ) {
  vMin=format( min(ds['beat']), big.mark=',' )
  vMax=format( max(ds['beat']), big.mark=',' )
  return (str_glue( 'Beats: ', vMin, ' - ', vMax ))
}

##
# Format the given value as a fixed-digit floating point number
# @method fmtFloat
# @param  val         The target value {Number};
# @param  [len=12 ] The number of digits {Number};
#
# @return The formatted value {String};
fmtFloat <- function(val, len=12 ) {
  vstr=format( val, digits=4, nsmall=4, big.mark=',' )

  #return ( format( val, justify='right', digits=9, nsmall=4 ) )
  return ( str_pad( vstr, len, pad=' ' ) )
}

##
# Format the given value as a fixed-digit integer number
# @method fmtInt
# @param  val         The target value {Number};
# @param  [len=7 ]  The number of digits {Number};
#
# @return The formatted value {String};
fmtInt <- function(val, len=7 ) {
  vstr=format( val, digits=4, nsmall=0, big.mark=',' )

  return ( str_pad( vstr, len, pad=' ' ) )
}

##
# Concatenate strings without a space
# @method concat
# @param  ...       The strings to concatenate {String};
#
# @return The concatenated strings {String};
concat <- function( ... ) {
  return ( paste( ..., sep='' ) )
}

##################################################################
setwd( srcDir )

# :NOTE: The CSV file SHOULD have been normalized via
#           kernel-beat/etc/update-beat-csv
#
data      <- read.csv( file  = concat(srcBase, '-gather.csv'),
                       header= TRUE,
                       sep   = ',' )

minMs     <- min( data$beatMs )
minBytes  <- min( data$beatB )
minBeat   <- min( data$beat )
minProcs  <- min( data$procs )
minProcs10<- floor( minProcs / 10 ) * 10

maxMs     <- max( data$beatMs )
maxBytes  <- max( data$beatB )
maxBeat   <- max( data$beat )
maxProcs  <- max( data$procs )

stime     <- summary( data$beatMs )
sbytes    <- summary( data$beatB )
sprocs    <- summary( data$procs )

sumTime   <- c( paste( 'Min    :', fmtFloat( stime['Min.'] ) ),
                paste( '1st Qu :', fmtFloat( stime['1st Qu.'] ) ),
                paste( 'Median :', fmtFloat( stime['Median'] ) ),
                paste( 'Mean   :', fmtFloat( stime['Mean'] ) ),
                paste( '3rd Qu :', fmtFloat( stime['3rd Qu.'] ) ),
                paste( 'Max    :', fmtFloat( stime['Max.'] ) ) )

sumBytes  <- c( paste( 'Min    :', fmtInt( sbytes['Min.'] ) ),
                paste( '1st Qu :', fmtInt( sbytes['1st Qu.'] ) ),
                paste( 'Median :', fmtInt( sbytes['Median'] ) ),
                paste( 'Mean   :', fmtInt( ceiling(sbytes['Mean'] )) ),
                paste( '3rd Qu :', fmtInt( sbytes['3rd Qu.'] ) ),
                paste( 'Max    :', fmtInt( sbytes['Max.'] ) ) )

sumProcs  <- c( paste( 'Min    :', fmtInt( sprocs['Min.'] ) ),
                paste( '1st Qu :', fmtInt( sprocs['1st Qu.'] ) ),
                paste( 'Median :', fmtInt( sprocs['Median'] ) ),
                paste( 'Mean   :', fmtInt( ceiling(sprocs['Mean'] )) ),
                paste( '3rd Qu :', fmtInt( sprocs['3rd Qu.'] ) ),
                paste( 'Max    :', fmtInt( sprocs['Max.'] ) ) )

##################################################################
# Plot the full data set (time, bytes, processes)
# with annotations
#
# 3 plots, divide lineHeight_factor by 3
pLineHeight_factor <- (lineHeight_factor / 3)
pAnnotate_x        <- (maxBeat * labelx_factor)

full_xlim   <- c( min( data$beat ), max( data$beat ) )

pFull_time  <- ggplot( data, aes( x=beat, y=beatMs ) ) +
  theme_bw() +
  geom_point( color='red', size=.5, alpha=1/4 ) +
  geom_line(  color='red',          alpha=1/8 ) +
  ggtitle( label=makeTitle(data), subtitle='full ~16h' ) +
  annotate(
    geom    = 'text',
    x       = pAnnotate_x,
    y       = c( maxMs,
                 maxMs - (maxMs / pLineHeight_factor),
                 maxMs - (maxMs / pLineHeight_factor * 2),
                 maxMs - (maxMs / pLineHeight_factor * 3),
                 maxMs - (maxMs / pLineHeight_factor * 4),
                 maxMs - (maxMs / pLineHeight_factor * 5) ),
    hjust   = 0,
    family  = 'Courier',
    color   = '#8693a0',
    label   = sumTime ) +
  labs( x='', y='milliseconds' ) +
  coord_cartesian( xlim=full_xlim, ylim=c(0, maxMs), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )

pFull_bytes <- ggplot( data, aes( x=beat, y=beatB ) ) +
  theme_bw() +
  geom_point( color='blue', size=.5, alpha=1/4 ) +
  geom_line(  color='blue',          alpha=1/8 ) +
  annotate(
    geom    = 'text',
    x       = pAnnotate_x,
    y       = c( maxBytes,
                 maxBytes - (maxBytes / pLineHeight_factor),
                 maxBytes - (maxBytes / pLineHeight_factor * 2),
                 maxBytes - (maxBytes / pLineHeight_factor * 3),
                 maxBytes - (maxBytes / pLineHeight_factor * 4),
                 maxBytes - (maxBytes / pLineHeight_factor * 5) ),
    hjust   = 0,
    family  = 'Courier',
    color   = '#8693a0',
    label   = sumBytes ) +
  labs( x='', y='bytes' ) +
  coord_cartesian( xlim=full_xlim, ylim=c(0, maxBytes), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )

pFull_procs  <- ggplot( data, aes( x=beat, y=procs ) ) +
  theme_bw() +
  geom_point( color='#458b01', size=.5, alpha=1/4 ) +
  geom_line(  color='#458b01',          alpha=1/8 ) +
  annotate(
    geom    = 'text',
    x       = pAnnotate_x,
    y       = c( maxProcs,
                 maxProcs - (maxProcs / pLineHeight_factor),
                 maxProcs - (maxProcs / pLineHeight_factor * 2),
                 maxProcs - (maxProcs / pLineHeight_factor * 3),
                 maxProcs - (maxProcs / pLineHeight_factor * 4),
                 maxProcs - (maxProcs / pLineHeight_factor * 5) ),
    hjust   = 0,
    family  = 'Courier',
    color   = '#8693a0',
    label   = sumProcs ) +
  labs( x='beat', y='processes' ) +
  coord_cartesian( xlim=full_xlim,
                   ylim=c(minProcs10, maxProcs), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )

grid.newpage()
grid.draw( rbind(ggplotGrob(pFull_time),
                 ggplotGrob(pFull_bytes),
                 ggplotGrob(pFull_procs),
                 size='last') )

dev.copy( png, concat(srcBase, '-full.png'), width=4500, height=4500, res=600 )
dev.off()


##################################################################
# Plot the first 2 hours
#
#first <- subset( data, beat <= 10000 )
first   <- subset( data, beat <= 7200 )

first_xlim <- c( min( first$beat ), max( first$beat ) )

pFirst_time  <- ggplot( first, aes( x=beat, y=beatMs ) ) +
  theme_bw() + # theme_bw, classic, dark, grey, light, linedraw, minimal
  geom_point( color='red', size=.5, alpha=1/4 ) +
  geom_line(  color='red',          alpha=1/8 ) +
  ggtitle( label=makeTitle(first), subtitle='first 2h' ) +
  labs( x='', y='milliseconds' ) +
  coord_cartesian( xlim=first_xlim, ylim=c(0, maxMs), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )


pFirst_bytes   <- ggplot( first, aes( x=beat, y=beatB ) ) +
  theme_bw() +
  geom_point( color='blue', size=.5, alpha=1/4 ) +
  geom_line(  color='blue',          alpha=1/8 ) +
  labs( x='beat', y='bytes' ) +
  coord_cartesian( xlim=first_xlim, ylim=c(0, maxBytes), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )

grid.newpage()
grid.draw( rbind(ggplotGrob(pFirst_time),
                 ggplotGrob(pFirst_bytes),
                 size='last') )

dev.copy( png, concat(srcBase, '-first.png'), width=4500, height=4500, res=600 )
dev.off()


##################################################################
# Plot the middle 4 hours
#
midBeat  <- floor( (maxBeat / 2 ) / 300 ) * 300
midStart <- midBeat - 7200
midEnd   <- midBeat + 7200
mid      <- subset( data, beat >= midStart & beat <= midEnd )

mid_xlim <- c( midStart, midEnd )

pMid_time  <- ggplot( mid, aes( x=beat, y=beatMs ) ) +
  theme_bw() +
  geom_point( color='red', size=.5, alpha=1/4 ) +
  geom_line(  color='red',          alpha=1/8 ) +
  ggtitle( label=makeTitle(mid), subtitle='middle 4h' ) +
  labs( x='', y='milliseconds' ) +
  coord_cartesian( xlim=mid_xlim, ylim=c(0, maxMs), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )

pMid_bytes <- ggplot( mid, aes( x=beat, y=beatB ) ) +
  theme_bw() +
  geom_point( color='blue', size=.5, alpha=1/4 ) +
  geom_line(  color='blue',          alpha=1/8 ) +
  labs( x='beat', y='bytes' ) +
  coord_cartesian( xlim=mid_xlim, ylim=c(0, maxBytes), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )

grid.newpage()
grid.draw( rbind(ggplotGrob(pMid_time),
                 ggplotGrob(pMid_bytes),
                 size='last') )

dev.copy( png, concat(srcBase, '-mid.png'), width=4500, height=4500, res=600 )
dev.off()


##################################################################
# Plot the last 2 hours
#
#last <- subset( data, beat >= 54880 )
#last <- subset( data, beat >= maxBeat - 1000 )
lastStart <- (ceiling( maxBeat / 300 ) * 300) - 7200
lastEnd   <- maxBeat
last      <- subset( data, beat >= lastStart )

last_xlim <- c( lastStart, lastEnd )

pLast_time  <- ggplot( last, aes( x=beat, y=beatMs ) ) +
  theme_bw() +
  geom_point( color='red', size=.5, alpha=1/4 ) +
  geom_line(  color='red',          alpha=1/8 ) +
  ggtitle( label=makeTitle(last), subtitle='last ~2h' ) +
  labs( x='', y='milliseconds' ) +
  coord_cartesian( xlim=last_xlim, ylim=c(0, maxMs), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )

pLast_bytes <- ggplot( last, aes( x=beat, y=beatB ) ) +
  theme_bw() +
  geom_point( color='blue', size=.5, alpha=1/4 ) +
  geom_line(  color='blue',          alpha=1/8 ) +
  labs( x='beat', y='bytes' ) +
  coord_cartesian( xlim=last_xlim, ylim=c(0, maxBytes), clip='off' ) +
  theme(
    plot.margin=margin( 10, 150, 10, 10, 'pt' )
  )

grid.newpage()
grid.draw( rbind(ggplotGrob(pLast_time),
                 ggplotGrob(pLast_bytes),
                 size='last') )

dev.copy( png, concat(srcBase, '-last.png'), width=4500, height=4500, res=600 )
dev.off()
