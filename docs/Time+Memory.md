Time and memory measurements.

# Run 70
Friday 2019-04-05 @ ~14:35 through Monday 2019-04-08 @ ~06:00 (228,349 beats).

Stress was added ( 2019-04-05 @ ~ 14:47 through 2019-04-08 @ ~ 05:55 ) using
various `stress-ng` options as well as random null-pointer-exception and
div-by-0 programs (via [kernel-beat/etc/stress.sh](../kernel-beat/etc/stress.sh)).

(Using workqueue defaults -- flags: `WQ_MEM_RECLAIM` (reentrant), maxActive: 1)


## First 2 hours
![First 2 hours](resources/time+memory-70-first.png)

## Middle 4 hours
![Middle 4 hours](resources/time+memory-70-mid.png)

## Last 2 hours
![Last 2 hours](resources/time+memory-70-last.png)

## Full set
![Full set](resources/time+memory-70-full.png)
![Full set with processes](resources/time+memory-70-procs.png)

---

# Run 80
Monday 2019-04-08 @ ~12:42 through Tuesday 2019-04-09 @ ~06:26 (63,312 beats).

Stress was added ( 2019-04-08 @ ~ 13:03 through 2019-04-09 @ ~ 06:04 ) using
various `stress-ng` options as well as random null-pointer-exception and
div-by-0 programs (via [kernel-beat/etc/stress.sh](../kernel-beat/etc/stress.sh)).

(Using workqueue defaults -- flags: `WQ_MEM_RECLAIM` (reentrant), maxActive: 1)


## First 2 hours
![First 2 hours](resources/time+memory-80-first.png)

## Middle 4 hours
![Middle 4 hours](resources/time+memory-80-mid.png)

## Last 2 hours
![Last 2 hours](resources/time+memory-80-last.png)

## Full set
![Full set](resources/time+memory-80-full.png)

## Process monitor
![Process Monitor](resources/time+memory-80-proc_mon.png)

## Usage by module
![Usage by module](resources/time+memory-80-module-full.png)

## Usage by module (every 1,000 beats)
![Usage by module, every 1,000](resources/time+memory-80-module-sample.png)


---

# Run 90
Thursday 2019-04-11 @ ~12:22 through Monday 2019-04-15 @ ~06:19 (321,098 beats).

Stress was added ( 2019-04-11 @ ~ 12:38 through 2019-04-15 @ ~ 06:05 ) using
various `stress-ng` options as well as random null-pointer-exception and
div-by-0 programs (via [kernel-beat/etc/stress.sh](../kernel-beat/etc/stress.sh)).

(Using workqueue defaults -- flags: `WQ_MEM_RECLAIM` (reentrant), maxActive: 1)


## First 2 hours
![First 2 hours](resources/time+memory-90-first.png)

## Middle 4 hours
![Middle 4 hours](resources/time+memory-90-mid.png)

## Last 2 hours
![Last 2 hours](resources/time+memory-90-last.png)

## Full set
![Full set](resources/time+memory-90-full.png)

## Process monitor
![Process Monitor](resources/time+memory-90-proc_mon.png)

## Usage by module

***Note:*** *It's unclear why this run appears to exhibit a ramp-down in the
catch-all `lkb` time. None of the other runs have this property...*

![Usage by module](resources/time+memory-90-module-full.png)

## Usage by module (every 1,000 beats)
![Usage by module, every 1,000](resources/time+memory-90-module-sample-1000.png)

## Usage by module (every 10,000 beats)
![Usage by module, every 10,000](resources/time+memory-90-module-sample-10000.png)


---

# Run 96

Monday 2019-04-15 @ ~10:41 through ~12:31 (6,603 beats).

Stress was added ( ~ 11:19 through ~ 12:03 ) using
various `stress-ng` options as well as random null-pointer-exception and
div-by-0 programs (via [kernel-beat/etc/stress.sh](../kernel-beat/etc/stress.sh)).


***Note:*** *Changed workqueues to use flags
`WQ_MEM_RECLAIM | WQ_HIGH_PRI | WQ_NON_REENTRANT` with a maximum active for the
`lkb` queue of 1 and for the `procMon` queue of 0
(which invokes a default of 256). These maximum queue values were chosen based
upon observation. Since multiple processes seem to frequently terminate
together, it seems sensible to allow multiple process monitor work items
(from the `procMon` queue) to be processed simultaneously.  In contrast, since
the normal beat collectors should never occur simultaneously, the maximum
active for the `lkb` queue was left at 1.*

***Note:*** *Using the `WQ_HIGH_PRI` option Seems to reduce collection times
to about 1/4 of runs without the flag. Unsure of the collatoral costs on a
loaded system.*




## First 45 minutes
![First 45 minutes](resources/time+memory-96-first.png)

## Middle 30 minutes
![Middle 30 minutes](resources/time+memory-96-mid.png)

## Last 45 minutes
![Last 45 minutes](resources/time+memory-96-last.png)

## Full set
![Full set](resources/time+memory-96-full.png)

## Usage by module
![Usage by module](resources/time+memory-96-module-full.png)

## Usage by module (every 10 beats)
![Usage by module, every 10](resources/time+memory-96-module-sample-10.png)

## Usage by module (every 100 beats)
![Usage by module, every 100](resources/time+memory-96-module-sample-100.png)

## Usage by module (every 1,000 beats)
![Usage by module, every 1,000](resources/time+memory-96-module-sample-1000.png)

