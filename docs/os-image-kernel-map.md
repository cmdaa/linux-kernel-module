# Compilation results

KernelBeat builds on:
- Linux 3.10.0    (centos:7)
- Linux 4.4.0     (ubuntu:16.04.5 aws)
- Linux 4.4.0     (ubuntu:16.04   generic)
- Linux 4.9.0     (debian:9-slim)
- Linux 4.18.0    (centos:8)

KernelBeat has built and tested on:
- Linux 3.10.0    (centos:7)
- Linux 4.4.0     (ubuntu:16.04.5 aws)

---
KernelBeat DOES NOT build on:
- Linux 3.16.0    (debian:8-slim)
  ```
  src/lkb.c: error: 'FTRACE_OPS_FL_IPMODIFY' undeclared
  src/lkb.c: error: implicit declaration of function
                      'ktime_get_mono_fast_ns'
  ```

- Linux 3.19.0    (ubuntu:15.04)
  ```
  broken package repo: cannot install linux kernel headers
  ```

- Linux 4.19.0    (debian:10-slim)
  ```
  src/lkb.c: error: implicit declaration of function 'setup_timer'
    vv
  src/kernel_beat.c:223:4:
    error: positional initialization of field in 'struct' declared
      with 'designated_init' attribute [-Werror=designated-init]
      {0},
       ^
    note: (near initialization for 's_sysctlVars[3]')
    vv
  src/modules/cpu.c:11:10: fatal error:
        linux/cputime.h: No such file or directory
  ```

- Linux 4.15.0    (ubuntu:16.04 oem)
  ```
  src/lkb.c: error: implicit declaration of function 'setup_timer'
  src/lkb.c: error: implicit declaration of function 'copy_to_user'
  ```

- Linux 5.4.0     (ubuntu:20.04)
  ```
  src/lkb.c: error: implicit declaration of function 'setup_timer'
      ^
      +- if we fix this -+
                         v
  src/kernel_beat.c:223:4:
    error: positional initialization of field in 'struct' declared
      with 'designated_init' attribute [-Werror=designated-init]
      {0},
       ^
    note: (near initialization for 's_sysctlVars[3]')
      ^
      +- if we fix this -+
                         v
  src/modules/cpu.c:11:10: fatal error:
        linux/cputime.h: No such file or directory
  ```

- Linux 5.8.0     (ubuntu:20.10)
  ```
  src/lkb.c: error: implicit declaration of function 'getboottime';
                      did you mean 'getboottime64'
  src/lkb.c: error: implicit declaration of function 'setup_timer'
  ```

- Linux 4.15+
  ```
  setup_timer => timer_setup and changed the api of the callback method

  #if LINUX_VERSION_CODE < KERNEL_VERSION(4,15,0)
    _lkb_periodic_timer( unsigned long data )
  #else
    _lkb_periodic_timer( struct timer_list* tl )
  #endif

  #if LINUX_VERSION_CODE < KERNEL_VERSION(4,15,0)
    setup_timer( &lkb.timer, _lkb_periodic_timer, 0);
  #else
    timer_setup( &lkb.timer, _lkb_periodic_timer, 0);
  #endif
  ```

- Linux 4.10 - 4.11
  ```
  Removed usecs_to_cputime64()
  ```

---
# Docker image:tag to linux kernel version mapping
KernelBeat may be built within a docker image.

This section documents the docker image:tag to kernel version mappings that
have been identified in practice.


## Centos images and kernel versions
```bash
yum info kernel-headers
```

Docker image:tag | Kernel  | Release        | Arch    | Notes
---------------- | ------- | -------------- | ------- | ---------------------
centos:7.0.1406  | 3.10.0  | 1160.15.2.el7  | x86_64  | ++ built and tested
centos:7.1.1503  | 3.10.0  | 1160.15.2.el7  | x86_64  |
centos:7.2.1511  | 3.10.0  | 1160.15.2.el7  | x86_64  |
centos:7.3.1611  | 3.10.0  | 1160.15.2.el7  | x86_64  |
centos:7.4.1708  | 3.10.0  | 1160.15.2.el7  | x86_64  |
centos:7.5.1804  | 3.10.0  | 1160.15.2.el7  | x86_64  |
centos:7.6.1810  | 3.10.0  | 1160.15.2.el7  | x86_64  |
centos:7.7.1908  | 3.10.0  | 1160.15.2.el7  | x86_64  |
centos:7.8.2003  | 3.10.0  | 1160.15.2.el7  | x86_64  |
centos:7.9.2009  | 3.10.0  | 1160.15.2.el7  | x86_64  | (centos:7)
.                |         |                |         |
centos:8.1.1911  | 4.18.0  | 240.10.1.el8_3 | x86_64  |
centos:8.2.2004  | 4.18.0  | 240.10.1.el8_3 | x86_64  |
centos:8.3.2011  | 4.18.0  | 240.10.1.el8_3 | x86_64  | (centos:8) ++ built and minor testing

--
## Debian images and kernel versions
```bash
apt update; apt show linux-headers-amd64
```

Docker image:tag | Kernel  | Release | Arch    | Notes
---------------- | ------- | ------- | ------- | ---------------
debian:8-slim    | 3.16.0  | 11      | amd64   | + builds
debian:9-slim    | 4.9.0   | 14      | amd64   | + builds
debian:10-slim   | 4.19.0  | 14      | amd64   | - build errors


## Ubuntu images and kernel versions
```bash
apt update; apt show linux-headers-generic
```

Docker image:tag | Kernel  | Release | Arch / linux-headers-\* | Notes
---------------- | ------- | ------- | ----------------------- | --------------------
ubuntu:15.04     | 3.19.0  | 15.14   | generic                 | ~ build errors
.                | 3.19.0  | 15.14   | virtual                 |
.                |         |         |                         |
ubuntu:16.04     | 4.4.0   | 201.207 | generic                 |
.                | 4.4.0   | 201.207 | virtual                 |
.                | 4.4.0   | 1087.85 | kvm                     |
.                | 4.4.0   | 1121    | aws                     | ++ built and tested
.                | 4.15.0  | 133.131 | oem                     | ~ build errors
.                |         |         |                         |
ubuntu:20.04     | 5.4.0   | 65.68   | generic                 | - build errors
.                | 5.4.0   | 65.68   | virtual                 |
.                | 5.4.0   | 1032.30 | kvm                     |
.                | 5.4.0   | 65.68   | oem                     |
.                |         |         |                         |
ubuntu:20.10     | 5.8.0   | 41.45   | generic                 | - build errors
.                | 5.8.0   | 41.45   | virtual                 |
.                | 5.8.0   | 1016.18 | kvm                     |

### Ubuntu version names
https://en.wikipedia.org/wiki/Ubuntu_version_history#Table_of_versions

Ubuntu  | name              | Linux       | Notes
------- | ----------------- | ----------- | --------------------
20.10   | Groovy Gorilla    | 5.8         | - build errors
20.04   | Focal Fossa       | 5.4         | - build errors
19.10   | Eoan Ermine       | 5.3         |
19.04   | Disco Dingo       | 5.0         |
18.10   | Cosmic Cuttlefish | 4.18        | - build errors
        |                   |             |   4.18.0-
        |                   |             |     [111 - 1109]-
        |                   |             |     [aws, generic, ...]
18.04   | Bionic Beaver     | 4.15        | - build errors
        |                   |             |   4.15.0-
        |                   |             |     [13  - 1025]-
        |                   |             |     [aws, generic, ...]
17.10   | Artful Aardvark   | 4.13        |
17.04   | Zesty Zapus       | 4.10        |
16.10   | Yakkety Yak       | 4.8         |
16.04   | Xenial Xerus      | 4.4         | ++ built and tested
15.10   | Wily Werewolf     | 4.2         |
15.04   | Vivid Vervet      | 3.19        | ~ build errors
14.10   | Utopic Unicorn    | 3.16        |
14.04   | Trusty Tahr       | 3.13        |
13.10   | Saucy Salamander  | 3.11        |
13.04   | Raring Ringtail   | 3.8         |
12.10   | Quantal Quetzal   | 3.5         |
12.04   | Precise Pangolin  | 3.2+        |
11.10   | Oneiric Ocelot    | 3.0         |
11.04   | Natty Narwhal     | 2.6.38      |
10.10   | Maverick Meerkat  | 2.6.35      |
10.04   | Lucid Lynx        | 2.6.32      |
09.10   | Karmic Koala      | 2.6.31      |
09.04   | Jaunty Jackalope  | 2.6.28      |
08.10   | Intrepid Ibex     | 2.6.27      |
08.04   | Hardy Heron       | 2.6.24      |
07.10   | Gutsy Gibbon      | 2.6.22      |
07.04   | Feisty Fawn       | 2.6.20      |
06.10   | Edgy Eft          | 2.6.17      |
06.06   | Dapper Drake      | 2.6.15      |
05.10   | Breezy Badger     | 2.6.12      |
05.04   | Hoary Hedgehog    | 2.6.10      |
04.10   | Warty Warthog     | 2.6.8       |
