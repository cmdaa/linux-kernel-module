/**
 *  Kernel-level udp sender.
 *
 */
#include "udp.h"

#include <linux/net.h>
#include <uapi/linux/in.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0)
static struct net* s_init_net = NULL;
#endif  // LINUX_VERSION_CODE

/**
 *  Create/open a new kerenel-level UDP socket.
 *  @method udp_open_socket
 *  @param  dst       The destination information {net_failover_t*};
 *
 *  @return The new socket {struct socket*};
 */
struct socket*
udp_open_socket( net_failover_t* dst ) {
  struct socket*  sock  = NULL;
  int             res;

  if (dst)  {}  // Squelch unused errors

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0)
  if (s_init_net == NULL) {
    s_init_net = lkb_resolve_one( "init_net" );

    printk(KERN_INFO "%s udp_open_socket(): init_net[ %p ]\n",
                     DEVICE_NAME, s_init_net);
    if (s_init_net == NULL) {
      return sock;
    }
  }

  res = sock_create_kern( s_init_net, AF_INET, SOCK_DGRAM, 0, &sock );

#else   // LINUX_VERSION_CODE < 4.2.0
  res = sock_create_kern( AF_INET, SOCK_DGRAM, 0, &sock );

#endif  // LINUX_VERSION_CODE

  if (res != 0) {
    printk(KERN_ALERT "%s udp_open_socket(): FAILED[ %d ]\n",
                      DEVICE_NAME, res);
  }

  return sock;
}

/**
 *  Close and release an existing kernel-level UDP socket.
 *  @method udp_close_socket
 *  @param  dst       The destination information {net_failover_t*};
 *
 *  @return void
 */
void
udp_close_socket( net_failover_t* dst ) {
  int res;

  if (dst->sock == NULL) { return; }

  res = dst->sock->ops->release( dst->sock );
  if (res == 0) {
    printk(KERN_INFO "%s udp_close_socket(): closed\n", DEVICE_NAME);

    dst->sock = NULL;

  } else {
    printk(KERN_ALERT "%s udp_close_socket(): FAILED[ %d ]\n",
                      DEVICE_NAME, res);
  }
}

/**
 *  Send the given set of data, described via io vectors, over the provided
 *  socket to the specified address.
 *  @method udp_send_iov
 *  @param  dst       The destination information {udp_failover_t*};
 *  @param  iov       The io vectors {struct iov*};
 *  @param  iovCount  The number of io vectors {size_t};
 *
 *  @return The number of bytes sent (< 0 on error) {ssize_t};
 */
ssize_t
udp_send_iov( udp_failover_t* dst,
              struct kvec*    iov,
              size_t          iovCount)
{
  unsigned int          flags   = MSG_CONFIRM;
  struct sockaddr_in    addr    = {
    .sin_family = AF_INET,
    .sin_port   = dst->port,
    .sin_addr   = { .s_addr= dst->host },
  };
  struct msghdr         msg     = {
    .msg_flags      = flags,
    .msg_name       = (struct sockaddr*)&addr,
    .msg_namelen    = sizeof( addr ),
  };
  size_t                nBytes  = 0;
  int                   idex;

  if (dst->sock == NULL || dst->port < 1 || dst->host < 1) {
    /* Socket not established:
     *  EIO     5   // I/O error
     *  ENXIO   6   // No such device or address
     *  ENODEV  19  // No such device
     *  EINVAL  22  // Invalid argument
     *  EPIPE   32  // Broken pipe
     */
    return 0; //-EIO;
  }

  // Count the number of bytes from all io vectors.
  for (idex = 0; idex < iovCount; idex++) {
    nBytes += iov[idex].iov_len;
  }

  return kernel_sendmsg( dst->sock, &msg, iov, iovCount, nBytes );
}

/**
 *  Send the given data over the provided socket to the specified address.
 *  @method udp_send
 *  @param  dst       The destination information {udp_failover_t*};
 *  @param  buf       The data to send {void*};
 *  @param  bufLen    The length (in bytes) of `buf` {size_t};
 *
 *  @return The number of bytes sent (< 0 on error) {ssize_t};
 */
ssize_t
udp_send( udp_failover_t* dst,
          void*           buf,
          size_t          bufLen)
{
  struct kvec           iov     = { .iov_base= buf, .iov_len= bufLen };

  return udp_send_iov( dst, &iov, 1 );
}
