/**
 *  Provides simple JSON-encoding directly into the circular buffer for basic
 *  data types.
 *
 *  Writes to the circular buffer are via `lkb_write()`.
 *
 */
#include "lkb.h"
#include "json.h"
#include "map.h"
#include <linux/string.h>   // strnlen()

/**
 *  Write a raw, unquoted string value as JSON to the circular buffer.
 *  @method json_write_raw_strn_val
 *  @param  val   The value {const char*};
 *  @param  max   The maximum number of characters from `val` {int};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_raw_strn_val( const char* val, int max ) {
  size_t valLen   = strnlen( val, max );
  size_t written  = 0;
  size_t idex;

  /* Encode any special characters while writing this string.
   *
   * According to the JSON spec (https://www.json.org/json-en.html), a string
   * may contain "Any codepoint except " or control characters" and the only
   * valid escapes are:
   *   \"     "
   *   \'     '
   *   \/     /
   *   \b     backspace           (0x08)
   *   \t     horizontal-tab      (0x09)
   *   \n     new-line            (0x0a)
   *   \f     new-page/form-feed  (0x0c)
   *   \r     carriage-return     (0x0d)
   *   \u#### Unicode codepoint where '#' are hex digits
   *
   *  Control characters may be encoded as a Unicode codepoint.
   */
  for (idex = 0; idex < valLen; idex++) {
    char  strc  = val[idex];

    switch( strc ) {
      case '"':   // Escape this double quote
        written += lkb_write( "\\\"", 2 );
        break;

      case '\b':  // Backspace
        written += lkb_write( "\\b", 2 );
        break;

      case '\t':  // Tab
        written += lkb_write( "\\t", 2 );
        break;

      case '\n':  // Line feed
        written += lkb_write( "\\n", 2 );
        break;

      case '\f':  // Form feed
        written += lkb_write( "\\f", 2 );
        break;

      case '\r':  // Carriage return
        written += lkb_write( "\\r", 2 );
        break;

      case '\\':
        if (idex+1 < valLen) {
          // Ensure the next character is a valid escape sequence
          char  nextc = val[idex+1];

          switch( nextc ) {
            case 'u': {
              // \u is valid iff the following 4 digts are hex
              u8  isValid = 1;

              if ( idex+6 > valLen) {
                // Not enough characters
                isValid = 0;

              } else {
                // Ensure the 4 characters following \u are hex digits
                size_t  jdex;

                for ( jdex = idex+2; jdex < idex+6; jdex++) {
                  u8  cur = val[jdex];

                  if ( cur < '0' || cur > 'f'  ||
                      (cur > '9' && cur < 'A') ||
                      (cur > 'F' && cur < 'a') ) {
                    // NOT a hex digit
                    isValid = 0;
                    break;
                  }
                }
              }

              if (isValid != 1) {
                /* This is NOT a valid \u sequence.
                 *
                 * Treat it as a simple, escaped ASCII character 'u' squelching
                 * this escape character and continuing to the actual 'u' for
                 * normal processing.
                 */
                continue;
              }
            } // Valid Escape sequence -- Fall through

            case '"':
            //case '\'':  // Doesn't appear to be valid in reality
            case '/':
            case 'b':
            case 't':
            case 'n':
            case 'f':
            case 'r':
              /* Valid escape sequence.
               *
               * Write both characters of this valid escape sequence.
               */
              written += lkb_write( "\\",   1 );
              written += lkb_write( &nextc, 1 );

              // Skip the next character since we just wrote it
              idex++;
              continue;

            default:
              if (nextc >= 0x20 && nextc <= 0x7f) {
                /* The next character is in the ASCII range and should NOT be
                 * escaped.
                 *
                 * Squelch this escape character and continue on to the
                 * following character for normal processing, essentially
                 * unescaping the ASCII character.
                 */
                continue;
              }
              break;
          }
        }

        // NOT a valid escape sequence so simply escape this escape character.
        written += lkb_write( "\\\\", 2 );
        break;

      default:
        if ((u8)strc < 0x20) {
          // Control character (< 0x20) -- encode it using the '\u####' format.
          char   numBuf[5];
          size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%04x",
                                                                (u8)strc );

          written += lkb_write( "\\u", 2 );
          written += lkb_write( numBuf, valLen );

        } else {
          // non-control codepoint
          written += lkb_write( &strc, 1 );
        }
        break;
    }
  }

  return written;
}

/**
 *  Write the provided key/string.
 *  @method json_write_key
 *  @param  key   The key/name {const char*};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_key( const char* key ) {
  size_t written  = 0;
  if (key != NULL) {
    size_t keyLen   = strnlen( key, JSON_MAX_KEY );

    // Write: "%key%":
    written += lkb_write( "\"", 1 );
    written += json_write_raw_strn_val( key,  keyLen );
    written += lkb_write( "\":", 2 );
  }

  return written;
}

/**
 *  Write the string value as JSON to the circular buffer.
 *  @method json_write_strn_val
 *  @param  val   The value {const char*};
 *  @param  max   The maximum number of characters from `val` {int};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_strn_val( const char* val, int max ) {
  size_t valLen   = strnlen( val, max );
  size_t written  = 0;

  written += lkb_write( "\"", 1 );
  written += json_write_raw_strn_val( val,  valLen );
  written += lkb_write( "\"", 1 );

  return written;
}

/**
 *  Write the unsigned 64-bit value as JSON to the lkb circular buffer.
 *  @method json_write_u64_val
 *  @param  val   The value {u64};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_u64_val( u64 val ) {
  // Max 64-bit unsigned: 18,446,744,073,709,551,615 == 20 characters
  char   numBuf[21];
  size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%llu", val );

  return lkb_write( numBuf, valLen );
}

/**
 *  Write the signed 64-bit value as JSON to the lkb circular buffer.
 *  @method json_write_s64_val
 *  @param  val   The value {s64};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_s64_val( s64 val ) {
  // Min 64-bit signed  : −9,223,372,036,854,775,808 == 20 characters
  // Max 64-bit signed  :  9,223,372,036,854,775,807 == 19 characters
  char   numBuf[21];
  size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%lld", val );

  return lkb_write( numBuf, valLen );
}

/**
 *  Write the unsigned 32-bit value as JSON to the lkb circular buffer.
 *  @method json_write_u32_val
 *  @param  val   The value {u32};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_u32_val( u32 val ) {
  // Max 32-bit unsigned:  8,589,934,591 == 10 characters
  char   numBuf[11];
  size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%u", val );

  return lkb_write( numBuf, valLen );
}

/**
 *  Write the signed 32-bit value as JSON to the lkb circular buffer.
 *  @method json_write_s32_val
 *  @param  val   The value {s32};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_s32_val( s32 val ) {
  // Min 32-bit signed: -2,147,483,648 == 11 characters
  // Max 32-bit signed:  2,147,483,647 == 10 characters
  char   numBuf[12];
  size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%d", val );

  return lkb_write( numBuf, valLen );
}

/**
 *  Write the unsigned 16-bit value as JSON to the lkb circular buffer.
 *  @method json_write_u16_val
 *  @param  val   The value {u16};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_u16_val( u16 val ) {
  // Max 16-bit unsigned:  65,536 == 5 characters
  char   numBuf[6];
  size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%u", val );

  return lkb_write( numBuf, valLen );
}

/**
 *  Write the signed 16-bit value as JSON to the lkb circular buffer.
 *  @method json_write_s16_val
 *  @param  val   The value {s16};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_s16_val( s16 val ) {
  // Min 16-bit signed  : -32,768 == 6 characters
  // Max 16-bit signed  :  32,768 == 5 characters
  char   numBuf[8];
  size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%d", val );

  return lkb_write( numBuf, valLen );
}

/**
 *  Write the unsigned 8-bit value as JSON to the lkb circular buffer.
 *  @method json_write_u8_val
 *  @param  val   The value {u8};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_u8_val( u8 val ) {
  // Max 8-bit unsigned:  255 = 3 characters
  char   numBuf[4];
  size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%u", val );

  return lkb_write( numBuf, valLen );
}

/**
 *  Write the signed 8-bit value as JSON to the lkb circular buffer.
 *  @method json_write_s8_val
 *  @param  val   The value {s8};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_s8_val( s8 val ) {
  // Min 8-bit signed: -128 == 4 characters
  // Max 8-bit signed:  127 == 3 characters
  char   numBuf[5];
  size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%d", val );

  return lkb_write( numBuf, valLen );
}

/**
 *  Write the unsigned 64-bit key/value as JSON to the lkb circular buffer.
 *  @method json_write_u64
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {u64};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_u64( const char* key, u64 val ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_u64_val( val );

  return written;
}

/**
 *  Write the signed 64-bit key/value as JSON to the lkb circular buffer.
 *  @method json_write_s64
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {s64};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_s64( const char* key, s64 val ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_s64_val( val );

  return written;
}

/**
 *  Write the unsigned 32-bit key/value as JSON to the lkb circular buffer.
 *  @method json_write_u32
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {u32};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_u32( const char* key, u32 val ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_u32_val( val );

  return written;
}

/**
 *  Write the signed 32-bit key/value as JSON to the lkb circular buffer.
 *  @method json_write_s32
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {s32};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_s32( const char* key, s32 val ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_s32_val( val );

  return written;
}

/**
 *  Write the unsigned 16-bit key/value as JSON to the lkb circular buffer.
 *  @method json_write_u16
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {u16};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_u16( const char* key, u16 val ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_u16_val( val );

  return written;
}

/**
 *  Write the signed 16-bit key/value as JSON to the lkb circular buffer.
 *  @method json_write_s16
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {s16};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_s16( const char* key, s16 val ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_s16_val( val );

  return written;
}

/**
 *  Write the unsigned 8-bit key/value as JSON to the lkb circular buffer.
 *  @method json_write_u8
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {u8};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_u8( const char* key, u8 val ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_u8_val( val );

  return written;
}

/**
 *  Write the signed 8-bit key/value as JSON to the lkb circular buffer.
 *  @method json_write_s8
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {s8};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_s8( const char* key, s8 val ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_s8_val( val );

  return written;
}

/**
 *  Write the string key/value as JSON to the circular buffer.
 *  @method json_write_strn
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {const char*};
 *  @param  max   The maximum number of characters from `val` {int};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_strn( const char* key, const char* val, int max ) {
  size_t written  = 0;

  // Write: "%key%": %val%
  written += json_write_key( key );
  written += json_write_strn_val( val, max );

  return written;
}

/**
 *  Given a map entry and pointer to a structure, write the JSON version of
 *  referenced value.
 *  @method json_write_map
 *  @param  entry   The map entry {map_t*};
 *  @param  pstruct A pointer to the containing structure {void*};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_map( map_t* entry, void* pstruct ) {
  ssize_t written = 0;
  void*   pval    = pstruct + entry->offset;

  switch( entry->type ) {
    case MAP_TYPE_STR:
      written = json_write_strn( entry->name, (char*)pval, entry->size );
      break;

    case MAP_TYPE_S8:
      written = json_write_s8( entry->name, *((s8*)pval) );
      break;

    case MAP_TYPE_U8:
      written = json_write_u8( entry->name, *((u8*)pval) );
      break;

    case MAP_TYPE_S16:
      written = json_write_s16( entry->name, *((s16*)pval) );
      break;

    case MAP_TYPE_U16:
      written = json_write_u16( entry->name, *((u16*)pval) );
      break;

    case MAP_TYPE_S32:
      written = json_write_s32( entry->name, *((s32*)pval) );
      break;

    case MAP_TYPE_U32:
      written = json_write_u32( entry->name, *((u32*)pval) );
      break;

    case MAP_TYPE_S64:
      written = json_write_s64( entry->name, *((s64*)pval) );
      break;

    case MAP_TYPE_U64:
      written = json_write_u64( entry->name, *((u64*)pval) );
      break;

    default:
      // :NOTE: MAP_TYPE_SPC MUST be handled by the caller
      break;
  }

  return written;
}
