/**
 *  Module: filesystems, fsstats
 *
 *  Beat-driven modules that collect filesystem metrics:
 *    - filesystems   collects the set of mounted filesystems with metrics
 *                    for each;
 *    - fsstats       collects overall filesystem usage metrics;
 *
 *  This module:
 *    - makes use of non-public, though exported, kernel symbols:
 *      - iterate_mounts()
 *
 *    - makes use of a hash table to monitor the set of active filesystems;
 *    - employes protections, via _isMountValid(), to ensure a VFS mount entry
 *      is in-fact valid;
 *
 *
 *  :NOTE: Unless this module is compiled using full kernel sources
 *         (indicated via the USE_KERNEL_SOURCES #define), a fragile
 *         KLUDGE is used to provide access to `struct mount` needed
 *         in order to access information about filesystem mount points.
 *
 *==========================================================================
 *  include/linux/fs.h:
 *     struct file_system_type {
 *       const char* name;
 *       int         fs_flags;
 *                     FS_REQUIRES_DEV, FS_BINARY_MOUNTDATA,
 *                     FS_HAS_SUBTYPE,  FS_USERNS_MOUNT,
 *                     FS_RENAME_DOS_D_MOVE
 *       ...
 *     }
 *
 *     struct super_block {
 *       ...
 *       dev_t                     s_dev;
 *       ...
 *       struct file_system_type*  s_type;
 *       ...
 *       unsigned long             s_flags;
 *       unsigned long             s_iflags;
 *       unsigned long             s_magic;
 *       struct dentry*            s_root;
 *       ...
 *     }
 *
 *  include/uapi/linux/magic.h:
 *    s_magic:
 *      0x187      : autofs       AUTOFS_SUPER_MAGIC
 *      0xef53     : ext4         EXT2/EXT3/EXT4_SUPER_MAGIC
 *      0xface     : vboxsf
 *      0x27e0eb   : cgroup       CGROUP_SUPER_MAGIC
 *      0x1021994  : tmpfs        TMPFS_MAGIC
 *      0x19800202 : mqueue
 *      0x6165676c : pstore       PSTOREFS_MAGIC
 *      0x64626720 : debugfs      DEBUGFS_MAGIC
 *      0x73636673 : securityfs   SECURITYFS_MAGIC
 *      0x958458f6 : hugetlbfs    HUGETLBFS_MAGIC
 *
 *  include/linux/dcache.h:
 *     struct qstr {
 *       union {
 *         struct { HASH_LEN_DECLARE; };
 *         u64 hash_len;
 *       };
 *       const unsigned char*  name;
 *     }
 *
 *     struct dentry {
 *       unsigned int        d_flags;  // protected by d_lock
 *                             DCACHE_OP_REVALIDATE
 *                             DCACHE_OP_WEAT_REVALIDATE
 *                             DCACHE_OP_REAL
 *                             DCACHE_DISCONNECTED
 *                             DCACHE_DENTRY_KILLED
 *                             DCACHE_NEED_AUTOMOUNT
 *       ...
 *       struct dentry*      d_parent;
 *       struct qstr         d_name;
 *       struct inode*       d_inode;
 *       struct super_block* d_sb;
 *       ...
 *     }
 *
 *  include/linux/mount.h:
 *     struct vfsmount {
 *       struct dentry*      mnt_root;
 *       struct superblock*  mnt_sb;
 *       int                 mnt_flags;
 *     }
 *
 *  include/linux/path.h
 *     struct path {
 *       struct vfsmount* mnt;
 *       struct dentry*   dentry;
 *     }
 *
 *  include/linux/statfs.h : struct kstatfs
 *    long            f_type;     // Seems to match s_magic from super_block
 *    long            f_bsize;    // optimal transfer block size
 *    u64             f_blocks;   // total blocks
 *    u64             f_bfree;    // free  blocks
 *    u64             f_bavail;   // free  blocks for unprivileged user
 *    u64             f_files;    // total file nodes
 *    u64             f_ffree;    // free  file nodes
 *    __kernel_fsid_t f_fsid;     // filesystem ID  : { int val[2]; }
 *    long            f_namelen;  // maximum length of filenames
 *    long            f_frsize;   // fragment size
 *    long            f_flags;    // mount flags
 *                      ST_RDONLY ST_NOSUID ST_NODEV ST_NOEXEC
 *                      ST_SYNCHRONOUS ST_VALID ST_MANDLOCK
 *                      ST_NOATIME ST_NODIRATIME ST_RELATIME
 *    long            f_spare[4]; // padding
 *
 *==========================================================================
 *  fs/proc_namespace.s : show_mountinfo( struct seq_file* m,
 *                                        struct vfsmount* mnt)
 *    struct vfsmount*    mnt = root.mnt;
 *    struct mount*       r   = real_mount( mnt );
 *
 *    struct super_block* sb       = mnt->mnt_sb
 *    struct path         mnt_path = { .dentry = mnt->mnt_root,
 *                                     .mnt     = mnt };
 *
 *    r->mnt_id
 *    r->mnt_parent->mnt_id
 *    MAJOR( sb->s_dev )
 *    MINOR( sb->s_dev )
 *
 *    if (sb->s_op->show_path) {
 *      ... sb->s_op->show_path( m, mnt->mnt_root );
 *      // ... path = dentry_path( dentry, buf, size );
 *
 *    } else {
 *      seq_dentry(m, mnt->mnt_root, " \t\n\\");
 *    }
 *    ...
 *
 *    // mountpoint
 *    err = seq_path_root(m, &mnt_path, &p->root, " \t\n\\");
 *    ...
 *
 *    seq_puts(m, mnt->mnt_flags & MNT_READONLY ? " ro" : " rw");
 *    ...
 */
#include "../lkb.h"

#include <linux/slab.h>           // kmalloc() / kfree()
#include <linux/sched.h>          // struct task_struct
#include <linux/statfs.h>         // struct kstatfs
#include <linux/nsproxy.h>        // struct nsproxy
#include <linux/mnt_namespace.h>  // put_mnt_ns()

#include <linux/types.h>          // struct hlist_node
#include <linux/dcache.h>         // struct dentry
#include <linux/path.h>           // struct path
#include <linux/mount.h>          // struct vfsmount
#include <linux/fs.h>             // struct super_block, struct file_system_type
#include <linux/fs_struct.h>      // struct fs_struct

#ifdef  USE_LKB_DIFF
# include <linux/list.h>          // struct hlist_node
# include <linux/hash.h>          // hash_long()
#endif  // USE_LKB_DIFF

#ifdef  USE_KERNEL_SOURCES  // {
  // :NOTE: Direct access to kernel sources is required for this include
# include <fs/mount.h>

#else   // USE_KERNEL_SOURCES  }{
  /**
   *  :KLUDGE: !!!!!
   *
   *  Without include access to kernel sources (fs/mount.h) we must revert to
   *  locally defining the portion of 'struct mount' that we need in order to
   *  access `mnt_devname`.
   */
  struct mount {
    struct hlist_node mnt_hash;
    struct mount *mnt_parent;
    struct dentry *mnt_mountpoint;
    struct vfsmount mnt;
    union {
      struct rcu_head mnt_rcu;
      struct llist_node mnt_llist;
    };
# ifdef CONFIG_SMP
    struct mnt_pcp __percpu *mnt_pcp;
# else
    int mnt_count;
    int mnt_writers;
# endif
    struct list_head mnt_mounts;    // list of children, anchored here
    struct list_head mnt_child;     // and going through their mnt_child
    struct list_head mnt_instance;  // mount instance on sb->s_mounts
    const char *mnt_devname;        // Name of device e.g. /dev/dsk/hda1
    // ...
  };

#endif  // USE_KERNEL_SOURCES  }

#include "../map.h"
#include "../json.h"

#include "filesystem.h"


// Track memory allocation
static u64  s_bytesAlloc = 0;
static u64  s_bytesFreed = 0;

/**
 *  The maximum mount path we are willing to present.
 */
#define FS_MAX_PATH  128

// :NOTE: If additional fields are added, `s_mapFs` MUST also be changed.
typedef struct {
  /* struct vfsmount*  mnt;     // VFS mount information
   * struct kstatfs    stat;    // Stat information
   *
   *  mnt (vfsmount*)
   *    -> mnt_sb (superblock*)
   *        -> s_dev    system-unique device id
   *        -> s_type   (file_system_type* : name)
   *
   *  struct path   mnt_path  = { .mnt = mnt, .dentry = mnt->mnt_root };
   *  struct mount* mnt_proc  = fs_getProcMount( mnt );
   *
   * From the above information, we have access to the necessary stats,
   * including:
   *    mountPoint:   '/var',       # fs_getMountpointPath( &mnt_path )
   *    deviceName:   '/dev/sda5',  # mnt_proc->mnt_devname
   *
   */
  u32 id;             /* Unique filesystem id / superblock device id
                       *  (mnt->mnt_sb->s_dev);
                       */
  u32 magic;          // Filesystem magic number (mnt->mnt_sb->s_magic);
  const char* type;   // Filesystem type (mnt->mnt_sb->s_type->name);

  u64 filesTotal;     // Total file count (stat.f_files);
  u64 filesFree;      // Available file count (stat.f_ffree);

  u64 sizeTotal;      // Total size in bytes (stat.f_bsize * stat.f_blocks);
  u64 sizeFree;       // Free size in bytes (stat.f_bsize * stat.f_bfree);
  u64 sizeAvail;      /* Free size in bytes, available to unprivileged users
                       * (stat.f_bsize * stat.f_bavail);
                       */
  //  sizeUsed = sizeTotal - sizeFree

  // Use a 32-bit hash to detect changes to the device name and mount point
  u32 hashMountpoint; /* Hash of the absolute path where the filesystem is
                       * mounted (mnt_proc->mnt_devname);
                       */
  u32 hashDevname;    /* Hash of the absolute path of the mounted device
                       * ( fs_getMountpointPath( &mnt_path ) );
                       */

  /* A simple counter/flag indicating when this entry was last visited in
   * response to a matching, active filesystem.
   */
  u32 lastBeat;

#ifdef  USE_LKB_DIFF
  // Hash table overflow chain
  struct hlist_node fs_chain;
#endif  // USE_LKB_DIFF

  struct rcu_head   fs_rcu;

} filesystem_t;

// :NOTE: If additional fields are added, `s_mapStat` MUST also be changed.
typedef struct {
  u32 count;        // mounted file system count

  u64 filesTotal;   // total files count
  u64 filesFree;    // available files

  u64 sizeTotal;    // total size (bytes)
  u64 sizeFree;
  u64 sizeAvail;    // available to an unprivileged user
  //  sizeUsed = sizeTotal - sizeFree

} fsstat_t;

/**
 *  The mapping from `filesystem_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all fields from `filesystem_t` MUST be
 *        identified.
 */
static map_t  s_mapFs[]   = {
  {"id",              MAP_TYPE_U32, OFFSETOF(filesystem_t, id)},
  {"type",            MAP_TYPE_U32, OFFSETOF(filesystem_t, magic)},

  {"filesTotal",      MAP_TYPE_U64, OFFSETOF(filesystem_t, filesTotal)},
  {"filesFree",       MAP_TYPE_U64, OFFSETOF(filesystem_t, filesFree)},

  {"sizeTotal",       MAP_TYPE_U64, OFFSETOF(filesystem_t, sizeTotal)},
  {"sizeFree",        MAP_TYPE_U64, OFFSETOF(filesystem_t, sizeFree)},
  {"sizeAvail",       MAP_TYPE_U64, OFFSETOF(filesystem_t, sizeAvail)},
  // sizeUsed = sizeTotal - sizeFree

  {"devName",         MAP_TYPE_SPC, OFFSETOF(filesystem_t, hashDevname)},
  {"mountpoint",      MAP_TYPE_SPC, OFFSETOF(filesystem_t, hashMountpoint)},
};
static int    s_mapFsCnt  = COUNT(s_mapFs);

/**
 *  The mapping from `fsstat_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all fields from `fsstat_t` MUST be identified.
 */
static map_t  s_mapStat[]     = {
  {"count",           MAP_TYPE_U32, OFFSETOF(fsstat_t, count)},

  {"filesTotal",      MAP_TYPE_U64, OFFSETOF(fsstat_t, filesTotal)},
  {"filesFree",       MAP_TYPE_U64, OFFSETOF(fsstat_t, filesFree)},

  {"sizeTotal",       MAP_TYPE_U64, OFFSETOF(fsstat_t, sizeTotal)},
  {"sizeFree",        MAP_TYPE_U64, OFFSETOF(fsstat_t, sizeFree)},
  {"sizeAvail",       MAP_TYPE_U64, OFFSETOF(fsstat_t, sizeAvail)},
  // sizeUsed = sizeTotal - sizeFree
};
static int      s_mapStatCnt  = COUNT(s_mapStat);

/**
 *  Iteration state passed to _process_mount() from lkb_fs_json().
 */
typedef struct {
  ssize_t   written;
  bool      needComma;
  fsstat_t  fsstat;
} iter_t;

/**
 *  A string/length/hash for use with mountpoint and device name handling.
 */
typedef struct {
  char*   str;
  size_t  len;
  u32     hash;
} strinfo_t;

#ifdef  USE_LKB_DIFF
static bool     s_haveStatLast  = FALSE;
static fsstat_t s_statLast;
#endif // USE_LKB_DIFF

/* :NOTE: Filled via `lkb_fs_json()` for use by `lkb_fsstat_json()`.
 *
 *        `lkb_fs_json()` MUST be invoked BEFORE `lkb_fsstat_json()` in order
 *        to gather the stats that will be reported.
 */
static bool     s_haveStatCur   = FALSE;
static fsstat_t s_statCur;

/**
 *  The set of non-exported kernel functions and variables we use
 *
 *  These are resolved and associated with the related s_kern entry in
 *  lkb_proc_init().
 */
static Ksyms_t s_syms[]    = {
  { "iterate_mounts" },
};
static int      s_numSyms   = COUNT(s_syms);

/**
 *  Resolved function accessor.
 *
 *  These are associated with the resolved s_syms entry in lkb_proc_init().
 */
static struct {
  // include/linux/fs.h
  int (*iterate_mounts)   (int (*)(struct vfsmount *, void *), void *,
                           struct vfsmount *);

} s_kern;

/*****************************************************************************
 * Private methods {
 */

/****************************************************************
 * Filesystem hash table to store the set of filesystems that
 * were active last beat.
 *
 *
 */

/**
 *  Fetch the value from a VFS mount entry used to uniqueuly identify a
 *  filesystem.
 */
#define fs_id(mnt)  ((u32)((mnt)->mnt_sb->s_dev))

#ifdef  USE_LKB_DIFF // {
/**
 *  Given a unique filesystem identifier (via fs_id()), generate a hash.
 */
#define fs_hashfn(id) hash_long((unsigned long)id, s_fsHash_shift)

/**
 *  Our filesystem hash table and shift/size information.
 */
static struct hlist_head* s_fsHash;
static unsigned int       s_fsHash_shift = 5;  // default to 32 buckets

/**
 *  Initialize the filesystem hash table.
 *  @method _fsHash_init
 *
 *  @return void
 *  @private
 */
static void
_fsHash_init( void ) {
  unsigned int  hashEntries = 1U << s_fsHash_shift;
  u32           nBytes      = sizeof(*s_fsHash) * hashEntries;
  unsigned int  idex;

  s_fsHash = kzalloc( nBytes, GFP_KERNEL );

  s_bytesAlloc += nBytes;

  // /*
  printk(KERN_INFO "%s _fsHash_init(): initialize %u hash buckets, "
                        "%u bytes\n",
                   DEVICE_NAME, hashEntries, nBytes);
  // */

  for (idex = 0; idex < hashEntries; idex++) {
    INIT_HLIST_HEAD( &s_fsHash[idex] );
  }
}

/**
 *  Check if a filesystem identified by the given VFS mount exists in our hash
 *  table.
 *  @method _fsHash_find
 *  @param  mnt   The VFS information {struct vfsmount*};
 *
 *  @return A pointer to the hashed filesystem if it exists {filesystem_t*};
 *  @private
 */
static filesystem_t*
_fsHash_find( struct vfsmount* mnt ) {
  u32           id    = fs_id( mnt );
  unsigned long hash  = fs_hashfn( id );
  filesystem_t* fs;

  rcu_read_lock();
  hlist_for_each_entry_rcu( fs, &s_fsHash[ hash ], fs_chain) {
    if (fs->id == id) {
      rcu_read_unlock();
      return fs;
    }
  }
  rcu_read_unlock();

  return NULL;
}

/**
 *  Create a dynamically allocated clone of the given entry.
 *  @method _fsHash_clone
 *  @param  fs        The entry to clone {filesystem_t*};
 *
 *  @note   This is generally needed because the incoming `fs` exists on the
 *          stack and now needs to exist in dynamic memory for storage in the
 *          hash table.
 *
 *  @return A dynamically allocated version of `fs` {filesystem_t*};
 *  @private
 */
static filesystem_t*
_fsHash_clone( filesystem_t* fs ) {
  filesystem_t* newFs = (filesystem_t*)kmalloc( sizeof(*fs), GFP_KERNEL );

  s_bytesAlloc += sizeof(*fs);

  memcpy( newFs, fs, sizeof(*fs) );

  return newFs;
}

/**
 *  Add the given entry to our hash table.
 *  @method _fsHash_add
 *  @param  fs    The entry to add {filesystem_t*};
 *
 *  @return A dynamically allocated clone of `fs` {filesystem_t*};
 *  @private
 */
static filesystem_t*
_fsHash_add( filesystem_t* fs ) {
  filesystem_t*       clone = _fsHash_clone( fs );
  unsigned long       hash  = fs_hashfn( clone->id );
  struct hlist_node*  node  = &(clone->fs_chain);

  rcu_read_lock();
    hlist_add_head_rcu( node, &s_fsHash[ hash ] );
  rcu_read_unlock();

  return clone;
}

/**
 *  Invoked via `call_rcu()` to perform the final storage free.
 *  @method __fsHash_del_rcu
 *  @param  rcu   The head of the RCU list {struct rcu_head*};
 *
 *  @return void
 *  @private
 */
static void
__fsHash_del_rcu( struct rcu_head* rcu ) {
  filesystem_t* fs  = container_of( rcu, filesystem_t, fs_rcu );

  s_bytesFreed += sizeof(*fs);

  kfree( fs );
}

/**
 *  Replace the contents of the old entry with that of the new.
 *  @method _fsHash_replace
 *  @param  oldFs     The entry to replace {filesystem_t*};
 *  @param  newFs     The entry to clone and add {filesystem_t*};
 *
 *  @note   If NO_INPLACE_REPLACE is defined, this will incur the cost of
 *          a full allocation (_fsHash_clone()) and rcu-based deletion.
 *
 *          Otherwise, it will replace the contents of `oldFs` with that of
 *          `newFs` maintaining the existing list pointers.
 *
 *  @return A dynamically allocated duplicate of `newFs` {filesystem_t*};
 *  @private
 */
static filesystem_t*
_fsHash_replace( filesystem_t* oldFs, filesystem_t* newFs ) {

#ifndef NO_INPLACE_REPLACE // {
  /* Replace the content of `oldFs` with that of `newFs` making sure we
   * properly maintain the list pointers.
   *
   * This avoids the cost of a  full allocation and rcu-based deletion.
   */
  rcu_read_lock();
    // Save the list pointers
    memcpy( &newFs->fs_chain, &oldFs->fs_chain, sizeof(newFs->fs_chain));

    // Copy in the new data
    memcpy( oldFs, newFs, sizeof(*newFs) );
  rcu_read_unlock();

  return oldFs;

#else   // NO_INPLACE_REPLACE }{

  // Create a clone, replace and delete the old entry
  filesystem_t* clone = _fsHash_clone( newFs );

  rcu_read_lock();

     hlist_replace_rcu( &oldFs->fs_chain, &clone->fs_chain );

     // Schedule resource cleanup for old
     call_rcu( &oldFs->fs_rcu, __fsHash_del_rcu );

  rcu_read_unlock();

  return clone;

#endif  // NO_INPLACE_REPLACE }

}

/**
 *  Remove the given entry from our hash table and free the storage.
 *  @method _fsHash_del
 *  @param  fs    The target entry {filesystem_t*};
 *
 *  @return void
 *  @private
 */
static void
_fsHash_del( filesystem_t* fs ) {
  hlist_del_rcu( &(fs->fs_chain) );

  call_rcu( &fs->fs_rcu, __fsHash_del_rcu );
}

/**
 *  Remove all entries from the hash table.
 *  @method _fsHash_clear
 *
 *  @return void
 *  @private
 */
static void
_fsHash_clear( void ) {
  unsigned int  hashEntries = 1U << s_fsHash_shift;
  unsigned int  idex;
  filesystem_t* fs;

  /*
  printk(KERN_INFO "%s _fsHash_clear(): clear %u hash buckets\n",
                   DEVICE_NAME, hashEntries);
  // */

  rcu_read_lock();

  for (idex = 0; idex < hashEntries; idex++) {

    hlist_for_each_entry_rcu( fs, &s_fsHash[idex], fs_chain ) {
      _fsHash_del( fs );
    }

    INIT_HLIST_HEAD( &s_fsHash[idex] );
  }

  rcu_read_unlock();
}

/**
 *  Walk the hash/cache and evict any entry with a `lastBeat` that doesn't
 *  match the current `lkb.beat` value, meaning it wasn't in the active
 *  filesystem list.
 *  @method _fsHash_sweep
 *
 *  @return void
 *  @private
 */
static void
_fsHash_sweep( void ) {
  unsigned int  hashEntries = 1U << s_fsHash_shift;
  unsigned int  idex;
  filesystem_t* fs;

  rcu_read_lock();

  for (idex = 0; idex < hashEntries; idex++) {

    hlist_for_each_entry_rcu( fs, &s_fsHash[idex], fs_chain ) {
      if (fs->lastBeat == lkb.beat) { continue; }

      // Evict this entry
      printk(KERN_INFO "%s _fsHash_sweep(): evict entry %u\n",
                       DEVICE_NAME, fs->id);

      _fsHash_del( fs );
    }
  }

  rcu_read_unlock();
}

/**
 *  Free the hash table along with all entries.
 *  @method _fsHash_fini
 *
 *  @return void
 *  @private
 */
static void
_fsHash_fini( void ) {
  unsigned int  hashEntries =  1U << s_fsHash_shift;

  /*
  printk(KERN_INFO "%s _fsHash_fini(): clear/free %u hash buckets\n",
                   DEVICE_NAME, hashEntries);
  // */

  // Empty the hash table
  _fsHash_clear();

  synchronize_rcu();

  s_bytesFreed += sizeof(*s_fsHash) * hashEntries;

  // Free the bucket memory
  kfree( s_fsHash );
}
#endif // USE_LKB_DIFF }
/****************************************************************/

/**
 *  Retrieve the root path.
 *  @method _getRootPath
 *  @param  root      The root path to fill {struct path*};
 *
 *  @note   The caller MUST invoke `path_put( root )` when finished.
 *
 *  @return void
 *  @private
 */
static void
_getRootPath( struct path* root ) {
  // protected via fs->lock, retrieves fs->root and invokes path_get()
  get_fs_root( current->fs, root );
}

/**
 *  Check if the given VFS mount appears valid.
 *  @method _isMountValid
 *  @param  mnt     The target mount {struct vfsmount*};
 *
 *  @return An indication of validity {bool};
 *  @private
 */
static bool
_isMountValid( struct vfsmount* mnt) {
  bool isValid = TRUE;
  if (mnt == NULL) {
    // end-of-list
    isValid = FALSE;

  } else if (! virt_addr_valid( mnt )) {
    /*
    printk(KERN_WARNING
              "%s _isMountValid(): mnt [ %p ] INVALID ...\n",
              DEVICE_NAME, mnt);
    // */
    isValid = FALSE;

  } else if ( mnt->mnt_root == NULL ) {
    // end-of-list
    isValid = FALSE;

  } else if (! virt_addr_valid( mnt->mnt_root ) ) {
    /*
    printk(KERN_WARNING
              "%s _isMountValid(): mnt->mnt_root[ %p ] : INVALID\n",
              DEVICE_NAME, mnt->mnt_root);
    // */
    isValid = FALSE;

  } else if (! virt_addr_valid( mnt->mnt_sb ) ) {
    /*
    printk(KERN_WARNING
              "%s _isMountValid(): mnt->mnt_sb[ %p ] : INVALID\n",
               DEVICE_NAME, mnt->mnt_sb);
    // */
    isValid = FALSE;

  } else if ( mnt->mnt_sb != mnt->mnt_root->d_sb )  {
    /*
    printk(KERN_WARNING
              "%s _isMountValid(): mnt->mnt_root->d_sb[ %p ] "
                "!== mnt->mnt_sb[ %p ] : INVALID\n",
                  DEVICE_NAME, mnt->mnt_root->d_sb, mnt->mnt_sb);
    // */
    isValid = FALSE;
  }

  return isValid;
}

/**
 *  Should the given filesystem be processed?
 *  @method _shouldProcess
 *  @param  mnt     The target mount {struct vfsmount*};
 *
 *  @return An indication of whether to process the filesystem {bool};
 *  @private
 */
static bool
_shouldProcess( struct vfsmount* mnt ) {
  /* https://www.elasic.co/guide/en/beats/metricbeat/6.5/metricbeat-metricset-system-fsstat.hml
   *
   * > Configuration:
   * > `filesytem.ignore_types` - A list of filesystem types to ignore. Metrics
   * > will not be collected from filesystems matching these types. This
   * > setting also affects the `filesystem` metricset. If this option is not
   * > set, metricbeat ignores all types for virtual devices in systems where
   * > this information is available (e.g. all types marked as `nodev` in
   * >                                     `/proc/filesystems` in Linux
   * >                                     systems).
   *
   * This information comes from the `fs_flags & FS_REQUIRES_DEV` in the
   * file_system_type (s_type) of the super block;
   *    mnt->mnt_sb->s_type->fs_flags
   */
  return (mnt->mnt_sb->s_type->fs_flags & FS_REQUIRES_DEV
              ? TRUE
              : FALSE);
}

/**
 *  Retrieve a string representation of the mountpoint.
 *  @method _getMountpointPath
 *  @param  path    The target path {struct path*};
 *  @param  buf     The buffer to fill {char*};
 *  @param  max     The size of `buf` {int};
 *
 *  @note   The caller should hold a reference to `mnt_path`
 *          (e.g. path_get(mnt_path));
 *
 *  @note   The returned pointer will most likely NOT be the start of `buf`;
 *
 *  @return The start of the string {char*};
 *  @private
 */
static char*
_getMountpointPath( struct path* mnt_path, char* buf, int max ) {
  char* pathRes;

  buf[0]  = '\0';
  pathRes = d_path( mnt_path, buf, max );
  if (pathRes == NULL || IS_ERR(pathRes)) {
    pathRes = buf;
  }
  return pathRes;
}

/**
 *  Given a VMS mount, retrieve the containing proc mount.
 *  @method _getProcMount
 *  @param  mnt     The target mount {struct vfsmount*};
 *
 *  @return A pointer to the contining proc mount {struct mount*};
 *  @private
 */
static struct mount*
_getProcMount( struct vfsmount* mnt ) {
  struct mount* procMount = container_of( mnt, struct mount, mnt );

  if (! virt_addr_valid( procMount )) {
    procMount = NULL;
  }

  return procMount;
}

/**
 *  Given the name of the backing device for the given mount.
 *  @method _getDeviceName
 *  @param  mnt     The target mount {struct vfsmount*};
 *
 *  @return A pointer to the contining proc mount {struct mount*};
 *  @private
 */
static char*
_getDeviceName( struct vfsmount* mnt ) {
  char* name  = NULL;

  if (! name) {
    // Attempt to access the 'struct mount' that contains `mnt`...
    struct mount*   procMount = _getProcMount( mnt );

    if (procMount && virt_addr_valid( procMount->mnt_devname) ) {
      name = (char*)procMount->mnt_devname;
    }
  }

  return (name ? name : "");
}

/**
 *  Include information about a single mount.
 *  @method _process_mount
 *  @param  mnt     The target mount {struct vfsmount*};
 *  @param  arg     A pointer to the collection repository {filesystem_t*};
 *
 *  @note   This is invoked via `iterate_mounts()` in `lkb_fs_json()`;
 *
 *  @return 0 for success, !0 for failure {int};
 *  @private
 */
static int
_process_mount( struct vfsmount* mnt, void* arg ) {
  iter_t*         iter                    = (iter_t*)arg;
  filesystem_t    _fs                     = { 0 };
  filesystem_t*   fs                      = &_fs;
  char            pathBuf[ FS_MAX_PATH ]  = { 0 };
  strinfo_t       devname                 = { NULL };
  strinfo_t       mountpoint              = { NULL };
  ssize_t         written                 = 0;
  int             err                     = 0;
  bool            needComma               = FALSE;
  u8              changes                 = 0;
#ifdef  USE_LKB_DIFF
  filesystem_t*   prevFs                  = NULL;
#endif  // USE_LKB_DIFF
  struct kstatfs  stat;
  struct path     mnt_path;
  int             idex;

  // Validate the incoming `mnt` pointer
  if (! _isMountValid( mnt )) {
    return -EINVAL;
  }

  if (! _shouldProcess( mnt )) {
    // Do not process but also don't stop the iteration
    return 0;
  }

  // Initialize `mnt_path`
  mnt_path.mnt    = mnt;
  mnt_path.dentry = mnt->mnt_root;

  // Grab a reference to `mnt_path` {
  // mntget( mnt_path.mnt ); dget( mnt_path.dentry );
  path_get( &mnt_path );

  /*
  printk(KERN_INFO "%s _process_mount(): id[ %8x : %8lx : %-10s ], "
                    "d_flags[ %x : dcache %s ]\n",
                   DEVICE_NAME,
                   fs_id( mnt ), mnt->mnt_sb->s_magic,
                   (mnt->mnt_sb->s_type ? mnt->mnt_sb->s_type->name : "--"),
                   mnt->mnt_root->d_flags,
                   ( (mnt->mnt_root->d_flags & DCACHE_ENTRY_TYPE)
                        == DCACHE_MISS_TYPE
                          ? "miss" : "hit"));
  // */

  /***************************************************
   * Gather filesystem information ...
   *
   */
  err = vfs_statfs( &mnt_path, &stat );
  if (err)  {
    goto done;
  }

  // Gather data about this specific filesystem
  fs->id         = fs_id( mnt );
  fs->magic      = mnt->mnt_sb->s_magic;
  fs->type       = (mnt->mnt_sb->s_type
                      ? (char*)mnt->mnt_sb->s_type->name
                      : NULL);

  fs->filesTotal = stat.f_files;
  fs->filesFree  = stat.f_ffree;

  fs->sizeTotal  = (stat.f_bsize * stat.f_blocks);
  fs->sizeFree   = (stat.f_bsize * stat.f_bfree);
  fs->sizeAvail  = (stat.f_bsize * stat.f_bavail);

  // Retrieve and hash the mount point.
  mountpoint.str  = _getMountpointPath( &mnt_path, pathBuf, sizeof(pathBuf) );
  mountpoint.len  = strnlen( mountpoint.str, sizeof(pathBuf) );
  mountpoint.hash = lkb_hash( (unsigned long)fs->id,
                              mountpoint.str, mountpoint.len );

  fs->hashMountpoint = mountpoint.hash;

  // Also retrieve and hash the name of the device used for the mount
  devname.str = _getDeviceName( mnt );
  if (devname.str) {
    devname.len  = strnlen( devname.str, FS_MAX_PATH );
    devname.hash = lkb_hash( (unsigned long)fs->id, devname.str, devname.len );

    fs->hashDevname = devname.hash;
  }

  /*
  printk(KERN_INFO "%s _process_mount(): id[ %8x : %8lx : %-10s ], "
                    "mountpoint[ %s ], devname[ %s ]\n",
                   DEVICE_NAME,
                   fs->id, mnt->mnt_sb->s_magic,
                   (fs->type       ? fs->type       : "--"),
                   (mountpoint.str ? mountpoint.str : "--"),
                   (devname.str    ? devname.str    : "--"));
  // */

  /***************************************************
   * Determine what to report ...
   *
   */
#ifdef  USE_LKB_DIFF  // {
  // See if we have a cached version of the target filesystem
  prevFs = _fsHash_find( mnt );

  if (prevFs) {
    // Identify any changes.
    void* cur   = (void*)fs;
    void* prev  = (void*)prevFs;

    for (idex = 0; idex < s_mapFsCnt; idex++) {
      map_t*  entry = &s_mapFs[idex];

      if (entry->offset == OFFSETOF(filesystem_t, hashMountpoint)) {

        if (mountpoint.len > 0 && mountpoint.hash != prevFs->hashMountpoint) {
          entry->changed = TRUE;
          changes++;

        }

      } else if (entry->offset == OFFSETOF(filesystem_t, hashDevname)) {

        if (devname.len > 0 && devname.hash != prevFs->hashDevname) {
          entry->changed = TRUE;
          changes++;

        }

      } else if (mapDiff( entry, cur, prev )) {
        entry->changed = TRUE;
        changes++;
      }
    }

    // Replace the current cache, ensuring a dynamically allocated `fs`.
    fs = _fsHash_replace( prevFs, fs );

  } else {
    /*
    printk(KERN_INFO "%s _process_mount( %u ): no previous state\n",
                     DEVICE_NAME, fs->id);
    // */

    // Add a new cache entry
    fs = _fsHash_add( fs );

    // Mark *all* members "changed"
    changes = s_mapFsCnt;
  }

#else   // USE_LKB_DIFF }{

  // Mark all members "changed"
  changes = s_mapFsCnt;

#endif  // USE_LKB_DIFF }

  /***************************************************
   * Generate a report for this filesystem
   *
   */

  // Update top-level summary data
  iter->fsstat.count++;
  iter->fsstat.filesTotal += fs->filesTotal;
  iter->fsstat.filesFree  += fs->filesFree;

  iter->fsstat.sizeTotal  += fs->sizeTotal;
  iter->fsstat.sizeFree   += fs->sizeFree;
  iter->fsstat.sizeAvail  += fs->sizeAvail;

  // Update the `lastBeat` value for this cache entry.
  fs->lastBeat = lkb.beat;

  if (changes < 1) {
    /* There were no changes at all for this filesystem so don't output
     * anything.
     *
     * assert( err == 0 );
     */
    goto done;
  }

  if (iter->needComma) { written += lkb_write( ",", 1 ); }

  written += lkb_write( "{", 1 );

  for (idex = 0; idex < s_mapFsCnt; idex++) {
    map_t*  entry = &s_mapFs[idex];

    if (changes < s_mapFsCnt && !entry->changed) { continue; }

    // This member has changed
    if (needComma)  { written += lkb_write( ",", 1 ); }

    if (entry->offset == OFFSETOF(filesystem_t, magic)) {
      // Output the filesystem type
      if (fs->type) {
        // Use the name (struct file_system_type*)
        int len = strnlen( fs->type, JSON_MAX_STR );

        written += json_write_strn( entry->name, (char*)fs->type, len );

      } else {
        // Write the magic number directly (u32) as the "type"
        written += json_write_map( entry, (void*)fs );
      }

    } else if (entry->offset == OFFSETOF(filesystem_t, hashMountpoint)) {
      // Output the mountpoint
      written += json_write_strn( entry->name, mountpoint.str, mountpoint.len );

    } else if (entry->offset == OFFSETOF(filesystem_t, hashDevname)) {
      // Output the device name
      written += json_write_strn( entry->name, devname.str, devname.len );

    } else {
      written  += json_write_map( entry, (void*)fs );
    }

    needComma = TRUE;
  }

  written += lkb_write( "}", 1 );

  iter->written   += written;
  iter->needComma  = TRUE;

done:
  // Release our reference to `mnt_path` }
  // dput( mnt_path.dentry ); mntput( mnt_path.mnt );
  path_put( &mnt_path );

  return err;
}

/* Private methods }
 *****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_fs_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_fs_init( void ) {
  lkb_resolve( s_syms, s_numSyms );

  // :XXX: Make the s_syms entries match the s_kern expectations!!
  s_kern.iterate_mounts   = s_syms[0].ptr;

#ifdef  USE_LKB_DIFF
  _fsHash_init();
#endif  // USE_LKB_DIFF

  printk(KERN_INFO "%s fs_init(): %lu bytes / filesystem\n",
                   DEVICE_NAME, sizeof(filesystem_t));
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_fs_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_fs_reset( void ) {
#ifdef  USE_LKB_DIFF
  _fsHash_clear();
#endif  // USE_LKB_DIFF

  s_haveStatCur = FALSE;
}

/**
 *  Finalize, cleaning up any allocated resources.
 *  @method lkb_fs_fini
 *
 *  @return void
 *  @protected
 */
static void
lkb_fs_fini( void ) {
#ifdef  USE_LKB_DIFF
  _fsHash_fini( );
#endif  // USE_LKB_DIFF

  if (s_bytesAlloc - s_bytesFreed != 0) {
    printk(KERN_WARNING "%s lkb_fs_fini()     : "
                        "%7llu bytes allocated, %7llu freed : %llu LEAKED\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed,
                                     s_bytesAlloc - s_bytesFreed);
  } else {
    printk(KERN_INFO    "%s lkb_fs_fini()     : "
                        "%7llu bytes allocated, %7llu freed\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed);
  }
}

/**
 *  Gather and write filesystem metrics as JSON to the circular buffer.
 *  @method lkb_fs_json
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_fs_json( void ) {
  ssize_t     written = 0;
  iter_t      iter;
  struct path root;
#ifdef  MEASURE_PERFORMANCE
  u64         start   = lkb_timestamp();
  u64         end;
#endif  // MEASURE_PERFORMANCE

  memset( &iter, 0, sizeof(iter) );

  // Write as JSON the FS measurements which have changed since the last beat.
  written += lkb_write( "[", 1 );

  _getRootPath( &root );  // {

    s_kern.iterate_mounts( _process_mount, &iter, root.mnt );

  path_put( &root );  // }

  written += iter.written;

  written += lkb_write( "]", 1 );

  // Save the collected summary information for lkb_fsstat_json()
  memcpy( &s_statCur, &iter.fsstat, sizeof(s_statCur) );
  s_haveStatCur = TRUE;

#ifdef  USE_LKB_DIFF
  // Evict any entry that was not seen in the mount list from our cache.
  _fsHash_sweep();
#endif  // USE_LKB_DIFF

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s fs_json()       : %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_fsstat_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_fsstat_init( void ) {
  printk(KERN_INFO "%s fsstat_init(): %lu bytes / beat\n",
                   DEVICE_NAME, sizeof(fsstat_t));
}

/**
 *  Gather and write filesystem metrics as JSON to the circular buffer.
 *  @method lkb_fs_json
 *
 *  @note   `lkb_fs_json()` MUST be invoked FIRST in order to gather the stats
 *          that will be reported in this summary.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_fsstat_json( void ) {
  ssize_t   written   = 0;
#ifdef  MEASURE_PERFORMANCE
  u64       start     = lkb_timestamp();
  u64       end;
#endif  // MEASURE_PERFORMANCE

  // Write as JSON the FS measurements which have changed since the last beat.
  written += lkb_write( "{", 1 );

  if (s_haveStatCur) {
    bool  needComma = FALSE;
    int   idex;

    for (idex = 0; idex < s_mapStatCnt; idex++) {
      map_t*  entry     = &s_mapStat[idex];
      void*   cur       = (void*)&s_statCur;
      bool    doOutput  = (
#ifdef  USE_LKB_DIFF
        !s_haveStatLast || mapDiff( entry, cur, (void*)&s_statLast )
#else  // USE_LKB_DIFF
        TRUE
#endif // USE_LKB_DIFF
      );

      if ( doOutput ) {
        if (needComma)  { written += lkb_write( ",", 1 ); }

        written  += json_write_map( entry, cur );
        needComma = TRUE;
      }
    }

#ifdef  USE_LKB_DIFF
    memcpy( &s_statLast, &s_statCur, sizeof(s_statLast) );
    s_haveStatLast = TRUE;
#endif // USE_LKB_DIFF
  }

  written += lkb_write( "}", 1 );

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s fsstat_json()   : %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_fsstat_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_fsstat_reset( void ) {
#ifdef  USE_LKB_DIFF
  s_haveStatLast = FALSE;
#endif  // USE_LKB_DIFF
}

/* Module methods }
 *****************************************************************************/

/**
 *  Expose this as TWO lkb modules.
 */
Module_t module_filesystem = {
  .name   = "filesystems",
  .init   = lkb_fs_init,
  .json   = lkb_fs_json,
  .reset  = lkb_fs_reset,
  .fini   = lkb_fs_fini,
};

Module_t module_fsstat = {
  .name   = "fsstats",
  .init   = lkb_fsstat_init,
  .json   = lkb_fsstat_json,
  .reset  = lkb_fsstat_reset,
};
