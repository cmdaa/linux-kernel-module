/**
 *  Module: networks
 *
 *  Beat-driven module that collects metrics for each available network
 *  interface.
 *
 *  This module:
 *    - presents metrics similar to those available via `/proc/net/dev`;
 *
 *    - makes use of a hash table to monitor the set of active network
 *      interfaces;
 *
 *==========================================================================
 *  include/linux/netdevice.h
 *    struct net_device
 *      char                    name[IFNAMSIZ];
 *      unsigned long           base_addr;  // device I/O address
 *      int                     irq;        // device IRQ number
 *      ...
 *      struct net_device_stats stats;
 *      ...
 *      // HW addr
 *      unsigned char           perm_addr[MAX_ADDR_LEN];
 *      unsigned char           addr_assign_type;
 *      unsigned char           addr_len;
 *      ...
 *      unsigned char*          dev_addr;
 */
#include "../lkb.h"

#include <linux/slab.h>           // kzalloc() / kfree()
#include <linux/sched.h>          // struct task_struct
#include <linux/nsproxy.h>        // struct nsproxy
#include <linux/net_namespace.h>  // struct net, get_net_ns_by_pid(), put_net()
#include <linux/netdevice.h>      // struct net_device, struct rtnl_link_stats64

#ifdef  USE_LKB_DIFF
# include <linux/list.h>          // struct hlist_node
# include <linux/hash.h>          // hash_long()
#endif  // USE_LKB_DIFF

#include "../map.h"
#include "../json.h"

#include "net.h"

// :NOTE: If additional fields are added, `s_mapNet` MUST also be changed.
typedef struct {
  u32   id;         // A unique id for this network device (ifindex)
  u32   lastBeat;   /* A simple counter/flag indicating when this entry was
                     * last visited in response to a matching, active
                     * filesystem.
                     */

  /*************************************************************************
   * Based upon the network-level stats structure
   *    include/uapi/linux/if_link.h : struct rtnl_link_stats64
   */

  //  Receive
  u64   rxBytes;          // Total bytes received;
  u64   rxPackets;        // Total packets received;
  u64   rxErrors;         // Packet receive errors (bad packets);
  u64   rxErrDropped;     /* Errors due to packet drop/miss
                           *  rx_dropped + rx_missed_errors;
                           */
  u64   rxErrFifo;        // Errors due to FIFO overruns;
  u64   rxErrFrame;       /* Errors due to framing issues
                           *  rx_frame_errors  +
                           *  rx_length_errors +
                           *  rx_over_errors   + // receiver ring buff overflow
                           *  rx_crc_errors    + // recved pkt with crc error
                           *  rx_frame_errors    // recv'd frame alignment error
                           */
  u64   rxCompressed;     // Compressed packets received (e.g. cslip);

  // Transmit
  u64   txBytes;          // Total bytes transmitted;
  u64   txPackets;        // Total packets transmitted;
  u64   txErrors;         // Packet transmit errors;
  u64   txErrDropped;     /* Errors due to packet drop
                           * (no space available in linux)
                           */
  u64   txErrFifo;        // Errors due to FIFO overruns;
  u64   txErrCarrier;     /* Errors due to carrier issues
                           *  tx_carrier_errors    +
                           *  tx_aborted_errors    +
                           *  tx_heartbeat_errors  +
                           *  tx_window_errors
                           */
  u64   txCompressed;     // Compressed packets transmitted (e.g. cslip);

  // Multicast / shared
  u64   multicast;        // Multicast packets received;
  u64   collisions;       // Packet collisions;


#ifdef  USE_LKB_DIFF
  // Hash table overflow chain
  struct hlist_node net_chain;
#endif  // USE_LKB_DIFF

  struct rcu_head   net_rcu;

} net_t;

/**
 *  The mapping from `net_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all reportable fields from `net_t` MUST be
 *        identified.
 */
static map_t  s_mapNet[]  = {
  {"id",            MAP_TYPE_U32, OFFSETOF(net_t, id)},

  // Receive
  {"rxBytes",       MAP_TYPE_U64, OFFSETOF(net_t, rxBytes)},
  {"rxPackets",     MAP_TYPE_U64, OFFSETOF(net_t, rxPackets)},
  {"rxErrors",      MAP_TYPE_U64, OFFSETOF(net_t, rxErrors)},
  {"rxErrDropped",  MAP_TYPE_U64, OFFSETOF(net_t, rxErrDropped)},
  {"rxErrFifo",     MAP_TYPE_U64, OFFSETOF(net_t, rxErrFifo)},
  {"rxErrFrame",    MAP_TYPE_U64, OFFSETOF(net_t, rxErrFrame)},
  {"rxCompressed",  MAP_TYPE_U64, OFFSETOF(net_t, rxCompressed)},

  // Transmit
  {"txBytes",       MAP_TYPE_U64, OFFSETOF(net_t, txBytes)},
  {"txPackets",     MAP_TYPE_U64, OFFSETOF(net_t, txPackets)},
  {"txErrors",      MAP_TYPE_U64, OFFSETOF(net_t, txErrors)},
  {"txErrDropped",  MAP_TYPE_U64, OFFSETOF(net_t, txErrDropped)},
  {"txErrFifo",     MAP_TYPE_U64, OFFSETOF(net_t, txErrFifo)},
  {"txErrCarrier",  MAP_TYPE_U64, OFFSETOF(net_t, txErrCarrier)},
  {"txCompressed",  MAP_TYPE_U64, OFFSETOF(net_t, txCompressed)},

  // Multicast / shared
  {"multicast",     MAP_TYPE_U64, OFFSETOF(net_t, multicast)},
  {"collisions",    MAP_TYPE_U64, OFFSETOF(net_t, collisions)},
};
static int    s_mapNetCnt = COUNT(s_mapNet);

// Track memory allocation
static u64  s_bytesAlloc = 0;
static u64  s_bytesFreed = 0;

/**
 *  Iteration state passed to _process_dev() from lkb_net_json().
 */
typedef struct {
  ssize_t   written;
  bool      needComma;
} iter_t;

/*****************************************************************************
 * Private methods {
 */

/****************************************************************
 * Network device hash table to store the set of devices that
 * were active last beat.
 *
 *
 */

/**
 *  Use the interface index as the unique it.
 *
 *  :NOTE: We MAY also be able to use the address of the device structure
 *         assuming it doesn't move while active:
 *            ((u32)((u64)(dev) & 0xffffffff))
 */
#define net_id(dev) ((u32)((dev)->ifindex))

#ifdef  USE_LKB_DIFF // {
/**
 *  Given a unique identifier (via net_id()), generate a hash.
 */
#define net_hashfn(id) hash_long((unsigned long)id, s_netHash_shift)

/**
 *  Our network hash table and shift/size information.
 */
static struct hlist_head* s_netHash       = NULL;
static unsigned int       s_netHash_shift = 3;  // default to 8 buckets

/**
 *  Initialize the network hash table.
 *  @method _netHash_init
 *
 *  @return void
 *  @private
 */
static void
_netHash_init( void ) {
  unsigned int  hashEntries = 1U << s_netHash_shift;
  u32           nBytes      = sizeof(*s_netHash) * hashEntries;
  unsigned int  idex;

  s_netHash = kzalloc( nBytes, GFP_KERNEL );

  s_bytesAlloc += nBytes;

  /*
  printk(KERN_INFO "%s _netHash_init(): initialize %u hash buckets, "
                        "%u bytes, s_netHash: %p\n",
                   DEVICE_NAME, hashEntries, nBytes, s_netHash);
  // */

  for (idex = 0; idex < hashEntries; idex++) {
    INIT_HLIST_HEAD( &s_netHash[idex] );
  }
}

/**
 *  Check if a network identified by the given VFS mount exists in our hash
 *  table.
 *  @method _netHash_find
 *  @param  dev   The network device {struct net_device*};
 *
 *  @return A pointer to the hashed network if it exists {net_t*};
 *  @private
 */
static net_t*
_netHash_find( struct net_device* dev ) {
  u32           id    = net_id( dev );
  unsigned long hash  = net_hashfn( id );
  net_t*        net;

  rcu_read_lock();
  hlist_for_each_entry_rcu( net, &s_netHash[ hash ], net_chain) {
    if (net->id == id) {
      rcu_read_unlock();
      return net;
    }
  }
  rcu_read_unlock();

  return NULL;
}

/**
 *  Create a dynamically allocated clone of the given entry.
 *  @method _netHash_clone
 *  @param  net       The entry to clone {net_t*};
 *
 *  @note   This is generally needed because the incoming `net` exists on the
 *          stack and now needs to exist in dynamic memory for storage in the
 *          hash table.
 *
 *  @return A dynamically allocated version of `net` {net_t*};
 *  @private
 */
static net_t*
_netHash_clone( net_t* net ) {
  u32    nBytes = sizeof(*net);
  net_t* newNet = (net_t*)kmalloc( nBytes, GFP_KERNEL );

  s_bytesAlloc += nBytes;

  /*
  printk(KERN_INFO "%s _netHash_clone(): allocate %u byte net_t: %p\n",
                   DEVICE_NAME, nBytes, newNet);
  // */

  memcpy( newNet, net, sizeof(*net) );

  return newNet;
}

/**
 *  Add the given entry to our hash table.
 *  @method _netHash_add
 *  @param  net   The entry to add {net_t*};
 *
 *  @return A dynamically allocated clone of `net` {net_t*};
 *  @private
 */
static net_t*
_netHash_add( net_t* net ) {
  net_t*              clone = _netHash_clone( net );
  unsigned long       hash  = net_hashfn( clone->id );
  struct hlist_node*  node  = &(clone->net_chain);

  rcu_read_lock();
    hlist_add_head_rcu( node, &s_netHash[ hash ] );
  rcu_read_unlock();

  return clone;
}

/**
 *  Invoked via `call_rcu()` to perform the final storage free.
 *  @method __netHash_del_rcu
 *  @param  rcu   The head of the RCU list {struct rcu_head*};
 *
 *  @return void
 *  @private
 */
static void
__netHash_del_rcu( struct rcu_head* rcu ) {
  net_t* net = container_of( rcu, net_t, net_rcu );

  /*
  printk(KERN_INFO "%s _netHash_del_rcu(): free net_t: %p\n",
                   DEVICE_NAME, net);
  // */

  s_bytesAlloc -= sizeof(*net);

  kfree( net );
}

/**
 *  Replace the contents of the old entry with that of the new.
 *  @method _netHash_replace
 *  @param  oldNet    The entry to replace {net_t*};
 *  @param  newNet    The entry to clone and add {net_t*};
 *
 *  @note   If NO_INPLACE_REPLACE is defined, this will incur the cost of
 *          a full allocation (_netHash_clone()) and rcu-based deletion.
 *
 *          Otherwise, it will replace the contents of `oldNet` with that of
 *          `newNet` maintaining the existing list pointers.
 *
 *  @return A dynamically allocated duplicate of `newNet` {net_t*};
 *  @private
 */
static net_t*
_netHash_replace( net_t* oldNet, net_t* newNet ) {

#ifndef NO_INPLACE_REPLACE // {
  /* Replace the content of `oldNet` with that of `newNet` making sure we
   * properly maintain the list pointers.
   *
   * This avoids the cost of a  full allocation and rcu-based deletion.
   */
  rcu_read_lock();
    // Save the list pointers
    memcpy( &newNet->net_chain, &oldNet->net_chain, sizeof(newNet->net_chain));

    // Copy in the new data
    memcpy( oldNet, newNet, sizeof(*newNet) );
  rcu_read_unlock();

  return oldNet;

#else   // NO_INPLACE_REPLACE }{

  // Create a clone, replace and delete the old entry
  net_t*  clone = _netHash_clone( newNet );

  rcu_read_lock();

     hlist_replace_rcu( &oldNet->net_chain, &clone->net_chain );

     // Schedule resource cleanup for old
     call_rcu( &oldNet->net_rcu, __netHash_del_rcu );

  rcu_read_unlock();

  return clone;

#endif  // NO_INPLACE_REPLACE }

}

/**
 *  Remove the given entry from our hash table and free the storage.
 *  @method _netHash_del
 *  @param  net   The target entry {net_t*};
 *
 *  @note   This should be invoked with `rcu_read_lock()`;
 *
 *  @return void
 *  @private
 */
static void
_netHash_del( net_t* net ) {
  hlist_del_rcu( &(net->net_chain) );

  call_rcu( &net->net_rcu, __netHash_del_rcu );
}

/**
 *  Remove all entries from the hash table.
 *  @method _netHash_clear
 *
 *  @return void
 *  @private
 */
static void
_netHash_clear( void ) {
  unsigned int  hashEntries = 1U << s_netHash_shift;
  unsigned int  idex;
  net_t*        net;

  /*
  printk(KERN_INFO "%s _netHash_clear(): clear %u hash buckets\n",
                   DEVICE_NAME, hashEntries);
  // */

  rcu_read_lock();

  for (idex = 0; idex < hashEntries; idex++) {

    hlist_for_each_entry_rcu( net, &s_netHash[idex], net_chain ) {
      _netHash_del( net );
    }

    INIT_HLIST_HEAD( &s_netHash[idex] );
  }

  rcu_read_unlock();
}

/**
 *  Walk the hash/cache and evict any entry with a `lastBeat` that doesn't
 *  match the current `lkb.beat` value, meaning it wasn't in the active
 *  network list.
 *  @method _netHash_sweep
 *
 *  @return void
 *  @private
 */
static void
_netHash_sweep( void ) {
  unsigned int  hashEntries = 1U << s_netHash_shift;
  unsigned int  idex;
  net_t*        net;

  rcu_read_lock();

  for (idex = 0; idex < hashEntries; idex++) {

    hlist_for_each_entry_rcu( net, &s_netHash[idex], net_chain ) {
      if (net->lastBeat == lkb.beat) { continue; }

      // Evict this entry
      printk(KERN_INFO "%s _netHash_sweep(): evict entry %u\n",
                       DEVICE_NAME, net->id);

      _netHash_del( net );
    }
  }

  rcu_read_unlock();
}

/**
 *  Free the hash table along with all entries.
 *  @method _netHash_fini
 *
 *  @return void
 *  @private
 */
static void
_netHash_fini( void ) {
  unsigned int  hashEntries =  1U << s_netHash_shift;

  /*
  printk(KERN_INFO "%s _netHash_fini(): clear/free %u hash buckets\n",
                   DEVICE_NAME, hashEntries);
  // */

  // Empty the hash table
  _netHash_clear();

  // Wait for __netHash_del_rcu() callbacks to finish freeing all entries
  synchronize_rcu();

  /*
  printk(KERN_INFO "%s _netHash_fini(): free s_netHash: %p\n",
                   DEVICE_NAME, s_netHash);
  // */

  s_bytesFreed += sizeof(*s_netHash) * hashEntries;

  // Free the bucket memory
  kfree( s_netHash );
}
#endif // USE_LKB_DIFF }
/****************************************************************/

/**
 *  Retrieve the network namespace.
 *  @method _getNetNs
 *
 *  @note   The caller MUST invoke `put_net( ret )` when finished.
 *
 *  @return The referenced network namespace {struct net*};
 *  @private
 */
static struct net*
_getNetNs( void ) {
  // net/core/net_namespace.c : get_net_ns_by_pid()
  struct net*     net = NULL;
  struct nsproxy* nsp;

  rcu_read_lock();
    nsp = current->nsproxy;
    if (nsp) {
      net = get_net( nsp->net_ns );
    }
  rcu_read_unlock();

  return net;
}

/**
 *  Include information about a single network device.
 *  @method _process_dev
 *  @param  dev     The target device {struct net_device*};
 *  @param  iter    The iteration state {iter_t*};
 *
 *  @note   This is invoked via `lkb_net_json()`;
 *
 *  net/core/net-procfs.c : dev_seq_printf_stats()
 *    %s   dev->name
 *    %llu stats->rx_bytes
 *    %llu stats->rx_packets
 *    %llu stats->rx_errors
 *    %llu stats->rx_dropped        + ->rx_missed_errors
 *    %llu stats->rx_fifo_errors
 *    %llu stats->rx_length_errors  + ->rx_over_errors
 *                                  + ->rx_crc_errors
 *                                  + ->rx_frame_errors
 *    %llu stats->rx_compressed
 *    %llu stats->rx_multicast
 *
 *    %llu stats->tx_bytes
 *    %llu stats->tx_packets
 *    %llu stats->tx_errors
 *    %llu stats->tx_dropped
 *    %llu stats->tx_fifo_errors
 *    %llu stats->tx_collisions
 *    %llu stats->tx_carrier_errors + ->tx_aborted_errors
 *                                  + ->tx_window_errors
 *                                  + ->tx_heartbeat_errors
 *    %llu stats->tx_compressed
 *
 *  --------------------------------------------------------------
 *  rx_errors         received bad packet
 *  rx_over_errors    receiver ring buffer overflow
 *  rx_crc_errors     received packet with crc error
 *  rx_frame_errors   received frame alignment error
 *  rx_fifo_errors    receiver fifo overrun
 *  rx_missed_errors  receiver missed packet
 *
 *  rx_dropped        no buffer space available
 *  rx_nohandler      dropped -- no handler found (not reported??)
 *
 *  tx_errors         packet transmission problems
 *  tx_dropped        no buffer space available
 *
 *  tx_compressed     cslip, etc
 *  rx_compressed     cslip, etc
 *  --------------------------------------------------------------
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @private
 */
static ssize_t
_process_dev( struct net_device* dev, iter_t* iter ) {
  ssize_t                   written   = 0;
  net_t                     _net      = { 0 };
  net_t*                    net       = &_net;
  u8                        changes   = 0;
#ifdef  USE_LKB_DIFF
  net_t*                    prevNet   = NULL;
#endif  // USE_LKB_DIFF
  struct rtnl_link_stats64  tmp;
  struct rtnl_link_stats64* stats;
  int                       idex;

  /***************************************************
   * Gather network information ...
   *
   */
  stats = dev_get_stats( dev, &tmp );

  /*
  printk(KERN_INFO "%s _processDev( %s s ): dev[ %p ], ifindex[ %d ], "
                        "perm_addr[ %d / %lu : %pM ]\n",
                   DEVICE_NAME, dev->name, dev, dev->ifindex,
                   dev->addr_len, sizeof(dev->perm_addr), dev->perm_addr);
  // */

  net->id            = net_id( dev );
  net->rxBytes       = stats->rx_bytes;
  net->rxPackets     = stats->rx_packets;
  net->rxErrors      = stats->rx_errors;
  net->rxErrDropped  = stats->rx_dropped +
                       stats->rx_missed_errors;
  net->rxErrFifo     = stats->rx_fifo_errors;
  net->rxErrFrame    = stats->rx_length_errors +
                       stats->rx_over_errors   +
                       stats->rx_crc_errors    +
                       stats->rx_frame_errors;
  net->rxCompressed  = stats->rx_compressed;

  net->txBytes       = stats->tx_bytes;
  net->txPackets     = stats->tx_packets;
  net->txErrors      = stats->tx_errors;
  net->txErrDropped  = stats->tx_dropped;
  net->txErrFifo     = stats->tx_fifo_errors;
  net->txErrCarrier  = stats->tx_carrier_errors +
                       stats->tx_aborted_errors +
                       stats->tx_window_errors  +
                       stats->tx_heartbeat_errors;
  net->txCompressed  = stats->tx_compressed;

  net->multicast     = stats->multicast;
  net->collisions    = stats->collisions;

  /***************************************************
   * Determine what to report ...
   *
   */
#ifdef  USE_LKB_DIFF  // {
  // See if we have a cached version of the target network
  prevNet = _netHash_find( dev );

  if (prevNet) {
    // Identify any changes.
    void* cur   = (void*)net;
    void* prev  = (void*)prevNet;

    for (idex = 0; idex < s_mapNetCnt; idex++) {
      map_t*  entry = &s_mapNet[idex];

      if (mapDiff( entry, cur, prev )) {
        entry->changed = TRUE;
        changes++;
      }
    }

    // Replace the current cache, ensuring a dynamically allocated `net`.
    net = _netHash_replace( prevNet, net );

  } else {
    /*
    printk(KERN_INFO "%s _process_mount( %u ): no previous state\n",
                     DEVICE_NAME, net->id);
    // */

    // Add a new cache entry
    net = _netHash_add( net );

    // Mark *all* members "changed"
    changes = s_mapNetCnt;
  }

#else   // USE_LKB_DIFF }{

  // Mark all members "changed"
  changes = s_mapNetCnt;

#endif  // USE_LKB_DIFF }

  /***************************************************
   * Generate a report for this network
   *
   */

  // Update the `lastBeat` value for this cache entry.
  net->lastBeat = lkb.beat;

  if (changes < 1) {
    /* There were no changes at all for this network so don't output
     * anything.
     *
     * assert( err == 0 );
     */
    goto done;
  }

  if (iter->needComma) { written += lkb_write( ",", 1 ); }

  written += lkb_write( "{", 1 );

  // struct net_device { char name[IFNAMSIZ]; ... };
  written += json_write_strn( "name", dev->name, sizeof(dev->name));

  for (idex = 0; idex < s_mapNetCnt; idex++) {
    map_t*  entry = &s_mapNet[idex];

    /* If this entry is NOT marked as `changed` AND is NOT `id`, skip it
     * (i.e. ALWAYS output the `id`).
     */
    if (changes < s_mapNetCnt &&
        !entry->changed &&
        entry->offset != OFFSETOF(net_t, id)) {
      continue;
    }

    // This member has changed
    written += lkb_write( ",", 1 );

    written  += json_write_map( entry, (void*)net );
  }

  written += lkb_write( "}", 1 );

  iter->written  += written;
  iter->needComma = TRUE;

done:
  return written;
}

/* Private methods }
 *****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_net_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_net_init( void ) {
#ifdef  USE_LKB_DIFF
  _netHash_init();
#endif  // USE_LKB_DIFF

  printk(KERN_INFO "%s net_init(): %lu bytes / network\n",
                   DEVICE_NAME, sizeof(net_t));
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_net_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_net_reset( void ) {
#ifdef  USE_LKB_DIFF
  _netHash_clear();
#endif  // USE_LKB_DIFF
}

/**
 *  Finalize, cleaning up any allocated resources.
 *  @method lkb_net_fini
 *
 *  @return void
 *  @protected
 */
static void
lkb_net_fini( void ) {
#ifdef  USE_LKB_DIFF
  _netHash_fini( );
#endif  // USE_LKB_DIFF

  if (s_bytesAlloc - s_bytesFreed != 0) {
    printk(KERN_WARNING "%s lkb_net_fini()    : "
                        "%7llu bytes allocated, %7llu freed : %llu LEAKED\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed,
                                     s_bytesAlloc - s_bytesFreed);
  } else {
    printk(KERN_INFO    "%s lkb_net_fini()    : "
                        "%7llu bytes allocated, %7llu freed\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed);
  }
}

/**
 *  Gather and write network metrics as JSON to the circular buffer.
 *  @method lkb_net_json
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_net_json( void ) {
  ssize_t             written = 0;
  iter_t              iter    = { 0 };
  struct net*         net;
  struct net_device*  dev;
#ifdef  MEASURE_PERFORMANCE
  u64                 start   = lkb_timestamp();
  u64                 end;
#endif  // MEASURE_PERFORMANCE

  /* Write as JSON the network measurements which have changed since the last
   * beat.
   */
  written += lkb_write( "[", 1 );

  net = _getNetNs();  // {

    for_each_netdev( net, dev ) {
      _process_dev( dev, &iter );
    }

  put_net( net );     // }

  written += iter.written;

  written += lkb_write( "]", 1 );

#ifdef  USE_LKB_DIFF
  // Evict any entry that was not seen in the current list from our cache.
  _netHash_sweep();
#endif  // USE_LKB_DIFF

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s net_json()      : %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/* Module methods }
 *****************************************************************************/

/**
 *  Expose this as an lkb module.
 */
Module_t module_net = {
  .name   = "networks",
  .init   = lkb_net_init,
  .json   = lkb_net_json,
  .reset  = lkb_net_reset,
  .fini   = lkb_net_fini,
};
