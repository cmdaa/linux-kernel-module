/**
 *  Module: cgroup_v1
 *
 *  Beat-driven module that collect metrics for v1 cgroups referenced via active
 *  processes, as indicated by the `proc` module during process traversal via
 *  calls to `cgroup_v1_touch_rcu()`;
 *
 *  This module:
 *    - attempts to normalize cgroup access across various versions of the Linux
 *      Kernel. In so doing, it makes use of non-public, though exported kernel
 *      symbols:
 *      - Linux >= v3.15.0
 *        - cgroup_roots    => s_kern.cgroup_roots
 *        - cgroup_subsys   => s_kern.cgroup_subsys
 *        - kernfs_name()   => s_kern.kernfs_name()
 *
 *      - Linux <  v3.15.0
 *        - roots           => s_kern.cgroup_roots
 *        - subsys          => s_kern.cgroup_subsys
 *
 *    - makes use of a hash table to monitor the set of active cgroup/subsystem
 *      pairs;
 *
 *    - employees subsystem-specific collectors to provide access to cgroup
 *      subsystem data:
 *        collector               /sys/fs equivalent/source
 *      - cgroup_module_blkio     /sys/fs/cgroup/blkio
 *      - cgroup_module_cpu       /sys/fs/cgroup/cpu
 *      - cgroup_module_cpuacct   /sys/fs/cgroup/cpuacct
 *      - cgroup_module_cpuset    /sys/fs/cgroup/cpuset
 *      - cgroup_module_freezer   /sys/fs/cgroup/freezer
 *      - cgroup_module_memory    /sys/fs/cgroup/memory
 *      - cgroup_module_netcls    /sys/fs/cgroup/net_cls
 *      - cgroup_module_netprio   /sys/fs/cgroup/net_prio
 *      - cgroup_module_pids      /sys/fs/cgroup/pids
 */
#ifndef _CGROUP_V1_H_
#define _CGROUP_V1_H_

#include <linux/sched.h>    // struct task_struct
#include <linux/cgroup.h>   // CGROUP_SUBSYS_COUNT
#include <linux/list.h>     // hlist_*

#include "../lkb.h"
#include "../map.h"         // For collectors

#include "proc.h"           // proc_t

// The maximum cgroup path we will collect
#define CGROUP_MAX_PATH 512

/*************************************************************
 * _subsys_id => _cgrp_id : v3.15
 */
# if     LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0) // {
#  define CGROUP_ROOT                 struct cgroup_root
#  define CGROUP_CFT_SET              struct cftype
#  define CGROUP_CFT_CONTEXT          struct cgroup_subsys_state

#  define CGROUP_SUBSYS_ID(_x)        ((_x) ? (_x)->id          : -1)
#  define CGROUP_SUBSYS_NAME(_x)      ((_x) ? (_x)->legacy_name : NULL)
#  define CGROUP_ROOT_CGROUP(_x)      ((_x) ? &(_x)->cgrp       : NULL)
#  define CGROUP_CSS_CFT_CONTEXT(_x)  (_x)

/**
 *  Given a subsystem, return the first entry in the set of cftypes
 *  @macro  CGROUP_SUBSYS_CFTSET
 *  @param  _ss     The target subsystem {struct cgroup_subsys*};
 *
 *  @return The first entry, useful in list_for_each_entry();
 */
#  define CGROUP_SUBSYS_CFTSET(_ss)   (_ss)->cfts

/**
 *  Given an entry from iterating over the set of cftypes, return the current
 *  cftype.
 *  @macro  CGROUP_SUBSYS_CFTSET_ITEM
 *  @param  _entry  The current iterator entry;
 *
 *  @return The associated cftype item {struct cftype*};
 */
#  define CGROUP_SUBSYS_CFTSET_ITEM(_entry) (_entry)

/*************************************************************/
# else   // LINUX_VERSION_CODE < 3.15.0 }{
#  define CGROUP_ROOT                 struct cgroupfs_root
#  define CGROUP_CFT_SET              struct cftype_set
#  define CGROUP_CFT_CONTEXT          struct cgroup

#  define CGROUP_SUBSYS_ID(_x)        ((_x) ? (_x)->subsys_id   : -1)
#  define CGROUP_SUBSYS_NAME(_x)      ((_x) ? (_x)->name        : NULL)
#  define CGROUP_ROOT_CGROUP(_x)      ((_x) ? &(_x)->top_cgroup : NULL)
#  define CGROUP_CSS_CFT_CONTEXT(_x)  ((_x) ? (_x)->cgroup      : NULL)

/**
 *  Given a subsystem, return the first entry in the set of cftypes
 *  @macro  CGROUP_SUBSYS_CFTSET
 *  @param  _ss     The target subsystem {struct cgroup_subsys*};
 *
 *  @return The first entry, useful in list_for_each_entry();
 */
#  define CGROUP_SUBSYS_CFTSET(_ss)   (_ss)->cftsets

/**
 *  Given an entry from iterating over the set of cftypes, return the current
 *  cftype.
 *  @macro  CGROUP_SUBSYS_CFTSET_ITEM
 *  @param  _entry  The current iterator entry;
 *
 *  @return The associated cftype item {struct cftype*};
 */
#  define CGROUP_SUBSYS_CFTSET_ITEM(_entry) (_entry)->cfts

# endif  // LINUX_VERSION_CODE  }

/**
 *  Given a pointer to a cgroup and a subsystem id, generate a unique id for
 *  the cgroup/subsystem.
 *  @macro  CGROUP_SSID_TO_UUID
 *  @param  _cg   The target cgroup {struct cgroup*};
 *  @param  _ssid The id of the target subsystem {u32};
 *
 *  @return The unique id of the cgroup/subsystem {u32};
 */
#define CGROUP_SSID_TO_UUID(_cg, _ssid) ((_cg)->id * 100 + (_ssid))

/**
 *  Given a subsystem and cftype, generate a full variable name that includes
 *  the subsystem name.
 *  @macro  CGROUP_GEN_VARIABLE_NAME
 *  @param  _ss     The target subsystem {struct cgroup_subsys*};
 *  @param  _cft    The target cftype {struct cftype*};
 *  @param  _buf    The destination buffer {char*};
 *  @param  _max    The maximum size of `_buf` {int};
 *
 *  The generated string will have the form:
 *    '%subsystem-name%.%variable-name%'
 *
 *  @return void
 */
#define CGROUP_GEN_VARIABLE_NAME( _ss, _cft, _buf, _max ) { \
  const char* _ssName  = CGROUP_SUBSYS_NAME(_ss); \
  int         _nameLen = 0; \
  if (_ssName != NULL) { \
    strncpy( _buf, _ssName, _max ); \
    _nameLen = strnlen( _buf, _max ); \
    (_buf)[_nameLen++] = '.'; \
  } \
  strncpy( (_buf) + _nameLen, (_cft)->name, (_max) - _nameLen ); \
}
//  snprintf( _buf, "%s.%s", CGROUP_SUBSYS_NAME(_ss), (_cft)->name )

/**
 *  Given a cgroup and subsystem id, return the target subsystem state.
 *  @macro  CGROUP_SUBSYS_STATE
 *  @param  _cg   The target cgroup {struct cgroup*};
 *  @param  _ss   The target subsystem {struct cgroup_subsys*};
 *
 *  @return The matching subsystem state {struct cgroup_subsys_state*};
 */
# define CGROUP_SUBSYS_STATE(_cg,_ss) ((_cg) && (_ss) \
                                      ? (_cg)->subsys[ CGROUP_SUBSYS_ID(_ss) ] \
                                      : NULL)

/**
 *  Iterate over the cftypes of the given subsystem invoking the given callback
 *  for each cftype entry.
 *  @macro  CGROUP_SUBSYS_CFTYPE_EACH
 *  @param  _pid    The id of the target process/task {s32};
 *  @param  _path   The cgroup path/name {char*};
 *  @param  _ss     The target subsystem {struct cgroup_subsys*};
 *  @param  _css    The subsystem state for the target cgroup
 *                  {struct cgroup_subsys_state*};
 *  @param  _cb     The callback to invoke {function};
 *                    void _cb( _pid, _path, _ss, struct cftype* );
 */
# define CGROUP_SUBSYS_CFTYPE_EACH( _pid, _path, _ss, _css, _cb )  { \
    CGROUP_CFT_SET* cfts; \
    list_for_each_entry( cfts, &CGROUP_SUBSYS_CFTSET(_ss), node ) { \
      struct cftype* cft  = CGROUP_SUBSYS_CFTSET_ITEM(cfts);  \
      while (cft && cft->name[0] != '\0') {  \
        _cb( _pid, _path, _ss, _css, cft ); \
        cft++; \
      }  \
    } \
}

/*************************************************************/

struct cftype_handler;
struct cgroup_diff;

/**
 *  cgroup cftype handler.
 *
 *  Upon initialization, the collector will identify the set of available
 *  cftypes for the target subsystem. This will determine the full number of
 *  entries (u64-values + u8-change-indicators) required for diff state.
 */
typedef struct cftype_handler {
  /**
   *  The cftype name handled by this extractor
   *  @prop name {char*};
   *
   *  @note This MUST match the value of 'name' from the target cftype.
   */
  char*   name;

  /**
   *  The number of entries (value/change) that will be generated from
   *  this cftype.
   *  @prop nEntries {u32};
   */
  u32     nEntries;

  /**
   *  The extractor for this cftype.
   *  @method extract
   *  @param  self      The cftype map entry containing this method
   *                    {cftype_handler_t*};
   *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
   *  @param  start     The starting index in the store state {u32};
   *  @param  doDiff    If true, prior data is available so perform a diff
   *                    before writing {bool};
   *
   *  A cftype extractor handles a single cftype and will fill `self->nEntries`
   *  values/change-indicators in `cgd`.
   *
   *  If this method is not provided, `cgroup_v1_cftype_extract()` will be used.
   *
   *  @return The number of detected changes {u32};
   */
  u32     (*extract)  ( struct cftype_handler*  self,
                        struct cgroup_diff*     cgd,
                        u32                     start,
                        bool                    doDiff );

  /**
   *  Write metrics as JSON to the circular buffer.
   *  @method json
   *  @param  self      The cftype map entry containing this method
   *                    {cftype_handler_t*};
   *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
   *  @param  start     The starting index in the store state {u32};
   *
   *  A JSON output handler will output `self->nEntries` from the values
   *  area as identified by the parallel change-indicators within `cgd`.
   *
   *  If this method is not provided, `cgroup_v1_cftype_json()` will be used.
   *
   *  @return The number of bytes written to the circular buffer {ssize_t};
   */
  ssize_t (*json) ( struct cftype_handler*  self,
                    struct cgroup_diff*     cgd,
                    u32                     start );

  /**
   *  The specific cftype, identified upon initialization.
   *  If this is null, this handler is considered in-active.
   *  @prop cft {struct cftype*};
   */
  struct cftype*  cft;

} cftype_handler_t;

/**
 *  cgroup/subsystem-specific cftype value and change store.
 *
 *  The allocated size of this data will be:
 *      sizeof( cgroup_store_t ) +
 *        nEntries * (sizeof(u64) + sizeof(bool))
 */
typedef struct {
  u32     nEntries;   // The number of u64 values and bool change indicators
  u32     nChanges;   // The count of changes indicated by the change indicators

  /* Immediately following will be two contiguous arrays that each contain
   * `nEntries` elements:
   *    - values  : an array of u64 values;
   *    - changes : an array of bool change indicators;
   */
   u64*   values;   // Typically points just beyond `cgroup_store_t`;
   bool*  changes;  // Typically points just beyond the end of `values`;
} cgroup_store_t;

/**
 *  Given a pointer to a store, return the pointer to the associated `values`
 *  array.
 *  @macro  GET_STORE_VALUES
 *  @param  _s      The target store {cgroup_store_t* | sub-class};
 *
 *  @return A pointer to the associated `values` array {u64*};
 */
#define GET_STORE_VALUES(_s)  (u64*)((void*)(_s) + sizeof(*(_s)))

/**
 *  Given a pointer to a store, return the pointer to the associated `changes`
 *  array.
 *  @macro  GET_STORE_CHANGES
 *  @param  _s      The target store {cgroup_store_t* | sub-class};
 *
 *  @return A pointer to the associated `values` array {u64*};
 */
#define GET_STORE_CHANGES(_s) (bool*)(GET_STORE_VALUES(_s) + (_s)->nEntries)

/**
 *  cgroup/subsystem-specific state and storage.
 *
 *  These will be generated by the cgroup collection modules according to
 *  the set of available cftypes / active cftype handlers.
 *
 *  :NOTE:  Since these entries are allocated via `cgroup_v1_touch()`, which
 *          has no knowledge of the set of cftypes the collection module
 *          supports, the `store` WILL be allocated when the collection module
 *          performs its first extraction.
 */
typedef struct cgroup_diff {
  u32   id;       /* The unique id of this entry:
                   *      CGROUP_SSID_TO_UUID( cgroup, subsys->id );
                   */
  u32   lastBeat; // The last beat that caused an update * to this entry;
  u32   pathLen;  // The allocated size of `path`;
  char* path;     /* The "absolute" path to the cgroup/subsystem
                   *      %subsys,names%/[%cgroup-hierarchy-path%/]
                   */

  /* cgroup/subsystem/state cache
   *  :NOTE: These values can be re-generated if we have the ids but since
   *         they are kernel system entities which, once allocated do not move,
   *         lookups would add unecessary complexity.
   */
  struct cgroup*              cgroup; // cgroup->id
  struct cgroup_subsys*       subsys; // subsys->id
  struct cgroup_subsys_state* css;    // css->id  : css_from_id()

  // Hash bucket chain
  struct hlist_node           hash_chain;
  struct rcu_head             hash_rcu;

  // Subsystem-specific cftype handlers and value/change store.
  u32                         nHandlers;
  cftype_handler_t*           handlers;
  cgroup_store_t*             store;

} cgroup_diff_t;

/**
 *  cgroup mapping/diff information for a specific set of cgroup/subsystems.
 */
typedef struct {
  u32             lastBeat;

  // cgroup data for the target by subsystem
  cgroup_diff_t   subsys[ CGROUP_SUBSYS_COUNT ];

} cgroup_v1_t;

/**
 *  A handle to a cgroup collection module.
 */
typedef struct {
  /**
   *  The module name / json key
   */
  char    name[ LKB_MODULE_MAX_NAME ];

  /**
   *  Initialize this collection module.
   *  @method init
   *  @param  subsys  A pointer to the cgroup subsystem {struct cgroup_subsys*};
   *
   *  @return void
   */
  void (*init)  ( struct cgroup_subsys* subsys );

  /**
   *  Gather subsystem metrics identifying any differences from any prior data.
   *  @method gather
   *  @param  entry   The cgroup data entry for the target cgroup/subsystem
   *                  {cgroup_diff_t*};
   *
   *  @return void
   */
  void (*gather) ( cgroup_diff_t* entry );

  /**
   *  Given a gathered cgroup data, write the JSON version of differences.
   *  @method json
   *  @param  entry     The cgroup data entry {cgroup_diff_t*};
   *
   *  @note   If this is not provided, `entry->map` will be iterated.
   *          If an individual entry has a json method, that will be invoked
   *          for output, otherwise, the `map_t` array of the entry will be
   *          iterated and output using json_map_diff();
   *
   *  @return The number of bytes written {int};
   */
  ssize_t (*json) ( cgroup_diff_t*  entry );

  /**
   *  Release any allocated data for the given entry.
   *  @method release
   *  @param  entry   The cgroup data entry for the target cgroup/subsystem
   *                  {cgroup_diff_t*};
   *
   *  @return void
   */
  void (*release)( cgroup_diff_t* entry );

  /**
   *  Reset any state upon periodic timer stop.
   *  @method reset
   *
   *  @return void
   */
  void (*reset) ( void );

  /**
   *  Finalize, cleaning up any allocated resources.
   *  @method fini
   *
   *  @return void
   */
  void (*fini)  ( void );

} cgroup_module_t;

/**
 *  Retrieve a reference to the cgroup namespace.
 *  @method cgroup_v1_getNs
 *  @param  task    The target task {struct task_struct*};
 *
 *  @note   The caller MUST have the task locked `task_lock(task)`;
 *
 *  @note   The caller MUST invoke `cgroup_v1_putNs( ret )` when finished;
 *
 *  @note   For Linux v4.5.0+, the returned namespace is actually
 *          {struct cgroup_namespace*};
 *
 *  @return The referenced cgroup namespace {void*};
 */
extern void* cgroup_v1_getNs( struct task_struct* task );

/**
 *  Release a reference to the cgroup namespace.
 *  @method cgroup_v1_putNs
 *  @param  ns      The target cgroup namespace retrieved via cgroup_v1_getNs()
 *                  {void*};
 *
 *  @note   For Linux v4.5.0+, `ns` is actually {struct cgroup_namespace*};
 *
 *  @return void
 *  @private
 */
extern void cgroup_v1_putNs( void* ns );

/**
 *  Create a standard cgroup store for the given number of entries.
 *  @method cgroup_v1_makeStore
 *  @param  nEntries  The number of entries to include {u32};
 *
 *  @return A pointer to the new store (NULL on error) {cgroup_store_t*};
 */
extern cgroup_store_t* cgroup_v1_makeStore(  u32 nEntries );

/**
 *  Given a cftype map set and cftype name, find the map entry with a matching
 *  name.
 *  @method cgroup_v1_findHandler
 *  @param  handlers  The array of handlers {cftype_handler_t*};
 *  @param  nHandlers The number of entries in `handlers` {u32};
 *  @param  name      The name of the target cftype {char*};
 *
 *  @return The target extractor, or NULL if not found {cftype_handler_t*};
 */
extern cftype_handler_t* cgroup_v1_findHandler( cftype_handler_t* handlers,
                                                u32               nHandlers,
                                                char*             name );

/**
 *  A cftype helper for simple extraction.
 *  @method cgroup_v1_cftype_extract
 *  @param  self      The cftype map entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  A cftype extractor handles a single cftype and will fill `self->nEntries`
 *  values/change-indicators in `cgd`.
 *
 *  @return The number of detected changes {u32};
 */
extern u32 cgroup_v1_cftype_extract(  cftype_handler_t* self,
                                      cgroup_diff_t*    cgd,
                                      u32               start,
                                      bool              doDiff );

/**
 *  A cftype helper to write metrics as JSON to the circular buffer.
 *  @method cgroup_v1_cftype_json
 *  @param  self      The cftype map entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *
 *  A JSON output handler will output `self->nEntries` from the values
 *  area as identified by the parallel change-indicators within `cgd`.
 *
 *  If this method is not provided, `cgroup_v1_cftype_json()` will be used.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 */
extern ssize_t cgroup_v1_cftype_json( cftype_handler_t* self,
                                      cgroup_diff_t*    cgd,
                                      u32               start );

/**
 *  A cftype helper to read from the '/sys/fs/cgroup' filesystem-based entry for
 *  the identified cgroup/subsystem/cftype.
 *  @method cgroup_v1_fsRead
 *  @param  cft       The target cftype {struct cftype*};
 *  @param  cgd       The cgroup data {cgroup_diff_t*};
 *  @param  buf       The buffer to populate {char*};
 *  @param  bufLen    The size (in bytes) of `buf` {u32};
 *
 *  @note   This should only be used for non-trivial cgroup data that is not
 *          directly accessible otherwise. cftype entries that use `read_u64()`
 *          and `read_s64()` should be handled using
 *          `cgroup_v1_cftype_extract()``;
 *
 *          Reading a file from the kernel is generally frowned upon but it's
 *          not clear what a better alternative is given the state required
 *          to make use of a `seq_file`
 *          (i.e. private data that points to the structures we don't have
 *                access to in the first place).
 *
 *          On the other hand, this isn't *actually* reading a file but rather
 *          invoking another kernel method to copy data into the provided
 *          buffer.
 *
 *  @return The number of bytes written to `buf` {ssize_t};
 */
extern ssize_t cgroup_v1_fsRead( struct cftype* cft,
                                 cgroup_diff_t* cgd,
                                 char*          buf,
                                 u32            bufLen );

/**
 *  Tag the cgroup related to the given cgroup state for update.
 *  @method cgroup_v1_touch_rcu
 *  @param  ns      The cgroup namespace retrieved via cgroup_v1_getNs()
 *                  {void*};
 *  @param  ssid    The id of the target subsystem {int};
 *  @param  css     The target cgroup subsystem state
 *                  {struct cgroup_subsys_state*};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return The id of the target cgroup/subsystem {u32};
 */
extern u32 cgroup_v1_touch_rcu( void*                        ns,
                                int                          ssid,
                                struct cgroup_subsys_state*  css );

/**
 *  Tag the cgroups related to the given task for update.
 *  @method cgroups_v1_touch_rcu
 *  @param  task    The target task/process {struct task_struct*}
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @note   This method will obtain a lock on the task to protect the
 *          contained cgroup information while in use.
 *
 *  @return void
 */
extern void cgroups_v1_touch_rcu( struct task_struct* task );

/**
 *  Retrieve cgroup information for the cgroups identified by the provided
 *  process data, and immediately report differences.
 *  @method cgroup_v1_report
 *  @param  proc    Information about the target task/process, collected
 *                  via the proc sub-module {proc_t*};
 *
 *  @note   This method will output a leading ',' before any diff output.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 */
extern ssize_t cgroup_v1_report( proc_t* proc );

/**
 *  Release  cgroup data entry.
 *  @method cgroup_v1_releaseEntry
 *  @param  data    A pointer to the data to release {cgroup_diff_t*};
 *
 *  @return void
 */
extern void cgroup_v1_releaseEntry( cgroup_diff_t* data );

/**
 *  Release any gathered cgroup information.
 *  @method cgroup_v1_release
 *  @param  data    A pointer to the data to release {cgroup_v1_t*};
 *
 *  @return void
 */
extern void cgroup_v1_release( cgroup_v1_t* data );

/**
 *  A module definition for the cgroup_v1 module.
 */
extern Module_t module_cgroup_v1;

#endif  // _CGROUP_V1_H_
