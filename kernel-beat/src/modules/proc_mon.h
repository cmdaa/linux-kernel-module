/**
 *  Module: procMon
 *
 *  Monitor process terminations, both normal and abnormal.
 *
 *  This module:
 *    - makes use of ftrace to hook:
 *        force_sig_info()  - invoked by the kernel whenever a process is being
 *                            sent an unblockable signal
 *                            (e.g. SIGFPE, SIGSEGV, SIGBUS, ...);
 *        sys_exit_group()  - invoked by a process to terminate a process group;
 *        sys_exit()        - invoked by a process to terminate;
 *
 *        sys_execve()      - iff MONITOR_PROCESS_CREATION is defined, monitor
 *                            creation of new processes via `execve()`;
 *        sys_clone()       - iff MONITOR_PROCESS_CREATION is defined, monitor
 *                            creation of new processes via `fork()`;
 *
 *    - makes use of a work queue to move primary work out of
 *      interrupt/trap/syscall contexts to reduce the impact on the primary
 *      paths;
 *            +-----------------------------------+
 *            | bottom-half : _pm_report          |  kernel      context
 *            +-----------------------------------+
 *            | top-half    : _pm_die_handler     |  interrupt   context
 *            |             : _pm_force_sig_info  |  signal/trap context
 *            |             : _pm_exit            |  syscall     context
 *            |             : _pm_exit_group      |  syscall     context
 *            +-----------------------------------+
 *
 *    - iff USE_DIE_NOTIFIER is defined, makes use of kernel-level die
 *      notifications to monitor unexpected process termination. This is the
 *      system used by `kdebug` for notifications of kernel faults.
 *
 *      Unfortunately, for user-level processes, this NOT triggered for
 *      exceptions like SIGSEGV, although it is triggered for SOME exceptions
 *      (e.g. SIGFPE / divide-by-zero);
 *
 *      User-level exceptions are better handled via the `force_sig_info()`
 *      ftrace hook.
 */
#ifndef _PROC_MON_H_
#define _PROC_MON_H_

/**
 *  A module definition.
 */
extern Module_t module_procMon;

#endif  // _PROC_MON_H_
