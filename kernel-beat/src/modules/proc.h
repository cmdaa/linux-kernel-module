/**
 *  Module: processes, process_summary
 *
 *  Beat-driven modules that collect process metrics:
 *    - processes       collects the set of active processes with metrics for
 *                      each;
 *    - process_summary collects overall process metrics;
 *
 *  This module:
 *    - makes use of non-public, though exported, kernel symbols:
 *      - get_task_cred()
 *      - get_files_struct()
 *      - put_files_struct()
 *      - access_remote_vm()
 *
 *    - makes use of a hash table to monitor the set of active processes;
 */
#ifndef _PROC_H_
#define _PROC_H_

#include <linux/sched.h>    // TASK_COMM_LEN, struct task_struct
#include <linux/cgroup.h>   // CGROUP_SUBSYS_COUNT

#ifdef  USE_LKB_DIFF
# include <linux/list.h>    // hlist_*
#endif  // USE_LKB_DIFF

/**
 *  Output all process properties with no change detection.
 *
 *  If provided to proc_json() as the 'changes' parameter, ALL items will be *
 *  output with no change detection performed.
 */
#define PROC_OUTPUT_ALL  ((u32)-1)

/**
 *  The maximum command line we can extract without allocated dynamic memory *
 *  (if this is exceeded and `noTrunc` is not TRUE, the last 3 bytes will be
 *   '...').
 */
#define MAX_CMDLINE_LEN 256


/**
 *  Information and metrics about a single process.
 *  @struct proc_t
 *
 *  :NOTE: If additional fields are added, `s_mapProc` in proc.c MUST also be
 *         changed.
 *
 *  :NOTE: This structure is exported for use by the cgroup_v1 and proc_mon
 *         modules.
 */
typedef struct {
  char  name[ TASK_COMM_LEN ];  /* Process name
                                 * (linux/include/sched.h#L1689)
                                 */
  s32   state;                  /* Process state:
                                 *  R (running)       0     TASK_RUNNING
                                 *  S (sleeping)      1     TASK_INTERRUPTIBLE
                                 *  D (disk sleep)    2     TASK_UNINTERRUPTIBLE
                                 *  T (stopped)       4     __TASK_STOPPED
                                 *  t (tracing stop)  8     __TASK_TRACED
                                 *  X (dead)          16    EXIT_DEAD
                                 *  Z (zombie)        32    EXIT_ZOMBIE
                                 *
                                 * The rest are kernel-based and NOT reported by /proc
                                 *  x (dead)          64    TASK_DEAD
                                 *  K (wake kill)     128   TASK_WAKEKILL
                                 *  W (waking)        256   TASK_WAKING
                                 *  P (parked)        512   TASK_PARKED
                                 *  N (noload)        1024  TASK_NOLOAD
                                 *  n (new)           2048  TASK_NEW
                                 */
  s32   pid;                    // Process id;
  s32   ppid;                   // Parent process id;
  s32   pgrp;                   // Parent group id;

  u32   uid;                    // Owning user id;

#ifdef  CONFIG_CGROUPS  // {
                                /* The set of cgroups to which this process
                                 * is assigned
                                 * (a cgroup id for each cgroup subsystem);
                                 *
                                 * :NOTE: Details about the cgroup are recorded
                                 *        via the `cgroup_v1` module in a
                                 *        separate * record indexed by cgroup
                                 *        id;
                                 */
  u32   cgroups[ CGROUP_SUBSYS_COUNT ];
#endif  // CONFIG_CGROUPS  }

  u64   cpuStart;               // CPU start time (seconds since epoch);
  u32   cpuUser;                // CPU time spent in user space;
  u32   cpuSys;                 // CPU time spent in kernel space;
  //    cpuTotal  = cpuUser + cpuSys;

  u32   fdOpen;                 // Number of currently opened files;
  u32   fdMax;                  // Maximum number of opened files;
  u32   fdLimitHard;            // Hard limit of open files;
  u32   fdLimitSoft;            // Soft limit of open files;

  u64   memRss;                 /* Memory allocated and in RAM (bytes);
                                 *  (R)esident (S)et (S)ize
                                 */
  u64   memShare;               // Shared memory (bytes);
  u64   memSize;                /* Virtual memory size (bytes): includes all
                                 * memory accessible by this process including
                                 * allocated and in-use, allocated but not
                                 * in-use, swapped out, and from shared
                                 * libraries;
                                 */

  u32   memFaultsMinor;         /* The number of minor memory faults
                                 * (no disk access);
                                 */
  u32   memFaultsMajor;         /* The number of major memory faults requiring
                                 * disk access;
                                 */
  //    memFaults = memFaultsMinor + memFaultsMajor;

  /* For detecting changes to strings like cmdline, possibly use
   *  full_name_hash( salt, str, len )  => u32
   *    defined:
   *      include/linux/stringhash.h
   *      fs/namei.c
   *
   *    examples:
   *      fs/namei.c
   *      fs/dcache.c
   *      fs/proc/proc_sysctl.c
   *      net/core/dev.c
   */
  u32   hashCmdline;

  /* A simple counter/flag indicating when this entry was last visited in
   * response to a matching, active process.
   */
  u32   lastBeat;

  /*  name    : "go"
   *  state   : "sleeping"
   *  pid     :
   *  ppid    :
   *  pgid    :
   *  username: "root"
   *  cmdline : "go test -tags=integration github.com/elastic/beats/..."
   *
   *  cpu     : { start_time: , total: { norm: { pct: }, pct: } }
   *  fd      : { limit: { hard:, soft: }, open: }
   *  memory  : { rss: { bytes:, pct: }, share:, size: }
   *
   *  cgroup  : {
   *    path  :
   *    blkio : { id:, path:, total: { bytes:, ios: } }
   *    cpu   : {
   *      cfs: { period:{us:}, quota:{us:}, shares: }
   *      id:, path:,
   *      rt: { period:{us:}, runtime:{us:} }
   *      stats: { periods:, throttled: {ns:, periods: } }
   *    }
   *    cpuacct: {
   *      id:, path:, percpu:{},
   *      stats:{ system:{ns:}, user:{ns:} }
   *      total:{ns:}
   *    }
   *    id:
   *    memory: {
   *      id:, path:
   *      kmem:{failures:, limitm:{bytes:}, usage:{bytes:, max:{bytes:}}}
   *      kmem_tcp:{failures:, limitm:{bytes:}, usage:{bytes:, max:{bytes:}}}
   *      mem:{failures:, limitm:{bytes:}, usage:{bytes:, max:{bytes:}}}
   *      memsw:{failures:, limitm:{bytes:}, usage:{bytes:, max:{bytes:}}}
   *      stats:{
   *        active_anon:{bytes:}, active_file:{bytes:}
   *        cache:{bytes:}, hierarchical_memory_limit:{bytes:}
   *        hierarchical_memsw_limit:{bytes:}
   *        inactive_anon:{bytes:}, inactive_file:{bytes:}
   *        mapped_file:{bytes:}, rss:{bytes:}, rss_huge:{bytes:}
   *        swap:{bytes:}, unevictable:{bytes:}
   *        major_page_faults:
   *        page_faults:
   *        pages_in:
   *        pages_out:
   *      }
   *    }
   *  }
   */

#ifdef  USE_LKB_DIFF
  struct hlist_node proc_chain;
#endif  // USE_LKB_DIFF

  struct rcu_head   proc_rcu;

} proc_t;

/**
 *  A string/length for use with command line retrieval.
 *  @struct cmdline_t
 *
 *  :NOTE: This structure is exported for use by the cgroup_v1 and proc_mon
 *         modules.
 */
typedef struct {
  char    _static[ MAX_CMDLINE_LEN ];
  char*   str;      /* Initialized to a pointer to `_static` via
                     * proc_getCmdline();
                     */
  bool    noTrunc;  /* If TRUE, do NOT truncate the command line, allocating
                     * space if needed, changing `str` so it no longer points
                     * to `_static`;
                     */
  bool    noHash;   // If TRUE, do NOT compute the hash;
  size_t  len;      // The length of the gathered command line (strlen(str));
  size_t  total;    // The total length (if < `len`, truncated);
  u32     hash;     // The hash of the gathered command line (if !noHash);
} cmdline_t;

/**
 *  Generate the command line of a process from its set of arguments.
 *  @method proc_getCmdline
 *  @param  task  A pointer to the target task {struct task_struct*};
 *  @param  proc  A pointer to the normalized process data {proc_t*};
 *  @param  cmd   A pointer to receive command line data {cmdline_t*};
 *
 *  @return void
 */
extern void proc_getCmdline( struct task_struct*  task,
                             proc_t*              proc,
                             cmdline_t*           cmd );

/**
 *  Release/free any dynamically allocated data within the given command line
 *  generated via `proc_getCmdline()`;
 *  @method proc_releaseCmdline
 *  @param  cmdline The command line to release {cmdline_t*};
 *
 *  @return void
 */
extern void proc_releaseCmdline( cmdline_t* cmdline );

/**
 *  Output a JSON version of changed information for a single process.
 *  @method proc_json
 *  @param  proc      A pointer to the normalized process data {proc_t*};
 *  @param  cmdline   The collected command line of the process {cmdline_t*};
 *  @param  changes   The number of items changed {u32};
 *
 *  @note   If `changes` is provided, is NOT PROC_OUTPUT_ALL and is less than
 *          `s_mapProcCnt`, then change indicators in `s_mapProc` will be used;
 *
 *  @note   This method neither opens nor closes the JSON structure, it
 *          simply writes key/value pairs assuming there is no need for
 *          a starting comma.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 */
extern ssize_t proc_json( proc_t* proc, cmdline_t* cmdline, u32 changes );

/**
 *  Gather system process summary metrics.
 *  @method proc_gather
 *  @param  task    The target task/process {struct task_struct*}
 *  @param  data    A pointer to the data to fill {proc_t*};
 *
 *  @return void
 */
extern void proc_gather( struct task_struct* task, proc_t* data );

/**
 *  A module definition for the per-process module.
 */
extern Module_t module_proc;

/**
 *  A module definition for the summary module.
 */
extern Module_t module_procSum;

#endif  // _PROC_H_
