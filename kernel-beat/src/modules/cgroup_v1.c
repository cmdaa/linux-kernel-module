/**
 *  Module: cgroup_v1
 *
 *  Beat-driven module that collect metrics for v1 cgroups referenced via active
 *  processes, as indicated by the `proc` module during process traversal via
 *  calls to `cgroup_v1_touch_rcu()`;
 *
 *  This module:
 *    - attempts to normalize cgroup access across various versions of the Linux
 *      Kernel. In so doing, it makes use of non-public, though exported kernel
 *      symbols:
 *      - Linux >= v3.15.0
 *        - cgroup_roots    => s_kern.cgroup_roots
 *        - cgroup_subsys   => s_kern.cgroup_subsys
 *        - kernfs_name()   => s_kern.kernfs_name()
 *
 *      - Linux <  v3.15.0
 *        - roots           => s_kern.cgroup_roots
 *        - subsys          => s_kern.cgroup_subsys
 *
 *    - makes use of a hash table to monitor the set of active cgroup/subsystem
 *      pairs;
 *
 *    - employees subsystem-specific collectors to provide access to cgroup
 *      subsystem data:
 *        collector               /sys/fs equivalent/source
 *      - cgroup_module_blkio     /sys/fs/cgroup/blkio
 *      - cgroup_module_cpu       /sys/fs/cgroup/cpu
 *      - cgroup_module_cpuacct   /sys/fs/cgroup/cpuacct
 *      - cgroup_module_cpuset    /sys/fs/cgroup/cpuset
 *      - cgroup_module_freezer   /sys/fs/cgroup/freezer
 *      - cgroup_module_memory    /sys/fs/cgroup/memory
 *      - cgroup_module_netcls    /sys/fs/cgroup/net_cls
 *      - cgroup_module_netprio   /sys/fs/cgroup/net_prio
 *      - cgroup_module_pids      /sys/fs/cgroup/pids
 */
#include "../lkb.h"

#include <linux/slab.h>       // kzalloc() / kfree()
#include <linux/sched.h>      // struct task_struct
#include <linux/cgroup.h>     // struct cgroup
#include <linux/list.h>       // list_for_each_entry()
#include <linux/hash.h>       // hash_long()

#include <linux/fs.h>         // struct file
#include <linux/seq_file.h>   // struct seq_file

#if     LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0)
# include <linux/kernfs.h>    // kernfs_name()
#endif // LINUX_VERSION_CODE

#if     LINUX_VERSION_CODE < KERNEL_VERSION(4,13,0)
/*    int kernel_read(struct file *file,
 *                      loff_t offset, char *addr, unsigned long count)
 */
# define  DO_KERNEL_READ(file,offset,addr,count) \
            kernel_read( (file), (offset), (addr), (count) )

#else
/*
 * Linux 4.13.0-rc7 : c41fbad015dabb0a40ecca50c3ff5658eb6471ff : 2017.09.04
 *    fs: move kernel_read to fs/read_write.c
 *
 * Linux 4.13.0-rc7 : bdd1d2d3d251c65b74ac4493e08db18971c09240 : 2017.09.04
 *  kernel_read() API changed:
 *    fs: fix kernel_read prototype
 *
 *    Use proper ssize_t and size_t types for the return value and count
 *    argument, move the offset last and make it an in/out argument like all
 *    other read/write helpers, and make the buf argument a void pointer to get
 *    rid of lots of casts in the callers.
 *
 *    int kernel_read(struct file *file,
 *                      loff_t offset, char *addr, unsigned long count)
 *      V
 *    ssize_t kernel_read(struct file *file,
 *                        void *buf, size_t count, loff_t *pos)
 */
# define  DO_KERNEL_READ(file,offset,addr,count) \
            kernel_read( (file), (void*)(addr), (size_t)(count), \
                         (loff_t*)(&(offset)) )
#endif

#include "../map.h"
#include "../json.h"

#include "cgroup_v1.h"

#ifdef  CONFIG_CGROUPS  // {

// cgroup modules {
# include "cgroup_v1/blkio.h"
# include "cgroup_v1/cpu.h"
# include "cgroup_v1/cpuacct.h"
# include "cgroup_v1/cpuset.h"
# include "cgroup_v1/freezer.h"
# include "cgroup_v1/memory.h"
# include "cgroup_v1/net_cls.h"
# include "cgroup_v1/net_prio.h"
# include "cgroup_v1/pids.h"

/**
 *  Is this module active?
 */
static bool s_isActive  = FALSE;

// Track memory allocation
static u64  s_bytesAlloc = 0;
static u64  s_bytesFreed = 0;

/**
 *  The full set of available cgroup collection modules.
 *
 *  This is used by `_cgroup_v1_initModules()` to initialize `s_cgroup_modules`;
 */
static cgroup_module_t* s_cgroup_modules_available[]  = {
  &cgroup_module_blkio,   // dynamic memory
  &cgroup_module_cpu,
  &cgroup_module_cpuacct, // dynamic memory
  &cgroup_module_cpuset,
  &cgroup_module_freezer,
  &cgroup_module_memory,
  &cgroup_module_netcls,
  &cgroup_module_netprio, // dynamic memory
  &cgroup_module_pids,
};
static int              s_availableCnt  = COUNT(s_cgroup_modules_available);
// cgroup modules }

/**
 *  The set of cgroup collection modules, from `s_cgroup_modules_available`
 *  currently in use for this system, indexed by subsystem id.
 *
 *  This is initialized via `cgroup_v1_init()` in `_cgroup_v1_initModules()`.
 */
static cgroup_module_t* s_cgroup_modules[ CGROUP_SUBSYS_COUNT ] = {0};

/**
 *  The set of non-exported kernel functions and variables we use
 *
 *  These are resolved and associated with the related s_kern entry in
 *  `cgroup_v1_init()`
 */
static Ksyms_t s_syms[]    = {
# if LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0)
  { "cgroup_roots" },
  { "cgroup_subsys" },
  { "kernfs_name" },

# else  // LINUX_VERSION_CODE
  { "roots" },
  { "subsys" },

# endif // LINUX_VERSION_CODE
};
static int      s_numSyms   = COUNT(s_syms);

/**
 *  Resolved pointer/function accessor.
 *
 *  These are associated with the resolved s_syms entry in
 *  cgroup_v1_init().
 */
static struct {
  /**
   *  The set of top-level cgroup controllers/subsystems.
   */
  struct list_head*      cgroup_roots;
  struct cgroup_subsys** cgroup_subsys;  // [CGROUP_SUBSYS_COUNT]
  //struct mutex*          cgroup_mutex;

# if     LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0)
  // include/linux/kernfs.h
  int                   (*kernfs_name)  (struct kernfs_node *kn,
                                         char *buf, size_t buflen);

# endif // LINUX_VERSION_CODE
} s_kern;


/**
 *  Iterate over all cgroup hierarchies.
 *  @macro  FOR_EACH_ROOT
 *  @param  _root   The iteration cursor -- current root {CGROUP_ROOT*};
 *
 *  @note   The type of `_root` is based upon the kernel version:
 *            v3.10 : struct cgroupfs_root* root;
 *            v4.9  : struct cgroup_root* root;
 *
 *  Based on:
 *      kernel/cgroup.c : for_each_root (v4.9);
 */
#define FOR_EACH_ROOT(_root) \
  list_for_each_entry(_root, s_kern.cgroup_roots, root_list)

/**
 *  Iterate over all enabled subsystems.
 *  @macro  FOR_EACH_SUBSYS
 *  @param  _ss     The iteration cursor -- current subsystem
 *                  {struct cgroup_subsys*};
 *  @param  _ssid   The index of `_ss` {int};
 *
 *  Based on:
 *      kernel/cgroup.c : for_each_subsys (v4.9);
 */
#define FOR_EACH_SUBSYS(_ss, _ssid) \
  for ((_ssid) = 0; (_ssid) < CGROUP_SUBSYS_COUNT &&  \
       (((_ss) = s_kern.cgroup_subsys[_ssid]) || true); (_ssid)++)

/**
 *  Retrieve the identified subsystem.
 *  @macro  GET_SUBSYS
 *  @param  _ssid   The subsystem id {int};
 *
 *  Based on:
 *      kernel/cgroup.c : for_each_subsys (v4.9);
 */
#define GET_SUBSYS(_ssid) \
  ((_ssid) < CGROUP_SUBSYS_COUNT ? s_kern.cgroup_subsys[_ssid] : NULL)

/*****************************************************************************
 * Private cgroup helpers {
 *
 */

/**
 *  Given a cgroup root, generate its name from all subsystems that are part of
 *  this hierarchy as well as any explicit root name.
 *  @method _cgroupRootName
 *  @param  root    The target cgroup root {CGROUP_ROOT*};
 *  @param  buf     The buffer to receive the name {char*};
 *  @param  len     The size of `buf` {int};
 *
 *  @return The number of bytes written to `buf` {int};
 *  @private
 */
static int
_cgroupRootName( CGROUP_ROOT* root, char* buf, int len ) {
  char* name    = buf;
  int   nameMax = len;
  int   namePos = 0;
  int   ncpy;

  if (root->subsys_mask) {
    // Assemble this root name from the set of associated subsystems
    struct cgroup_subsys* subsys  = NULL;
    int                   ssid;

    FOR_EACH_SUBSYS(subsys, ssid) {
      if (root->subsys_mask & (1 << ssid)) {
        ncpy = snprintf(name + namePos, nameMax - namePos,
                        "%s%s",
                        (namePos > 0 ? "," : ""),
                        CGROUP_SUBSYS_NAME( subsys ));
        namePos = (ncpy < nameMax - namePos ? namePos + ncpy : nameMax);
      }
    }
  }

  // If there is an explicit root name, include it
  if (root->name && root->name[0] != 0) {
    ncpy    = snprintf(name + namePos, nameMax - namePos,
                       "%sname=%s",
                       (namePos > 0 ? "," : ""),
                       root->name);
    namePos = (ncpy < nameMax - namePos ? namePos + ncpy : nameMax);
  }
  name[namePos] = '\0';

  return namePos;
}

/**
 *  Given a cgroup, generate its "absolute" path.
 *  @method _cgroupPath
 *  @param  cgroup  The target cgroup {struct cgroup*};
 *  @param  buf     The buffer to receive the name {char*};
 *  @param  len     The size of `buf` {int};
 *  @param  ns      The value from `cgroup_v1_getNs {void*}
 *
 *  For our purposes, the "absolute" path has the form:
 *      %subsys,names%/[%cgroup-hierarchy-path%/]
 *
 *  @return The number of bytes written to `buf` {int};
 *  @private
 */
static int
_cgroupPath( struct cgroup* cgroup, char* buf, int len, void* ns ) {
  char*         path      = buf;
  int           pathMax   = len;
  int           pathPos   = 0;
  int           ncpy;
  CGROUP_ROOT*  root;

  path[0] = '\0';

  if (cgroup == NULL) { return pathPos; }

  root = cgroup->root;

  // Add the cgroup root name
  ncpy    = _cgroupRootName( root, path + pathPos, pathMax - pathPos );
  pathPos = (ncpy < pathMax - pathPos ? pathPos + ncpy : pathMax);

  // Include the cgroup hierarchy path
# if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,5,0)
  //task_cgroup_path( task, cname, sizeof(cname) );
  cgroup_path_ns( cgroup, path + pathPos, pathMax - pathPos,
                  (struct cgroup_namespace*)ns );

# else   // LINUX_VERSION_CODE < 4.5.0
  cgroup_path( cgroup, path + pathPos, pathMax - pathPos );

# endif  // LINUX_VERSION_CODE

  pathPos = strnlen( path, pathMax );

  /*
  printk(KERN_INFO
          "%s _cgroupPath(): %d bytes[ %s ] ...\n",
          DEVICE_NAME, pathPos, path);
  // */

  // Ensure the path ends with '/' and is NULL terminated
  if (path[pathPos-1] != '/')  { path[pathPos++] = '/'; }
  path[pathPos] = '\0';

  /* If there may be additional non-ASCII ending characters,
   * we can strip them with something like the following:
   *
  if (pathPos > 0) {
    char* cur = &path[pathPos];

    // Look for the last ASCII character in the range:
    //  [._0-9a-z]
    while (cur > path && *cur != '.' && *cur != '_' &&
                         !(*cur >= '0' && *cur <= '9') &&
                         !(*cur >= 'a' && *cur <= 'z') ) {
      cur--;
    }

    if (cur > path) {
      // Ensure the path is terminated with '/' and a NULL character.
      (*++cur) = '/';
      (*++cur) = '\0';

      pathPos = cur - path;

      printk(KERN_WARNING
              "%s _cgroupPath(): %d bytes[ %s ]: FINAL\n",
              DEVICE_NAME, pathPos, path);
    }
  }
  // */

  return pathPos;
}

/* Private cgroup helpers }
 *****************************************************************************/

/*****************************************************************************
 * Private helpers {
 *
 */

/****************************************************************
 * cgroup hash table to store the normalized cgroup/subsystem
 * data that was active last beat.
 *  {
 */

/**
 *  Given a pointer to a cgroup and subsystem, generate the hash key for
 *  the cgroup/subsystem.
 *  @macro  CGROUP_DIFF_ID
 *  @param  _cg   The target cgroup {struct cgroup*};
 *  @param  _ss   The target subsystem {struct cgroup_subsys*};
 *
 *  @return The hash key {u32};
 */
#define CGROUP_DIFF_ID(_cg, _ss) CGROUP_SSID_TO_UUID(_cg, CGROUP_SUBSYS_ID(_ss))

/**
 *  Generate a hash from a process id (pid)
 */
#define cgroup_hashfn(hash) hash_long((unsigned long)hash, s_cgroupHash_shift)

/**
 *  Our pid hash table and shift/size information.
 */
static struct hlist_head* s_cgroupHash;
static unsigned int       s_cgroupHash_shift = 10; // default to 1024 buckets

/**
 *  Initialize the cgroup hash table.
 *  @method _cgroupHash_init
 *
 *  @return void
 *  @private
 */
static void
_cgroupHash_init( void ) {
  unsigned int  hashEntries = 1U << s_cgroupHash_shift;
  u32           nBytes      = sizeof(*s_cgroupHash) * hashEntries;
  unsigned int  idex;

  s_cgroupHash = kzalloc( nBytes, GFP_KERNEL );

  s_bytesAlloc += nBytes;

  /*
  printk(KERN_INFO "%s _cgroupHash_init(): initialize %u hash buckets, "
                        "%u bytes [ %llu / %llu ]\n",
                   DEVICE_NAME, hashEntries, nBytes,
                   s_bytesAlloc, s_bytesFreed);
  // */

  for (idex = 0; idex < hashEntries; idex++) {
    INIT_HLIST_HEAD( &s_cgroupHash[idex] );
  }
}

/**
 *  Check if a cgroup with the given `hash` exists in our hash table.
 *  @method _cgroupHash_find
 *  @param  key   The generated key of the target entry {u32};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return A pointer to the hashed entry if it exists {cgroup_diff_t*};
 *  @private
 */
static cgroup_diff_t*
_cgroupHash_find( u32 key ) {
  unsigned long   hash  = cgroup_hashfn( key );
  cgroup_diff_t*  res   = NULL;
  cgroup_diff_t*  entry = NULL;

  hlist_for_each_entry_rcu( entry, &s_cgroupHash[ hash ], hash_chain) {
    if (entry && entry->id == key) {
      res = entry;
      break;
    }
  }

  return res;
}

/**
 *  Add the given entry to our hash table.
 *  @method _cgroupHash_add
 *  @param  entry   The entry to add {cgroup_diff_t*};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return The `entry` {cgroup_diff_t*};
 *  @private
 */
static cgroup_diff_t*
_cgroupHash_add( cgroup_diff_t* entry ) {
  unsigned long       hash  = cgroup_hashfn( entry->id );
  struct hlist_node*  node  = &(entry->hash_chain);

  hlist_add_head_rcu( node, &s_cgroupHash[ hash ] );

  return entry;
}

/**
 *  Release  cgroup data entry.
 *  @method _cgroup_v1_releaseEntry_rcu
 *  @param  entry                 A pointer to the target entry
 *                                {cgroup_diff_t*};
 *  @param  [leaveEntry = FALSE]  If TRUTHy, do NOT free `entry` {bool};
 *
 *  @note   rcu_read_lock() should be held by a caller/ancestor.
 *
 *  @return void
 *  @private
 */
static void
_cgroup_v1_releaseEntry_rcu( cgroup_diff_t* entry, bool leaveEntry ) {
  u32               ssid      = CGROUP_SUBSYS_ID( entry->subsys );
  cgroup_module_t*  cgmodule  = s_cgroup_modules[ ssid ];

  if (entry->path != NULL)  {
    u32 nBytes  = entry->pathLen + 1;

    kfree( entry->path );
    entry->path = NULL;

    s_bytesFreed += nBytes;
  }

  if (cgmodule && cgmodule->release) {
    // Allow the collection module to release any allocated data
    cgmodule->release( entry );

    entry->store = NULL;

  } else {
    // Release any allocated data ourselves
    if (entry->store != NULL) {
      cgroup_store_t* store = entry->store;
      u32             nBytes  = sizeof( *store ) +
                                  (store->nEntries *
                                    (sizeof(u64) + sizeof(bool)));

      kfree( store );
      entry->store = NULL;

      s_bytesFreed += nBytes;
    }

  }

  if (entry->css) {
    // Release our reference to the cgroup_subsys_state
    css_put( entry->css );

    entry->css    = NULL;
    entry->subsys = NULL;
    entry->cgroup = NULL;
  }

  if (! leaveEntry) {
    s_bytesFreed += sizeof( *entry );

    kfree( entry );
  }

  /*
  printk(KERN_INFO "%s _cgroup_v1_releaseEntry_rcu(): "
                    "free entry %p [ %llu / %llu ]\n",
                   DEVICE_NAME, entry, s_bytesAlloc, s_bytesFreed);
  // */
}

/**
 *  Invoked via `call_rcu()` to perform the final storage free.
 *  @method __cgroupHash_del_rcu
 *  @param  rcu   The head of the RCU list {struct rcu_head*};
 *
 *  @note   rcu_read_lock() should be held by a caller/ancestor.
 *
 *  @return void
 *  @private
 */
static void
__cgroupHash_del_rcu( struct rcu_head* rcu ) {
  cgroup_diff_t*  entry       = container_of( rcu, cgroup_diff_t, hash_rcu );
  bool            leaveEntry  = FALSE;

  /*
  printk(KERN_INFO "%s _cgroupHash_del_rcu(): free cgroup_diff_t: %p\n",
                   DEVICE_NAME, entry);
  // */

  // Release all memory allocated for `entry` (including `entry` itself).
  _cgroup_v1_releaseEntry_rcu( entry, leaveEntry );
}

/**
 *  Remove the given entry from our hash table and free the storage.
 *  @method _cgroupHash_del
 *  @param  entry   The target entry {cgroup_diff_t*};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return void
 *  @private
 */
static void
_cgroupHash_del( cgroup_diff_t* entry ) {
  //hash_del( &entry->hash_chain );
  hlist_del_rcu( &(entry->hash_chain) );

  call_rcu( &entry->hash_rcu, __cgroupHash_del_rcu );
}

/**
 *  Remove all entries from the hash table.
 *  @method _cgroupHash_clear
 *
 *  @note   THIS method will obtain and release rcu_read_lock().
 *
 *  @return void
 *  @private
 */
static void
_cgroupHash_clear( void ) {
  unsigned int    hashEntries = 1U << s_cgroupHash_shift;
  unsigned int    idex;
  cgroup_diff_t*  entry;

  /*
  printk(KERN_INFO "%s _cgroupHash_clear(): clear %u hash buckets\n",
                   DEVICE_NAME, hashEntries);
  // */

  rcu_read_lock();

  for (idex = 0; idex < hashEntries; idex++) {

    hlist_for_each_entry_rcu( entry, &s_cgroupHash[idex], hash_chain ) {
      _cgroupHash_del( entry );
    }

    INIT_HLIST_HEAD( &s_cgroupHash[idex] );
  }

  rcu_read_unlock();
}

/**
 *  Free the hash table along with all entries.
 *  @method _cgroupHash_fini
 *
 *  @return void
 *  @private
 */
static void
_cgroupHash_fini( void ) {
  unsigned int  hashEntries = 1U << s_cgroupHash_shift;
  u32           nBytes      = sizeof(*s_cgroupHash) * hashEntries;

  // Empty the hash table
  _cgroupHash_clear();

  // Wait for __cgroupHash_del_rcu() callbacks to finish freeing all entries
  synchronize_rcu();

  s_bytesFreed += nBytes;

  /*
  printk(KERN_INFO "%s _cgroupHash_fini(): free %u hash buckets, "
                        "%u bytes [ %llu / %llu ]\n",
                   DEVICE_NAME, hashEntries, nBytes,
                   s_bytesAlloc, s_bytesFreed);
  // */

  // Free the bucket memory
  kfree( s_cgroupHash );
}
/*  cgroup hash table }
 ****************************************************************/

/**
 *  Initialize the set of cgroup collection modules.
 *  @method _cgroup_v1_initModules
 *
 *  @note   This populates `s_cgroup_modules` from `s_cgroup_modules_available`;
 *
 *  @return void
 *  @private
 */
static void
_cgroup_v1_initModules( void ) {
  struct cgroup_subsys* subsys  = NULL;
  int                   ssid;

  FOR_EACH_SUBSYS(subsys, ssid) {
    /* Determine which cgroup collection module is associated with this
     * subsystem and add it to `s_cgroup_modules` indexed by subsystem id
     * (ssid);
     */
    const char*       name      = CGROUP_SUBSYS_NAME( subsys );
    cgroup_module_t*  cgmodule  = NULL;
    int               idex;

    // Locate the collection module for this subsystem
    for (idex = 0; idex < s_availableCnt; idex++) {
      cgroup_module_t*  entry = s_cgroup_modules_available[ idex ];

      if (! strncmp( name, entry->name, sizeof(entry->name) )) {
        cgmodule = entry;
        break;
      }
    }

    if (cgmodule) {
      // There is a collection module for this subsystem
      s_cgroup_modules[ ssid ] = cgmodule;

      if (cgmodule && cgmodule->init) {
        // Initialize the collection module
        cgmodule->init( subsys );
      }

      /*
      printk(KERN_INFO  "%s CGROUP Subsystem ssid[ %2d / %2d ]: %s :"
                                " attached %s collection module\n",
                        DEVICE_NAME, ssid,
                        CGROUP_SUBSYS_ID(subsys),
                        name, cgmodule->name);
      // */

    } else {
      printk(KERN_WARNING
                "%s CGROUP Subsystem ssid[ %2d / %2d ]: %s :"
                      " NO collection module\n",
                DEVICE_NAME, ssid,
                CGROUP_SUBSYS_ID(subsys),
                name);
    }
  }
}

/**
 *  Fetch or create a cgroup data entry.
 *  @method _cgroup_v1_getEntry
 *  @param  cgroup    The target cgroup {struct cgroup*};
 *  @param  subsys    The target subsystem {struct cgroup_subsys*};
 *  @param  ns        Any namespace required to generate a path {void*};
 *  @param  [css]     The target cgroup subsystem state
 *                    {struct cgroup_subsys_state*};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return The retrieved or newly created entry {cgroup_diff_t*};
 *  @private
 */
static cgroup_diff_t*
_cgroup_v1_getEntry( struct cgroup*               cgroup,
                     struct cgroup_subsys*        subsys,
                     void*                        ns,
                     struct cgroup_subsys_state*  css ) {
  u32             id        = CGROUP_DIFF_ID(cgroup, subsys);
  cgroup_diff_t*  entry     = NULL;

  entry = _cgroupHash_find( id );
  if (! entry) {
    // Create a new entry starting with the "absolute" path
    char  path[ CGROUP_MAX_PATH ] = {0};

    // assert(css == NULL || css == CGROUP_SUBSYS_STATE( cgroup, subsys->id ));
    if (css == NULL) {
      css = CGROUP_SUBSYS_STATE( cgroup, subsys );
    }
    // /*
    else if (css != CGROUP_SUBSYS_STATE( cgroup, subsys ) ) {
      printk(KERN_ALERT "%s cgroup_v1_getEntry(): "
                            "css mismatch! incoming[ %p ] != [ %p ]\n",
                        DEVICE_NAME, css,
                        CGROUP_SUBSYS_STATE( cgroup, subsys ) );
    }
    // */

    // Allocate space for the new entry
    entry = (cgroup_diff_t*)kzalloc( sizeof(*entry), GFP_KERNEL );
    if (! entry) {
      printk(KERN_ALERT "%s cgroup_v1_getEntry(): "
                            "Allocation failed for cgroup[ %u ], ssid[ %u ]\n",
                        DEVICE_NAME, cgroup->id, CGROUP_SUBSYS_ID(subsys));
      return NULL;
    }

    s_bytesAlloc += sizeof(*entry);

    // Grab a reference to the css
    css_get(css);

    entry->id     = id;
    entry->subsys = subsys;
    entry->cgroup = cgroup;
    entry->css    = css;

    /*
    entry->ssid  = ssid;
    entry->cgid  = (u32)cgroup->id;
    entry->cssid = (u32)((u64)css->id & 0xffffffff);
    // */

    // Gather the "absolute" path
    entry->pathLen = _cgroupPath( cgroup, path, sizeof(path), ns );

    /*
    printk(KERN_INFO "%s cgroup_v1_getEntry(): cache MISS for %04d: %s\n",
                      DEVICE_NAME, id, path);
    // */

    // Allocate space for `path` and copy it in.
    entry->path = (char*)kzalloc( entry->pathLen + 1, GFP_KERNEL );
    if (entry->path) {
      memcpy( entry->path, path, entry->pathLen );

      s_bytesAlloc += entry->pathLen + 1;
    }

    /*
    printk(KERN_INFO "%s cgroup_v1_getEntry(): "
                          "create entry %p [ %llu / %llu ]\n",
                     DEVICE_NAME, entry, s_bytesAlloc, s_bytesFreed);
    // */

    // Add this new entry to the hash table.
    _cgroupHash_add( entry );

  } else {

    // Follow any changes to 'subsys', 'cgroup', or 'css' ...
    if (subsys != NULL && entry->subsys != subsys) {
      printk(KERN_ALERT "%s _cgroup_v1_getEntry(): "
                            "cached 'subsys' for cgroup[ %u ], ssid[ %u ] "
                            "differs: incoming[ %p ] != [ %p ]\n",
                        DEVICE_NAME, cgroup->id, CGROUP_SUBSYS_ID(subsys),
                                     subsys, entry->subsys);
      entry->subsys = subsys;
    }

    if (cgroup != NULL && entry->cgroup != cgroup) {
      printk(KERN_ALERT "%s _cgroup_v1_getEntry(): "
                            "cached 'cgroup' differs: "
                            "incoming[ id:%u, %p ] != [ id:%u, %p ]\n",
                            DEVICE_NAME, cgroup->id, cgroup,
                                         entry->cgroup->id, entry->cgroup);

      entry->cgroup = cgroup;
    }

    if (css != NULL && entry->css != css) {
      printk(KERN_ALERT "%s _cgroup_v1_getEntry(): "
                            "cached 'css' for cgroup[ %u ], ssid[ %u ] "
                            "differs: incoming[ %p ] != [ %p ]\n",
                        DEVICE_NAME, cgroup->id, CGROUP_SUBSYS_ID(subsys),
                                     css, entry->css);

      // Grab a reference to the new css
      css_get(css);

      // Release our reference to the previous cgroup_subsys_state
      if (entry->css) { css_put( entry->css ); }

      entry->css = css;
    }
  }

  return entry;
}

/**
 *  Perform gather and diff for cgroup entry.
 *  @method _cgroup_v1_gather
 *  @param  entry     The cgroup data entry {cgroup_diff_t*};
 *
 *  @return void
 *  @protected
 */
static void
_cgroup_v1_gather( cgroup_diff_t*  entry ) {
  u32               ssid      = CGROUP_SUBSYS_ID( entry->subsys );
  cgroup_module_t*  cgmodule  = s_cgroup_modules[ ssid ];

  // assert( entry->subsys == s_kern.cgroup_subsys[ssid] );
  // assert( entry->css    == CGROUP_SUBSYS_STATE( cgroup,
  //                                               entry->subsys ));

  if (cgmodule && cgmodule->gather) {
    // Gather and diff
    cgmodule->gather( entry );
  }
}

/**
 *  Perform gather, diff, and output for cgroup entry.
 *  @method _cgroup_v1_json
 *  @param  entry     The cgroup data entry {cgroup_diff_t*};
 *  @param  needComma Is a comma needed before any output? {bool};
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
_cgroup_v1_json( cgroup_diff_t* entry, bool needComma ) {
  u32               ssid      = CGROUP_SUBSYS_ID( entry->subsys );
  cgroup_module_t*  cgmodule  = s_cgroup_modules[ ssid ];
  cgroup_store_t*   store     = entry->store;
  ssize_t           written   = 0;

  // Perform a gather and diff on the target entry.
  _cgroup_v1_gather( entry );

  // :XXX: In case _cgroup_v1_gather() caused the store to be created or moved
  store = entry->store;

  if (store != NULL && store->nChanges > 0) {
    // There are one or more diffs for this entry
    if (needComma)  { written += lkb_write( ",", 1 ); }

    written += lkb_write( "{", 1 );

    // Include the hierarchy_id and cgroup/subsystem path
    written += json_write_u32(  "id",   entry->id );
    written += lkb_write( ",", 1 );
    written += json_write_u32(  "hid",  entry->cgroup->root->hierarchy_id );
    written += lkb_write( ",", 1 );
    written += json_write_strn( "path", entry->path, CGROUP_MAX_PATH );

    if (cgmodule->json) {
      /* Let the collection module handle output of all diffs.
       *
       * :NOTE: The output handler is expected to include a leading ','
       *        before any diff output.
       */
      written += cgmodule->json( entry );

    } else {
      /* Iterate over the handlers and store items for this entry...
       *
       * :NOTE: All output handlers invoked from here are expected to include a
       *        leading ',' before any diff output.
       */
      u32               storeDex  = 0;
      cftype_handler_t* handler;

      for (handler = entry->handlers; handler && handler->name; handler++) {
        if (handler->cft == NULL) { continue; }

        if (handler->json) {
          // Use the handler-provided json callback for output.
          written += handler->json( handler, entry, storeDex );

        } else {
          // Use the common cftype output helper
          written += cgroup_v1_cftype_json( handler, entry, storeDex );

        }

        storeDex += handler->nEntries;
      }
    }

    written += lkb_write( "}", 1 );
  }

  return written;
}
/* Private helpers }
 *****************************************************************************/
#endif  // CONFIG_CGROUPS  }

/*****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize this sub-module.
 *  @method cgroup_v1_init
 *
 *  @return void
 *  @protected
 */
static void
cgroup_v1_init( void ) {
#ifdef  CONFIG_CGROUPS  // {
  CGROUP_ROOT*  root  = NULL;

  lkb_resolve( s_syms, s_numSyms );

  // :XXX: Make the s_syms entries match the s_kern expectations!!
  s_kern.cgroup_roots  = s_syms[0].ptr;
  s_kern.cgroup_subsys = s_syms[1].ptr;
  //s_kern.cgroup_mutex  = s_syms[2].ptr;

# if LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0)
  s_kern.kernfs_name   = s_syms[2].ptr;

# endif // LINUX_VERSION_CODE

  /*
  printk(KERN_INFO "CGROUP Roots[ %p ], subsys[ %p ]\n",
                   s_kern.cgroup_roots, s_kern.cgroup_subsys );
  // */

  _cgroupHash_init();

  /* Determine which cgroup subsystems are active and, if there is an
   * available collection module for that subsystem, activate it.
   */
  _cgroup_v1_initModules();

  FOR_EACH_ROOT(root) {
    char  name[MAX_CGROUP_ROOT_NAMELEN]  = {0};

    _cgroupRootName( root, name, sizeof(name) );

    /*
    printk(KERN_INFO  "%s CGROUP Root hid[ %2d ]: %s\n",
                      DEVICE_NAME,
                      root->hierarchy_id,
                      name);
    // */

    /* :TODO: Walk this hiearchy and output initial information about each node.
     *    v3.9  cgroup_for_each_descendant_pre(pos, cgroup)
     *    v4.10 css_for_each_descendant_pre(pos, css)
     *
     *  For now, we rely on `cgroup_v1_touch_rcu()` to identify active cgroups,
     *  which are then reported via `cgroup_v1_json()`.
     *
     *  `cgroup_v1_touch_rcu()` is invoked during the process table walk
     *  (proc.c : proc_gather()).
     *
    struct cgroup*  child;
    CGROUP_FOR_EACH_LIVE_CHILD( child, CGROUP_ROOT_CGROUP(root) ) {
      struct cgroup_subsys* subsys  = NULL;
      int                   ssid;

      FOR_EACH_SUBSYS(subsys, ssid) {
        struct cgroup_subsys_state* css = NULL;
        if ( !(root->subsys_mask & (1 << ssid)) ||
              (css = CGROUP_SUBSYS_STATE(child, subsys)) == NULL) {
          continue;
        }
      }
    }
    // */
  }

  s_isActive = TRUE;
#endif  // CONFIG_CGROUPS  }

  printk(KERN_INFO "%s cgroup_v1_init(): ~ %lu bytes / cgroup:subsystem\n",
                   DEVICE_NAME, sizeof(cgroup_diff_t));
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method cgroup_v1_reset
 *
 *  @return void
 *  @protected
 */
static void
cgroup_v1_reset( void ) {
#ifdef  CONFIG_CGROUPS  // {
  int idex;

  /*
  printk(KERN_INFO "%s cgroup_v1_reset(): ...\n", DEVICE_NAME);
  // */

  // Walk all collection modules and invoke reset on any that support it
  for (idex = 0; idex < CGROUP_SUBSYS_COUNT; idex++) {
    cgroup_module_t*  cgmodule  = s_cgroup_modules[ idex ];
    if (cgmodule && cgmodule->reset) {
      /*
      printk(KERN_INFO "%s cgroup_v1_reset(): collector[ %s ] ...\n",
                        DEVICE_NAME, cgmodule->name);
      // */

      cgmodule->reset();
    }
  }

  // Clear the cgroup hash table
  _cgroupHash_clear();

  /*
  printk(KERN_INFO "%s cgroup_v1_reset(): hash cleared\n", DEVICE_NAME);
  // */

#endif  // CONFIG_CGROUPS  }
}

/**
 *  Finalize, cleaning up any allocated resources.
 *  @method cgroup_v1_fini
 *
 *  @return void
 *  @protected
 */
static void
cgroup_v1_fini( void ) {
  // Walk all collection modules and invoke reset on any that support it
  int idex;

  if (! s_isActive) { return; }

  _cgroupHash_fini( );

  for (idex = 0; idex < CGROUP_SUBSYS_COUNT; idex++) {
    cgroup_module_t*  cgmodule  = s_cgroup_modules[ idex ];
    if (cgmodule && cgmodule->fini) {
      cgmodule->fini();
    }
  }

  if (s_bytesAlloc - s_bytesFreed != 0) {
    printk(KERN_WARNING "%s cgroup_v1_fini()  : "
                        "%7llu bytes allocated, %7llu freed : %llu LEAKED\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed,
                                     s_bytesAlloc - s_bytesFreed);
  } else {
    printk(KERN_INFO    "%s cgroup_v1_fini()  : "
                        "%7llu bytes allocated, %7llu freed\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed);
  }

  s_isActive = FALSE;
}

/**
 *  Gather and write cgroup metrics as JSON to the circular buffer.
 *  @method cgroup_v1_json
 *
 *  @note   THIS method will obtain and release rcu_read_lock().
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
cgroup_v1_json( void ) {
  ssize_t   written   = 0;
#ifdef  MEASURE_PERFORMANCE
  u64       start     = lkb_timestamp();
  u64       end;
#endif  // MEASURE_PERFORMANCE

#ifdef  CONFIG_CGROUPS  // {
  /* Traverse all entries in our hash table and for each that has
   * 'lastBeat == lkb.beat' (courtesy of the process module), update
   * cgroup/subsystem metrics and output any differences.
   */
  bool            needComma   = FALSE;
  unsigned int    hashEntries = 1U << s_cgroupHash_shift;
  unsigned int    idex;
  cgroup_diff_t*  entry;

  written += lkb_write( "[", 1 );

  rcu_read_lock();
  for (idex = 0; idex < hashEntries; idex++) {

    hlist_for_each_entry_rcu( entry, &s_cgroupHash[idex], hash_chain ) {
      /*
      printk(KERN_INFO "%s cgroup_v1_json(): %2d:%-2d : %04u : "
                            "cssid[ %llu ] : %s, lastBeat[ %u ], beat[ %u ]\n",
                       DEVICE_NAME,
                       entry->subsys->id, entry->cgroup->id,  //cgroup->id,
                       entry->id,
                       (u64)entry->css->id,
                       entry->path,
                       entry->lastBeat, lkb.beat);
      // */

      if (entry->lastBeat >= lkb.beat) {
        // This cgroup was touched this beat so gather and output any diffs
        ssize_t entryWrite  = _cgroup_v1_json( entry, needComma );
        if (entryWrite > 0) { needComma = TRUE; }
        written += entryWrite;
      }
    }
  }
  rcu_read_unlock();

  written += lkb_write( "]", 1 );
#endif  // CONFIG_CGROUPS  }

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s cgroup_v1_json(): %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/* Module methods }
 *****************************************************************************
 * Public methods {
 *
 */

/**
 *  Retrieve a reference to the cgroup namespace.
 *  @method cgroup_v1_getNs
 *  @param  task    The target task {struct task_struct*};
 *
 *  @note   The caller MUST have the task locked `task_lock(task)`;
 *
 *  @note   The caller MUST invoke `cgroup_v1_putNs( ret )` when finished;
 *
 *  @return The referenced cgroup namespace {void*};
 */
void*  /* struct cgroup_namespace* */
cgroup_v1_getNs( struct task_struct* task ) {
  void* ns  = NULL;

# if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,5,0)  // {
  // kernel/cgroup.c : cgroupns_get( task )
  struct cgroup_namespace*  rns = NULL;
  struct nsproxy*           nsp = task->nsproxy;
  if (nsp) {
    rns = nsp->cgroup_ns;
    get_cgroup_ns( rns );

    ns = (void*)rns;
  }
# endif // LINUX_VERSION_CODE // }

  return ns;
}

/**
 *  Release a reference to the cgroup namespace.
 *  @method cgroup_v1_putNs
 *  @param  ns      The target cgroup namespace {void*};
 *
 *  @note   For Linux v4.5.0+, `ns` is actually {struct cgroup_namespace*};
 *
 *  @return void
 *  @private
 */
void
cgroup_v1_putNs( void* ns /* struct cgroup_namespace* */ ) {

# if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,5,0)  // {
  if (ns) {
    //struct cgroup_namespace*  rns = (struct cgroup_namespace*)ns;
    put_cgroup_ns( ns );
  }
# endif // LINUX_VERSION_CODE // }
}

/**
 *  Create a standard cgroup store for the given number of entries.
 *  @method cgroup_v1_makeStore
 *  @param  nEntries  The number of entries to include {u32};
 *
 *  The space required for a cgroup_store_t:
 *    sizeof( cgroup_store_t )  : 24-bytes
 *      nEntries  4-bytes (1 32-bit value)
 *      nChanges  4-bytes (1 32-bit value)
 *      values    8-bytes (1 64-bit pointer)
 *      changes   8-bytes (1 64-bit pointer)
 *
 *    allocated size:
 *      cgroup_store_t  24-bytes
 *      values          nEntries * 8-bytes
 *      changes         nEntries * 1-byte
 *
 *  @return A pointer to the new store (NULL on error) {cgroup_store_t*};
 */
cgroup_store_t*
cgroup_v1_makeStore(  u32 nEntries ) {
  cgroup_store_t* store   = NULL;
  u32             nBytes  = sizeof( *store ) +
                              nEntries * (sizeof(u64) + sizeof(bool));

  store = kzalloc( nBytes, GFP_KERNEL );
  if (store == NULL) {
    printk(KERN_ALERT "%s cgroup_v1_makeStore(): "
                            "Cannot allocate %u bytes\n",
                      DEVICE_NAME, nBytes);
    return store;
  }

  s_bytesAlloc += nBytes;

  /*
  printk(KERN_INFO "%s cgroup_v1_makeStore(): "
                      "create %u byte, %u entry store: [ %llu / %llu ]\n",
                   DEVICE_NAME, nBytes, nEntries,
                   s_bytesAlloc, s_bytesFreed);
  // */

  store->nEntries = nEntries;
  store->values   = GET_STORE_VALUES(  store );
  store->changes  = GET_STORE_CHANGES( store );

  return store;
}

/**
 *  Given a cftype map set and cftype name, find the map entry with a matching
 *  name.
 *  @method cgroup_v1_findHandler
 *  @param  handlers  The array of handlers {cftype_handler_t*};
 *  @param  nHandlers The number of entries in `handlers` {u32};
 *  @param  name      The name of the target cftype {char*};
 *
 *  @return The target entry, or NULL if not found {cftype_handler_t*};
 */
cftype_handler_t*
cgroup_v1_findHandler(  cftype_handler_t* handlers,
                        u32               nHandlers,
                        char*             name ) {

  cftype_handler_t* res    = NULL;
  u32               idex;

  for (idex = 0; idex < nHandlers; idex++) {
    cftype_handler_t* entry = &handlers[idex];

    if (!strncmp( entry->name, name, MAX_CFTYPE_NAME )) {
      res = entry;
      break;
    }
  }

  return res;
}

/**
 *  A cftype helper to read from the '/sys/fs/cgroup' filesystem-based entry for
 *  the identified cgroup/subsystem/cftype.
 *  @method cgroup_v1_fsRead
 *  @param  cft       The target cftype {struct cftype*};
 *  @param  cgd       The cgroup data {cgroup_diff_t*};
 *  @param  buf       The buffer to populate {char*};
 *  @param  bufLen    The size (in bytes) of `buf` {u32};
 *
 *  @note   This should only be used for non-trivial cgroup data that is not
 *          directly accessible otherwise. cftype entries that use `read_u64()`
 *          and `read_s64()` should be handled using
 *          `cgroup_v1_cftype_extract()``;
 *
 *          Reading a file from the kernel is generally frowned upon but it's
 *          not clear what a better alternative is given the state required
 *          to make use of a `seq_file`
 *          (i.e. private data that points to the structures we don't have
 *                access to in the first place).
 *
 *          On the other hand, this isn't *actually* reading a file but rather
 *          invoking another kernel method to copy data into the provided
 *          buffer.
 *
 *  @return The number of bytes written to `buf` {ssize_t};
 */
ssize_t
cgroup_v1_fsRead( struct cftype*  cft,
                  cgroup_diff_t*  cgd,
                  char*           buf,
                  u32             bufLen ) {
  ssize_t       read      = 0;

#ifdef  USE_CGROUP_FSREAD // {
  char          path[ CGROUP_MAX_PATH ] = {0};
  loff_t        offset                  = 0;
  struct file*  file;

  /* Generate the absolute path to the target cgroup data:
   *    /sys/fs/cgroup/%cgroup-path%/%subsys-name%.%cft-name%
   */
  if (snprintf( path, sizeof(path), "/sys/fs/cgroup/%s%s.%s",
                cgd->path,
                CGROUP_SUBSYS_NAME( cgd->subsys ),
                cft->name ) < 1) {

    // No path generated...
    return read;
  }

  /*
  printk(KERN_INFO "%s cgroup_v1_fsRead(): name[ %s : %s ] => path[ %s ] ...\n",
                     DEVICE_NAME, cgd->path, cft->name, path);
  // */

  // Attempt to open the cgroup data file
  file = filp_open( path, O_RDONLY, 0 );
  if (IS_ERR(file)) {
    // :TODO: If the open fails, disable this entry.
    /*
    printk(KERN_WARNING "%s cgroup_v1_fsRead(): "
                              "cgd->path[ %s ], cft->name[ %s ], "
                              "subsys[ %s ] "
                              "=> path[ %s ]: error on open: %ld\n",
                       DEVICE_NAME,
                       cgd->path, cft->name,
                       CGROUP_SUBSYS_NAME( cgd->subsys ),
                       path, PTR_ERR(file));
    // */
    return read;
  }

  // Attempt to read from the cgroup data file
  read = DO_KERNEL_READ( file, offset, buf, bufLen - 1 );
  if (read >= 0) {
    // Ensure the buffer is null-terminated
    buf[read] = '\0';

  } else if (read < 0) {
    printk(KERN_WARNING "%s cgroup_v1_fsRead(): name[ %s ], path[ %s ], %d: "
                                "error on read: %ld\n",
                       DEVICE_NAME, cft->name, path, bufLen - 1, read);

  }

  // Close the sequence file
  filp_close( file, NULL );
#endif  // USE_CGROUP_FSREAD }

  return read;
}

/**
 *  A cftype helper for simple extraction.
 *  @method cgroup_v1_cftype_extract
 *  @param  self      The cftype map entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  A cftype extractor handles a single cftype and will fill
 *  `self->store->nEntries` values/change-indicators in `cgd`.
 *
 *  @return The number of detected changes {u32};
 */
u32
cgroup_v1_cftype_extract( cftype_handler_t* self,
                          cgroup_diff_t*    cgd,
                          u32               start,
                          bool              doDiff ) {

  struct cftype*      cft       = self->cft;
  cgroup_store_t*     store     = cgd->store;
  u64*                values    = NULL;
  bool*               changes   = NULL;
  u32                 nChanges  = 0;
  CGROUP_CFT_CONTEXT* context   = CGROUP_CSS_CFT_CONTEXT(cgd->css);

  if (cft == NULL || store == NULL) { return nChanges; }

  // Grab pointers to the `values` and `changes` arrays from the store
  values  = store->values  + start;
  changes = store->changes + start;

  if (self->nEntries > 1) {
    // :TODO: This is a cftype that provides multiple values
    printk(KERN_WARNING "%s cgroup_v1_cftype_extract(): %s%s "
                              "provides %u entries: NYI",
                           DEVICE_NAME, cgd->path, self->name, self->nEntries);

  } else {
    // This is a cftype that provides a single 64-bit value.
    u64                     oldVal  = *values;
    union { u64 u; s64 s; } newVal  = {0};

    if (cft->read_u64) {
      newVal.u = cft->read_u64( context, cft );

    } else if (cft->read_s64) {
      newVal.s = cft->read_s64( context, cft );

    } else {
      printk(KERN_WARNING "%s cgroup_v1_cftype_extract(): %s%s:%s: "
                              "provides neither 'read_u64()' nor 'read_s64()': "
                              "no-value\n",
                           DEVICE_NAME, cgd->path, self->name, cft->name);

      printk(KERN_WARNING "%s cgroup_v1_cftype_extract(): %s%s:%s: "
                              "read_u64[ %p ], read_s64[ %p ]\n",
                           DEVICE_NAME, cgd->path, self->name, cft->name,
                                        cft->read_u64, cft->read_s64);

# if     LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0) // {
      printk(KERN_WARNING "%s cgroup_v1_cftype_extract():3.15+: %s%s:%s: "
                              "seq_show[ %p ], seq_start[ %p ], "
                              "seq_next[ %p ], seq_stop[ %p ]\n",
                           DEVICE_NAME, cgd->path, self->name, cft->name,
                                        cft->seq_show, cft->seq_start,
                                        cft->seq_next, cft->seq_stop);
# else   // LINUX_VERSION_CODE < 3.15.0 }{
      printk(KERN_WARNING "%s cgroup_v1_cftype_extract():3.15-: %s%s:%s: "
                              "read[ %p ], read_map[ %p ], "
                              "read_seq_string[ %p ]\n",
                           DEVICE_NAME, cgd->path, self->name, cft->name,
                                        cft->read, cft->read_map,
                                        cft->read_seq_string);
# endif  // LINUX_VERSION_CODE  }
    }

    if (!doDiff || oldVal != newVal.u) {
      *changes = TRUE;
      *values  = newVal.u;
      nChanges++;

    } else {
      *changes = FALSE;
    }

    /* Debug output {
    if (cft->read_s64) {
      printk(KERN_INFO "%s cgroup_v1_cftype_extract():s64.%u: %s%s "
                              "[ %lld => %lld : %lld ]: %s [ %u total ]\n",
                           DEVICE_NAME, start,
                           cgd->path, self->name,
                           (s64)oldVal, newVal.s, (s64)*values,
                           (*changes ? "DIFF" : "no-diff"),
                           nChanges);

    } else {
      printk(KERN_INFO "%s cgroup_v1_cftype_extract():u64.%u: %s%s "
                              "[ %llu => %llu : %llu ]: %s [ %u total ]\n",
                           DEVICE_NAME, start,
                           cgd->path, self->name,
                           oldVal, newVal.u, *values,
                           (changes[0] ? "DIFF" : "no-diff"),
                           nChanges);
    }
    // Debug output } */
  }

  return nChanges;
}

/**
 *  A cftype helper to write metrics as JSON to the circular buffer.
 *  @method cgroup_v1_cftype_json
 *  @param  self      The cftype map entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *
 *  A JSON output handler will output `self->store->nEntries` from the values
 *  area as identified by the parallel change-indicators within `cgd`.
 *
 *  If this method is not provided, `cgroup_v1_cftype_json()` will be used.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 */
ssize_t
cgroup_v1_cftype_json(  cftype_handler_t* self,
                        cgroup_diff_t*    cgd,
                        u32               start) {

  cgroup_store_t* store     = cgd->store;
  ssize_t         written   = 0;
  u64*            values    = NULL;
  bool*           changes   = NULL;
  u32             nChanges  = 0;
  u32             idex;

  if (self->cft == NULL || store == NULL || store->nChanges < 1) {
    return written;
  }

  // Grab pointers to the `values` and `changes` arrays from the store
  values  = store->values  + start;
  changes = store->changes + start;

  // Are there any changes in the values managed by this handler?
  for (idex = 0; idex < self->nEntries; idex++) {
    if (changes[idex])  { nChanges++; }
  }

  /*
  printk(KERN_INFO "%s cgroup_v1_cftype_json():%u: %s%s "
                          "%u changes in %u entries ...\n",
                       DEVICE_NAME, start,
                       cgd->path, self->name,
                       nChanges, self->nEntries);
  // */

  if (nChanges < 1) { return written; }

  /*********************************************************************
   * There are changes so include the leading ','
   */
  written += lkb_write( ",", 1 );

  // Output the key name '%subsys-name%.%cftype-name%'
  written += lkb_write( "\"", 1 );
  written += json_write_raw_strn_val( CGROUP_SUBSYS_NAME(cgd->subsys),
                                      CGROUP_MAX_PATH );
  written += lkb_write( ".", 1 );
  written += json_write_raw_strn_val( self->name, CGROUP_MAX_PATH );
  written += lkb_write( "\":", 2 );

  if (self->nEntries > 1) {
    // Treat this as an array of 64-bit values
    written += lkb_write( "[", 1 );

  }

  for (idex = 0; idex < self->nEntries; idex++) {
    /* :NOTE: If this is an array, we output ALL elements since at least one
     *        has changed and we need to maintain an array with matching slots.
     */
    if (idex > 0) { written += lkb_write( ",", 1 ); }

    if (self->cft->read_s64) {
      // Output an s64 value
      written += json_write_s64_val( (s64)values[idex] );

      /*
      printk(KERN_INFO
                "%s cgroup_v1_cftype_json():%u: %s%s:%u: s64[ %lld ]\n",
                  DEVICE_NAME, start, cgd->path, self->name,
                  idex, (s64)values[idex]);
      // */

    } else {
      // Output a u64 value
      written += json_write_u64_val( values[idex] );

      /*
      printk(KERN_INFO
                "%s cgroup_v1_cftype_json():%u: %s%s:%u: u64[ %lld ]\n",
                  DEVICE_NAME, start, cgd->path, self->name,
                  idex, (s64)values[idex]);
      // */
    }
  }

  if (self->nEntries > 1) {
    written += lkb_write( "]", 1 );
  }

  return written;
}

/**
 *  Tag the cgroup related to the given cgroup state for update.
 *  @method cgroup_v1_touch_rcu
 *  @param  ns      The cgroup namespace retrieved via cgroup_v1_getNs()
 *                  {void*};
 *  @param  ssid    The id of the target subsystem {int};
 *  @param  css     The target cgroup subsystem state
 *                  {struct cgroup_subsys_state*};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return The id of the target cgroup/subsystem (-1 on failure) {u32};
 */
u32
cgroup_v1_touch_rcu( void*                        ns,
                     int                          ssid,
                     struct cgroup_subsys_state*  css ) {
#ifdef  CONFIG_CGROUPS  // {
  struct cgroup_subsys* ss      = GET_SUBSYS( ssid );
  struct cgroup*        cgroup  = (css ? css->cgroup : NULL);
  cgroup_diff_t*        entry;

  if (css == NULL || ss == NULL || cgroup == NULL) {
    return (u32)-1;
  }

  entry = _cgroup_v1_getEntry( cgroup, ss, ns, css );
  if (entry == NULL) {
    return (u32)-1;
  }

  // assert( entry         != NULL );
  // assert( entry->css    == css );
  // assert( entry->cgroup == cgroup );
  // assert( entry->subsys == ss );

  /*
  printk(KERN_INFO "%s cgroup_v1_touch_rcu(): %2d:%-2d : %04u : "
                        "cssid[ %llu ] : %s, lastBeat[ %u => %u ]\n",
                   DEVICE_NAME,
                   entry->subsys->id, entry->cgroup->id,  //cgroup->id,
                   entry->id,
                   (u64)entry->css->id,
                   entry->path,
                   entry->lastBeat, lkb.beat);
  // */

  // Touch this entry for cgroup processing later
  entry->lastBeat = lkb.beat;
#endif  // CONFIG_CGROUPS  }

  return entry->id;
}

/**
 *  Tag the cgroups related to the given task for update.
 *  @method cgroups_v1_touch_rcu
 *  @param  task    The target task/process {struct task_struct*}
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @note   This method will obtain a lock on the task to protect the
 *          contained cgroup information while in use.
 *
 *  @return void
 */
void
cgroups_v1_touch_rcu( struct task_struct* task ) {
#ifdef  CONFIG_CGROUPS  // {
  int   ssid;
  void* ns;

  if (! s_isActive) {
    // This module is not active so ignore this touch request.
    return;
  }

  task_lock( task );  // {

  ns = cgroup_v1_getNs( task );

  for (ssid = 0; ssid < CGROUP_SUBSYS_COUNT; ssid++) {
    struct cgroup_subsys_state* css = task_get_css( task, ssid );
    // assert( css != NULL );

    cgroup_v1_touch_rcu( ns, ssid, css );

    /* Release our temporary reference to the cgroup_subsys_state
     *
     * (any required long-term reference will have been obtained by
     *  _cgroup_v1_getEntry())
     */
    css_put( css );
  }

  cgroup_v1_putNs( ns );

  task_unlock( task );  // }

#endif  // CONFIG_CGROUPS  }
}

/**
 *  Retrieve cgroup information for the cgroups identified by the provided
 *  process data, and immediately report differences.
 *  @method cgroup_v1_report
 *  @param  proc    Information about the target task/process, collected
 *                  via the proc sub-module {proc_t*};
 *
 *  @note   This method will output a leading ',' before any diff output.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 */
ssize_t
cgroup_v1_report( proc_t* proc ) {

  ssize_t written = 0;

#ifdef  CONFIG_CGROUPS  // {
  bool    needComma = FALSE;
  s32     pid;
  int     idex;

  if (! s_isActive || proc == NULL) {
    // This module is not active or we were given an invalid `proc`.
    return written;
  }

  pid = proc->pid;

  // Output the leading ',', key and object open
  written += lkb_write( ",", 1 );
  written += json_write_key( "cgroup_v1");
  written += lkb_write( "[", 1 );

  for (idex = 0; idex < CGROUP_SUBSYS_COUNT; idex++) {
    u32             id        = proc->cgroups[ idex ];
    cgroup_diff_t*  entry     = _cgroupHash_find( id );
    ssize_t         subWrite  = 0;

    if (! entry) {
      printk(KERN_WARNING
                "%s cgroup_v1_report( %6d ): no entry for cgroup %u\n",
                            DEVICE_NAME, pid, id);
      continue;
    }

    // Gather and report any differences for this cgroup/subsystem.
    subWrite  += _cgroup_v1_json( entry, needComma );
    if (subWrite > 0) {
      needComma = TRUE;
      written  += subWrite;
    }
  }

  // Close the object
  written += lkb_write( "]", 1 );

#endif  // CONFIG_CGROUPS  }

  return written;
}

/**
 *  Release  cgroup data entry.
 *  @method cgroup_v1_releaseEntry
 *  @param  entry   A pointer to the target entry {cgroup_diff_t*};
 *
 *  @return void
 */
void
cgroup_v1_releaseEntry( cgroup_diff_t* entry ) {
  bool  leaveEntry  = FALSE;

  rcu_read_lock();
    _cgroup_v1_releaseEntry_rcu( entry, leaveEntry );
  rcu_read_unlock();
}

/**
 *  Release any gathered cgroup information.
 *  @method cgroup_v1_release
 *  @param  data    A pointer to the data to release {cgroup_v1_t*};
 *
 *  @return void
 */
void
cgroup_v1_release( cgroup_v1_t* data ) {
#ifdef  CONFIG_CGROUPS  // {
  bool  leaveEntry  = TRUE;
  int   ssid;

  rcu_read_lock();

    for (ssid = 0; ssid < CGROUP_SUBSYS_COUNT; ssid++) {
      _cgroup_v1_releaseEntry_rcu( &data->subsys[ ssid ], leaveEntry );
    }

  rcu_read_unlock();

#endif  // CONFIG_CGROUPS  }
}
/* Public methods }
 *****************************************************************************/

/**
 *  Expose this as an lkb module.
 */
Module_t module_cgroup_v1 = {
  .name   = "cgroup_v1",
  .init   = cgroup_v1_init,
  .json   = cgroup_v1_json,
  .reset  = cgroup_v1_reset,
  .fini   = cgroup_v1_fini,
};
