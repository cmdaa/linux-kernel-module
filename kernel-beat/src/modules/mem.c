/**
 *  Module: mem
 *
 *  Beat-driven module that collects memory usage metrics.
 *
 *  This module:
 *    - presents metrics similar to those available via `/proc/meminfo`;
 *
 *    - makes use of non-public, though exported, kernel symbols:
 *      - si_swapinfo()
 *      - total_swapcache_pages()
 *      - vm_commit_limit()
 *
 *      - vm_committed_as
 *      - hstates
 *      - default_hstate_idx
 */
#include "../lkb.h"

#include <linux/mm.h>       // si_meminfo(), si_mem_available()
#include <linux/hugetlb.h>  // hugepages_supported(), default_hstate
#include <linux/mman.h>     // vm_commit_limit(), vm_committed_as
#include <linux/mmzone.h>   // NR_FILE_PAGES
#include <linux/swap.h>     // si_swapinfo(), total_swapcache_pages()
#include <linux/vmstat.h>   // global_page_state()      (v 3.10)
                            // global_node_page_state() (v 4.9)
#include <linux/vmalloc.h>  // VMALLOC_TOTAL
#include <asm/page.h>       // PAGE_SHIFT


#include "../map.h"
#include "../json.h"

#include "mem.h"

// :XXX: global_page_state was changed to global_node_page_state after v4.4.0
#if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,5,0)
# define NODE_PAGE_STATE
#endif  // LINUX_VERSION_CODE

#ifdef  NODE_PAGE_STATE
// linux >= 4 : global_node_page_state()
# define  GLOBAL_NODE_PAGE_STATE global_node_page_state

#else   // !NODE_PAGE_STATE
// linux <  4 : global_page_state()
# define  GLOBAL_NODE_PAGE_STATE global_page_state

#endif  // NODE_PAGE_STATE

/* :NOTE: If additional fields are added, `s_map` MUST also be changed.
 *
 *  The comments below reference the variable names as presented by:
 *    /proc/meminfo
 */
typedef struct {
  u64 total;            // Total memory (bytes)           [MemTotal];
  u64 free;             // Free memory (bytes)            [MemFree];
  u64 available;        // Avaialble memory (bytes)       [MemAvailable];
  u64 used;             // Used memory (bytes)            [MemTotal-MemFree];
  u64 actualUsed;       // Actual used memory (bytes)   [MemTotal-MemAvailable];
  u64 buffer;           // Memory used as buffer (bytes)  [Buffers];
  u64 shared;           // Shared memory (bytes)          [Shmem];
  // Unavailable directly
  u64 cached;           // Memory used as cache (bytes)   [Cached];

  u64 swapTotal;        // Total swap (bytes)             [SwapTotal];
  u64 swapFree;         // Free swap (bytes)              [SwapFree];
  u64 swapUsed;         // Used swap (bytes)              [SwapTotal-SwapFree];
  // Unavailable directly
  u64 swapCached;       // Cached swap (bytes)            [SwapCached];

  //#ifdef CONFIG_HIGHMEM {
  u64 highTotal;        // Total high memory (bytes)      [HighTotal];
  u64 highFree;         // Free  high memory (bytes)      [HighFree];
  u64 lowTotal;         // Total low  memory (bytes)      [LowTotal];
  u64 lowFree;          // Free  low  memory (bytes)      [LowFree];
  //#endif // CONFIG_HIGHMEM }

  // Unavailable directly
  u64 commitLimit;      // Virtual memory commit limit (bytes)[CommitLimit];
  u64 commited;         // Committed virtual memory (bytes)   [Commited_AS];

  u64 kernStack;        // Kernel stack size (bytes)          [KernelStack];
  u64 vmallocTotal;     // Total virtual memory (bytes)       [VmallocTotal];

  // Pages
  u32 active;           // Active memory (pages)              [Active];
  u32 inactive;         // Inactive memory (pages)            [Inactive];

  u32 activeAnon;       // Active anonymous memory (pages)    [Active(anon)];
  u32 inactiveAnon;     // Inactive anonymous memory (pages)  [Inactive(anon)];

  u32 activeFile;       // Active file memory (pages)         [Active(file)];
  u32 inactiveFile;     // Inactive file memory (pages)       [Inactive(file)];

  u32 unevictable;      /* Unevictable memory (pages)         [Unevictable];
                         * (e.g. RAMfs, locked shared memory,
                         *       locked virtual memory)
                         */
  u32 mlock;            // Locked memory (pages)              [Mlocked];

  u32 dirty;            // Dirty, writable pages              [Dirty];
  u32 writeback;        // Pages under writeback              [Writeback];
  u32 anonMapped;       // Anonymously mapped pages           [AnonPages];
  u32 nfsUnstable;      // NFS unstable pages                 [NFS_Unstable];

  u32 slab;             // Kernel allocated memory (pages)    [Slab];
  u32 slabReclaimable;  // Reclaimable slabs                  [SReclaimable];
  u32 slabUnreclaimable;// Unreclaimable slabs                [SUnreclaimable];

  u32 pageTables;       // Total page tables                  [PageTables];
  u32 bounce;           // Pages for bounce buffers           [Bounce];
  u32 writebackTmp;     // Temporary writeback pages          [WritebackTmp];

  u32 unit;             // Page size (bytes)

  // Unavailable directly
  u32 hugePagesTotal;   // Total number of huge pages         [HugePages_Total];
  u32 hugePagesFree;    // Free huge pages                    [HugePages_Free];
  u32 hugePagesReserved;// Reserved huge pages                [HugePages_Rsvd];
  u32 hugePagesSurplus; // Surplus huge pages                 [HugePages_Surp];
  u32 hugePageSize;     // Huge page size (bytes)             [Hugepagesize];

} mem_t;

/**
 *  The mapping from `mem_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all fields from `mem_t` MUST be identified.
 */
static map_t  s_map[] = {
  {"total",         MAP_TYPE_U64, OFFSETOF(mem_t,total)},
  {"free",          MAP_TYPE_U64, OFFSETOF(mem_t,free)},
  {"available",     MAP_TYPE_U64, OFFSETOF(mem_t,available)},
  {"used",          MAP_TYPE_U64, OFFSETOF(mem_t,used)},
  {"actualUsed",    MAP_TYPE_U64, OFFSETOF(mem_t,actualUsed)},
  {"buffer",        MAP_TYPE_U64, OFFSETOF(mem_t,buffer)},
  {"shared",        MAP_TYPE_U64, OFFSETOF(mem_t,shared)},

  // Unavaialble directly
  {"cached",        MAP_TYPE_U64, OFFSETOF(mem_t,cached)},

  {"swapTotal",     MAP_TYPE_U64, OFFSETOF(mem_t,swapTotal)},
  {"swapFree",      MAP_TYPE_U64, OFFSETOF(mem_t,swapFree)},
  {"swapUsed",      MAP_TYPE_U64, OFFSETOF(mem_t,swapUsed)},

  // Unavailable directly
  {"swapCached",    MAP_TYPE_U64, OFFSETOF(mem_t,swapCached)},

  //#ifdef CONFIG_HIGHMEM {
  {"highTotal",     MAP_TYPE_U64, OFFSETOF(mem_t,highTotal)},
  {"highFree",      MAP_TYPE_U64, OFFSETOF(mem_t,highFree)},
  {"lowTotal",      MAP_TYPE_U64, OFFSETOF(mem_t,lowTotal)},
  {"lowFree",       MAP_TYPE_U64, OFFSETOF(mem_t,lowFree)},
  //#endif // CONFIG_HIGHMEM }

  // Unavailable directly
  {"commitLimit",   MAP_TYPE_U64, OFFSETOF(mem_t,commitLimit)},
  {"commited",      MAP_TYPE_U64, OFFSETOF(mem_t,commited)},

  {"kernStack",     MAP_TYPE_U64, OFFSETOF(mem_t,kernStack)},
  {"vmallocTotal",  MAP_TYPE_U64, OFFSETOF(mem_t,vmallocTotal)},

  // pages
  {"active",        MAP_TYPE_U32, OFFSETOF(mem_t,active)},
  {"inactive",      MAP_TYPE_U32, OFFSETOF(mem_t,inactive)},
  {"activeAnon",    MAP_TYPE_U32, OFFSETOF(mem_t,activeAnon)},
  {"inactiveAnon",  MAP_TYPE_U32, OFFSETOF(mem_t,inactiveAnon)},
  {"activeFile",    MAP_TYPE_U32, OFFSETOF(mem_t,activeFile)},
  {"inactiveFile",  MAP_TYPE_U32, OFFSETOF(mem_t,inactiveFile)},

  {"unevictable",   MAP_TYPE_U32, OFFSETOF(mem_t,unevictable)},
  {"mlock",         MAP_TYPE_U32, OFFSETOF(mem_t,mlock)},

  {"dirty",         MAP_TYPE_U32, OFFSETOF(mem_t,dirty)},
  {"writeback",     MAP_TYPE_U32, OFFSETOF(mem_t,writeback)},
  {"anonMapped",    MAP_TYPE_U32, OFFSETOF(mem_t,anonMapped)},
  {"nfsUnstable",   MAP_TYPE_U32, OFFSETOF(mem_t,nfsUnstable)},

  {"slab",          MAP_TYPE_U32, OFFSETOF(mem_t,slab)},
  {"slabReclaimable",
                    MAP_TYPE_U32, OFFSETOF(mem_t,slabReclaimable)},
  {"slabUnreclaimable",
                    MAP_TYPE_U32, OFFSETOF(mem_t,slabUnreclaimable)},

  {"pageTables",    MAP_TYPE_U32, OFFSETOF(mem_t,pageTables)},
  {"bounce",        MAP_TYPE_U32, OFFSETOF(mem_t,bounce)},
  {"writebackTmp",  MAP_TYPE_U32, OFFSETOF(mem_t,writebackTmp)},

  {"unit",          MAP_TYPE_U32, OFFSETOF(mem_t,unit)},

  // Unavailable directly
  {"hugePagesTotal",MAP_TYPE_U32, OFFSETOF(mem_t,hugePagesTotal)},
  {"hugePagesFree", MAP_TYPE_U32, OFFSETOF(mem_t,hugePagesFree)},
  {"hugePagesReserved",
                    MAP_TYPE_U32, OFFSETOF(mem_t,hugePagesReserved)},
  {"hugePagesSurplus",
                    MAP_TYPE_U32, OFFSETOF(mem_t,hugePagesSurplus)},
  {"hugePageSize",  MAP_TYPE_U32, OFFSETOF(mem_t,hugePageSize)},
};
static int    s_mapCnt    = COUNT(s_map);

#ifdef  USE_LKB_DIFF
static bool   s_haveLast  = FALSE;
static mem_t  s_memLast;
#endif // USE_LKB_DIFF

/**
 *  The set of non-exported kernel functions and variables we use
 *
 *  These are resolved and associated with the related s_kern entry in
 *  lkb_mem_init().
 */
static Ksyms_t s_syms[]    = {
  // Methods
  /* 0 */ { "si_swapinfo" },
  /* 1 */ { "total_swapcache_pages" },
  /* 2 */ { "vm_commit_limit" },

  // Variables
  /* 3 */ { "vm_committed_as" },
  /* 4 */ { "hstates" },
  /* 5 */ { "default_hstate_idx" },
};
static int      s_numSyms   = (sizeof(s_syms) / sizeof(s_syms[0]));

/**
 *  Resolved function accessor.
 *
 *  These are associated with the resolved s_syms entry in lkb_mem_init().
 */
static struct {
  /************************************************************
   * Methods
   *
   * include/linux/swap.h
   */
  void          (*si_swapinfo)            (struct sysinfo *);
  unsigned long (*total_swapcache_pages)  (void);
  // include/linux/mman.h
  unsigned long (*vm_commit_limit)        (void);

  /************************************************************
   * Variables
   *
   * include/linux/mman.h
   */
  struct percpu_counter* vm_committed_as;
  // include/linux/hugetlb.h
  struct hstate*         hstates;
  unsigned int*          default_hstate_idx;
} s_kern;

/*****************************************************************************
 * Private methods {
 *
 */

/**
 *  @method _mem_gather
 *  @param  data    A pointer to the memory data to fill {mem_t*};
 *
 *  @return void
 *  @private
 */
static void
_mem_gather( mem_t* data ) {
  struct sysinfo  info;
  u32             available;
  u32             pages[ NR_LRU_LISTS ];
  int             lru;
  u32             committed;
  s32             cached;

  si_meminfo(  &info );

  /* Unavailable directly {
   *  `si_swapinfo()`, `vm_committed_as`, `vm_commit_limit()`
   */
  s_kern.si_swapinfo( &info );
  committed = percpu_counter_read_positive( s_kern.vm_committed_as );

  data->commitLimit = lkb_bytes( s_kern.vm_commit_limit() );
  data->commited    = lkb_bytes( committed );
  // Unavailable directly } */

  available = lkb_bytes( si_mem_available() );

  /* Unavailable directly {
   *  `total_swapcache_pages()`
   */
  cached    = GLOBAL_NODE_PAGE_STATE( NR_FILE_PAGES)
                - s_kern.total_swapcache_pages() - info.bufferram;

  if (cached < 0) { cached = 0; }

  data->cached     = lkb_bytes( cached );
  data->swapCached = lkb_bytes( s_kern.total_swapcache_pages() );
  // Unavailable directly } */

  for (lru = LRU_BASE; lru < NR_LRU_LISTS; lru++) {
    pages[lru] = GLOBAL_NODE_PAGE_STATE( NR_LRU_BASE + lru );
  }

  // All internal measurements are in pages (info.mem_unit)
  data->total        = lkb_bytes( info.totalram );
  data->free         = lkb_bytes( info.freeram );
  data->available    = available;
  data->used         = data->total - data->free;
  data->actualUsed   = data->total - data->available;
  data->buffer       = lkb_bytes( info.bufferram );
  data->shared       = lkb_bytes( info.sharedram );

  data->swapTotal    = lkb_bytes( info.totalswap );
  data->swapFree     = lkb_bytes( info.freeswap );
  data->swapUsed     = data->swapTotal - data->swapFree;

  if (data->swapFree > data->swapTotal || data->swapUsed > data->swapTotal) {
    /* :XXX: We're seeing info.
     *          totalswap   =  0
     *          freeswap    =  1
     *          => swapUsed = -1 (4294967295)
     *
    printk(KERN_INFO "%s unexpected values for swap memory - "
                          "total[ %u ], free[ %u ], used[ %u ]."
                          "  Setting swap used to 0.",
           DEVICE_NAME, data->swapTotal, data->swapFree, data->swapUsed);
    // */

    data->swapFree = data->swapTotal;
    data->swapUsed = 0;
  }

  //#ifdef  CONFIG_HIGHMEM {
  data->highTotal = lkb_bytes( info.totalhigh );
  data->highFree  = lkb_bytes( info.freehigh );

  data->lowTotal  = data->total - data->highTotal;
  data->lowFree   = data->free  - data->highFree;
  //data->lowTotal  = info.totalram - info.totalhigh;
  //data->lowFree   = info.freeram  - info.freehigh;
  //#endif  // CONFIG_HIGHMEM }
  //
#ifdef  NODE_PAGE_STATE
  data->kernStack         = GLOBAL_NODE_PAGE_STATE( NR_KERNEL_STACK_KB ) << 10;
#else   // ! NODE_PAGE_STATE
  data->kernStack         = GLOBAL_NODE_PAGE_STATE( NR_KERNEL_STACK ) *
                            THREAD_SIZE;
#endif  // NODE_PAGE_STATE
  data->vmallocTotal      = VMALLOC_TOTAL;

  // Pages : Active / Inactive
  data->active       = pages[LRU_ACTIVE_ANON]   + pages[LRU_ACTIVE_FILE];
  data->inactive     = pages[LRU_INACTIVE_ANON] + pages[LRU_INACTIVE_FILE];

  // Active(anon) / Inactive(anon)
  data->activeAnon   = pages[LRU_ACTIVE_ANON];
  data->inactiveAnon = pages[LRU_INACTIVE_ANON];

  // Active(file) / Inactive(file)
  data->activeFile   = pages[LRU_ACTIVE_FILE];
  data->inactiveFile = pages[LRU_INACTIVE_FILE];

  data->unevictable  = pages[LRU_UNEVICTABLE];
  data->mlock        = GLOBAL_NODE_PAGE_STATE(NR_MLOCK);

  data->dirty             = GLOBAL_NODE_PAGE_STATE( NR_FILE_DIRTY );
  data->writeback         = GLOBAL_NODE_PAGE_STATE( NR_WRITEBACK );
#ifdef  NODE_PAGE_STATE
  data->anonMapped        = GLOBAL_NODE_PAGE_STATE( NR_ANON_MAPPED );
#endif  // NODE_PAGE_STATE
  data->nfsUnstable       = GLOBAL_NODE_PAGE_STATE( NR_UNSTABLE_NFS );

  data->slabReclaimable   = GLOBAL_NODE_PAGE_STATE( NR_SLAB_RECLAIMABLE );
  data->slabUnreclaimable = GLOBAL_NODE_PAGE_STATE( NR_SLAB_UNRECLAIMABLE );
  data->slab              = data->slabReclaimable + data->slabUnreclaimable;

  data->pageTables        = GLOBAL_NODE_PAGE_STATE( NR_PAGETABLE );
  data->bounce            = GLOBAL_NODE_PAGE_STATE( NR_BOUNCE );
  data->writebackTmp      = GLOBAL_NODE_PAGE_STATE( NR_WRITEBACK_TEMP );

  data->unit              = info.mem_unit;

  /* Unavailable directly {
   *  `hstates` is NOT exported making `default_hstate` unavailable
   */
  if (hugepages_supported()) {
    /* #define default_hstate (hstates[default_hstate_idx])
     * struct hstate* hs = &default_hstate;
     */
    struct hstate* hs = &(s_kern.hstates[*s_kern.default_hstate_idx]);

    data->hugePagesTotal    = hs->nr_huge_pages;
    data->hugePagesFree     = hs->free_huge_pages;
    data->hugePagesReserved = hs->resv_huge_pages;
    data->hugePagesSurplus  = hs->surplus_huge_pages;
    data->hugePageSize      = 1UL << (huge_page_order(hs) + PAGE_SHIFT);
  }
  // Unavailable directly } */
}

/* Private methods }
 *****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_mem_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_mem_init( void ) {
  lkb_resolve( s_syms, s_numSyms );

  // :XXX: Make the s_syms entries match the s_kern expectations!!
  // Methods
  s_kern.si_swapinfo            = s_syms[0].ptr;
  s_kern.total_swapcache_pages  = s_syms[1].ptr;
  s_kern.vm_commit_limit        = s_syms[2].ptr;

  // Variables
  s_kern.vm_committed_as        = s_syms[3].ptr;
  s_kern.hstates                = s_syms[4].ptr;
  s_kern.default_hstate_idx     = s_syms[5].ptr;

  printk(KERN_INFO "%s mem_init(): %lu bytes / beat\n",
                   DEVICE_NAME, sizeof(mem_t));
}

/**
 *  Gather and write memory metrics as JSON to the circular buffer.
 *  @method lkb_mem_json
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_mem_json( void ) {
  ssize_t written   = 0;
  bool    needComma = 0;
  int     idex;
  mem_t   mem;
#ifdef  MEASURE_PERFORMANCE
  u64     start     = lkb_timestamp();
  u64     end;
#endif  // MEASURE_PERFORMANCE

  memset( &mem, 0, sizeof(mem) );

  _mem_gather( &mem );

  /* Write as JSON the memory measurements which have changed since the last
   * beat.
   */
  written += lkb_write( "{", 1 );

  for (idex = 0; idex < s_mapCnt; idex++) {
    map_t*  entry     = &s_map[idex];
    void*   cur       = (void*)&mem;
    bool    doOutput  = (
#ifdef  USE_LKB_DIFF
        !s_haveLast || mapDiff( entry, cur, (void*)&s_memLast )
#else  // USE_LKB_DIFF
        TRUE
#endif // USE_LKB_DIFF
    );

    if ( doOutput ) {
      if (needComma)  { written += lkb_write( ",", 1 ); }

      written  += json_write_map( entry, cur );
      needComma = 1;
    }
  }

  written += lkb_write( "}", 1 );

#ifdef  USE_LKB_DIFF
  memcpy( &s_memLast, &mem, sizeof(s_memLast) );
  s_haveLast = TRUE;
#endif // USE_LKB_DIFF

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s mem_json()      : %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_mem_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_mem_reset( void ) {
#ifdef  USE_LKB_DIFF
  s_haveLast = FALSE;
#endif  // USE_LKB_DIFF
}
/* Module methods }
 *****************************************************************************/

/**
 *  Expose this as an lkb module.
 */
Module_t module_mem = {
  .name   = "mem",
  .init   = lkb_mem_init,
  .json   = lkb_mem_json,
  .reset  = lkb_mem_reset,
};
