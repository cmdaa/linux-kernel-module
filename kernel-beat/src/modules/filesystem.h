/**
 *  Module: filesystems, fsstats
 *
 *  Beat-driven modules that collect filesystem metrics:
 *    - filesystems   collects the set of mounted filesystems with metrics
 *                    for each;
 *    - fsstats       collects overall filesystem usage metrics;
 *
 *  This module:
 *    - makes use of non-public, though exported, kernel symbols:
 *      - iterate_mounts()
 *
 *    - makes use of a hash table to monitor the set of active filesystems;
 *    - employes protections, via _isMountValid(), to ensure a VFS mount entry
 *      is in-fact valid;
 *
 *
 *  :NOTE: Unless this module is compiled using full kernel sources
 *         (indicated via the USE_KERNEL_SOURCES #define), a fragile
 *         KLUDGE is used to provide access to `struct mount` needed
 *         in order to access information about filesystem mount points.
 */
#ifndef _FILESYSTEM_H_
#define _FILESYSTEM_H_

/**
 *  A definition for the per-filesystem module.
 */
extern Module_t module_filesystem;

/**
 *  A definition for the overall stats module.
 */
extern Module_t module_fsstat;

#endif  // _FILESYSTEM_H_
