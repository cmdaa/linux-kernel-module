/**
 *  Module: processes, process_summary
 *
 *  Beat-driven modules that collect process metrics:
 *    - processes       collects the set of active processes with metrics for
 *                      each;
 *    - process_summary collects overall process metrics;
 *
 *  This module:
 *    - makes use of non-public, though exported, kernel symbols:
 *      - get_task_cred()
 *      - get_files_struct()
 *      - put_files_struct()
 *      - access_remote_vm()
 *
 *    - makes use of a hash table to monitor the set of active processes;
 */
#include "../lkb.h"

#include <linux/slab.h>       // kmalloc() / kfree()
#include <linux/sched.h>      // task_struct
#include <linux/fdtable.h>    /* get_files_struct(), put_files_struct(),
                               * files_fdtable()
                               */
#include <linux/time.h>       // timespec_add_ns(), timespec_to_ns()
#include <linux/bitops.h>     // fls()
#include <linux/mm.h>         // access_remote_vm()

#ifdef  USE_LKB_DIFF
# include <linux/hash.h>      // hash_long()
#endif  // USE_LKB_DIFF

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,10,0)
/* Linux 4.10.0
 *  6e84f31522f931027bf695752087ece278c10d3f : 4.10.0 : 2017.03.02
 *    We are going to split <linux/sched/mm.h> out of <linux/sched.h>, which
 *    will have to be picked up from other headers and a couple of .c files.
 *
 *    Create a trivial placeholder <linux/sched/mm.h> file that just
 *    maps to <linux/sched.h> to make this patch obviously correct and
 *    bisectable.
 *
 *    The APIs that are going to be moved first are:
 *
 *       mm_alloc()
 *       __mmdrop()
 *       mmdrop()
 *       mmdrop_async_fn()
 *       mmdrop_async()
 *       mmget_not_zero()
 *       mmput()
 *       mmput_async()
 *       get_task_mm()
 *       mm_access()
 *       mm_release()
 */
# include <linux/sched/mm.h>  // get_task_mm();

#endif

#include "../map.h"
#include "../json.h"

#include "proc.h"
#include "cgroup_v1.h"

// :NOTE: If additional fields are added, `s_mapSum` MUST also be changed.
typedef struct {
  u32   total;
  u32   sleeping;
  u32   running;
  u32   idle;
  u32   stopped;
  u32   zombie;
  u32   dead;
  u32   unknown;
} procSum_t;

/**
 *  The mapping from `proc_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all fields from `proc_t` MUST be identified.
 */
static map_t  s_mapProc[]   = {
  {"name",            MAP_TYPE_STR, OFFSETOF(proc_t, name),SIZEOF(proc_t,name)},

  {"state",           MAP_TYPE_S32, OFFSETOF(proc_t, state)},
  {"pid",             MAP_TYPE_S32, OFFSETOF(proc_t, pid)},
  {"ppid",            MAP_TYPE_S32, OFFSETOF(proc_t, ppid)},
  {"pgrp",            MAP_TYPE_S32, OFFSETOF(proc_t, pgrp)},

  {"uid",             MAP_TYPE_U32, OFFSETOF(proc_t, uid)},

#ifdef  CONFIG_CGROUPS  // {
  /* By using MAP_TYPE_SPC and including a size, we allow the use of mapDiff()
   * which will perform a simple memcmp()
   */
  {"cgroups",         MAP_TYPE_SPC, OFFSETOF(proc_t, cgroups),
                                                      SIZEOF(proc_t,cgroups)},
#endif  // CONFIG_CGROUPS  }

  {"cpuStart",        MAP_TYPE_U64, OFFSETOF(proc_t, cpuStart)},
  {"cpuUser",         MAP_TYPE_U32, OFFSETOF(proc_t, cpuUser)},
  {"cpuSys",          MAP_TYPE_U32, OFFSETOF(proc_t, cpuSys)},

  {"fdOpen",          MAP_TYPE_U32, OFFSETOF(proc_t, fdOpen)},
  {"fdMax",           MAP_TYPE_U32, OFFSETOF(proc_t, fdMax)},
  {"fdLimitHard",     MAP_TYPE_U32, OFFSETOF(proc_t, fdLimitHard)},
  {"fdLimitSoft",     MAP_TYPE_U32, OFFSETOF(proc_t, fdLimitSoft)},

  {"memRss",          MAP_TYPE_U64, OFFSETOF(proc_t, memRss)},
  {"memShare",        MAP_TYPE_U64, OFFSETOF(proc_t, memShare)},
  {"memSize",         MAP_TYPE_U64, OFFSETOF(proc_t, memSize)},

  {"memFaultsMinor",  MAP_TYPE_U32, OFFSETOF(proc_t, memFaultsMinor)},
  {"memFaultsMinor",  MAP_TYPE_U32, OFFSETOF(proc_t, memFaultsMinor)},
  {"memFaultsMajor",  MAP_TYPE_U32, OFFSETOF(proc_t, memFaultsMajor)},

  {"cmdLine",         MAP_TYPE_SPC, OFFSETOF(proc_t, hashCmdline)},
};
static int    s_mapProcCnt  = COUNT(s_mapProc);

/**
 *  The mapping from `procSum_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all fields from `procSum_t` MUST be identified.
 */
static map_t  s_mapSum[]      = {
  {"total",     MAP_TYPE_U32, OFFSETOF(procSum_t, total)},
  {"sleeping",  MAP_TYPE_U32, OFFSETOF(procSum_t, sleeping)},
  {"running",   MAP_TYPE_U32, OFFSETOF(procSum_t, running)},
  {"idle",      MAP_TYPE_U32, OFFSETOF(procSum_t, idle)},
  {"stopped",   MAP_TYPE_U32, OFFSETOF(procSum_t, stopped)},
  {"zombie",    MAP_TYPE_U32, OFFSETOF(procSum_t, zombie)},
  {"dead",      MAP_TYPE_U32, OFFSETOF(procSum_t, dead)},
  {"unknown",   MAP_TYPE_U32, OFFSETOF(procSum_t, unknown)},
};
static int        s_mapSumCnt = COUNT(s_mapSum);

// Track memory allocation
static u64  s_bytesAlloc = 0;
static u64  s_bytesFreed = 0;

#ifdef  USE_LKB_DIFF
static bool       s_haveSumLast = FALSE;
static procSum_t  s_procSumLast;
#endif // USE_LKB_DIFF

/* :NOTE: Filled via `lkb_proc_json()` for use by `lkb_procSum_json()`.
 *
 *        `lkb_proc_json()` MUST be invoked BEFORE `lkb_procSum_json()` in
 *        order to gather the stats that will be reported.
 */
static bool       s_haveSumCur  = FALSE;
static procSum_t  s_procSumCur;

/**
 *  The set of non-exported kernel functions and variables we use
 *
 *  These are resolved and associated with the related s_kern entry in
 *  lkb_proc_init().
 */
static Ksyms_t s_syms[]    = {
  { "get_task_cred" },
  { "get_files_struct" },
  { "put_files_struct" },
  { "access_remote_vm" },
};
static int      s_numSyms   = COUNT(s_syms);

/**
 *  Resolved function accessor.
 *
 *  These are associated with the resolved s_syms entry in lkb_proc_init().
 */
static struct {
  // include/linux/cred.h
  const struct cred*    (*get_task_cred)    (struct task_struct *);

  // include/linux/fdtable.h
  struct files_struct*  (*get_files_struct) (struct task_struct *);
  void                  (*put_files_struct) (struct files_struct *);

  // include/linux/mm.h
  int                   (*access_remote_vm) (struct mm_struct* mm,
                                             unsigned long addr,
                                             void* buf,
                                             int len,
                                             unsigned int gup_flags);

} s_kern;

/* Process/task states (kernel/include/linux/sched.h):
 *  task.state (runnability), task.exit_state (exit status):
 *    0     TASK_RUNNING          R (running)
 *    1     TASK_INTERRUPTIBLE    S (sleeping)
 *    2     TASK_UNINTERRUPTIBLE  D (disk sleep)
 *    4     __TASK_STOPPED        T (stopped)
 *    8     __TASK_TRACED         t (tracing stop)
 *    16    EXIT_DEAD             X (dead)
 *    32    EXIT_ZOMBIE           Z (zombie)
 *    48    EXIT_TRACE              (DEAD | ZOMBIE)
 *    64    TASK_DEAD             x
 *    128   TASK_WAKEKILL         K
 *    256   TASK_WAKING           W
 *    512   TASK_PARKED           P
 *    1024  TASK_NOLOAD           N
 *    2048  TASK_NEW              n
 *    4096  TASK_STATE_MAX
 */
static const char* const s_states[] = {
  "R (running)",      // 0     TASK_RUNNING
  "S (sleeping)",     // 1     TASK_INTERRUPTIBLE
  "D (disk sleep)",   // 2     TASK_UNINTERRUPTIBLE
  "T (stopped)",      // 4     __TASK_STOPPED
  "t (tracing stop)", // 8     __TASK_TRACED
  "X (dead)",         // 16    EXIT_DEAD
  "Z (zombie)",       // 32    EXIT_ZOMBIE
  // The rest are kernel-based and NOT reported by /proc
  "x (dead)",         // 64    TASK_DEAD
  "K (wake kill)",    // 128   TASK_WAKEKILL
  "W (waking)",       // 256   TASK_WAKING
  "P (parked)",       // 512   TASK_PARKED
  "N (noload)",       // 1024  TASK_NOLOAD
  "n (new)",          // 2048  TASK_NEW
};
static int        s_numStates = COUNT(s_states);

// :NOTE: This MUST agree with the maximum string in `s_states`
#define MAX_STATE_LEN   17

/*****************************************************************************
 * Private methods {
 *
 */

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,10,0)
/* Linux 4.10.0
 *  Sometime around
 *    6e84f31522f931027bf695752087ece278c10d3f : 4.10.0 : 2017.03.02
 *
 *  Removed cputime_to_clock_t() in favor of nsec_to_clock_t()
 *
 *  nsec_to_clock_t() is NOT exported so we have to do it ourselves and since
 *  we do, keep the old name (cputime_to_clock_t()).
 */
typedef unsigned long __nocast cputime_t;

static u64
cputime_to_clock_t(u64 x) {

#if (NSEC_PER_SEC % USER_HZ) == 0
  return div_u64(x, NSEC_PER_SEC / USER_HZ);
#elif (USER_HZ % 512) == 0
  return div_u64(x * USER_HZ / 512, NSEC_PER_SEC / 512);
#else
  /*
   * max relative error 5.7e-8 (1.8s per year) for USER_HZ <= 1024,
   * overflow after 64.99 years.
   * exact for HZ=60, 72, 90, 120, 144, 180, 300, 600, 900, ...
   */
  return div_u64(x * 9, (9ull * NSEC_PER_SEC + (USER_HZ / 2)) / USER_HZ);
#endif
}
#endif

/****************************************************************
 * PID hash table to store the set of normalized processes that
 * were active last beat.
 *
 *
 */
#ifdef  USE_LKB_DIFF // {

/**
 *  Generate a hash from a process id (pid)
 */
#define proc_hashfn(pid)  hash_long((unsigned long)pid, s_procHash_shift)

/**
 *  Our pid hash table and shift/size information.
 */
static struct hlist_head* s_procHash;
static unsigned int       s_procHash_shift = 10; // default to 1024 buckets

/**
 *  Initialize the process hash table, scaled according to the amount of
 *  system memory from a minimum of 16 slots up to 4096 slots for 1GB or more.
 *  @method _procHash_init
 *
 *  @return void
 *  @private
 */
static void
_procHash_init( void ) {
  unsigned int  hashEntries = 1U << s_procHash_shift;
  u32           nBytes      = sizeof(*s_procHash) * hashEntries;
  unsigned int  idex;

  s_procHash = kzalloc( nBytes, GFP_KERNEL );
  s_bytesAlloc += nBytes;

  // /*
  printk(KERN_INFO "%s _procHash_init(): initialize %u hash buckets, "
                        "%u bytes\n",
                   DEVICE_NAME, hashEntries, nBytes);
  // */

  for (idex = 0; idex < hashEntries; idex++) {
    INIT_HLIST_HEAD( &s_procHash[idex] );
  }
}

/**
 *  Check if a process with the given `pid` exists in our hash table.
 *  @method _procHash_find
 *  @param  pid     The pid of the target process {s32};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return A pointer to the hashed process if it exists {proc_t*};
 *  @private
 */
static proc_t*
_procHash_find( s32 pid ) {
  unsigned long hash  = proc_hashfn( pid );
  proc_t*       proc;

  hlist_for_each_entry_rcu( proc, &s_procHash[ hash ], proc_chain) {
    if (proc->pid == pid) {
      return proc;
    }
  }

  return NULL;
}

/**
 *  Create a dynamically allocated clone of the given process structure.
 *  @method _procHash_clone
 *  @param  proc    The process to clone {proc_t*};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @note   This is generally needed because the incoming `proc` exists on the
 *          stack and now needs to exist in dynamic memory for storage in the
 *          hash table.
 *
 *  @return A dynamically allocated version of `proc` {proc_t*};
 *  @private
 */
static proc_t*
_procHash_clone( proc_t* proc ) {
  u32     nBytes  = sizeof(*proc);
  proc_t* newProc = (proc_t*)kmalloc( nBytes, GFP_KERNEL );

  s_bytesAlloc += nBytes;

  /*
  printk(KERN_INFO "%s _procHash_clone(): allocate %u bytes for proc_t: %p\n",
                   DEVICE_NAME, nBytes, newProc);
  // */

  memcpy( newProc, proc, sizeof(*proc) );

  return newProc;
}

/**
 *  Add the given process to our hash table.
 *  @method _procHash_add
 *  @param  proc    The process to clone {proc_t*};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return A dynamically allocated clone of `proc` {proc_t*};
 *  @private
 */
static proc_t*
_procHash_add( proc_t* proc ) {
  proc_t*             clone = _procHash_clone( proc );
  struct hlist_node*  node  = &(clone->proc_chain);
  unsigned long       hash  = proc_hashfn( clone->pid );

  //hash_add( s_procHash, clone, clone->pid );
  hlist_add_head_rcu( node, &s_procHash[ hash ] );

  return clone;
}

/**
 *  Invoked via `call_rcu()` to perform the final storage free.
 *  @method __procHash_del_rcu
 *  @param  rcu   The head of the RCU list {struct rcu_head*};
 *
 *  @return void
 *  @private
 */
static void
__procHash_del_rcu( struct rcu_head* rcu ) {
  proc_t* proc = container_of( rcu, proc_t, proc_rcu );

  /*
  printk(KERN_INFO "%s _procHash_del_rcu(): free proc_t: %p\n",
                   DEVICE_NAME, proc);
  // */

  s_bytesFreed += sizeof( *proc );

  kfree( proc );
}

/**
 *  Remove the given process from our hash table and free the storage.
 *  @method _procHash_del
 *  @param  proc    The target process {proc_t*};
 *
 *  @note   rcu_read_lock() MUST be held prior to calling this method.
 *
 *  @return void
 *  @private
 */
static void
_procHash_del( proc_t* proc ) {
  //hash_del( &proc->proc_chain );
  hlist_del_rcu( &(proc->proc_chain) );

  call_rcu( &proc->proc_rcu, __procHash_del_rcu );
}

/**
 *  Replace the contents of the old entry with that of the new.
 *  @method _procHash_replace
 *  @param  oldProc   The entry to replace {proc_t*};
 *  @param  newProc   The entry to clone and add {proc_t*};
 *
 *  @note   If NO_INPLACE_REPLACE is defined, this will incur the cost of
 *          a full allocation (_procHash_clone()) and rcu-based deletion.
 *
 *          Otherwise, it will replace the contents of `oldProc` with that of
 *          `newProc` maintaining the existing list pointers.
 *
 *  @return A dynamically allocated duplicate of `newProc` {proc_t*};
 *  @private
 */
static proc_t*
_procHash_replace( proc_t* oldProc, proc_t* newProc ) {

#ifndef NO_INPLACE_REPLACE // {
  /* Replace the content of `oldProc` with that of `newProc` making sure we
   * properly maintain the list pointers.
   *
   * This avoids the cost of a  full allocation and rcu-based deletion.
   */
  rcu_read_lock();
    // Save the list pointers
    memcpy( &newProc->proc_chain, &oldProc->proc_chain,
                                  sizeof(newProc->proc_chain));

    // Copy in the new data
    memcpy( oldProc, newProc, sizeof(*newProc) );
  rcu_read_unlock();

  return oldProc;

#else   // NO_INPLACE_REPLACE }{

  // Create a clone, replace and delete the old entry
  proc_t*  clone = _procHash_clone( newProc );

  rcu_read_lock();

     hlist_replace_rcu( &oldProc->proc_chain, &clone->proc_chain );

     // Schedule resource cleanup for old
     call_rcu( &oldProc->proc_rcu, __procHash_del_rcu );

  rcu_read_unlock();

  return clone;

#endif  // NO_INPLACE_REPLACE }

}

/**
 *  Remove all entries from the hash table.
 *  @method _procHash_clear
 *
 *  @return void
 *  @private
 */
static void
_procHash_clear( void ) {
  unsigned int  hashEntries = 1U << s_procHash_shift;
  unsigned int  idex;
  proc_t*       proc;

  /*
  printk(KERN_INFO "%s _procHash_clear(): clear %u hash buckets\n",
                   DEVICE_NAME, hashEntries);
  // */

  rcu_read_lock();

  for (idex = 0; idex < hashEntries; idex++) {

    hlist_for_each_entry_rcu( proc, &s_procHash[idex], proc_chain ) {
      _procHash_del( proc );
    }

    INIT_HLIST_HEAD( &s_procHash[idex] );
  }

  rcu_read_unlock();
}

/**
 *  Walk the process hash/cache and evict any entry with a `lastBeat` that
 *  doesn't match the current `lkb.beat` value, meaning it wasn't on the active
 *  process list.
 *  @method _procHash_sweep
 *
 *  @note   THIS method will obtain and release rcu_read_lock().
 *
 *  @return void
 *  @private
 */
static void
_procHash_sweep( void ) {
  unsigned int  hashEntries = 1U << s_procHash_shift;
  unsigned int  idex;
  proc_t*       proc;

  rcu_read_lock();

  for (idex = 0; idex < hashEntries; idex++) {

    hlist_for_each_entry_rcu( proc, &s_procHash[idex], proc_chain ) {
      if (proc->lastBeat == lkb.beat) { continue; }

      // Evict this process entry
      printk(KERN_INFO "%s _procHash_sweep(): evict process %u\n",
                       DEVICE_NAME, proc->pid);

      _procHash_del( proc );
    }
  }

  rcu_read_unlock();
}

/**
 *  Free the process hash table along with all entries.
 *  @method _procHash_fini
 *
 *  @note   THIS method will obtain and release rcu_read_lock().
 *
 *  @return void
 *  @private
 */
static void
_procHash_fini( void ) {
  unsigned int  hashEntries = 1U << s_procHash_shift;

  /*
  printk(KERN_INFO "%s _procHash_fini(): clear/free %u hash buckets\n",
                   DEVICE_NAME, hashEntries);
  // */

  // Empty the hash table
  _procHash_clear();

  // Wait for __procHash_del_rcu() callbacks to finish freeing all entries
  synchronize_rcu();

  /*
  printk(KERN_INFO "%s _procHash_fini(): free s_procHash: %p\n",
                   DEVICE_NAME, s_procHash);
  // */

  s_bytesFreed += (sizeof(*s_procHash) * hashEntries);

  // Free the bucket memory
  kfree( s_procHash );
}
#endif // USE_LKB_DIFF }
/****************************************************************/

/**
 *  Given a process state, return the string representation of that state.
 *  @method _proc_stateStr
 *  @param  state   The process/task state {long};
 *
 *  @note   The value expected by `state` originates in `struct task_struct` as
 *          (task->state | task->exit_state). This value is placed into
 *          `proc_t` as simply `state` by `proc_gather()`.
 *
 *  @note   This will only produce a subset from `s_states` due to the use of
 *          TASK_REPORT as a mask:
 *            R (running)        // 0     TASK_RUNNING
 *            S (sleeping)       // 1     TASK_INTERRUPTIBLE
 *            D (disk sleep)     // 2     TASK_UNINTERRUPTIBLE
 *            T (stopped)        // 4     __TASK_STOPPED
 *            t (tracing stop)   // 8     __TASK_TRACED
 *            X (dead)           // 16    EXIT_DEAD
 *            Z (zombie)         // 32    EXIT_ZOMBIE
 *
 *  @return The string representation of the task's current state {char*};
 *  @private
 */
static const char*
_proc_stateStr( long state ) {
  /* #define TASK_REPORT   (TASK_RUNNING         | \
   *                        TASK_INTERRUPTIBLE   | \
   *                        TASK_UNINTERRUPTIBLE | \
   *                        __TASK_STOPPED       | \
   *                        __TASK_TRACED        | \
   *                        EXIT_DEAD            | \
   *                        EXIT_ZOMBIE)
   *
   * :NOTE: Parked tasks do not run; they sit in __kthread_parkme().
   *        Without this check, we would report them as running, which is
   *        clearly wrong, so we report them as sleeping instead.
   */
  long          nState  = (state & TASK_PARKED
                            ? TASK_INTERRUPTIBLE
                            : state & TASK_REPORT);
  unsigned int  idex    = fls( nState );
  if (idex >= s_numStates) {
    return "unknown";
  }

  return s_states[idex];
}

/**
 *  Given a task structure, return the string representation of the task's
 *  current state.
 *  @method proc_taskStateStr
 *  @param  task    A pointer to the target task {struct task_struct*};
 *
 *  @return The string representation of the task's current state {char*};
 *  @private
 */
static const char*
_proc_taskStateStr( struct task_struct* task ) {
  return _proc_stateStr( task->state | task->exit_state );

#if 0
  unsigned int  state = (task->state | task->exit_state) & TASK_REPORT;
  unsigned int  idex;

  /* Parked tasks do not run; they sit in __kthread_parkme().
   * Without this check, we would report them as running, which is clearly
   * wrong, so we report them as sleeping instead.
   */
  if (task->state == TASK_PARKED) {
    state = TASK_INTERRUPTIBLE;
  }

  idex = fls( state );
  if (idex >= s_numStates) {
    return "unknown";
  }
  return s_states[idex];
#endif
}

/**
 *  Given a target task and normalized process data, determine whether the
 *  process has been observed, if so what has changed, and output a JSON
 *  version of either the entire process state if it hasn't been observed
 *  before, or changes from the previous state.
 *  @method _proc_json_diff
 *  @param  task          A pointer to the target task {struct task_struct*};
 *  @param  proc          A pointer to the normalized process data {proc_t*};
 *  @param  needTopComma  If true, a comma is needed before any output {bool};
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @private
 */
static ssize_t
_proc_json_diff( struct task_struct* task, proc_t* proc, bool needTopComma ) {
  ssize_t   written     = 0;
  u32       changes     = 0;
  cmdline_t cmdline;
#ifdef  USE_LKB_DIFF
  proc_t*   prevProc    = NULL;
  int       idex;
#endif  // USE_LKB_DIFF

  // Initialize cmdline gathering to avoid truncation
  memset( &cmdline, 0, sizeof(cmdline) );
  cmdline.noTrunc = TRUE;

  // Fetch the command line and compute its hash
  proc_getCmdline( task, proc, &cmdline );

  // Remember the command line hash
  proc->hashCmdline = cmdline.hash;

#ifdef  USE_LKB_DIFF  // {
  // See if we have a cached version of the target process
  rcu_read_lock();
  prevProc = _procHash_find( proc->pid );

  if (prevProc) {
    // Identify any changes.
    void* cur   = (void*)proc;
    void* prev  = (void*)prevProc;

    for (idex = 0; idex < s_mapProcCnt; idex++) {
      map_t*  entry = &s_mapProc[idex];

      entry->changed = FALSE;

      if (entry->offset == OFFSETOF(proc_t, hashCmdline)) {

        if (cmdline.len > 0 && cmdline.hash != prevProc->hashCmdline) {
          entry->changed = TRUE;
          changes++;

        }

      } else if (mapDiff( entry, cur, prev )) {
        /*
        if (entry->offset == OFFSETOF(proc_t, state)) {
          printk(KERN_INFO "%s _proc_json_diff( %u ): state[ %d => %d ]\n",
                     DEVICE_NAME, proc->pid, prevProc->state, proc->state);
        }
        // */

        entry->changed = TRUE;
        changes++;
      }
    }

    // Replace the current cache, ensuring a dynamically allocated `proc`.
    proc = _procHash_replace( prevProc, proc );

  } else {
    /*
    printk(KERN_INFO "%s _proc_json_diff( %u ): no previous state\n",
                     DEVICE_NAME, proc->pid);
    // */

    // Add a new cache entry
    proc = _procHash_add( proc );

    // Mark *all* members "changed"
    changes = s_mapProcCnt;
  }

  rcu_read_unlock();

#else   // USE_LKB_DIFF }{
  changes = s_mapProcCnt;

#endif  // USE_LKB_DIFF }

  // Update the `lastBeat` value for this cache entry.
  proc->lastBeat = lkb.beat;

  if (changes < 1) {
    // There were no changes at all for this process so don't output anything
    goto done;
  }

  if (needTopComma) { written += lkb_write( ",", 1 ); }

  written += lkb_write( "{", 1 );

  written += proc_json( proc, &cmdline, changes );

  written += lkb_write( "}", 1 );

done:
  // Release the full command line string if dynamically allocated
  proc_releaseCmdline( &cmdline );
  return written;
}

/* Private methods }
 *****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_proc_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_proc_init( void ) {
  lkb_resolve( s_syms, s_numSyms );

  // :XXX: Make the s_syms entries match the s_kern expectations!!
  s_kern.get_task_cred    = s_syms[0].ptr;
  s_kern.get_files_struct = s_syms[1].ptr;
  s_kern.put_files_struct = s_syms[2].ptr;
  s_kern.access_remote_vm = s_syms[3].ptr;

#ifdef  USE_LKB_DIFF
  _procHash_init();
#endif  // USE_LKB_DIFF

  printk(KERN_INFO "%s proc_init(): %lu bytes / process\n",
                   DEVICE_NAME, sizeof(proc_t));
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_proc_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_proc_reset( void ) {
#ifdef  USE_LKB_DIFF
  _procHash_clear();
#endif  // USE_LKB_DIFF

  s_haveSumCur  = FALSE;
}

/**
 *  Finalize, cleaning up any allocated resources.
 *  @method lkb_proc_fini
 *
 *  @return void
 *  @protected
 */
static void
lkb_proc_fini( void ) {
#ifdef  USE_LKB_DIFF
  _procHash_fini( );
#endif  // USE_LKB_DIFF

  if (s_bytesAlloc - s_bytesFreed != 0) {
    printk(KERN_WARNING "%s lkb_proc_fini()   : "
                        "%7llu bytes allocated, %7llu freed : %llu LEAKED\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed,
                                     s_bytesAlloc - s_bytesFreed);
  } else {
    printk(KERN_INFO    "%s lkb_proc_fini()   : "
                        "%7llu bytes allocated, %7llu freed\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed);
  }
}

/**
 *  Gather and write system process summary metrics as JSON to the circular
 *  buffer.
 *  @method lkb_proc_json
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_proc_json( void ) {
  ssize_t             written   = 0;
  bool                needComma = 0;
  struct task_struct* task;
  procSum_t           summary   = { 0 };
#ifdef  MEASURE_PERFORMANCE
  u64     start     = lkb_timestamp();
  u64     end;
#endif  // MEASURE_PERFORMANCE

  // Write as JSON the load measurements which have changed since the last beat.
  written += lkb_write( "[", 1 );

  rcu_read_lock();    // {
    // Walk all processes and for each, invoke proc_gather( task, &proc );
    for_each_process( task ) {
      proc_t  proc;
      ssize_t procWritten;

      summary.total++;

      switch( _proc_taskStateStr( task )[0] ) {
        case 'R': summary.running++;  break;  // TASK_RUNNING
        case 'S': summary.sleeping++; break;  // TASK_INTERRUPTIBLE
        case 'D': summary.idle++;     break;  // TASK_UNINTERRUPTIBLE
        case 'T': summary.stopped++;  break;  // __TASK_STOPPED
        case 't': summary.stopped++;  break;  // __TASK_TRACED (tracing stop)
        case 'X': summary.dead++;     break;  // EXIT_DEAD
        case 'Z': summary.zombie++;   break;  // EXIT_ZOMBIE
        default:  summary.unknown++;  break;
      }

      // :XXX: DO NOT invoke task_lock( task ) -- it will lock the entire system
      proc_gather( task, &proc );

      procWritten = _proc_json_diff( task, &proc, needComma );
      if (procWritten > 0) {
        written  += procWritten;
        needComma = TRUE;
      }
    }
  rcu_read_unlock();  // }

  written += lkb_write( "]", 1 );

  // Save the collected summary information for lkb_procSum_json()
  memcpy( &s_procSumCur, &summary, sizeof(s_procSumCur) );
  s_haveSumCur = TRUE;

#ifdef  USE_LKB_DIFF
  // Evict any process that was not seen in the process table from our cache.
  _procHash_sweep();
#endif  // USE_LKB_DIFF

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s proc_json()     : %6ld bytes in %7llu ns, beat %u "
                                            "(%u procs)\n",
                   DEVICE_NAME, written, end - start, lkb.beat, summary.total);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_procSum_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_procSum_init( void ) {
  printk(KERN_INFO "%s procSum_init(): %lu bytes / beat\n",
                   DEVICE_NAME, sizeof(procSum_t));
}

/**
 *  Gather and write system process summary metrics as JSON to the circular
 *  buffer.
 *  @method lkb_procSum_json
 *
 *  @note   `lkb_proc_json()` MUST be invoked FIRST in order to gather the
 *          stats that will be reported in this summary.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_procSum_json( void ) {
  ssize_t   written   = 0;
#ifdef  MEASURE_PERFORMANCE
  u64       start     = lkb_timestamp();
  u64       end;
#endif  // MEASURE_PERFORMANCE

  // Write as JSON the load measurements which have changed since the last beat.
  written += lkb_write( "{", 1 );

  if (s_haveSumCur) {
    bool  needComma = 0;
    int   idex;

    for (idex = 0; idex < s_mapSumCnt; idex++) {
      map_t*  entry     = &s_mapSum[idex];
      void*   cur       = (void*)&s_procSumCur;
      bool    doOutput  = (
#ifdef  USE_LKB_DIFF
          !s_haveSumLast || mapDiff( entry, cur, (void*)&s_procSumLast )
#else  // USE_LKB_DIFF
          TRUE
#endif // USE_LKB_DIFF
      );

      if ( doOutput ) {
        if (needComma)  { written += lkb_write( ",", 1 ); }

        written  += json_write_map( entry, cur );
        needComma = TRUE;
      }
    }
  }

  written += lkb_write( "}", 1 );

#ifdef  USE_LKB_DIFF
  memcpy( &s_procSumLast, &s_procSumCur, sizeof(s_procSumLast) );
  s_haveSumLast = TRUE;
#endif // USE_LKB_DIFF

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s procSum_json()  : %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_procSum_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_procSum_reset( void ) {
#ifdef  USE_LKB_DIFF
  s_haveSumLast = FALSE;
#endif  // USE_LKB_DIFF
}

/* Module methods }
 *****************************************************************************
 * Public methods {
 *
 */

/**
 *  Generate the command line of a process from its set of arguments.
 *  @method proc_getCmdline
 *  @param  task                A pointer to the target task
 *                              {struct task_struct*};
 *  @param  proc                A pointer to the normalized process data
 *                              {proc_t*};
 *  @param  cmdline             A pointer to receive command line data
 *                              {cmdline_t*};
 *  @param  [cmdline->noTrunc]  If TRUE, retrieve the full command line,
 *                              otherwise, limit it to `MAX_CMDLINE_LEN`
 *                              {bool};
 *  @param  [cmdline->noHash]   If TRUE, do NOT compute the hash of the
 *                              gathered command line {bool};
 *
 *  @return void
 */
void
proc_getCmdline( struct task_struct* task, proc_t* proc, cmdline_t* cmdline ) {
  unsigned long     arg_start, arg_end;
  ssize_t           rv;
  char*             eos;
  struct mm_struct* mm    = get_task_mm( task );
  char*             cur;

  if (!mm) {
    // Cannot access task memory
    return;
  }

  if (!mm->env_end) {
    // The process has not spawned far enough to even have a command line
    goto done;
  }

  // Grab the start and end of the argument list
  down_read( &mm->mmap_sem );
    arg_start = mm->arg_start;
    arg_end   = mm->arg_end;
  up_read( &mm->mmap_sem );

  if (arg_start >= arg_end )  {
    // Empty argument list
    goto done;
  }
  cmdline->total = arg_end - arg_start;

  // Leave room for a terminating '...' if there is not enough room.
  cmdline->len = min( cmdline->total, sizeof(cmdline->_static) - 4 );

  if (cmdline->total > cmdline->len && cmdline->noTrunc) {
    // Allocate space for the full command line.
    /*
    printk(KERN_INFO "%s proc_getCmdline():noTrunc: allocate space for "
                                                    "%lu bytes\n",
                     DEVICE_NAME, cmdline->total);
    // */

    cmdline->str = kzalloc( cmdline->total + 1, GFP_KERNEL );
    cmdline->len = cmdline->total;

    s_bytesAlloc += cmdline->total + 1;

  } else {
    // Use the static buffer
    cmdline->str = cmdline->_static;

  }

  // The command line shares address space with code and data ...
  rv = s_kern.access_remote_vm( mm, arg_start,
                                cmdline->str, cmdline->len, FOLL_ANON );
  if (rv <= 0) {
    // Cannot access the process memory
    goto done;
  }

  /* Each individual argument will be separately null terminated so replace all
   * null termination characters prior to end-of-string with ' '.
   */
  cur = cmdline->str;
  eos = &(cur[rv]);
  while (cur < eos) {
    if (*cur == '\0') { *cur = ' '; }
    cur++;
  }

  if (cmdline->total > cmdline->len) {
    // The full command line is too large -- end it with "..."
    memcpy( cur, "...", 3 );
    cur += 3;
  }

  *cur = '\0';  // Ensure null termination

  // Set the final length
  cmdline->len = (size_t)(cur - cmdline->str);

  /*
  printk(KERN_INFO "%s proc_getCmdline():%s: total[ %lu ], len[ %lu ]\n",
                   DEVICE_NAME,
                   (cmdline->str == cmdline->_static
                      ? "static" : "allocd"),
                   cmdline->total, cmdline->len);
  // */

  if (cmdline->len > 0 && !cmdline->noHash) {
    // Compute the hash
    cmdline->hash = lkb_hash( proc->pid, cmdline->str, cmdline->len );

    /*
    printk(KERN_INFO "%s proc_getCmdline(): %lu [ %s ] : 0x%x\n",
                     DEVICE_NAME, cmdline->len, cmdline->str, cmdline->hash);
    // */
  }

done:
  // Release our reference to the task's memory
  mmput( mm );
}

/**
 *  Release/free any dynamically allocated data within the given command line
 *  generated via `proc_getCmdline()`;
 *  @method proc_releaseCmdline
 *  @param  cmdline The command line to release {cmdline_t*};
 *
 *  @return void
 */
void
proc_releaseCmdline( cmdline_t* cmdline ) {

  if (cmdline->str != NULL && cmdline->str != cmdline->_static) {
    // assert( cmdline->noTrunc == TRUE );
    // assert( cmdline->len     == cmdline->total );
    s_bytesFreed += cmdline->total + 1;

    kfree( cmdline->str );
  }

}

/**
 *  Output a JSON version of changed information for a single process.
 *  @method proc_json
 *  @param  proc      A pointer to the normalized process data {proc_t*};
 *  @param  cmdline   The collected command line of the process {cmdline_t*};
 *  @param  changes   The number of items changed {u32};
 *
 *  @note   If `changes` is provided, is NOT PROC_OUTPUT_ALL and is less than
 *          `s_mapProcCnt`, then change indicators in `s_mapProc` will be used;
 *
 *  @note   This method neither opens nor closes the JSON structure, it
 *          simply writes key/value pairs assuming there is no need for
 *          a starting comma.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 */
ssize_t
proc_json( proc_t* proc, cmdline_t* cmdline, u32 changes ) {
  ssize_t   written     = 0;
  bool      needComma   = FALSE;
  int       idex;

  /* :TODO: Write the JSON as:
   *  state:, pid:, ppid:, pgrp:, uid:, cmdline:,
   *  cpu  : { start:, total: },                // vice cpu%Name%
   *  fd   : { open:, limit: {hard:, soft:} },  // vice fd%Name%
   *  mem  : { rss:, share:, size:, faults: }   // vice mem%Name%
   */

  for (idex = 0; idex < s_mapProcCnt; idex++) {
    map_t*  entry = &s_mapProc[idex];

    /* If this entry is NOT marked as `changed` AND is NOT `pid`, skip it
     * (i.e. ALWAYS output the `pid`).
     */
    if (changes < s_mapProcCnt &&
        !entry->changed &&
        entry->offset != OFFSETOF(proc_t, pid)) {
      continue;
    }

    // This member has changed
    if (needComma)  { written += lkb_write( ",", 1 ); }

    if (entry->offset == OFFSETOF(proc_t, hashCmdline)) {
      // Output the command line
      written += json_write_strn( entry->name, cmdline->str, cmdline->len );

    } else if (entry->offset == OFFSETOF(proc_t, state)) {
      // Convert the s32 state to a string
      char* stateStr  = (char*)_proc_stateStr( proc->state );

      /*
      printk(KERN_INFO "%s proc_json( %u ): state changed[ %d => %s ]\n",
                 DEVICE_NAME, proc->pid, proc->state, stateStr);
      // */

      written += json_write_strn( entry->name, stateStr, MAX_STATE_LEN );

#ifdef  CONFIG_CGROUPS  // {
    } else if (entry->offset == OFFSETOF(proc_t, cgroups)) {
      // assert( COUNT(proc->cgroups) == CGROUP_SUBSYS_COUNT );
      int ssid;

      written += json_write_key( entry->name );
      written += lkb_write( "[", 1 );

      for (ssid = 0; ssid < CGROUP_SUBSYS_COUNT; ssid++) {
        if (ssid > 0) { written += lkb_write( ",", 1 ); }

        written += json_write_u32_val( proc->cgroups[ssid] );
      }

      written += lkb_write( "]", 1 );

#endif  // CONFIG_CGROUPS  }

    } else {
      written  += json_write_map( entry, (void*)proc );
    }

    needComma = TRUE;
  }

  return written;
}

/**
 *  Gather system process summary metrics.
 *  @method proc_gather
 *  @param  task    The target task/process {struct task_struct*}
 *  @param  data    A pointer to the data to fill {proc_t*};
 *
 *  @return void
 */
void
proc_gather( struct task_struct* task, proc_t* data ) {
  struct files_struct*  files;
  struct signal_struct* sig;
  struct mm_struct*     mm;
  struct timespec       start       = { 0 };
  cputime_t             cutime      = 0;
  cputime_t             cstime      = 0;
  //struct pid_namespace* ns      = task_active_pid_ns( task );
  const struct cred*    cred;

  memset( data, 0, sizeof(*data) );

  // linux/fs/proc/array.c : do_task_stat()
  get_task_comm( data->name, task );  // strcpy(), no put() required
  data->state = task->state | task->exit_state;
  data->pid   = task_pid_nr( task );    // == task->pid
  data->ppid  = task_ppid_nr( task );
  data->pgrp  = task_pgrp_vnr( task );
  /*
  data->pgrp = task_pid_nr_ns( task, ns );
  data->pgrp = task_pgrp_nr_ns( task, &init_pid_ns );
  data->pgrp = task_pgrp_nr( task ); // marked 'obsolete, do not use'
  data->pgrp = *task_pgrp( task );   // marked as unsafe to dereference
  // */

  /* linux/fs/proc/array.c : do_task_stat()
   *    start_time = nsec_to_clock_t( task->real_start_time );
   *
   *  nsec_to_clock_t() isn't directly exported BUT we can accomplish the same
   *  purpose using methods and macros from 'include/linux/time.h'
   *
   *  We also want the start time as absolute instead of an offset from the
   *  boot time, so add in the boot time.
   */
  memcpy( &start, &lkb.bootTime, sizeof(start) );

#if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,0,0)
  /* task_struct {
   *  u64 real_start_time;
   * }
   */
  timespec_add_ns( &start, task->real_start_time );

#else   // LINUX_VERSION_CODE < 4.0.0
  /* task_struct {
   *  struct timespec real_start_time;
   * }
   *
   * struct timespec {
   *  long  ts_sec;
   *  long  ts_nsec;
   * }
   */
  timespec_add_ns( &start, timespec_to_ns( &task->real_start_time ) );
#endif  // LINUX_VERSION_CODE

#ifdef  CONFIG_CGROUPS  // {
  // Gather the set of cgroups currently in use by this task
  task_lock( task );  // {
  {
    void* ns    = cgroup_v1_getNs( task );
    int   ssid;

    for (ssid = 0; ssid < CGROUP_SUBSYS_COUNT; ssid++) {
      struct cgroup_subsys_state* css = task_get_css( task, ssid );
      u32                         id;

      /* Touch this task-associated cgroup to:
       *    1) identify the set of active cgroup/subsystems;
       *    2) tag each as "still active";
       *    3) retrieve the id of the cgroup/subsystem;
       *
       * This helps the cgroup module:
       *    - recognize the full set of cgroup/subsystems without the need to
       *      explicitly traverse the cgroup hierarchy;
       *    - recognize which cgroup/subsystems are still in-use;
       */
      rcu_read_lock();
        id = cgroup_v1_touch_rcu( ns, ssid, css );
      rcu_read_unlock();

      /*
      printk(KERN_WARNING
              "%s proc_gather( %6d ): cgroup #%d: %u\n",
              DEVICE_NAME, data->pid, ssid, id);
      // */

      data->cgroups[ ssid ] = id;

      css_put( css );
    }

    cgroup_v1_putNs( ns );
  }
  task_unlock( task );
#endif  // CONFIG_CGROUPS  }

  // Retain seconds only
  data->cpuStart = start.tv_sec;


  /* linux/fs/proc/array.c : task_state()
   *  get_task_cred(task) => struct cred* : uid, euid, suid, fsuid,
   *                                        gid, egid, sgid, fsgid
   */
  cred = s_kern.get_task_cred( task );
  if ( cred ) {
    data->uid = from_kuid_munged( cred->user_ns, cred->uid );
    put_cred( cred );
  }

  /* linux/fs/proc/fd.c : tid_fd_revalidate()
   *  get_files_struct(task) => struct files_struct* : count
   */
  files = s_kern.get_files_struct( task );
  if ( files ) {
    /* Count the number of open files for this process
     * (i.e. all non-NULL entries in files->fdt[]).
     */
    int fd  = 0;
    int max_fds;

    rcu_read_lock();
      max_fds = files_fdtable(files)->max_fds;

      for (fd = 0; fd < max_fds; fd++) {
        if (!fcheck_files(files, fd)) {
          continue;
        }
        data->fdOpen++;
      }
    rcu_read_unlock();

    data->fdMax = (u32)max_fds;

    s_kern.put_files_struct( files );

    /*
    printk(KERN_INFO "%s proc_gather( %d ): fds[ %u / %d ]\n",
                     DEVICE_NAME, data->pid, data->fdOpen, max_fds);
    // */
  }


  // linux/fs/proc/array.c : do_task_stat()
  /*
  if (!groupLevel) {
    min_flt = task->min_flt;
    maj_flt = task->maj_flt;
  }
  // */

  /* linux/fs/proc/array.c : do_task_stat()
   * Grab data as quickly as possible and THEN manipulate it {
   *
   * :XXX:  lock_task_sighand() is an inline function that uses the non-exported
   *        __lock_task_sighand() AND we cannot resolve it...
   *
   *        Can we just jump in and try to use it???
   *
   *        Perhaps we should use spin_lock/unlock_irqsave() ?
   */
  sig = task->signal;
  if ( sig ) {
    cutime = sig->cutime;
    cstime = sig->cstime;

    data->memFaultsMinor = sig->cmin_flt;
    data->memFaultsMajor = sig->cmaj_flt;

    // linux/fs/proc/base.c : proc_pid_limits()
    // Grab a copy of the RLIMIT_NOFILE rlimit entry
    data->fdLimitHard = sig->rlim[RLIMIT_NOFILE].rlim_max; // RLIM_INFINITY
    data->fdLimitSoft = sig->rlim[RLIMIT_NOFILE].rlim_cur;

    /*
    if (groupLevel) {
      struct task_struct* t = task;
      do {
        min_flt += t->min_flt;
        maj_flt += t->maj_flt;
      } while_each_thread( task, t );

      min_flt += sig->min_flt;
      maj_flt += sig->maj_flt;
    }

    ppid = task_tgid_nr_ns( task->real_parent, ns );
    pgid = task_pgrp_nr_ns( task, ns );
    sid  = task_session_nr_ns( task, ns );
    // */

    //unlock_task_sighand( task, &flags );
  }
  // Grab data as quickly as possible and THEN manipulate it }

  data->cpuUser = cputime_to_clock_t( cutime );
  data->cpuSys  = cputime_to_clock_t( cstime );

  /* linux/fs/proc/array.c : proc_pid_statm()
   *  get_task_mm(task) => struct mm_struct* mm
   *
   *  size = task_statm(mm, &shared, &text, &data, &resident);
   *  mmput(mm);
   *
   * linux/fs/proc/task_mmu.c : task_statm()
   *    *shared   = get_mm_counter(mm, MM_FILEPAGES) +
   *                get_mm_counter(mm, MM_SHMEMPAGES);
   *    *text     = ...
   *    *data     = ...
   *    *resident = *shared + get_mm_counter(mm, MM_ANONPAGES);
   *    return mm->total_vm;
   */
  mm = get_task_mm( task );
  if ( mm ) {
    // :NOTE: Memory is reported by unit where unit is typically pages
    // :XXX:  MM_SHMEMPAGES was added sometime after v4.4.0
    data->memShare = lkb_bytes(get_mm_counter(mm, MM_FILEPAGES) +
#ifdef  MM_SHMEMPAGES
                               get_mm_counter(mm, MM_SHMEMPAGES)
#else   // ! MM_SHMEMPAGES
                               0
#endif  // MM_SHMEMPAGES
                     );
    data->memRss   = data->memShare +
                     lkb_bytes(get_mm_counter(mm, MM_ANONPAGES));
    data->memSize  = lkb_bytes(mm->total_vm);

    mmput( mm );
  }

  /* linux/fs/proc/array.c : do_task_stat()
  data->cmdLine
  // */
}
/* Public methods }
 *****************************************************************************/

/**
 *  Expose this as TWO lkb module.
 */
Module_t module_proc = {
  .name   = "processes",
  .init   = lkb_proc_init,
  .json   = lkb_proc_json,
  .reset  = lkb_proc_reset,
  .fini   = lkb_proc_fini,
};

Module_t module_procSum = {
  .name   = "process_summary",
  .init   = lkb_procSum_init,
  .json   = lkb_procSum_json,
  .reset  = lkb_procSum_reset,
};
