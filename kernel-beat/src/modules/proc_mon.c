/**
 *  Module: procMon
 *
 *  Monitor process terminations, both normal and abnormal.
 *
 *  This module:
 *    - makes use of ftrace to hook:
 *        force_sig_info()  - invoked by the kernel whenever a process is being
 *                            sent an unblockable signal
 *                            (e.g. SIGFPE, SIGSEGV, SIGBUS, ...);
 *        sys_exit_group()  - invoked by a process to terminate a process group;
 *        sys_exit()        - invoked by a process to terminate;
 *
 *        sys_execve()      - iff MONITOR_PROCESS_CREATION is defined, monitor
 *                            creation of new processes via `execve()`;
 *        sys_clone()       - iff MONITOR_PROCESS_CREATION is defined, monitor
 *                            creation of new processes via `fork()`;
 *
 *    - makes use of a work queue to move primary work out of
 *      interrupt/trap/syscall contexts to reduce the impact on the primary
 *      paths;
 *            +-----------------------------------+
 *            | bottom-half : _pm_report          |  kernel      context
 *            +-----------------------------------+
 *            | top-half    : _pm_die_handler     |  interrupt   context
 *            |             : _pm_force_sig_info  |  signal/trap context
 *            |             : _pm_exit            |  syscall     context
 *            |             : _pm_exit_group      |  syscall     context
 *            +-----------------------------------+
 *
 *    - iff USE_DIE_NOTIFIER is defined, makes use of kernel-level die
 *      notifications to monitor unexpected process termination. This is the
 *      system used by `kdebug` for notifications of kernel faults.
 *
 *      Unfortunately, for user-level processes, this NOT triggered for
 *      exceptions like SIGSEGV, although it is triggered for SOME exceptions
 *      (e.g. SIGFPE / divide-by-zero);
 *
 *      User-level exceptions are better handled via the `force_sig_info()`
 *      ftrace hook.
 */
#include "../lkb.h"
#include "../map.h"
#include "../json.h"

#include "proc.h"
#include "proc_mon.h"
#include "cgroup_v1.h"

#include <linux/slab.h>       // kzalloc() / kfree()
#include <linux/signal.h>     // struct siginfo
#include <linux/kdebug.h>     // (un)register_die_notifier(), struct die_args

// Avoid tail optimization issues with ftrace hooks
#pragma GCC optimize("-fno-optimize-sibling-calls")

#ifdef  USE_DIE_NOTIFIER  // {
# include <linux/notifier.h>  // struct notifier_block
#endif  // USE_DIE_NOTIFIER  }

// Track memory allocation
static u64  s_bytesAlloc = 0;
static u64  s_bytesFreed = 0;

/**
 *  Work queue to allow top-level interrupt handler queue work for the
 *  bottom-level.
 */
static struct workqueue_struct*  s_mon_queue;

/**
 *  Process exit/exception report state.
 */
typedef struct {
  struct work_struct  work;

  bool                isException;
  struct die_args     die;          // iff `isException`
  struct pt_regs      regs;         // iff `isException` && die->regs

  long                ret;          // via _pm_exit/exit_group()

  proc_t              proc;
  cmdline_t           cmdline;

} ProcExit_t;

/*****************************************************************************
 * Private methods {
 *
 */

/**
 *  Check if the process monitor should be active.
 *  @method _pm_isActive
 *
 *  This method is used to determine whether process termination information
 *  should be collected and reported.
 *
 *  @return An indication of whether the process monitor gather should continue
 *          {bool};
 *  @private
 */
static bool
_pm_isActive( void ) {
  /* Only allow process monitor collection and reporting if the beat timer is
   * active.
   */
  return (lkb.started ? TRUE : FALSE);
}

/**
 *  Handle process information reporting.
 *  @method _pm_report
 *  @param  work    The work to perform {struct work_struct*};
 *
 *  @note   This is the bottom-half of the interrupt/trap/syscall handler,
 *          scheduled via the `s_mon_queue` workqueue:
 *
 *  @return void
 *  @private
 */
static void
_pm_report( struct work_struct* work ) {
  //ProcExit_t* procExit  = (ProcExit_t*)work;
  ProcExit_t* procExit  = container_of( work, ProcExit_t, work );
  proc_t*     proc      = &procExit->proc;
  ssize_t     written   = 0;
#ifdef  MEASURE_PERFORMANCE
  u64         start     = lkb_timestamp();
  u64         end;
#endif  // MEASURE_PERFORMANCE

  mutex_lock( &lkb.writeLock );   // writer lock {

    written += lkb_write( "{", 1 );
    written += json_write_u32( "timestamp", lkb_time() );

    written += lkb_write( ",", 1 );
    written += json_write_u32( "beat", lkb.beat);

    written += lkb_write( ",", 1 );
    written += json_write_key( "process_terminated" );
    written += lkb_write( "{", 1 );

    /*
    printk(KERN_INFO "%s _pm_report(): ret[ %ld ], name[ %s ], cmdLine[ %s ], "
                                      "state[ %x ], "
                                      "pid[ %d ], ppid[ %d ], pgrp[ %d ], "
                                      "uid[ %d ], cpu[ %llu u:%u s:%u ], "
                                      "fd[ %u h:%u s:%u ], "
                                      "mem[ %llu r:%llu s:%llu f:%u F:%u ]\n",
                     DEVICE_NAME, procExit->ret,
                                  proc->name, procExit->cmdline.str,
                                  proc->state,
                                  proc->pid, proc->ppid, proc->pgrp,
                                  proc->uid,
                                  proc->cpuStart, proc->cpuUser, proc->cpuSys,
                                  proc->fdOpen,
                                  proc->fdLimitHard, proc->fdLimitSoft,
                                  proc->memSize, proc->memRss, proc->memShare,
                                  proc->memFaultsMinor, proc->memFaultsMajor);
    // */

    written += proc_json( proc, &procExit->cmdline, PROC_OUTPUT_ALL );

    written += lkb_write( ",", 1 );
    if (sizeof(procExit->ret) == 8) {
      // long : 64-bit
      written += json_write_s64( "exit", procExit->ret );

    } else {
      // ASSUME long : 32-bit
      written += json_write_s32( "exit", procExit->ret );

    }

    if (procExit->isException) {
      struct die_args*  die = &procExit->die;

      /*
      printk(KERN_INFO "%s _pm_report():EXCEPTION: "
                                     "pid[ %d ], name[ %s ], cmdline[ %s ], "
                                     "str[ %s ], err[ %ld ], "
                                     "trapnr[ %d ], signr[ %d ]\n",
                       DEVICE_NAME,
                       proc->pid,   proc->name, procExit->cmdline.str,
                       die->str,    die->err,
                       die->trapnr, die->signr);
      // */

      // :XXX: Is accessing `die->str` safe ???
      written += lkb_write( ",", 1 );
      written += json_write_strn( "exception", (char*)die->str, JSON_MAX_STR );

      written += lkb_write( ",", 1 );
      if (sizeof(procExit->ret) == 8) {
        // long : 64-bit
        written += json_write_s64( "error", die->err );

      } else {
        // long : 32-bit
        written += json_write_s32( "error", die->err );

      }

      // int : 32-bit {
      written += lkb_write( ",", 1 );
      written += json_write_s32( "trapNum", die->trapnr );

      written += lkb_write( ",", 1 );
      written += json_write_s32( "sigNum", die->signr );
      // int : 32-bit }

      if (die->regs) {
        // assert( die->regs == &procExit.regs );

        /*
        printk(KERN_INFO "%s _pm_report():EXCEPTION: die_args: "
                                       "ip[ %lx ] ...\n",
                         DEVICE_NAME, die->regs->ip);
        // */

        if (sizeof(die->regs->ip) == 8) {
          // unsigned long : 64-bit
          written += lkb_write( ",", 1 );
          written += json_write_u64( "regIp", die->regs->ip );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regCs", die->regs->cs );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regFlags", die->regs->flags );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regSp", die->regs->sp );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regSs", die->regs->ss );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regAx", die->regs->ax );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regAxOrig", die->regs->orig_ax );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regBp", die->regs->bp );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regCx", die->regs->cx );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regDx", die->regs->dx );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regSi", die->regs->si );

          written += lkb_write( ",", 1 );
          written += json_write_u64( "regDi", die->regs->di );

        } else {
          // ASSUME unsigned long : 32-bit
          written += lkb_write( ",", 1 );
          written += json_write_u32( "regIp", die->regs->ip );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regCs", die->regs->cs );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regFlags", die->regs->flags );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regSp", die->regs->sp );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regSs", die->regs->ss );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regAx", die->regs->ax );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regAxOrig", die->regs->orig_ax );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regBp", die->regs->bp );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regCx", die->regs->cx );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regDx", die->regs->dx );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regSi", die->regs->si );

          written += lkb_write( ",", 1 );
          written += json_write_u32( "regDi", die->regs->di );
        }
      }
    }

    written += lkb_write( "}", 1 );

    // Gather and output task-releated cgroup metrics
    written += cgroup_v1_report( proc );

    written += lkb_write( "}\n", 2 );

  mutex_unlock( &lkb.writeLock ); // writer lock }

  s_bytesFreed += sizeof(*procExit);

  /*
  printk(KERN_INFO "%s proc_mon()      : free %lu bytes [ %llu ]\n",
                   DEVICE_NAME, sizeof(*procExit), s_bytesFreed);
  // */

  proc_releaseCmdline( &procExit->cmdline );

  kfree( procExit );

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s proc_mon()      : %6ld bytes in %7llu ns, beat %u "
                                                  "[ %llu / %llu ]\n",
                   DEVICE_NAME, written, end - start, lkb.beat,
                   s_bytesAlloc, s_bytesFreed);
#endif  // MEASURE_PERFORMANCE
}

/**
 *  Gather process information for the current task.
 *  @method _pm_gather
 *  @param  task  A pointer to the target task {struct task_struct*};
 *  @param  work  A pointer to receive process information {ProcExit_t*};
 *
 *  @return void
 *  @private
 */
void
_pm_gather( struct task_struct* task, ProcExit_t* work ) {

  // Gather process information.
  proc_gather( task, &work->proc );

  // Initialize cmdline gathering to avoid truncation and exclude hashing
  memset( &work->cmdline, 0, sizeof(work->cmdline) );
  work->cmdline.noTrunc = TRUE;
  work->cmdline.noHash  = TRUE;

  // Gather the process command line.
  proc_getCmdline( task, &work->proc, &work->cmdline );
}

/******************************************
 * die notifier {
 *
 */
#ifdef  USE_DIE_NOTIFIER  // {

/**
 *  A notifier callback for process exceptions.
 *  @method _pm_die_handler
 *  @param  self      The notifier block information {struct notifier_block*};
 *  @param  val       ... {unsigned long};
 *  @param  data      Exception arguments {void*};
 *
 *  @note   This is a top-half of the interrupt/trap/syscall handler, which is
 *          in interrupt-context and triggers extra-interrupt-context work via
 *          the `s_mon_queue` workqueue:
 *
 *  @return Handler status (NOTIFY_STOP to ignore the exception) {int};
 */
static int
_pm_die_handler( struct notifier_block* self, unsigned long val, void* data ) {

  if (_pm_isActive()) {
    struct die_args*  args  = (struct die_args*)data;
    ProcExit_t*       work;
    // struct pt_regs* regs, const char* str, long err, int trapnr, int signr

    pr_debug("%s _pm_die_handler(): val[ %lu], die_args: "
                                   "ip[ %lx ], str[ %s ], err[ %ld ], "
                                   "trapnr[ %d ], signr[ %d ]\n",
             DEVICE_NAME, val, args->regs->ip, args->str, args->err,
             args->trapnr, args->signr);

    work = kzalloc( sizeof(*work), GFP_KERNEL );

    s_bytesAlloc += sizeof(*work);

    /*
    printk(KERN_INFO "%s _pm_die_handler(): allocated %llu bytes [ %llu ]\n",
                     DEVICE_NAME, sizeof(*work), s_bytesAlloc);
    // */

    // Copy the die information
    memcpy( &work->die, args, sizeof(*args) );
    memcpy( &work->regs, args->regs, sizeof(work->regs) );
    work->die.regs = &work->regs;

    // Mark this an an exception with `die` and `regs` information
    work->isException = TRUE;
    work->ret         = args->err;  // Promote die->err

    /* Gather process information along with any command line.
     *
     *  :NOTE:  Since this process (current) is being terminated, it likely
     *          exist when the work queue triggers so we need to collect
     *          the relevant data immediately.
     */
    _pm_gather( current, work );

    // Add work to the work queue with the gathered information
    INIT_WORK( &work->work, _pm_report );

    queue_work( s_mon_queue, &work->work );
  }

  return 0;
}

static struct notifier_block  s_die_notifier = {
  .notifier_call  = _pm_die_handler,
};

#endif  // USE_DIE_NOTIFIER  }
/* die notifier }
 ******************************************
 * Segmentation fault monitor {
 *  Hook `force_sig_info`
 *
 ***********************************
 *
 *  How can we monitor for general segmentation faults within any process?
 *
 *  do_page_fault() cannot be directly hooked via either kprobes or ftrace
 *  since it is marked with a '__kprobes' restriction.
 *
 *  Using kprobes:
 *      #include <linux/kprobes.h>
 *
 *      struct kprobe kp  = {
 *        .symbol_name  = "do_page_fault",
 *        .pre          = int  _pre(  struct kprobe*  p,
 *                                    struct pt_regs* r ),
 *        .post         = void _post( struct kprobe*  p,
 *                                    struct pt_regs* r,
 *                                    unsigned long   flags ),
 *        .fault        = int  _fault(struct kprobe*  p,
 *                                    struct pt_regs* r,
 *                                    int             trapnr ),
 *      };
 *
 *      register_kprobe( &kp ); // Returns -EINVAL;
 *      ...
 *      unregister_kprobe( &kp );
 *
 *
 *  Generate die_args:
 *    struct pt_regs* regs;
 *    const char*     str;
 *    long            err;
 *    int             trapnr;
 *    int             signr;
 *
 *     work->die.str    = "segfault";
 *     work->die.err    = -SIGSEGV;
 *     work->die.trapnr = 0;
 *     work->die.signr  = SIGSEGV;
 *     work->die.regs   = &work->regs;
 *
 ***********************************
 *
 *  For x86, arch/x86/mm/fault.c:
 *    - kernel faults are logged via:
 *        * static void           __bad_area_nosemaphore()
 *        * static void           do_sigbus()
 *          static noinline void  mm_fault_error()
 *          static void __kprobes __do_page_fault() (via do_page_fault())
 *            -> static noinline void no_context()
 *              -> static void show_fault_oops()
 *
 *    - segfaults     are logged via:
 *          static noinline void mm_fault_error()
 *        *   -> static noinline void bad_area_nosemaphore()
 *              -> static void __bad_area_nosemaphore()
 *                -> static inline void show_signal_msg()
 *
 ***********************************
 *  Perhaps force_sig_info() from kernel/signal.c ??
 */
static int
(*_real_force_sig_info)  (int                  sig,
                          struct siginfo*      info,
                          struct task_struct*  task);

/**
 *  Hooked `force_sig_info`
 *  @method _pm_force_sig_info
 *  @param  sig     The signal {int};
 *  @param  info    The signal information {struct siginfo*};
 *  @param  task    The effected task {struct task_struct*};
 *
 *  @note   This is a top-half of the interrupt/trap/syscall handler, which is
 *          in interrupt-context and triggers extra-interrupt-context work via
 *          the `s_mon_queue` workqueue:
 *
 *  @return A result code {int};
 */
static int
_pm_force_sig_info(int                  sig,
                   struct siginfo*      info,
                   struct task_struct*  task) {
  int ret;

  if (_pm_isActive()) {
    ProcExit_t* work;

    // /*
    printk(KERN_INFO "%s _pm_force_sig_info(): pid[ %d ], sig[ %d : %d ], "
                                              "si_errno[ %d ], si_code[ %d ]\n",
             DEVICE_NAME, task_pid_nr( task ), sig, info->si_signo,
                          info->si_errno, info->si_code );
    // */

    work = kzalloc( sizeof(*work), GFP_KERNEL );

    s_bytesAlloc += sizeof(*work);

    /*
    printk(KERN_INFO "%s _pm_force_sig_info(): allocated %lu bytes [ %llu ]\n",
                     DEVICE_NAME, sizeof(*work), s_bytesAlloc);
    // */

    /* Copy the signal information
     *    struct siginfo
     *      int si_signo;
     *      int si_errno;
     *      int si_code;
     *
     *      union { ... }
     *        // Access macros
     *
     *        // kill()
     *        si_pid        // sender's pid
     *        si_uid        // sender's uid
     *
     *        // POSIX.1b timers
     *        si_tid        // timer id
     *        si_overrun    // timer overrun
     *
     *        // SIGCHLD
     *        si_status     // process exit code
     *        si_utime      // process user time
     *        si_stime      // process syst time
     *
     *        // SIGILL, SIGFPE, SIGSEGV, SIGBUS
     *        si_addr       // faulting instruction / memory reference
     *        si_trapno     // faulting trap number (__ARCH_SI_TRAPNO)
     *        si_addr_lsb   // LSB of reported address
     *        si_lower      // : si_code = SEGV_BNDERR
     *        si_upper      // : si_code = SEGV_BNDERR
     *        si_pkey       // : si_code = SEGV_PKUERR
     *
     *        // SIGPOLL
     *        si_band       // POLL_IN, POLL_OUT, POLL_MSG
     *        si_fd
     *
     *        // SIGSYS   (__ARCH_SIGSYS)
     *        si_call_addr
     *        si_syscall
     *        si_arch
     *
     ******************************************************************
     *  Generate die_args:
     *    struct pt_regs* regs;
     *    const char*     str;
     *    long            err;
     *    int             trapnr;
     *    int             signr;
     *
     *  Make a copy of the task registers
     */
    memcpy( &work->regs, task_pt_regs( task ), sizeof(work->regs) );
    work->die.regs  = &work->regs;

    work->die.signr = sig;            // === info->si_signo ??
    work->die.err   = info->si_errno;

    switch( sig ) {
      case SIGHUP:    work->die.str = "SIGHUP";   break;
      case SIGINT:    work->die.str = "SIGINT";   break;
      case SIGQUIT:   work->die.str = "SIGQUIT";  break;
      case SIGTRAP:   work->die.str = "SIGTRAP";  break;
      case SIGABRT:   work->die.str = "SIGABRT";  break;  // SIGIOT
      case SIGKILL:   work->die.str = "SIGKILL";  break;
      case SIGUSR1:   work->die.str = "SIGUSR1";  break;
      case SIGUSR2:   work->die.str = "SIGUSR2";  break;
      case SIGPIPE:   work->die.str = "SIGPIPE";  break;
      case SIGALRM:   work->die.str = "SIGALRM";  break;
      case SIGTERM:   work->die.str = "SIGTERM";  break;
      case SIGSTKFLT: work->die.str = "SIGSTKFLT";break;
      case SIGCONT:   work->die.str = "SIGCONT";  break;
      case SIGSTOP:   work->die.str = "SIGSTOP";  break;
      case SIGTSTP:   work->die.str = "SIGTSTP";  break;
      case SIGTTIN:   work->die.str = "SIGTTIN";  break;
      case SIGTTOU:   work->die.str = "SIGTTOU";  break;
      case SIGURG:    work->die.str = "SIGURG";   break;
      case SIGXCPU:   work->die.str = "SIGXCPU";  break;
      case SIGXFSZ:   work->die.str = "SIGXFSZ";  break;
      case SIGVTALRM: work->die.str = "SIGVTALRM";break;
      case SIGPROF:   work->die.str = "SIGPROF";  break;
      case SIGWINCH:  work->die.str = "SIGWINCH"; break;
      case SIGIO:     work->die.str = "SIGIO";    break;  // SIGPOLL
      case SIGPWR:    work->die.str = "SIGPWR";   break;
      case SIGSYS:    work->die.str = "SIGSYS";   break;

      case SIGCHLD:
        // process exit code
        work->die.err = info->si_status;
        break;

      /****************************************************
       * SIGILL, SIGFPE, SIGSEGV, SIGBUS {
       *
       */
      case SIGILL:
        if (!work->die.str) { work->die.str = "SIGILL"; }
        // fall-through

      case SIGFPE:
        if (!work->die.str) { work->die.str = "SIGFPE"; }
        // fall-through

      case SIGSEGV:
        if (!work->die.str) { work->die.str = "SIGSEGV"; }
        // fall-through

      case SIGBUS:
        if (!work->die.str) { work->die.str = "SIGBUS"; }

#if defined(CONFIG_X86) // x86, arm, m32r, score, unicore32
        work->die.err    = task->thread.error_code;
        work->die.trapnr = task->thread.trap_nr;
#endif                  // x86, arm, m32r, score, unicore32

        // #ifdef __ARCH_SI_TRAPNO
        // work->die.trapnr = info->trapno;
        // #endif
        break;
      /* SIGILL, SIGFPE, SIGSEGV, SIGBUS }
       ****************************************************/

      default:
        work->die.str = "UNKNOWN";
        break;
    }

    // Mark this an an exception with `die` and optional `regs` information
    work->isException = TRUE;
    work->ret         = work->die.err;  // Promote die->err

    /* Gather process information along with any command line.
     *
     *  :NOTE:  Since this process (current) is being terminated, it likely
     *          exist when the work queue triggers so we need to collect
     *          the relevant data immediately.
     */
    _pm_gather( current, work );

    // Add work to the work queue with the gathered information
    INIT_WORK( &work->work, _pm_report );

    queue_work( s_mon_queue, &work->work );
  }

  /************************************************************************
   * Invoke the call
   *
   *  :WARNING: tail optimizations MUST be disabled or this will cause
   *            an infinite loop where the return becomes a jump back into
   *            this same function.
   */
  ret = _real_force_sig_info( sig, info, task );

  return ret;
}

/*
 *  Hook `force_sig_info`
 * Segmentation fault monitor }
 ******************************************
 * `sys_execve` syscall hooks {
 *
 *  include/linux/syscalls.h
 */
#ifdef  MONITOR_PROCESS_CREATION  // {
static asmlinkage long
(*_real_sys_execve) (const char __user *filename,
                     const char __user *const __user *argv,
                     const char __user *const __user *envp);

/**
 *  Hooked `sys_execve` syscall
 *  @method _pm_execve
 *  @param  filename    The name of the file being execed {char*};
 *  @param  argv        The incoming arguments {char**};
 *  @param  envp        The incoming environment {char**};
 *
 *  @return A result code {long};
 */
static asmlinkage long
_pm_execve(const char __user *filename,
           const char __user *const __user *argv,
           const char __user *const __user *envp) {
  long  ret;

  printk(KERN_INFO "%s _pm_execve(): filename[ %p ], argv[ %p ], envp[ %p ]\n",
                   DEVICE_NAME, filename, argv, envp);

  /************************************************************************
   * Invoke the real syscall
   *
   *  :WARNING: tail optimizations MUST be disabled or this will cause
   *            an infinite loop where the return becomes a jump back into
   *            this same function.
   */
  ret = _real_sys_execve( filename, argv, envp );

  return ret;
}
#endif  // MONITOR_PROCESS_CREATION  }

/* `sys_execve` syscall hooks }
 ******************************************
 * `sys_clone` syscall hooks {
 *
 *  include/linux/syscalls.h
 */
#ifdef  MONITOR_PROCESS_CREATION  // {

/************************************************************
 * Define CLONE_ARGS_DEF and CLONE_ARGS_CALL
 * based upon configuration {
 */
# ifdef CONFIG_CLONE_BACKWARDS           // CONFIG_CLONE_* {

#  if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,0,0) // >= 4.0.0 {
#   define CLONE_ARGS_DEF  \
      unsigned long  clone_flags, \
      unsigned long  newsp, \
      int __user*    parent_tidptr, \
      unsigned long  tls, \
      int __user*    child_tidptr

#  else   // LINUX_VERSION_CODE < 4.0.0 }{
#   define CLONE_ARGS_DEF  \
      unsigned long  clone_flags, \
      unsigned long  newsp, \
      int __user*    parent_tidptr, \
      int            tls, \
      int __user*    child_tidptr

#  endif  // LINUX_VERSION_CODE }

#  define CLONE_ARGS_CALL clone_flags, newsp, parent_tidptr, tls, child_tidptr

/************************************************************/
# elif defined(CONFIG_CLONE_BACKWARDS2)  // CONFIG_CLONE_* }{

#  if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,0,0) // >= 4.0.0 {
#   define CLONE_ARGS_DEF  \
      unsigned long  newsp, \
      unsigned long  clone_flags, \
      int __user*    parent_tidptr, \
      int __user*    child_tidptr, \
      unsigned long  tls
#  else   // LINUX_VERSION_CODE < 4.0.0 }{
#   define CLONE_ARGS_DEF  \
      unsigned long  newsp, \
      unsigned long  clone_flags, \
      int __user*    parent_tidptr, \
      int __user*    child_tidptr, \
      int            tls
#  endif  // LINUX_VERSION_CODE }

#  define CLONE_ARGS_CALL newsp, clone_flags, parent_tidptr, child_tidptr, tls

/************************************************************/
# elif defined(CONFIG_CLONE_BACKWARDS3)  // CONFIG_CLONE_* }{

#  if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,0,0) // >= 4.0.0 {
#   define CLONE_ARGS_DEF  \
      unsigned long  clone_flags, \
      unsigned long  newsp, \
      int            stack_size, \
      int __user*    parent_tidptr, \
      int __user*    child_tidptr, \
      unsigned long  tls
#  else   // LINUX_VERSION_CODE < 4.0.0 }{
#   define CLONE_ARGS_DEF  \
      unsigned long  clone_flags, \
      unsigned long  newsp, \
      int            stack_size, \
      int __user*    parent_tidptr, \
      int __user*    child_tidptr, \
      int            tls
#  endif  // LINUX_VERSION_CODE }

#  define CLONE_ARGS_CALL clone_flags, newsp, stack_size, \
                         parent_tidptr, child_tidptr, tls

/************************************************************/
# else                                   // CONFIG_CLONE_* }{

#  if     LINUX_VERSION_CODE >= KERNEL_VERSION(4,0,0) // >= 4.0.0 {
#   define CLONE_ARGS_DEF  \
      unsigned long  clone_flags, \
      unsigned long  newsp, \
      int __user*    parent_tidptr, \
      int __user*    child_tidptr, \
      unsigned long  tls
#  else   // LINUX_VERSION_CODE < 4.0.0 }{
#   define CLONE_ARGS_DEF  \
      unsigned long  clone_flags, \
      unsigned long  newsp, \
      int __user*    parent_tidptr, \
      int __user*    child_tidptr, \
      int            tls
#  endif  // LINUX_VERSION_CODE }

#  define CLONE_ARGS_CALL clone_flags, newsp, parent_tidptr, child_tidptr, tls

# endif                                  // CONFIG_CLONE_* }
/* Define CLONE_ARGS_DEF and CLONE_ARGS_CALL
 * based upon configuration }
 ************************************************************/

// A pointer to the real `sys_clone` system call
static asmlinkage long (*_real_sys_clone)  ( CLONE_ARGS_DEF );

/**
 *  Hooked `sys_clone` syscall
 *  @method _pm_clone
 *  ... parameter order is dependant upon configuration ...
 *  @param  clone_flags
 *  @param  newsp
 *  @param  parent_tidptr
 *  @param  child_tidptr
 *  @param  [stack_size]
 *  @param  [tls]
 *
 *  @return A result code {long};
 */
static asmlinkage long
_pm_clone( CLONE_ARGS_DEF ) {
  long  ret;

  printk(KERN_INFO "%s _pm_clone(): clone_flags[ %lx ], newsp[ %lx ], "
                           "parent_tidptr[ %p ], child_tidptr[ %p ]...\n",
                   DEVICE_NAME, clone_flags, newsp,
                                parent_tidptr, child_tidptr);

  /************************************************************************
   * Invoke the real syscall
   *
   *  :WARNING: tail optimizations MUST be disabled or this will cause
   *            an infinite loop where the return becomes a jump back into
   *            this same function.
   */
  ret = _real_sys_clone( CLONE_ARGS_CALL );

  return ret;
}
#endif  // MONITOR_PROCESS_CREATION  }

/* `sys_clone` syscall hooks }
 ******************************************
 * `sys_exit` syscall hook {
 *
 *  include/linux/syscalls.h
 */
static asmlinkage long
(*_real_sys_exit) (int error_code);

/**
 *  Hooked `sys_exit` syscall
 *  @method _pm_exit
 *  @param  error_code  The error/exit code {int};
 *
 *  @note   This is a top-half of the interrupt/trap/syscall handler, which is
 *          in syscall-context and triggers extra-syscall-context work via the
 *          `s_mon_queue` workqueue:
 *
 *  @return A result code {long};
 */
static asmlinkage long
_pm_exit(int error_code) {
  long  ret;

  if (_pm_isActive()) {
    ProcExit_t* work;

    pr_debug("%s _pm_exit(): error_code[ %d ]\n", DEVICE_NAME, error_code);

    work = kzalloc( sizeof(*work), GFP_KERNEL );

    s_bytesAlloc += sizeof(*work);

    /*
    printk(KERN_INFO "%s _pm_exit(): allocated %lu bytes [ %llu ]\n",
                     DEVICE_NAME, sizeof(*work), s_bytesAlloc);
    // */

    // EXPLICITLY mark this as NOT an exception.
    work->isException = FALSE;
    work->ret         = (long)error_code;

    /* Gather process information along with any command line.
     *
     *  :NOTE:  Since this process (current) is being terminated, it likely
     *          exist when the work queue triggers so we need to collect
     *          the relevant data immediately.
     */
    _pm_gather( current, work );

    // Add work to the work queue with the gathered information
    INIT_WORK( &work->work, _pm_report );

    queue_work( s_mon_queue, &work->work );
  }

  /************************************************************************
   * Invoke the real syscall
   *
   *  :WARNING: tail optimizations MUST be disabled or this will cause
   *            an infinite loop where the return becomes a jump back into
   *            this same function.
   */
  ret = _real_sys_exit( error_code );

  return ret;
}

/* `sys_exit` syscall hook }
 ******************************************
 * `sys_exit_group` syscall hook {
 *
 *  include/linux/syscalls.h
 */
static asmlinkage long
(*_real_sys_exit_group) (int error_code);

/**
 *  Hooked `sys_exit_group` syscall
 *  @method _pm_exit_group
 *  @param  error_code  The error/exit code {int};
 *
 *  @note   This is a top-half of the interrupt/trap/syscall handler, which is
 *          in syscall-context and triggers extra-syscall-context work via the
 *          `s_mon_queue` workqueue:
 *
 *  @return A result code {long};
 */
static asmlinkage long
_pm_exit_group(int error_code) {
  long  ret;

  if (_pm_isActive()) {
    ProcExit_t* work;

    pr_debug("%s _pm_exit_group(): error_code[ %d ]\n",
             DEVICE_NAME, error_code);

    work = kzalloc( sizeof(*work), GFP_KERNEL );

    s_bytesAlloc += sizeof(*work);

    /*
    printk(KERN_INFO "%s _pm_exit_group(): allocated %lu bytes [ %llu ]\n",
                     DEVICE_NAME, sizeof(*work), s_bytesAlloc);
    // */

    // EXPLICITLY mark this as NOT an exception.
    work->isException = FALSE;
    work->ret         = (long)error_code;

    /* Gather process information along with any command line.
     *
     *  :NOTE:  Since this process (current) is being terminated, it likely
     *          exist when the work queue triggers so we need to collect
     *          the relevant data immediately.
     */
    _pm_gather( current, work );

    // Add work to the work queue with the gathered information
    INIT_WORK( &work->work, _pm_report );

    queue_work( s_mon_queue, &work->work );
  }

  /************************************************************************
   * Invoke the real syscall
   *
   *  :WARNING: tail optimizations MUST be disabled or this will cause
   *            an infinite loop where the return becomes a jump back into
   *            this same function.
   */
  ret = _real_sys_exit_group( error_code );

  return ret;
}


/* `sys_exit_group` syscall hook }
 ******************************************/

static Hook_t s_hooks[] = {
  LKB_HOOK( "force_sig_info", _pm_force_sig_info, &_real_force_sig_info ),
  LKB_HOOK( "sys_exit_group", _pm_exit_group,     &_real_sys_exit_group ),
  LKB_HOOK( "sys_exit",       _pm_exit,           &_real_sys_exit ),
#ifdef  MONITOR_PROCESS_CREATION  // {
  LKB_HOOK( "sys_execve",     _pm_execve,         &_real_sys_execve ),
  LKB_HOOK( "sys_clone",      _pm_clone,          &_real_sys_clone ),
#endif  // MONITOR_PROCESS_CREATION  }
};
static size_t s_nHooks  = sizeof(s_hooks) / sizeof(s_hooks[0]);

/* Private methods }
 *****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_procMon_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_procMon_init( void ) {
  size_t  idex;

  pr_debug("%s procMon_init(): %lu hooks ...\n", DEVICE_NAME, s_nHooks);

  /* Initialize our work queue
   *  - specifying `max_active` of 0 causes the default value of 256 to be used;
   */
  s_mon_queue = alloc_workqueue("%s",
                                WQ_MEM_RECLAIM
                                | WQ_HIGHPRI
#if     LINUX_VERSION_CODE < KERNEL_VERSION(3,15,0) // < 3.15.0 {
                                | WQ_NON_REENTRANT
#endif  // LINUX_VERSION_CODE < KERNEL_VERSION(3,15,0) }
                                , 0, "procMon");

#ifdef  USE_DIE_NOTIFIER  // {
  // Add our die notifier
  register_die_notifier( &s_die_notifier );
#endif  // USE_DIE_NOTIFIER  }

  // Register ftrace hooks
  for (idex = 0; idex < s_nHooks; idex++) {
    Hook_t* hook  = &s_hooks[idex];

    lkb_hook( hook );

    /*
    printk(KERN_INFO "%s procMon_init(): hook '%s' : "
                                    "addr[ %lx ], repl[ %p ], "
                                    "orig[ %p : %lx ]\n",
                     DEVICE_NAME, hook->name,
                                  hook->addr, hook->repl,
                                  hook->orig,
                                  *((unsigned long*)(hook->orig)) );
    // */
  }

  /*
  printk(KERN_INFO "%s procMon_init(): %lu hooks set.\n",
                   DEVICE_NAME, s_nHooks);
  // */
}

/**
 *  Finalize, cleaning up any allocated resources.
 *  @method lkb_procMon_fini
 *
 *  @return void
 *  @protected
 */
static void
lkb_procMon_fini( void ) {
  size_t  idex;

  // Unregister ftrace hooks
  for (idex = 0; idex < s_nHooks; idex++) {
    Hook_t* hook  = &s_hooks[idex];

    /*
    printk(KERN_INFO "%s procMon_fini(): unhook '%s' ...\n",
                     DEVICE_NAME, hook->name);
    // */

    lkb_unhook( hook );
  }

#ifdef  USE_DIE_NOTIFIER  // {
  // Remove our die notifier
  unregister_die_notifier( &s_die_notifier );
#endif  // USE_DIE_NOTIFIER  }

  // Flush our work queue
  flush_workqueue( s_mon_queue );

  // Cleanup our work queue
  destroy_workqueue( s_mon_queue );

  if (s_bytesAlloc - s_bytesFreed != 0) {
    printk(KERN_WARNING "%s lkb_procMon_fini(): "
                        "%7llu bytes allocated, %7llu freed : %llu LEAKED\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed,
                                     s_bytesAlloc - s_bytesFreed);
  } else {
    printk(KERN_INFO    "%s lkb_procMon_fini(): "
                        "%7llu bytes allocated, %7llu freed\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed);
  }
}
/* Module methods }
 *****************************************************************************/

/**
 *  Expose this as an lkb module, but not one that reports every beat
 */
Module_t module_procMon = {
  .name   = "procMon",
  .init   = lkb_procMon_init,
  .fini   = lkb_procMon_fini,
};
