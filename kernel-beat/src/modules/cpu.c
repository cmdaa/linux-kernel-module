/**
 *  Module: cpu
 *
 *  Beat-driven module that collects CPU metrics.
 *
 *  Much of this is influenced by kernel src/fs/proc/stat.c
 */
#include "../lkb.h"

#include <linux/sched.h>        // struct task_struct, TASK_INTERRUPTIBLE, ...
#include <linux/kernel_stat.h>  // kcpustat_cpu()
#include <linux/tick.h>         // cget_cpu_idle/iowait_time_us()

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,10,0)
/* Linux < 4.10.0-rc6
 *    get_idle_time() et al
 *      idle = usecs_to_cputime64( idle_time );
 */
# include <linux/cputime.h>     // usecs_to_cputime64(), cputime64_to_clock_t()

#else
/* Linux 4.10.0-rc6
 *  7fb1327ee9b92fca27662f9b9d60c7c3376d6c69 : 4.10.0-rc6 : 2017.02.01
 *    Kernel CPU stats are stored in cputime_t which is an architecture defined
 *    type, and hence a bit opaque and requiring accessors and mutators for any
 *    operation.
 *
 *    Converting them to nsecs simplifies the code and is one step toward the
 *    removal of cputime_t in the core code.
 *
 *    get_idle_time() et al
 *      idle = idle_usecs * NSEC_PER_USEC;
 */
# define usecs_to_cputime64( __usecs)  ((__force u64)((__usecs) * NSEC_PER_USEC))
#endif

#include "../map.h"
#include "../json.h"

#include "cpu.h"

// :NOTE: If additional fields are added, `s_map` MUST also be changed.
typedef struct {
  u64 user;       // CPU time spent in user space;
  u64 nice;       // CPU time spent on low-priority processes;
  u64 sys;        // CPU time spent in kernel space;

  u64 idle;       // CPU time spent idle;
  u64 iowait;     // CPU time spent waiting on disk access;

  u64 irq;        // CPU time spent servicing hardware interrupts;
  u64 softIrq;    // CPU time spent servicing software interrupts;

  u64 steal;      /* CPU time spent in involuntary wait by the virtual CPU
                   * while the hypervisor was servicing another processor;
                   */
  u64 guest;      // CPU time spent by a virtual CPU in a hypervisor;
  u64 guestNice;  /* CPU time spent by a virtual CPU in a hypervisor on
                   * low-priority processes;
                   */

} cpu_t;

/**
 *  The mapping from `cpu_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all fields from `cpu_t` MUST be identified.
 */
static map_t  s_map[] = {
  {"user",      MAP_TYPE_U64, OFFSETOF(cpu_t,user)},
  {"nice",      MAP_TYPE_U64, OFFSETOF(cpu_t,nice)},
  {"sys",       MAP_TYPE_U64, OFFSETOF(cpu_t,sys)},
  {"idle",      MAP_TYPE_U64, OFFSETOF(cpu_t,idle)},
  {"iowait",    MAP_TYPE_U64, OFFSETOF(cpu_t,iowait)},
  {"irq",       MAP_TYPE_U64, OFFSETOF(cpu_t,irq)},
  {"softIrq",   MAP_TYPE_U64, OFFSETOF(cpu_t,softIrq)},
  {"steal",     MAP_TYPE_U64, OFFSETOF(cpu_t,steal)},
  {"guest",     MAP_TYPE_U64, OFFSETOF(cpu_t,guest)},
  {"guestNice", MAP_TYPE_U64, OFFSETOF(cpu_t,guestNice)},
};
static int    s_mapCnt    = COUNT(s_map);

#ifdef  USE_LKB_DIFF
static bool   s_haveLast  = FALSE;
static cpu_t  s_cpuLast;
#endif  // USE_LKB_DIFF

/*****************************************************************************
 * Private methods {
 *
 */

/**
 *  Compute the idle time for the identified cpu.
 *  @method _get_idle_time
 *  @param  cpu     The target cpu {int};
 *
 *  @return The idle time {u64};
 *  @private
 */
static u64
_get_idle_time( int cpu ) {
  u64 idle, idle_time = -1ULL;

  if (cpu_online(cpu)) { idle_time = get_cpu_idle_time_us(cpu, NULL ); }

  if (idle_time == -1ULL) {
    // !NO_HZ or cpu offline so we can rely on cpustat.idle
    idle = kcpustat_cpu(cpu).cpustat[CPUTIME_IDLE];
  } else {
    idle = usecs_to_cputime64( idle_time );
  }

  return idle;
}

/**
 *  Compute the iowait time for the identified cpu.
 *  @method _get_iowait_time
 *  @param  cpu     The target cpu {int};
 *
 *  @return The iowait time {u64};
 *  @private
 */
static u64
_get_iowait_time( int cpu ) {
  u64 iowait, iowait_time = -1ULL;

  if (cpu_online(cpu)) { iowait_time = get_cpu_iowait_time_us(cpu, NULL ); }

  if (iowait_time == -1ULL) {
    // !NO_HZ or cpu offline so we can rely on cpustat.iowait
    iowait = kcpustat_cpu(cpu).cpustat[CPUTIME_IOWAIT];
  } else {
    iowait = usecs_to_cputime64( iowait_time );
  }

  return iowait;
}

/**
 *  Gather CPU metrics.
 *  @method _cpu_gather
 *  @param  data    A pointer to the cpu data to fill {cpu_t*};
 *
 *  @return void
 *  @private
 */
static void
_cpu_gather( cpu_t* data ) {
  int idex;

  for_each_possible_cpu( idex ) {

    data->user      += kcpustat_cpu( idex ).cpustat[CPUTIME_USER];
    data->nice      += kcpustat_cpu( idex ).cpustat[CPUTIME_NICE];
    data->sys       += kcpustat_cpu( idex ).cpustat[CPUTIME_SYSTEM];
    data->irq       += kcpustat_cpu( idex ).cpustat[CPUTIME_IRQ];
    data->softIrq   += kcpustat_cpu( idex ).cpustat[CPUTIME_SOFTIRQ];
    data->steal     += kcpustat_cpu( idex ).cpustat[CPUTIME_STEAL];
    data->guest     += kcpustat_cpu( idex ).cpustat[CPUTIME_GUEST];
    data->guestNice += kcpustat_cpu( idex ).cpustat[CPUTIME_GUEST_NICE];

    data->idle      += _get_idle_time( idex );
    data->iowait    += _get_iowait_time( idex );
  }
}

/* Private methods }
 *****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_cpu_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_cpu_init( void ) {
  printk(KERN_INFO "%s cpu_init(): %lu bytes / beat\n",
                   DEVICE_NAME, sizeof(cpu_t));
}

/**
 *  Gather and write CPU metrics as JSON to the circular buffer.
 *  @method cpu_json
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_cpu_json( void ) {
  ssize_t written   = 0;
  bool    needComma = 0;
  int     idex;
  cpu_t   cpu;
#ifdef  MEASURE_PERFORMANCE
  u64     start     = lkb_timestamp();
  u64     end;
#endif  // MEASURE_PERFORMANCE

  memset( &cpu, 0, sizeof(cpu) );

  _cpu_gather( &cpu );

  // Write as JSON the CPU measurements which have changed since the last beat.
  written += lkb_write( "{", 1 );

  for (idex = 0; idex < s_mapCnt; idex++) {
    map_t*  entry     = &s_map[idex];
    void*   cur       = (void*)&cpu;
    bool    doOutput  = (
#ifdef  USE_LKB_DIFF
        !s_haveLast || mapDiff( entry, cur, (void*)&s_cpuLast )
#else  // USE_LKB_DIFF
        TRUE
#endif // USE_LKB_DIFF
    );

    if ( doOutput ) {
      if (needComma)  { written += lkb_write( ",", 1 ); }

      written  += json_write_map( entry, cur );
      needComma = 1;
    }
  }

  written += lkb_write( "}", 1 );

#ifdef  USE_LKB_DIFF
  memcpy( &s_cpuLast, &cpu, sizeof(s_cpuLast) );
  s_haveLast = TRUE;
#endif // USE_LKB_DIFF

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s cpu_json()      : %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_cpu_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_cpu_reset( void ) {
#ifdef  USE_LKB_DIFF
  s_haveLast = FALSE;
#endif  // USE_LKB_DIFF
}

/* Module methods }
 *****************************************************************************/

/**
 *  Expose this as an lkb module.
 */
Module_t module_cpu = {
  .name   = "cpu",
  .init   = lkb_cpu_init,
  .json   = lkb_cpu_json,
  .reset  = lkb_cpu_reset,
};
