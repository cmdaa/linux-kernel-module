/**
 *  Module: load
 *
 *  Beat-driven module that collects system load metrics.
 *
 *  This module:
 *    - presents metrics similar to those available via `/proc/loadavg`;
 *
 *    - makes use of non-public, though exported, kernel symbols:
 *      - get_avenrun()
 */
#include "../lkb.h"

#include <linux/sched.h>        // get_avenrun(), avenrun, FSHIFT, nr_threads
#include <linux/cpumask.h>      // num_online_cpus(), num_possible_cpus(), ...
#include <uapi/linux/sysinfo.h> // SI_LOAD_SHIFT

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,10,0)
/* Linux 4.10.0
 *  dea38c74cb9205341f52b8d8ae18f61247a43ea8 : 4.10.0 : 2017.03.02
 *    Move these bits to <linux/sched/loadavg.h>, to reduce the size and
 *    complexity of <linux/sched.h>.
 *
 *    FIXED_1, FSHIFT, ...
 */
# include <linux/sched/loadavg.h>
#endif

#include "../map.h"
#include "../json.h"

#include "load.h"

// :NOTE: If additional fields are added, `s_map` also be changed.
typedef struct {
  u32 load1;    // === sysinfo.load[0]
  u32 load5;    // === sysinfo.load[1]
  u32 load15;   // === sysinfo.load[2]
  u32 cores;
} load_t;

/**
 *  The mapping from `load_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all fields from `load_t` MUST be identified.
 */
static map_t  s_map[] = {
  {"one",       MAP_TYPE_U32, OFFSETOF(load_t,load1)},
  {"five",      MAP_TYPE_U32, OFFSETOF(load_t,load5)},
  {"fifteen",   MAP_TYPE_U32, OFFSETOF(load_t,load15)},
  {"cores",     MAP_TYPE_U32, OFFSETOF(load_t,cores)},
};
static int      s_mapCnt    = COUNT(s_map);

#ifdef  USE_LKB_DIFF
static bool     s_haveLast  = FALSE;
static load_t   s_loadLast;
#endif // USE_LKB_DIFF

/**
 *  The set of non-exported kernel functions and variables we use
 *
 *  These are resolved and associated with the related s_kern entry in
 *  lkb_load_init().
 */
static Ksyms_t s_syms[]    = {
  { "get_avenrun" },
};
static int      s_numSyms   = (sizeof(s_syms) / sizeof(s_syms[0]));

/**
 *  Resolved function accessor.
 *
 *  These are associated with the resolved s_syms entry in lkb_load_init().
 */
static struct {
  // include/linux/sched.h
  void  (*get_avenrun)  (unsigned long*, unsigned long, int);
} s_kern;

/*****************************************************************************
 * Private methods {
 *
 */

/**
 *  Get the load average using sched/loadavg.c logic on `avenrun`
 *  @method _get_loadAverage
 *  @param  data    A pointer to the load data to fill {load_t*};
 *
 *  @return void
 *  @private
 */
static void
_get_loadAverage( load_t* data ) {
  unsigned long loads[3];
  /* kernel/sched/loadavg.c : get_avenrun( loads, offset, shift )
   *    loads[0] = (avenrun[0] + offset) << shift;
   *    ...
   *
   * kernel/sys.c : do_sysinfo() :
   *    get_avenrun( loads, 0, SI_LOAD_SHIFT - FSHIFT );
   *                           16            - 11
   *
   * fs/proc/loadavg.c : loadavg_proc_show() :
   *    get_avenrun(avnrun, FIXED_1/200, 0);
   *                        (1<<FSHIFT) => ( 2048 / 200) => 10
   *                            11
   *    reporting them via:
   *      "%lu.%02lu", LOAD_INT(avnrun[0]), LOAD_FRAC(avnrun[0])
   *
   *      #define LOAD_INT(x) ((x) >> FSHIFT)
   *      #define LOAD_FRAC(x) LOAD_INT(((x) & (FIXED_1-1)) * 100)
   *
   * Follow proc/loadavg.c
   */
  s_kern.get_avenrun( loads, FIXED_1/200, 0 );

  data->load1  = (u32)loads[0];
  data->load5  = (u32)loads[1];
  data->load15 = (u32)loads[2];

#if 0   // Without lkb_resolve() {
  u32 offset  = 0;
  int shift   = SI_LOAD_SHIFT - FSHIFT;

  /* :XXX: `avenrun` from kernel/sched/loadavg.c
   *       has a note that EXPORT_SYMBOL should be removed.
   */
  data->load1  = (avenrun[0] + offset) << shift;
  data->load5  = (avenrun[1] + offset) << shift;
  data->load15 = (avenrun[2] + offset) << shift;
#endif  // Without lkb_resolve() }
}

/**
 *  Get the number of procesing cores.
 *  @method _get_cores
 *  @param  data    A pointer to the load data to fill {load_t*};
 *
 *  @return void
 *  @private
 */
static void
_get_cores( load_t* data ) {
  /* num_online_cpus()
   * num_posible_cpus()
   * num_present_cpus()
   * num_active_cpus()
   * NR_CPUS
   *
   *                                512             2              2
   *  printk(KERN_INFO "%s NR_CPUS[ %d ], possible[ %d ], present[ %d ]\n",
   *         DEVICE_NAME, NR_CPUS, num_possible_cpus(), num_present_cpus());
   */
  data->cores = num_present_cpus();

  /*
  int idex;

  for_each_present_cpu( idex ) {
    data->cores++;
  }
  // */
}

/**
 *  Gather system load metrics.
 *  @method _load_gather
 *  @param  data    A pointer to the load data to fill {load_t*};
 *
 *  @return void
 *  @private
 */
static void
_load_gather( load_t* data ) {
  _get_loadAverage( data );
  _get_cores(       data );
}

/* Private methods {
 *****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_load_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_load_init( void ) {
  lkb_resolve( s_syms, s_numSyms );

  // :XXX: Make the s_syms entries match the s_kern expectations!!
  s_kern.get_avenrun = s_syms[0].ptr;

  printk(KERN_INFO "%s load_init(): %lu bytes / beat\n",
                   DEVICE_NAME, sizeof(load_t));
}

/**
 *  Gather and write system load metrics as JSON to the circular buffer.
 *  @method lkb_load_json
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_load_json( void ) {
  ssize_t written   = 0;
  bool    needComma = 0;
  char    pctBuf[8] = {0};
  int     idex;
  load_t  load;
#ifdef  MEASURE_PERFORMANCE
  u64     start     = lkb_timestamp();
  u64     end;
#endif  // MEASURE_PERFORMANCE

  memset( &load, 0, sizeof(load) );

  _load_gather( &load );

  // Write as JSON the load measurements which have changed since the last beat.
  written += lkb_write( "{", 1 );

  for (idex = 0; idex < s_mapCnt; idex++) {
    map_t*  entry     = &s_map[idex];
    void*   cur       = (void*)&load;
    bool    doOutput  = (
#ifdef  USE_LKB_DIFF
        !s_haveLast || mapDiff( entry, cur, (void*)&s_loadLast )
#else  // USE_LKB_DIFF
        TRUE
#endif // USE_LKB_DIFF
    );

    if ( doOutput ) {
      if (needComma)  { written += lkb_write( ",", 1 ); }

      if (entry->offset == OFFSETOF(load_t, cores)) {
        written  += json_write_map( entry, cur );

      } else {
        /* The other measures are fixed-integer percentages
         *    "%lu.%02lu", LOAD_INT(avnrun[0]), LOAD_FRAC(avnrun[0])
         *
         *    #define LOAD_INT(x) ((x) >> FSHIFT)
         *    #define LOAD_FRAC(x) LOAD_INT(((x) & (FIXED_1-1)) * 100)
         *
         *    FSHIFT  11
         *    FIXED_1 (1<<FSHIFT) => 2048
         */
        void* pval    = cur + entry->offset;
        u32   val     = *((u32*)pval);
        u32   valInt  = val >> FSHIFT;
        u32   valFrac = ((val & (FIXED_1-1)) * 100) >> FSHIFT;
        int   len     = snprintf( pctBuf, sizeof(pctBuf), "%u.%02u",
                                  valInt, valFrac );

        /*
        printk(KERN_INFO "%s load_json()   : %s[ %u ] : "
                                  "int[ %u ], fract[ %u ] => [ %d : %s ]\n",
                         DEVICE_NAME, entry->name, val,
                         valInt, valFrac, len, pctBuf);
        // */

        written += json_write_key( entry->name );
        written += lkb_write( pctBuf, len );
      }

      needComma = 1;
    }
  }

  written += lkb_write( "}", 1 );

#ifdef  USE_LKB_DIFF
  memcpy( &s_loadLast, &load, sizeof(s_loadLast) );
  s_haveLast = TRUE;
#endif // USE_LKB_DIFF

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s load_json()     : %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_load_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_load_reset( void ) {
#ifdef  USE_LKB_DIFF
  s_haveLast = FALSE;
#endif  // USE_LKB_DIFF
}

/* Module methods }
 *****************************************************************************/

/**
 *  Expose this as an lkb module.
 */
Module_t module_load = {
  .name   = "load",
  .init   = lkb_load_init,
  .json   = lkb_load_json,
  .reset  = lkb_load_reset,
};
