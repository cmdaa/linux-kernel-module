/**
 *  cgroup v1 collector:  memory
 *
 *  This collector provides access to the `memory` cgroup subsystem metrics
 *  available via `/sys/fs/cgroup/memory/memory.*`:
 *    - memsw.usage_in_bytes          read_u64();
 *    - memsw.max_usage_in_bytes      read_u64();
 *    - memsw.limit_in_bytes          read_u64();
 *    - memsw.failcnt                 read_u64();
 *    - usage_in_bytes                read_u64();
 *    - max_usage_in_bytes            read_u64();
 *    - limit_in_bytes                read_u64();
 *    - soft_limit_in_bytes           read_u64();
 *    - failcnt                       read_u64();
 *    - kmem.limit_in_bytes           read_u64();
 *    - kmem.usage_in_bytes           read_u64();
 *    - kmem.failcnt                  read_u64();
 *    - kmem.max_usage_in_bytes       read_u64();
 *    - kmem.tcp.limit_in_bytes       read_u64();
 *    - kmem.tcp.usage_in_bytes       read_u64();
 *    - kmem.tcp.failcnt              read_u64();
 *    - kmem.tcp.max_usage_in_bytes   read_u64();
 *    - swap.current                  read_u64();
 *    - use_hierarchy                 read_u64();
 *    - swappiness                    read_u64();
 *    - move_charge_at_immigrate      read_u64();
 *
 *  :NOTE:  The following cgroup sequence-files are not currently collected:
 *            - swap.max
 *            - stat
 *                cache, rss, rss_huge, mapped_file, dirty, writeback,
 *                pgpgin, pgpgout, pgfault, pgmajfault,
 *                inactive_anon, active_anon, inactive_file, active_file,
 *                unevictable, hierarchical_memory_limit,
 *
 *                total_cache, total_rss, total_rss_huge, total_mapped_file,
 *                total_dirty, total_writeback,
 *                total_pgpgin, total_pgpgout, total_pgfault, total_pgmajfault,
 *                total_inactive_anon, total_active_anon,
 *                total_inactive_file, total_active_file,
 *                total_unevictable
 *
 *            - oom_control
 *                oom_kill_disable, under_oom
 *
 *            - pressure_level
 *                *** CANNOT READ ***
 *
 *            - numa_stat
 *                total=%v% N0=%v%
 *                file=%v% N0=%v%
 *                anon=%v% N0=%v%
 *                unevictable=%v% N0=%v%
 *                hierarchical_total=%v% N0=%v%
 *                hierarchical_file=%v% N0=%v%
 *                hierarchical_anon=%v% N0=%v%
 *                hierarchical_unevictable=%v% N0=%v%
 *
 *            - kmem.slabinfo
 *                slabinfo - version: 2.1
 *                # name            <active_objs> <num_objs> <objsize> \
 *                                    <objperslab> <pagesperslab> : tunables \
 *                                    <limit> <batchcount> <sharedfactor> : \
 *                                    slabdata <active_slabs> <num_slabs> \
 *                                    <sharedavail>
 *
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 */
#ifndef _CGROUP_MEMORY_H_
#define _CGROUP_MEMORY_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_memory;

#endif  // _CGROUP_MEMORY_H_
