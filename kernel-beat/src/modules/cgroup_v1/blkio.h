/**
 *  cgroup v1 collector:  blkio
 *
 *  This collector:
 *    - provides access to the `blkio` cgroup subsystem metrics available via
 *      `/sys/fs/cgroup/blkio/blkio.*` but consolidated by device major/minor
 *      number;
 *
 *    - makes use of a hash table to monitor the set of referenced block devices
 *      indexed by device major/minor number;
 *
 *    - accesses data from sequence-files:
 *        - blkio sequence files come in two flavors:
 *          - those that report pairs   : %maj%:%min% u64
 *          - those that report triples : %maj%:%min% %type% u64
 *
 *          - triple types are: Read, Write, Sync, Async, and Total
 *
 *  :NOTE:  Most data is available only if using the CFQ, or complete fairness
 *          queueing, disk scheduler.
 *
 *  :NOTE:  Access to sequence-file-based metrics
 *          (and thus all metrics available to this collector) may be restricted
 *          by excluding the USE_CGROUP_FSREAD #define.
 */
#ifndef _CGROUP_BLKIO_H_
#define _CGROUP_BLKIO_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_blkio;

#endif  // _CGROUP_BLKIO_H_
