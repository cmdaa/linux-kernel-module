/**
 *  cgroup v1 collector:  pids
 *
 *  This collector provides access to the `pids` cgroup subsystem metrics
 *  available via `/sys/fs/cgroup/pids/ * /pids.*`:
 *    - current     read_s64();
 *    - max         via sequence-file (s64 or 'max');
 *
 *  :NOTE:  The following cgroup sequence-files are not currently collected:
 *            - events
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 */
#ifndef _CGROUP_PIDS_H_
#define _CGROUP_PIDS_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_pids;

#endif  // _CGROUP_PIDS_H_
