/**
 *  cgroup v1 collector:  freezer
 *
 *  This collector provides access to the `freezer` cgroup subsystem metrics
 *  available via `/sys/fs/cgroup/freezer/freezer.*`:
 *    - self_freezing               read_u64();
 *    - parent_freezing             read_u64();
 *
 *  :NOTE:  The following cgroup sequence-files are not currently collected:
 *            - state
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 */
#ifndef _CGROUP_FREEZER_H_
#define _CGROUP_FREEZER_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_freezer;

#endif  // _CGROUP_FREEZER_H_
