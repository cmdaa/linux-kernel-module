/**
 *  cgroup metric interface for the `perf_event` cgroup subsystem.
 *
 *=============================================================================
 *    kernel/events/core.c
 *      v4.9  -- no directly accessible metrics...
 *
 *=============================================================================
 *    kernel/events/core.c
 *      v3.10 -- no directly accessible metrics...
 */
