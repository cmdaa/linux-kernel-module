/**
 *  cgroup v1 collector:  net_prio
 *
 *  This collector provides access to the `net_prio` (aka 'net_cls,net_prio')
 *  cgroup subsystem metrics available via `/sys/fs/cgroup/net_prio/net_prio.*`:
 *    - prioidx     read_u64();
 *    - ifpriomap   via sequence-file indexed by interface name
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 *
 *=============================================================================
 *    net/core/netprio_cgroup.c
 *      v4.9
 *        "prioidx"
 *          .read_u64 = read_prioidx( struct cgroup_subsys_state *css,
 *                                    struct cftype *cft)
 *
 *        "ifpriomap"
 *          .read_map = read_priomap( struct seq_file *sf, void *v)
 *              %iface% u32
 *
 *=============================================================================
 *    net/core/netprio_cgroup.c
 *      v3.10
 *        "prioidx"
 *          .read_u64 = read_prioidx( struct cgroup *cgrp,
 *                                    struct cftype *cft)
 *
 *        "ifpriomap"
 *          .read_map = read_priomap( struct cgroup *cont,
 *                                    struct cftype *cft,
 *                                    struct cgroup_map_cb *cb)
 */
#include "../../json.h"

#include "../cgroup_v1.h"

#include <linux/slab.h>         // kzalloc() / krealloc() / kfree()
#include <uapi/linux/if.h>      // IFNAMSIZ;

// Track memory allocation
static u64  s_bytesAlloc = 0;
static u64  s_bytesFreed = 0;

/**
 *  The number of generated entries, initialized via `cgm_netprio_init()` and
 *  associated with `s_handler`.
 *
 *  Handlers are divided into two primary groups:
 *    - those associated with top-level, cgroup/subsystem metrics;
 *    - those that generate metrics associated with a network interface
 *      via the `ifpriomap` sequence file.
 */
static u32  s_topEntries  = 0;  // Top-level handler count;
static u32  s_ifEntries   = 0;  // Interface handler count;
static u32  s_ifFirst     = 0;  // Index of the `ifpriomap` handler;


#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
/* A shared read buffer for cftype generated string/data for 'ifpriomap'.
 *
 *  The data will be key/value pairs:
 *    - key   : an interface name (IFNAMSIZ == 16);
 *    - value : a u32             (max 10-digits [8,589,934,591]);
 *
 *  Maximum per entry: 28 characters/bytes including the separating space and
 *  ending '\0' or '\n';
 *
 *  Most likely, the priority values will be much less than the maximum
 *  10-digits.
 *
 *  If we assume priorities will be 3-digits or less, then each entry would have
 *  no more than (16 + 1 + 3 + 1) = 21 characters/bytes.
 *
 *  512-bytes allows for 24 interfaces where each is <= 21 characters/bytes.
 */
static char     s_readBuf[512]  = {0};
static u32      s_readBufSize   = sizeof(s_readBuf);
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD

/**
 *  Per interface data.
 */
typedef struct {
  char  name[IFNAMSIZ];
  u32   id;             // A hash of `name`

  u32   priority;
  bool  isDiff;         // The single diff/change flag (for `priority`).

  // List chain
  struct hlist_node nif_chain;

} netprio_iface_t;

/**
 *  Over-ride cgroup_store_t to include a per-interface list.
 *
 */
typedef struct {
  /***********************************************
   * Per-interface stats, one entry per interface
   * that appears in the 'net_prio.ifpriomap'
   * sequence file.
   *
   * :NOTE: This is the head of a list of
   *        `netprio_iface_t` entries associated
   *        with THIS cgroup/subsystem.
   */
  struct hlist_head ifaces;

  /* The primary store:
   *    Given `cgroup_store_t* pcgs`
   *      netprio_store_t* pnis = container_of(pcgs, netprio_store_t, cgStore);
   *
   *    For this store:
   *      - `nChanges` identifies the number of changes in both the top-level
   *        `prioidx` as well as those in the `ifaces` list;
   */
  cgroup_store_t    cgStore;

} netprio_store_t;

/*****************************************************************************/

/****************************************************************
 * Interface list helpers {
 */

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
/**
 *  Check if a interface identified by the given id exists in the device
 *  list.
 *  @method _nifList_find
 *  @param  store The netprio store {netprio_store_t*};
 *  @param  id    The id of the target interface {u32};
 *
 *  @return A pointer to the device entry if it exists {netprio_iface_t*};
 *  @private
 */
static netprio_iface_t*
_nifList_find( netprio_store_t* store, u32 id ) {
  netprio_iface_t*  res     = NULL;
  netprio_iface_t*  entry;

  rcu_read_lock();
    hlist_for_each_entry_rcu( entry, &store->ifaces, nif_chain) {
      if (entry->id == id) {
        res = entry;
        break;
      }
    }
  rcu_read_unlock();

  return res;
}

/**
 *  Add the given entry to the device list.
 *  @method _nifList_add
 *  @param  store The netprio store {netprio_store_t*};
 *  @param  entry The entry to add {netprio_iface_t*};
 *
 *  @return void
 *  @private
 */
static void
_nifList_add( netprio_store_t* store, netprio_iface_t* entry ) {
  // assert( _nifList_find( store, entry ) == NULL )
  rcu_read_lock();
    hlist_add_head_rcu( &entry->nif_chain, &store->ifaces );
  rcu_read_unlock();
}

/**
 *  Find or create an entry for the given device id.
 *  @method _nifList_get
 *  @param  store The netprio store {netprio_store_t*};
 *  @param  id    The id of the target interface {u32};
 *  @param  name  The target interface "name" {char*};
 *
 *  @return A pointer to the device entry, NULL on failure to allocate
 *          {netprio_iface_t*};
 *  @private
 */
static netprio_iface_t*
_nifList_get( netprio_store_t* store, u32 id, char* name ) {
  netprio_iface_t*  entry = _nifList_find( store, id );

  if (entry == NULL) {
    // Device not found -- create a new entry
    u32 nBytes  = sizeof( *entry );

    entry = kzalloc( nBytes, GFP_KERNEL );

    /*
    printk(KERN_INFO "%s _nifList_get(): cache MISS for x%06x: %s\n",
                      DEVICE_NAME, id, name);
    // */

    if (entry) {
      // Add this new entry to our list
      entry->id = id;

      // Copy in the device "name"
      strncpy( entry->name, name, sizeof(entry->name) );

      s_bytesAlloc += nBytes;

      /*
      printk(KERN_INFO
                "%s _nifList_get(): "
                  "Created new %u byte entry for '%s', %u entries: %p\n",
                    DEVICE_NAME, nBytes, name, s_devEntries, entry);
      // */

      _nifList_add( store, entry );

    } else {
      printk(KERN_WARNING "%s _nifList_get(): "
                              "Cannot create device entry for x%06x\n",
                     DEVICE_NAME, id);
    }
  }

  return entry;
}

/**
 *  Remove the given entry from our hash table and free the storage.
 *  @method _nifList_del_rcu
 *  @param  entry The target entry {netprio_iface_t*};
 *
 *  @note   rcu_read_lock() should be held by a caller/ancestor.
 *
 *  @return void
 *  @private
 */
static void
_nifList_del_rcu( netprio_iface_t* entry ) {
  /*
  printk(KERN_INFO "%s _nifList_del_rcu(): delete netprio_iface_t[ %s ] ...\n",
                   DEVICE_NAME, entry->name);
  // */

  hlist_del_rcu( &(entry->nif_chain) );

  s_bytesFreed += sizeof( *entry );

  kfree( entry );
}

/**
 *  Remove all entries from the device list of the given entry.
 *  @method _nifList_clear_rcu
 *  @param  store The netprio store {netprio_store_t*};
 *
 *  @note   rcu_read_lock() should be held by a caller/ancestor.
 *
 *  @return void
 *  @private
 */
static void
_nifList_clear_rcu( netprio_store_t* store ) {
  netprio_iface_t*  entry;

  /*
  printk(KERN_INFO "%s _nifList_clear_rcu(): clear device list\n",
                   DEVICE_NAME);
  // */

  hlist_for_each_entry_rcu( entry, &store->ifaces, nif_chain ) {
    _nifList_del_rcu( entry );
  }

  INIT_HLIST_HEAD( &store->ifaces );
}
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD
/* Interface list helpers }
 *****************************************************************************/

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
/**
 *  Generate a 32-bit hash for the given interface name.
 *  @method _netprio_hash
 *  @param  name    The name of the interface {char*};
 *
 *  @return The 32-bit hash {u32};
 */
static u32
_netprio_hash( char* name ) {
  unsigned long salt  = 0;
  u32           len   = strnlen( name, IFNAMSIZ );

  return lkb_hash( salt, name, len );
}
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD

/**
 *  Extract data an 'ifpriomap' cftype sequence.
 *  @method _ifpriomap_extract
 *  @param  self      The cftype handler entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  The format of the retrieved sequence string is:
 *      %if-name% u32
 *      ...
 *
 *  @return The number of detected changes {u32};
 */
static u32
_ifpriomap_extract( cftype_handler_t* self,
                    cgroup_diff_t*    cgd,
                    u32               start,
                    bool              doDiff ) {

  u32             nChanges  = 0;

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)

  struct cftype*    cft       = self->cft;
  netprio_store_t*  store     = NULL;
  ssize_t           read      = 0;
  char*             cur       = s_readBuf;
  char*             end       = &s_readBuf[ s_readBufSize ];
  char*             key       = NULL;
  char*             val       = NULL;
  u32               consumed  = 0;

  if (cft == NULL || cgd->store == NULL || s_readBuf == NULL) {
    return nChanges;
  }

  store = container_of( cgd->store, netprio_store_t, cgStore );

  /* Read the cftype generated string/data into a buffer and then parse the
   * buffer to extract interface-related stats.
   */
  read = cgroup_v1_fsRead( cft, cgd, s_readBuf, s_readBufSize );
  if (read < 1) { return nChanges; }

  /*
  printk(KERN_INFO "%s _ifpriomap_extract():%u: %s%s: "
                            "read %ld bytes[ %s ]\n",
                      DEVICE_NAME, start, cgd->path, self->name,
                                   read, s_readBuf);
  // */

  end = &s_readBuf[ read ];

  // While we have a key/num pair ...
  while ( (consumed = lkb_getKeyNum( cur, end-cur, &key, &val )) > 0 ) {
    // ... locate or create an interface entry for the key
    u32               id      = _netprio_hash( key );
    netprio_iface_t*  ifEntry = _nifList_get( store, id, key );

    /*
    printk(KERN_INFO "%s _ifpriomap_extract():%u: %s%s: "
                              "key[ %s ], id[ %u ], ifEntry[ %p ] ...\n",
                        DEVICE_NAME, start, cgd->path, self->name,
                                     key, id, ifEntry);
    // */

    if (ifEntry) {
      u32 newVal;

      if (kstrtou32( val, 10, &newVal )) {
        printk(KERN_WARNING "%s _ifpriomap_extract():%02d: name[ %s ], "
                                "iface[ %s ], str[ %s ]: cannot parse number\n",
                             DEVICE_NAME, start, cft->name, key, val);
      } else {

        if (!doDiff || ifEntry->priority != newVal) {
          // Difference
          ifEntry->isDiff   = TRUE;
          ifEntry->priority = newVal;
          nChanges++;

        } else {
          ifEntry->isDiff = FALSE;
        }

        /*
        printk(KERN_INFO
                  "%s _ifpriomap_extract():%u: %s%s: "
                        "iface[ %s ], priority[ %u ]: %s\n",
                  DEVICE_NAME, start, cgd->path, self->name,
                  ifEntry->name, ifEntry->priority,
                  (ifEntry->isDiff ? "DIFF" : "no-change"));
        // */

      }
    } else {
      printk(KERN_ALERT
              "%s _ifpriomap_extract():%u: No interface entry found for "
                            "key[ %s ], val[ %s ]\n",
                DEVICE_NAME, start, key, val);
    }

    // Move beyond this key/value pair
    cur += consumed;
  }
#endif

  return nChanges;
}

/**
 *  Write the JSON version of the identified interface if there are any
 *  difference.
 *  @method _ifpriomap_json
 *  @param  cgd       The top-level cgroup diff entry {cgroup_diff_t*};
 *  @param  iface     The target interface entry {netprio_iface_t};
 *  @param  needComma If truthy, a comma is required before any output {bool};
 *
 *  @return The number of bytes written {int};
 *  @private
 */
static ssize_t
_ifpriomap_json(  cgroup_diff_t*    cgd,
                  netprio_iface_t*  iface,
                  bool              needComma ) {

  ssize_t written   = 0;

  if (! iface->isDiff)  { return written; }

  // Output change for this rwstats entry
  if (needComma)  { written += lkb_write( ",", 1 ); }

  /* :TODO: Change this to an object with `name` and `priority`.
   */
  written += lkb_write( "{", 1 );

  // netprio_iface_t { char name[IFNAMSIZ]; ... }
  written += json_write_strn( "name", iface->name, sizeof(iface->name) );
  written += lkb_write( ",", 1 );
  written += json_write_u32( "priority", iface->priority );

  written += lkb_write( "}", 1 );

  return written;
}
/*****************************************************************************/

/**
 *  The mapping of cftype to handling information.
 *
 *  This array and associated data will be updated during module initalization
 *  to:
 *    - initialize the cftype pointer for each entry that can be supported;
 *    - unsupported entries will have a NULL `cft` property;
 *    - `s_topEntries` will be initialized to the total number of entries that
 *      may be generated by the active handlers;
 *    - `s_ifEntries` will be initialized to the total number of entries that
 *      may be generated by the full set of available sequence cftypes;
 *    - `s_ifFirst` will be initialized to the index of the first
 *      interface-related entry;
 */
static cftype_handler_t   s_handler[] = {
  { .name= "prioidx",         .nEntries= 1 },

  // Sequence-related properties
  { .name= "ifpriomap",       .nEntries= 1, .extract= _ifpriomap_extract },

  // Empty entry terminator : 'name == NULL'
  { 0 },
};
static u32                s_handlerCnt  = COUNT( s_handler ) - 1;

/**
 *  Associate the provided entry with our handler information and ensure space
 *  has been allocated for subsystem-specific data.
 *  @method _cgm_netprio_prepEntry
 *  @param  entry   The target entry {cgroup_diff_t*};
 *
 *  @return A success/failure indicator (0 == success) {int};
 *  @private
 */
static int
_cgm_netprio_prepEntry( cgroup_diff_t*  entry ) {

  if (entry->store == NULL) {
    /* Allocate our custom store, which includes a cgroup_store_t so we can
     * treat it as a sort-of sub-class;
     *
     *    netprio_store_t*  pNifStore   = ...;
     *    cgroup_store_t*   pCgStore    = &pNifStore->cgStore;
     *    netprio_store_t*  pNifStore2  = container_of( pCgStore,
     *                                                  netprio_store_t,
     *                                                  cgStore );
     *    pNifStore == pNifStore2
     *
     *    sizeof( cgroup_store_t )  : 24-bytes
     *      nEntries  4-bytes (1 32-bit value)
     *      nChanges  4-bytes (1 32-bit value)
     *      values    8-bytes (1 64-bit pointer)
     *      changes   8-bytes (1 64-bit pointer)
     *
     *    sizeof( netprio_store_t )   : 32-bytes
     *      ifaces     8-bytes (1 64-bit pointer)
     *      cgStore   24-bytes
     *
     *    allocated size:
     *      netprio_store_t 32-bytes
     *      values          s_topEntries * 8-bytes
     *      changes         s_topEntries * 1-byte
     */
    u32             nBytes  = sizeof( netprio_store_t ) +
                                s_topEntries * (sizeof(u64) + sizeof(bool));
    netprio_store_t*  store   = NULL;

    store = kzalloc( nBytes, GFP_KERNEL );
    if (store == NULL) {
      printk(KERN_ALERT "%s _cgm_netprio_prepEntry(): Cannot create 'store'\n",
                        DEVICE_NAME);
      return -1;
    }

    s_bytesAlloc += nBytes;

    // Initialize the interface list
    INIT_HLIST_HEAD( &store->ifaces );

    entry->store            = &store->cgStore;
    entry->store->nEntries  = s_topEntries;
    entry->store->values    = GET_STORE_VALUES(  entry->store );
    entry->store->changes   = GET_STORE_CHANGES( entry->store );

    entry->handlers         = s_handler;
    entry->nHandlers        = s_handlerCnt;

  } else {
    /* Reset the change counters of the top-level store and all interface
     * entries.
     */
    netprio_store_t*  store = container_of( entry->store,
                                            netprio_store_t,
                                            cgStore);
    netprio_iface_t*  iface;

    entry->store->nChanges = 0;

    rcu_read_lock();
      hlist_for_each_entry_rcu( iface, &store->ifaces, nif_chain) {
        iface->isDiff = FALSE;
      }
    rcu_read_unlock();
  }

  return 0;
}

/*****************************************************************************
 * cgroup module methods {
 *
 */

/**
 *  Initialize this collection module.
 *  @method cgm_netprio_init
 *  @param  subsys  A pointer to the cgroup subsystem {struct cgroup_subsys*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_netprio_init( struct cgroup_subsys* subsys ) {
#ifdef  CONFIG_CGROUPS  // {
  u32             nActive = 0;
  CGROUP_CFT_SET* cfts;

  /*
  printk(KERN_INFO "%s cgm_netprio_init(): ssid[ %2d : %s ]\n",
                   DEVICE_NAME,
                   CGROUP_SUBSYS_ID(subsys),
                   CGROUP_SUBSYS_NAME( subsys ));
  // */

  /* Fill in the cftype `cft` entries identifying (and counting) the final set
   * of active entries.
   *
   *  :NOTE: Upon completion, any inactive entry will have a NULL `cft`
   *         property.
   *
   *         By "inactive" we mean an entry for which no match exists in the
   *         cftype set of the provided subsystem.
   */
  s_topEntries  = 0;
  s_ifEntries   = 0;
  s_ifFirst     = -1;
  list_for_each_entry( cfts, &CGROUP_SUBSYS_CFTSET(subsys), node ) {
    struct cftype* cft = CGROUP_SUBSYS_CFTSET_ITEM(cfts);

    while (cft && cft->name[0] != '\0') {

      cftype_handler_t* handler = cgroup_v1_findHandler( s_handler,
                                                         s_handlerCnt,
                                                         cft->name );
      if (handler != NULL && handler->cft == NULL) {
        u32 hdex  = INDEXOF( s_handler, handler );

        handler->cft = cft;

        nActive++;

        if (handler->extract == _ifpriomap_extract) {
          handler->nEntries  = 1;
          s_ifEntries       += handler->nEntries;

          if (hdex < s_ifFirst) { s_ifFirst = hdex; }

        } else {

          if (! handler->extract) {
            // Fall-back to the cftype extract helper
            handler->extract = cgroup_v1_cftype_extract;
          }

          s_topEntries    += handler->nEntries;
        }

        if (! handler->json) {
          // Use the default json output helper
          handler->json = cgroup_v1_cftype_json;
        }

      } else {
        // else, skip this cftype : generates no map entries
        printk(KERN_WARNING "%s cgm_netprio_init(): SKIP cftype[ %s ]\n",
                             DEVICE_NAME, cft->name);
      }

      cft++;
    }
  }

  // /* Memory metrics {
  {
    u32 nBytes_ss = sizeof( netprio_store_t ) +
                      s_topEntries * (sizeof(u64) + sizeof(bool));
    u32 nBytes_if = sizeof( cgroup_store_t ) +
                      s_ifEntries * sizeof(netprio_iface_t);

    printk(KERN_INFO
            "%s cgm_netprio_init(): ssid[ %2d : %s ] : "
                  "%u bytes / cgroup:subsystem + %u bytes / interface\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ),
            nBytes_ss, nBytes_if);
  }
  // /* Memory metrics }

#endif  // CONFIG_CGROUPS  }
}

/**
 *  Gather subsystem metrics identifying any differences from any prior data.
 *  @method gather
 *  @param  entry   The cgroup diff entry for the target cgroup/subsystem
 *                  {cgroup_diff_t*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_netprio_gather( cgroup_diff_t*  entry ) {
  cgroup_store_t*   store   = entry->store;
  bool              doDiff  = (store != NULL);
  u32               hdex    = 0;
  u32               sdex    = 0;

  if (_cgm_netprio_prepEntry( entry )) {
    // Preparation failed
    return;
  }

  // In case `entry->store` was initially created or moved
  store = entry->store;

  /* Normalize the subsystem data, writing to `entry->data` and, if there was
   * pre-existing `entry->data`, identifying differences in both
   * `entry->map[].changed` and `entry->changed`;
   *
   *  :NOTE:  Since this cgroup handler separates out cftypes into top-level and
   *          per-block-device, we need to iterate over ALL handlers to allow
   *          each to properly scatter the metrics it collects.
   */
  for (hdex = 0, sdex = 0; hdex < s_handlerCnt; hdex++) {
    cftype_handler_t* handler = &entry->handlers[ hdex ];

    /* if (hdex == s_ifFirst) {
     *   All further entries reference interface entries with values/changes
     *   in `netprio_iface_t` entries.
     * }
     */

    if (! handler->cft) { continue; }

    /*
    printk(KERN_INFO "%s cgm_netprio_gather(): path[ %s ]: "
                                "handler %d: %s ...\n",
                       DEVICE_NAME, entry->path, hdex, handler->name);
    // */

    // assert( mapEntry->extract != NULL );
    store->nChanges += handler->extract( handler, entry, sdex, doDiff );

    sdex += handler->nEntries;
  }
}

/**
 *  Given a map entry and pointer to a structure, write the JSON version of
 *  referenced value.
 *  @method cgm_netprio_json
 *  @param  cgd     The cgroup diff entry {cgroup_diff_t*};
 *
 *  @note The output handler is expected to include a leading ',' before any
 *        diff output. This will actually be handled by the handler json
 *        method.
 *
 *  @return The number of bytes written {int};
 */
ssize_t
cgm_netprio_json( cgroup_diff_t* cgd ) {

  ssize_t           written   = 0;
  u32               hdex      = 0;
  u32               sdex      = 0;
  bool              needComma = FALSE;
  cgroup_store_t*   cgStore   = cgd->store;
  netprio_store_t*  store     = NULL;
  netprio_iface_t*  iface;

  if (cgStore == NULL || cgStore->nChanges < 1) {
    return written;
  }

  store = container_of( cgStore, netprio_store_t, cgStore );

  // Iterate over the handlers for the top-level items
  for (hdex = 0, sdex = 0;
        hdex < cgd->nHandlers && sdex < cgStore->nEntries;
          hdex++) {
    cftype_handler_t* handler = &cgd->handlers[ hdex ];

    if (! handler->cft) { continue; }

    // assert( handler->json != NULL );
    written += handler->json( handler, cgd, sdex );
    sdex    += handler->nEntries;
  }

  /* Iterate over collected interface entries headed at store->ifaces
   * and output any entries that have changes.
   */
  written += lkb_write( ",", 1 );
  written += json_write_key( "interfaces" );
  written += lkb_write( "[", 1 );

  rcu_read_lock();
    hlist_for_each_entry_rcu( iface, &store->ifaces, nif_chain) {

      if (! iface->isDiff)  { continue; }

      written  += _ifpriomap_json( cgd, iface, needComma );
      needComma = TRUE;
    }
  rcu_read_unlock();

  written += lkb_write( "]", 1 );

  return written;
}

/**
 *  Release any allocated data for the given entry.
 *  @method cgm_netprio_release_rcu
 *  @param  entry   The cgroup diff entry for the target cgroup/subsystem
 *                  {cgroup_diff_t*};
 *
 *  @note   This method will be called from an RCU deletion callback
 *          (__cgroupHash_del_rcu()), which holds rcu_read_lock() so we
 *          cannot do anything that relies on obtaining the RCU lock ourselves
 *          (e.g. synchronize_rcu()).
 *
 *  @return void
 *  @protected
 */
static void
cgm_netprio_release_rcu( cgroup_diff_t* entry ) {
  // Free any data allocated and pointed to from within `data`
  if (entry->store)  {
    netprio_store_t*  store   = container_of( entry->store,
                                              netprio_store_t,
                                              cgStore );
    u32               nBytes  = sizeof( *store ) +
                                s_topEntries * (sizeof(u64) + sizeof(bool));

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
    _nifList_clear_rcu( store );
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD

    kfree( store );

    entry->store = NULL;

    s_bytesFreed += nBytes;
  }
}

/**
 *  Finalize, cleaning up any allocated resources.
 *  @method fini
 *
 *  @return void
 *  @protected
 */
static void
cgm_netprio_fini( void ) {

  if (s_bytesAlloc - s_bytesFreed != 0) {
    printk(KERN_WARNING "%s cgm_netprio_fini(): "
                        "%7llu bytes allocated, %7llu freed : %llu LEAKED\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed,
                                     s_bytesAlloc - s_bytesFreed);
  } else {
    printk(KERN_INFO    "%s cgm_netprio_fini(): "
                        "%7llu bytes allocated, %7llu freed\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed);
  }

}

/* cgroup module methods }
 *****************************************************************************/

/**
 *  Expose this as a cgroup module.
 */
cgroup_module_t cgroup_module_netprio = {
  .name     = "net_prio",
  .init     = cgm_netprio_init,
  .gather   = cgm_netprio_gather,
  .json     = cgm_netprio_json,
  .release  = cgm_netprio_release_rcu,
  .fini     = cgm_netprio_fini,
};
