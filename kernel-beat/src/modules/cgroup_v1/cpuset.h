/**
 *  cgroup v1 collector:  cpuset
 *
 *  This collector provides access to the `cpuset` cgroup subsystem metrics
 *  available via `/sys/fs/cgroup/cpuset/cpuset.*`:
 *    - cpu_exclusive               read_u64();
 *    - mem_exclusive               read_u64();
 *    - mem_hardwall                read_u64();
 *    - sched_load_balance          read_u64();
 *    - memory_migrate              read_u64();
 *    - memory_pressure             read_u64();
 *    - memory_spread_page          read_u64();
 *    - memory_spread_slab          read_u64();
 *    - memory_pressure_enabled     read_u64();
 *    - sched_relax_domain_level    read_s64();
 *
 *  :NOTE:  The following cgroup sequence-files are not currently collected:
 *            - cpus
 *            - mems
 *            - effective_cpus
 *            - effective_mems
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 */
#ifndef _CGROUP_CPUSET_H_
#define _CGROUP_CPUSET_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_cpuset;

#endif  // _CGROUP_CPUSET_H_
