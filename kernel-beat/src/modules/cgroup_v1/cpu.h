/**
 *  cgroup v1 collector:  cpu
 *
 *  This collector provides access to the `cpu` (aka 'cpu,cpuacct') cgroup
 *  subsystem metrics available via `/sys/fs/cgroup/cpu/cpu.*`:
 *    - shares          read_u64()
 *    - cfs_quota_us    read_u64()
 *    - cfs_period_us   read_u64()
 *    - rt_runtime_us   read_u64()
 *    - rt_period_us    read_u64()
 *    - nr_periods      via `stat` sequence-file;
 *    - nr_throttled    via `stat` sequence-file;
 *    - throttled_time  via `stat` sequence-file;
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 */
#ifndef _CGROUP_CPU_H_
#define _CGROUP_CPU_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_cpu;

#endif  // _CGROUP_CPU_H_
