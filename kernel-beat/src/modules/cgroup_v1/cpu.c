/**
 *  cgroup v1 collector:  cpu
 *
 *  This collector provides access to the `cpu` (aka 'cpu,cpuacct') cgroup
 *  subsystem metrics available via `/sys/fs/cgroup/cpu/cpu.*`:
 *    - shares          read_u64()
 *    - cfs_quota_us    read_u64()
 *    - cfs_period_us   read_u64()
 *    - rt_runtime_us   read_u64()
 *    - rt_period_us    read_u64()
 *    - nr_periods      via `stat` sequence-file;
 *    - nr_throttled    via `stat` sequence-file;
 *    - throttled_time  via `stat` sequence-file;
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 *
 *=============================================================================
 *    kernel/sched/core.c
 *      v4.9
 *        "shares"
 *          .read_u64 = cpu_shares_read_u64(    struct cgroup_subsys_state *css,
 *                                              struct cftype *cft)
 *
 *        "cfs_quota_us"
 *          .read_s64 = cpu_cfs_quota_read_s64( struct cgroup_subsys_state *css,
 *                                              struct cftype *cft)
 *
 *        "cfs_period_us"
 *          .read_u64 = cpu_cfs_period_read_u64(struct cgroup_subsys_state *css,
 *                                              struct cftype *cft)
 *
 *        "stat"
 *          .seq_show = cpu_stats_show(         struct seq_file *sf,
 *                                              void *v)
 *
 *        "rt_runtime_us"
 *          .read_s64 = cpu_rt_runtime_read(    struct cgroup_subsys_state *css,
 *                                              struct cftype *cft)
 *
 *        "rt_period_us"
 *          .read_u64 = cpu_rt_period_read_uint(struct cgroup_subsys_state *css,
 *                                              struct cftype *cft)
 *
 *=============================================================================
 *    kernel/sched/core.c
 *      v3.10
 *        "shares"
 *          .read_u64 = cpu_shares_read_u64(    struct cgroup *cgrp,
 *                                              struct cftype *cft)
 *        "cfs_quota_us"
 *          .read_s64 = cpu_cfs_quota_read_s64( struct cgroup *cgrp,
 *                                              struct cftype *cft)
 *        "cfs_period_us"
 *          .read_u64 = cpu_cfs_period_read_u64(struct cgroup *cgrp,
 *                                              struct cftype *cft)
 *        "stat"
 *          .read_map = cpu_stats_show(         struct cgroup *cgrp,
 *                                              struct cftype *cft,
 *                                              struct cgroup_map_cb *cb)
 *        "rt_runtime_us"
 *          .read_s64 = cpu_rt_runtime_read(    struct cgroup *cgrp,
 *                                              struct cftype *cft)
 *        "rt_period_us"
 *          .read_u64 = cpu_rt_period_read_uint(struct cgroup *cgrp,
 *                                              struct cftype *cft)
 */
#include "../../json.h"

#include "../cgroup_v1.h"

/*****************************************************************************/

/**
 *  The extractor for the `stat` cftype sequence.
 *  @method _cpuStat_extract
 *  @param  self      The cftype handler entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  A cftype extractor handles a single cftype and will fill `self->nEntries`
 *  values/change-indicators in `cgd`.
 *
 *  @note   Values are mapped as:
 *            values[0] => nr_periods s32
 *            values[1] => nr_throttled s32
 *            values[2] => throttled_time u64
 *
 *  @return The number of detected changes {u32};
 */
static u32
_cpuStat_extract( cftype_handler_t* self,
                  cgroup_diff_t*    cgd,
                  u32               start,
                  bool              doDiff ) {

  u32             nChanges  = 0;

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
  cgroup_store_t* store     = cgd->store;
  struct cftype*  cft       = self->cft;
  ssize_t         read      = 0;
  char            buf[128]  = {0};

  if (cft == NULL || store == NULL)  { return nChanges; }

  read = cgroup_v1_fsRead( cft, cgd, buf, sizeof(buf) );
  if (read > 0) {
    /* Process the incoming data
     *   nr_periods s32       => values[0]
     *   nr_throttled s32     => values[1]
     *   throttled_time u64   => values[2]
     */
    u64*  values    = store->values  + start;
    bool* changes   = store->changes + start;
    char* cur       = buf;
    char* end       = &buf[read];
    char* key       = NULL;
    char* val       = NULL;
    u32   consumed  = 0;

    while ( (consumed = lkb_getKeyNum( cur, end-cur, &key, &val )) > 0 ) {
      u32 index   = 0;
      u64 oldVal;
      u64 newVal;

      if (!strncmp( key, "nr_periods", 10 )) {
        s32 nv;
        index = 0;

        if (kstrtos32( val, 10, &nv )) {
          printk(KERN_WARNING "%s _cpustat_extract(): name[ %s ], "
                               "key[ %s ]: cannot parse number[ %s ]\n",
                               DEVICE_NAME, cft->name, key, val);
        } else {
          newVal = (u64)nv;
        }

      } else if (!strncmp( key, "nr_throttled", 12 )) {
        s32 nv;
        index = 1;

        if (kstrtos32( val, 10, &nv )) {
          printk(KERN_WARNING "%s _cpustat_extract(): name[ %s ], "
                               "key[ %s ], val[ %s ]: cannot parse number\n",
                               DEVICE_NAME, cft->name, key, val);
        } else {
          newVal = (u64)nv;
        }

      } else if (!strncmp( key, "throttled_time", 14 )) {
        index = 2;

        if (kstrtou64( val, 10, &newVal )) {
          printk(KERN_WARNING "%s _cpustat_extract(): name[ %s ], "
                               "key[ %s ], val[ %s ]: cannot parse number\n",
                               DEVICE_NAME, cft->name, key, val);
        }

      } else {
        printk(KERN_WARNING "%s _cpustat_extract(): name[ %s ]: "
                             "unexpected key[ %s ]\n",
                             DEVICE_NAME, cft->name, cur);
        break;
      }

      oldVal = values[ index ];

      if (!doDiff || newVal != oldVal) {
        changes[ index ] = TRUE;
        values[ index ]  = newVal;
        nChanges++;

      } else {
        changes[ index ] = FALSE;

      }

      /*
      printk(KERN_INFO "%s _cpuStat_extract():%u.%u: %s%s "
                              "[ %llu => %llu ]: %s [ %u total ]\n",
                           DEVICE_NAME, start, index,
                           cgd->path, self->name,
                           oldVal, newVal,
                           (changes[ index ] ? "DIFF" : "no-diff"),
                           nChanges);
      // */

      // Move beyond this key/value pair
      cur += consumed;
    }
  }

#endif

  return nChanges;
}

/**
 *  The JSON output handler for the `stat` cftype sequence.
 *  @method _cpuStat_json
 *  @param  self      The cftype map entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *
 *  A JSON output handler will output `self->nEntries` from the values
 *  area as identified by the parallel change-indicators within `cgd`.
 *
 *  If this method is not provided, `cgroup_v1_cftype_json()` will be used.
 *
 *  @note   Values are expected to be mapped as:
 *            values[0] => nr_periods s32
 *            values[1] => nr_throttled s32
 *            values[2] => throttled_time u64
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 */
static ssize_t
_cpuStat_json(  cftype_handler_t* self,
                cgroup_diff_t*    cgd,
                u32               start ) {

  ssize_t         written = 0;
  cgroup_store_t* store   = cgd->store;
  u64*            values  = NULL;
  bool*           changes = NULL;
  u32             idex;

  if (self->cft == NULL || store == NULL || store->nChanges < 1) {
    return written;
  }

  // Grab pointers to the `values` and `changes` arrays from the store
  values  = store->values  + start;
  changes = store->changes + start;

  for (idex = 0; idex < self->nEntries; idex++) {
    if (! changes[idex])  { continue; }

    // :XXX: We are required to always include a ',' before any output
    written += lkb_write( ",", 1 );

    switch( idex ) {
      case 0: // nr_periods s32
        written += json_write_s32( "cpu.stat.nr_periods", (s32)values[idex] );
        break;

      case 1: // nr_throttled s32
        written += json_write_s32( "cpu.stat.nr_throttled", (s32)values[idex] );
        break;

      case 2: // throttled_time u64
        written += json_write_u64( "cpu.stat.throttled_time", values[idex] );
        break;


      default:
        // BUG!
        printk(KERN_WARNING "%s _cpuStat_json(): "
                                  "Unexpected index %u > 2 [ %u ]\n",
                            DEVICE_NAME, idex, self->nEntries);
        break;
    }
  }

  return written;
}

/*****************************************************************************/

/**
 *  The mapping of cftype to handling information.
 *
 *  This array and associated data will be updated during module initalization
 *  to:
 *    - initialize the cftype pointer for each entry that can be supported;
 *    - unsupported entries will have a NULL `cft` property;
 *    - `s_entries` will be initialized to the total number of entries that may
 *      be generated by the active handlers;
 */
static cftype_handler_t   s_handler[] = {
  { .name= "shares",        .nEntries= 1 },
  { .name= "cfs_quota_us",  .nEntries= 1 },
  { .name= "cfs_period_us", .nEntries= 1 },
  { .name= "rt_runtime_us", .nEntries= 1 },
  { .name= "rt_period_us",  .nEntries= 1 },

  /* _cpuStat_extract()
   *    nr_periods s32
   *    nr_throttled s32
   *    throttled_time u64
   */
  { .name= "stat",          .nEntries= 3, .extract= _cpuStat_extract,
                                          .json   = _cpuStat_json },

  // Empty entry terminator : 'name == NULL'
  { 0 },
};
static u32                s_handlerCnt  = COUNT( s_handler ) - 1;
static u32                s_entries     = 0;

/**
 *  Associate the provided entry with our handler information and ensure space
 *  has been allocated for subsystem-specific data.
 *  @method _cgm_cpu_prepEntry
 *  @param  entry   The target entry {cgroup_diff_t*};
 *
 *  @return A success/failure indicator (0 == success) {int};
 *  @private
 */
static int
_cgm_cpu_prepEntry( cgroup_diff_t*  entry ) {

  if (entry->store == NULL) {
    // Create a new subsystem-specific store
    entry->store = cgroup_v1_makeStore( s_entries );
    if (entry->store == NULL) {
      printk(KERN_ALERT "%s _cgm_cpu_prepEntry(): Cannot create 'store'\n",
                        DEVICE_NAME);
      return -1;
    }

    // Include handler information
    entry->handlers  = s_handler;
    entry->nHandlers = s_handlerCnt;

  } else {
    // Reset the change counter
    entry->store->nChanges = 0;

  }

  return 0;
}

/*****************************************************************************
 * cgroup module methods {
 *
 */

/**
 *  Initialize this collection module.
 *  @method cgm_cpu_init
 *  @param  subsys  A pointer to the cgroup subsystem {struct cgroup_subsys*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_cpu_init( struct cgroup_subsys* subsys ) {
#ifdef  CONFIG_CGROUPS  // {
  u32             nActive = 0;
  CGROUP_CFT_SET* cfts;

  /*
  printk(KERN_INFO
            "%s cgm_cpu_init(): ssid[ %2d : %s ]\n",
              DEVICE_NAME,
              CGROUP_SUBSYS_ID(subsys),
              CGROUP_SUBSYS_NAME( subsys ));
  // */

  /* Fill in the cftype `cft` entries identifying (and counting) the final set
   * of active entries.
   *
   *  :NOTE: Upon completion, any inactive entry will have a NULL `cft`
   *         property.
   *
   *         By "inactive" we mean an entry for which no match exists in the
   *         cftype set of the provided subsystem.
   */
  s_entries = 0;
  list_for_each_entry( cfts, &CGROUP_SUBSYS_CFTSET(subsys), node ) {
    struct cftype* cft = CGROUP_SUBSYS_CFTSET_ITEM(cfts);

    while (cft && cft->name[0] != '\0') {

      cftype_handler_t* handler = cgroup_v1_findHandler( s_handler,
                                                         s_handlerCnt,
                                                         cft->name );
      if (handler != NULL) {
        handler->cft = cft;

        nActive++;
        s_entries += handler->nEntries;

        /*
        printk(KERN_INFO "%s cgm_cpu_init(): "
                              "Activate handler '%s', %u entries\n",
                           DEVICE_NAME, handler->name, handler->nEntries);
        // */

        if (! handler->extract) {
          // Use the default extract helper
          handler->extract = cgroup_v1_cftype_extract;
        }
        if (! handler->json) {
          // Use the default json output helper
          handler->json = cgroup_v1_cftype_json;
        }

      } else {
        // else, skip this cftype : generates no map entries
        printk(KERN_WARNING "%s cgm_cpu_init(): SKIP cftype[ %s ]\n",
                             DEVICE_NAME, cft->name);
      }

      cft++;
    }
  }

  /*
  printk(KERN_INFO
            "%s cgm_cpu_init(): ssid[ %2d : %s ] : "
                          "%u / %u handlers, %u entries\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ),
            nActive, s_handlerCnt,
            s_entries);
  // */

  // /* Memory metrics {
  {
    u32 nBytes_ss = sizeof( cgroup_store_t ) +
                      s_entries * (sizeof(u64) + sizeof(bool));

    printk(KERN_INFO
            "%s cgm_cpu_init(): ssid[ %2d : %s ] : "
                  "%u bytes / cgroup:subsystem\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ),
            nBytes_ss);
  }
  // /* Memory metrics }

#endif  // CONFIG_CGROUPS  }
}

/**
 *  Gather subsystem metrics identifying any differences from any prior data.
 *  @method cgm_cpu_gather
 *  @param  entry   The cgroup diff entry for the target cgroup/subsystem
 *                  {cgroup_diff_t*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_cpu_gather( cgroup_diff_t*  entry ) {
  cgroup_store_t*   store   = entry->store;
  bool              doDiff  = (store != NULL);
  u32               hdex    = 0;
  u32               sdex    = 0;

  if (_cgm_cpu_prepEntry( entry )) {
    // Preparation failed
    return;
  }

  // In case `entry->store` was initially created or moved
  store = entry->store;

  /*
  printk(KERN_INFO "%s cgm_cpu_gather(): path[ %s ]: "
                              "%u handlers, %u entries ...\n",
                     DEVICE_NAME, entry->path,
                                  entry->nHandlers, store->nEntries);
  // */

  /* Normalize the subsystem data, writing to `entry->data` and, if there was
   * pre-existing `entry->data`, identifying differences in both
   * `entry->map[].changed` and `entry->changed`;
   */
  for (hdex = 0, sdex = 0;
        hdex < entry->nHandlers && sdex < store->nEntries;
          hdex++) {
    cftype_handler_t* handler = &entry->handlers[ hdex ];

    if (! handler->cft) { continue; }

    /*
    printk(KERN_INFO "%s cgm_cpu_gather(): path[ %s ]: invoke '%s', "
                                "%u entries starting at %u ...\n",
                       DEVICE_NAME, entry->path,
                                    handler->name, handler->nEntries, sdex);
    // */

    // assert( mapEntry->extract != NULL );
    store->nChanges += handler->extract( handler, entry, sdex, doDiff );

    sdex += handler->nEntries;
  }
}

/* cgroup module methods }
 *****************************************************************************/

/**
 *  Expose this as a cgroup module.
 */
cgroup_module_t cgroup_module_cpu = {
  .name     = "cpu",
  .init     = cgm_cpu_init,
  .gather   = cgm_cpu_gather,
};
