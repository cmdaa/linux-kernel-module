/**
 *  cgroup metric interface for the `hugetlb` cgroup subsystem.
 *
 *  :TODO:  Implement.
 *            v4.9  : Uses simple, directly readable u64 values;
 *            v3.10 : reports all values in a sequence-file-like stream that
 *                    must be parsed;
 *
 *=============================================================================
 *    mm/hugetlb_cgroup.c
 *      v4.9
 *        "limit_in_bytes"      : MEMFILE_PRIVATE(idx, RES_LIMIT)
 *        "usage_in_bytes"      : MEMFILE_PRIVATE(idx, RES_USAGE)
 *        "max_usage_in_bytes"  : MEMFILE_PRIVATE(idx, RES_MAX_USAGE)
 *        "failcnt"             : MEMFILE_PRIVATE(idx, RES_FAILCNT)
 *          .read_u64 = hugetlb_cgroup_read_u64;
 *
 *=============================================================================
 *    mm/hugetlb_cgroup.c
 *      v3.10
 *        "usage_in_bytes"      : MEMFILE_PRIVATE(idx, RES_USAGE);
 *        "max_usage_in_bytes"  : MEMFILE_PRIVATE(idx, RES_MAX_USAGE);
 *        "failcnt"             : MEMFILE_PRIVATE(idx, RES_FAILCNT);
 *          .read = hugetlb_cgroup_read(  struct cgroup *cgroup,
 *                                        struct cftype *cft,
 *                                        struct file *file,
 *                                        char __user *buf,
 *                                        size_t nbytes,
 *                                        loff_t *ppos)
 */
