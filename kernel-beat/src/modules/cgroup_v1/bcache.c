/**
 *  cgroup metric interface for the `bcache` cgroup subsystem.
 *
 *  :TODO:  Implement (only available for v3.10)
 *
 *=============================================================================
 *    :XXX: NOT IN v4.9
 *
 *=============================================================================
 *    drivers/md/bcache/request.c
 *      v3.10
 *        "cache_mode"
 *          .read     = cache_mode_read(              struct cgroup *cgrp,
 *                                                    struct cftype *cft,
 *                                                    struct file *file,
 *                                                    char __user *buf,
 *                                                    size_t nbytes,
 *                                                    loff_t *ppos)
 *
 *        "verify"
 *          .read_u64 = bch_verify_read(              struct cgroup *cgrp,
 *                                                    struct cftype *cft)
 *
 *        "cache_hits"
 *          .read_u64 = bch_cache_hits_read(          struct cgroup *cgrp,
 *                                                    struct cftype *cft)
 *
 *        "cache_misses"
 *          .read_u64 = bch_cache_misses_read(        struct cgroup *cgrp,
 *                                                    struct cftype *cft)
 *
 *        "cache_bypass_hits"
 *          .read_u64 = bch_cache_bypass_hits_read(   struct cgroup *cgrp,
 *                                                    struct cftype *cft)
 *
 *        "cache_bypass_misses"
 *          .read_u64 = bch_cache_bypass_misses_read( struct cgroup *cgrp,
 *                                                    struct cftype *cft)
 */
