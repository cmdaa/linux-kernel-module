/**
 *  cgroup v1 collector:  blkio
 *
 *  This collector:
 *    - provides access to the `blkio` cgroup subsystem metrics available via
 *      `/sys/fs/cgroup/blkio/blkio.*` but consolidated by device major/minor
 *      number;
 *
 *    - makes use of a hash table to monitor the set of referenced block devices
 *      indexed by device major/minor number;
 *
 *    - accesses data from sequence-files:
 *        - blkio sequence files come in two flavors:
 *          - those that report pairs   : %maj%:%min% u64
 *          - those that report triples : %maj%:%min% %type% u64
 *
 *          - triple types are: Read, Write, Sync, Async, Discard, and Total
 *
 *  :NOTE:  Most data is available only if using the CFQ, or complete fairness
 *          queueing, disk scheduler.
 *
 *  :NOTE:  Access to sequence-file-based metrics
 *          (and thus all metrics available to this collector) may be restricted
 *          by excluding the USE_CGROUP_FSREAD #define.
 *
 *=============================================================================
 *    block/cfq-iosched.c
 *      v4.9
 *        offsetof(struct cfq_group, ...)
 *
 *        "leaf_weight_device"
 *          .seq_show   = cfqg_print_leaf_weight_device(  struct seq_file *sf,
 *                                                        void *v)
 *
 *        "leaf_weight"
 *          .seq_show   = cfq_print_leaf_weight(          struct seq_file *sf,
 *                                                        void *v)
 *
 *        "weight_device"
 *          .seq_show   = cfqg_print_weight_device(       struct seq_file *sf,
 *                                                        void *v)
 *
 *        "weight"
 *          .seq_show   = cfq_print_weight(               struct seq_file *sf,
 *                                                        void *v)
 *
 *        "time"                        : offsetof( ..., stats.time)
 *          .seq_show   = cfqg_print_stat(                struct seq_file *sf,
 *                                                        void *v)
 *
 *        "sectors"
 *          .seq_show   = cfqg_print_stat_sectors(        struct seq_file *sf,
 *                                                        void *v)
 *
 *        "io_service_time"             : offsetof( ..., stats.service_time)
 *        "io_wait_time"                : offsetof( ..., stats.wait_time)
 *        "io_merged"                   : offsetof( ..., stats.merged)
 *        "io_queued"                   : offsetof( ..., stats.queued)
 *          .seq_show   = cfqg_print_rwstat(              struct seq_file *sf,
 *                                                        void *v)
 *
 *        "time_recursive"              : offsetof( ..., stats.time)
 *          .seq_show   = cfqg_print_stat_recursive(      struct seq_file *sf,
 *                                                        void *v)
 *
 *        "sectors_recursive"
 *          .seq_show   = cfqg_print_stat_sectors_recursive(struct seq_file *sf,
 *                                                        void *v)
 *
 *        "io_service_time_recursive"   : offsetof( ..., stats.service_time)
 *        "io_wait_time_recursive"      : offsetof( ..., stats.wait_time)
 *        "io_merged_recursive"         : offsetof( ..., stats.merged)
 *        "io_queued_recursive"         : offsetof( ..., stats.queued)
 *          .seq_show   = cfqg_print_rwstat_recursive(    struct seq_file *sf,
 *                                                        void *v)
 *
 *        "avg_queue_size"
 *          .seq_show   = cfqg_print_avg_queue_size(      struct seq_file *sf,
 *                                                        void *v)
 *
 *        "group_wait_time"             : offsetof( ..., stats.group_wait_time)
 *        "idle_time"                   : offsetof( ..., stats.idle_time)
 *        "empty_time"                  : offsetof( ..., stats.empty_time)
 *        "dequeue"                     : offsetof( ..., stats.dequeue)
 *        "unaccounted_time"            : offsetof( ..., stats.unaccounted_time)
 *          .seq_show   = cfqg_print_stat(                struct seq_file *sf,
 *                                                        void *v)
 *
 *    block/blk-cgroup.c
 *      v4.9
 *        "io_service_bytes"            : (unsigned long)&blkcg_policy_cfq
 *        "throttle.io_service_bytes"   : (unsigned long)&blkcg_policy_throtl
 *          .seq_show   = blkg_print_stat_bytes(          struct seq_file *sf,
 *                                                        void *v);
 *
 *        "io_serviced"                 : (unsigned long)&blkcg_policy_cfq
 *        "throttle.io_serviced"        : (unsigned long)&blkcg_policy_throtl
 *          .seq_show   = blkg_print_stat_ios(            struct seq_file *sf,
 *                                                        void *v);
 *
 *        "io_service_bytes_recursive"  : (unsigned long)&blkcg_policy_cfq
 *          .seq_show   = blkg_print_stat_bytes_recursive(struct seq_file *sf,
 *                                                        void *v);
 *
 *        "io_serviced_recursive"       : (unsigned long)&blkcg_policy_cfq
 *          .seq_show   = blkg_print_stat_ios_recursive(  struct seq_file *sf,
 *                                                        void *v);
 *
 *    block/blk-throttle.c
 *      v4.9
 *        offsetof(struct throtl_grp, ...)
 *
 *        "throttle.read_bps_device"    : offsetof( ..., bps[READ])
 *        "throttle.write_bps_device"   : offsetof( ..., bps[WRITE])
 *          .seq_show   = tg_print_conf_u64(struct seq_file *sf, void *v)
 *
 *        "throttle.read_iops_device"   : offsetof( ..., iops[READ])
 *        "throttle.write_iops_device"  : offsetof( ..., iops[WRITE])
 *          .seq_show   = tg_print_conf_uint(struct seq_file *sf, void *v)
 *
 *=============================================================================
 *    block/cfq-iosched.c
 *      v3.10
 *        offsetof(struct cfq_group, ...)
 *
 *        "leaf_weight_device"
 *          .read_seq_string  = cfqg_print_leaf_weight_device(
 *                                                       struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "leaf_weight"
 *          .read_seq_string  = cfq_print_leaf_weight(   struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "weight_device"
 *          .read_seq_string  = cfqg_print_weight_device(struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "weight"
 *          .read_seq_string  = cfq_print_weight(        struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "time"                        : offsetof( ..., stats.time)
 *        "sectors"                     : offsetof( ..., stats.sectors)
 *        "group_wait_time"             : offsetof( ..., stats.group_wait_time)
 *        "idle_time"                   : offsetof( ..., stats.idle_time)
 *        "empty_time"                  : offsetof( ..., stats.empty_time)
 *        "dequeue"                     : offsetof( ..., stats.dequeue)
 *        "unaccounted_time"            : offsetof( ..., stats.unaccounted_time)
 *          .read_seq_string  = cfqg_print_stat(         struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "io_service_bytes"            : offsetof( ..., stats.service_bytes)
 *        "io_serviced"                 : offsetof( ..., stats.serviced)
 *        "io_service_time"             : offsetof( ..., stats.service_time)
 *        "io_wait_time"                : offsetof( ..., stats.wait_time)
 *        "io_merged"                   : offsetof( ..., stats.merged)
 *        "io_queued"                   : offsetof( ..., stats.queued)
 *          .read_seq_string  = cfqg_print_rwstat(       struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "time_recursive"              : offsetof( ..., stats.time)
 *        "sectors_recursive"           : offsetof( ..., stats.sectors)
 *          .read_seq_string  = cfqg_print_stat_recursive(
 *                                                       struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "io_service_bytes_recursive"  : offsetof( ..., stats.service_bytes)
 *        "io_serviced_recursive"       : offsetof( ..., stats.serviced)
 *        "io_service_time_recursive"   : offsetof( ..., stats.service_time)
 *        "io_wait_time_recursive"      : offsetof( ..., stats.wait_time)
 *        "io_merged_recursive"         : offsetof( ..., stats.merged)
 *        "io_queued_recursive"         : offsetof( ..., stats.queued)
 *          .read_seq_string  = cfqg_print_rwstat_recursive(
 *                                                       struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "avg_queue_size"
 *          .read_seq_string  = cfqg_print_avg_queue_size(
 *                                                       struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *    block/blk-throttle.c
 *      v3.10
 *        offsetof(struct throtl_grp, ...)
 *
 *        "throttle.read_bps_device"    : offsetof( ..., bps[READ])
 *          .read_seq_string  = tg_print_conf_u64(       struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        "throttle.read_iops_device"   : offsetof( ..., iops[READ])
 *        "throttle.write_iops_device"  : offsetof( ..., iops[WRITE])
 *          .read_seq_string  = tg_print_conf_uint(      struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *        ---------
 *        offsetof(struct tg_stats_cpu, ...)
 *
 *        "throttle.io_service_bytes"   : offsetof( ..., service_bytes)
 *        "throttle.io_serviced"        : offsetof( ..., serviced)
 *          .read_seq_string  = tg_print_cpu_rwstat(     struct cgroup *cgrp,
 *                                                       struct cftype *cft,
 *                                                       struct seq_file *sf)
 *
 *=============================================================================
 */
#include "../../map.h"
#include "../../json.h"

#include "../cgroup_v1.h"

#include <linux/slab.h>         // kzalloc() / krealloc() / kfree()
#include <linux/kdev_t.h>       // MKDEV()
#include <linux/fs.h>           // BDEVNAME_SIZE
#include <linux/list.h>         // struct hlist_node

// Track memory allocation
static u64  s_bytesAlloc = 0;
static u64  s_bytesFreed = 0;

/**
 *  Is this collector active?
 */
static bool s_isActive    = FALSE;

/**
 *  The number of generated entries, initialized via `cgm_blkio_init()` and
 *  associated with `s_handler`.
 *
 *  Handlers are divided into two primary groups:
 *    - those associated with top-level, cgroup/subsystem metrics;
 *    - those that generate metrics associated with one or more block devices
 *      via sequence files formatted as pairs or triples;
 */
static u32  s_topEntries  = 0;  // Top-level handler count;
static u32  s_devEntries  = 0;  // Block device handler count;
static u32  s_devFirst    = 0;  // Index of the first block device handler;

/**
 *  Generate/consume rwstats values in the order defined here.
 */
static char*  s_rwstats_label[]   = {
  "read", "write", "sync", "async", "discard", "total",
};
static u32    s_rwstats_labelCnt  = COUNT(s_rwstats_label);
#define       RWSTATS_LABEL_MAX_LEN 7

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
/**
 *  Common sequence read buffer.
 *
 *  blkio sequence files come in two flavors:
 *  - those that report pairs   : %maj%:%min% u64
 *  - those that report triples : %maj%:%min% %type% u64
 *
 *  Triple types are: Read, Write, Sync, Async, Discard, and Total
 *
 *  Sizes:
 *    Using 1-byte ASCII characters
 *
 *    Major / Minor numbers : 32-bits:    12 for major, 20 for minor
 *                            range       0..4095       0..1048575
 *                            characters  1-4           1-7
 *                            max 12 characters (with ':' separator)
 *                              though most systems will likely be 3-7
 *
 *    u64                     0..18,446,744,073,709,551,615
 *                            max 20 characters
 *
 *    Pairs:
 *      %maj%:%min%           max 12 characters [  3 -  7 ]
 *      u64                   max 20 characters [  1 -  9 ]
 *                            max 34 characters [  6 - 18 ] with separators
 *                                                          and line terminator
 *
 *    Triples:
 *      %maj%:%min%           max 12 characters [  3 -  7 ]
 *      %type%                max  5 characters [  4 -  5 ]
 *      u64                   max 20 characters [  1 -  9 ]
 *                            max 40 characters [ 11 - 24 ] with separators
 *                                                          and line terminator
 *
 *    Triple entries: 5 lines / device @  40 characters [ 11 -  24 ] each
 *                  :                    200 characters [ 55 - 120 ] total
 *    Pair   entries: 1 line  / device @  34 characters [  6 -  18 ] total
 *
 *  Given 55 - 200 characters per device for triples, 4096 bytes can accomodate
 *    20 - 74 devices each with 5 triples
 *
 *  Given 6 -  34 characters per device for pairs, 4096 bytes can accomodate
 *    120 - 682 devices each with 1 pair
 */
static char s_readBuf[4096] = {0};
static u32  s_readBufSize   = sizeof(s_readBuf);
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD

/**
 *  Statistics for a single blkio device, identified by '%maj%:%min%'
 *  (e.g. '8:0');
 *
 *  When created, the full size is:
 *      sizeof( bio_stats_t ) +
 *        sizeof( cgroup_store_t ) +
 *          s_devEntries * (sizeof(u64) + sizeof(bool));
 */
typedef struct {
  char              name[BDEVNAME_SIZE];
  u32               id;               // The device id == MKDEV(major, minor);

  // Hash table overflow chain
  struct hlist_node bio_chain;

  /* Subsystem-specific cftype value/change store.
   *
   * :NOTE: We are able to fully allocate this structure on creation so `store`
   *        point to the memory immediately following this entry.
   */
  cgroup_store_t*   store;

} bio_stats_t;

/**
 *  Over-ride cgroup_store_t to include a per-device list.
 *
 *  :NOTE: This collection module has minimal directly accessible data.
 *         The data primarily comes from sequence files that each
 *         generate/update multiple `bio_stats_t` device entries.
 */
typedef struct {
  /***********************************************
   * Per-device stats, one entry per device
   * that appears in the sequence files.
   *
   * :NOTE: This is the head of a list of
   *        `bio_stats_t` entries associated
   *        with THIS cgroup/subsystem.
   */
  struct hlist_head devices;

  /* The primary store:
   *    Given `cgroup_store_t* pcgs`
   *      blkio_store_t* pbis = container_of( pcgs, blkio_store_t, cgStore );
   */
  cgroup_store_t    cgStore;

} blkio_store_t;

/*****************************************************************************/

/****************************************************************
 * Block device list helpers {
 */

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
/**
 *  Check if a block device identified by the given id exists in the device
 *  list.
 *  @method _bioList_find
 *  @param  store The block io store {blkio_store_t*};
 *  @param  id    The id of the target block device {u32};
 *
 *  @return A pointer to the device entry if it exists {bio_stats_t*};
 *  @private
 */
static bio_stats_t*
_bioList_find( blkio_store_t* store, u32 id ) {
  bio_stats_t*  res     = NULL;
  bio_stats_t*  entry;

  rcu_read_lock();
    hlist_for_each_entry_rcu( entry, &store->devices, bio_chain) {
      if (entry->id == id) {
        res = entry;
        break;
      }
    }
  rcu_read_unlock();

  return res;
}

/**
 *  Add the given entry to the device list.
 *  @method _bioList_add
 *  @param  store The block io store {blkio_store_t*};
 *  @param  entry The entry to add {bio_stats_t*};
 *
 *  @return void
 *  @private
 */
static void
_bioList_add( blkio_store_t* store, bio_stats_t* entry ) {
  // assert( _bioList_find( store, entry ) == NULL )
  rcu_read_lock();
    hlist_add_head_rcu( &entry->bio_chain, &store->devices );
  rcu_read_unlock();
}

/**
 *  Find or create an entry for the given device id.
 *  @method _bioList_get
 *  @param  store The block io store {blkio_store_t*};
 *  @param  id    The id of the target block device {u32};
 *  @param  name  The target block device "name" {char*};
 *
 *  @return A pointer to the device entry, NULL on failure to allocate
 *          {bio_stats_t*};
 *  @private
 */
static bio_stats_t*
_bioList_get( blkio_store_t* store, u32 id, char* name ) {
  bio_stats_t*  entry = _bioList_find( store, id );

  if (entry == NULL) {
    // Device not found -- create a new entry
    u32 nBytes  = sizeof( *entry ) +
                    sizeof( *entry->store ) +
                    s_devEntries * (sizeof(u64) + sizeof(bool));
    entry = kzalloc( nBytes, GFP_KERNEL );

    /*
    printk(KERN_INFO "%s _bioList_get(): cache MISS for x%06x: %s\n",
                      DEVICE_NAME, id, name);
    // */

    if (entry) {
      // Add this new entry to our list
      entry->id = id;

      // Copy in the device "name"
      strncpy( entry->name, name, sizeof(entry->name) );

      s_bytesAlloc += nBytes;

      /*
      printk(KERN_INFO
                "%s _bioList_get(): "
                  "Created new %u byte entry for '%s', %u entries: %p\n",
                    DEVICE_NAME, nBytes, name, s_devEntries, entry);
      // */

      // Point to to the area just beyond this entry
      entry->store = (cgroup_store_t*)((void*)entry + sizeof(*entry));

      entry->store->nEntries = s_devEntries;
      entry->store->values   = GET_STORE_VALUES(  entry->store );
      entry->store->changes  = GET_STORE_CHANGES( entry->store );

      /*
      printk(KERN_INFO
                "%s _bioList_get(): "
                  "entry->store[ %p ], %u entries, "
                  "values[ %p ], changes[ %p ]\n",
                    DEVICE_NAME,
                    entry->store, entry->store->nEntries,
                    entry->store->values, entry->store->changes);
      // */

      _bioList_add( store, entry );

    } else {
      printk(KERN_WARNING "%s _bioList_get(): "
                              "Cannot create device entry for x%06x\n",
                     DEVICE_NAME, id);
    }
  }

  return entry;
}

/**
 *  Remove the given entry from our hash table and free the storage.
 *  @method _bioList_del_rcu
 *  @param  entry The target entry {bio_stats_t*};
 *
 *  @note   rcu_read_lock() should be held by a caller/ancestor.
 *
 *  @return void
 *  @private
 */
static void
_bioList_del_rcu( bio_stats_t* entry ) {
  u32         nBytes  = sizeof( *entry ) +
                          sizeof( *entry->store ) +
                          s_devEntries * (sizeof(u64) + sizeof(bool));

  hlist_del_rcu( &(entry->bio_chain) );

  s_bytesFreed += nBytes;

  kfree( entry );
}

/**
 *  Remove all entries from the device list of the given entry.
 *  @method _bioList_clear_rcu
 *  @param  store The block io store {blkio_store_t*};
 *
 *  @note   rcu_read_lock() should be held by a caller/ancestor.
 *
 *  @return void
 *  @private
 */
static void
_bioList_clear_rcu( blkio_store_t* store ) {
  bio_stats_t*  entry;

  /*
  printk(KERN_INFO "%s _bioList_clear_rcu(): clear device list\n",
                   DEVICE_NAME);
  // */

  hlist_for_each_entry_rcu( entry, &store->devices, bio_chain ) {
    _bioList_del_rcu( entry );
  }

  INIT_HLIST_HEAD( &store->devices );
}
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD

/* Block device list helpers }
 *****************************************************************************/

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
/**
 *  Given a string containg '%maj%:%min%', extract the major and minor numbers
 *  and convert them to a 32-bit device id.
 *  @method _blkio_extract_dev
 *  @param  buf       The target buffer {char*};
 *
 *  @return The device id (-1 on failure) {u32};
 *  @private
 */
static u32
_blkio_extract_dev( char* buf ) {
  char* maj   = buf;
  char* min   = buf;
  char* sep   = buf;
  bool  fail  = FALSE;
  u32   major;
  u32   minor;

  while( *sep != '\0' && *sep != ':' ) {
    sep++;
  }
  if (*sep != ':') { return -1; }

  // Null terminate for kstrtou32() and parse the major number
  *sep = '\0';
  if (kstrtou32( maj, 10, &major )) {
    // failed to parse the major number
    fail = TRUE;
  }

  /* Replace the null terminator with the original separator so we're not
   * actually changing the caller's buffer.
   */
  *sep = ':';
  if (fail) { return -1; }

  // Parse the minor number
  min = sep + 1;
  if (kstrtou32( min, 10, &minor )) {
    // failed to parse the major number
    return -1;
  }

  // Generate the full 32-bit device id
  return MKDEV( major, minor );
}
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD

/**
 *  Extract data from a cftype that will generate a single value.
 *  @method _blkio_extract_one
 *  @param  self      The cftype handler entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  @note   This is used for top-level stats;
 *
 *  @return The number of detected changes {u32};
 */
static u32
_blkio_extract_one( cftype_handler_t* self,
                    cgroup_diff_t*    cgd,
                    u32               start,
                    bool              doDiff ) {

  u32             nChanges  = 0;

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)

  struct cftype*  cft       = self->cft;
  ssize_t         read      = 0;
  cgroup_store_t* store     = cgd->store;
  u64*            values    = NULL; // into current `store`;
  bool*           changes   = NULL; // into current `store`;
  u64             newVal;

  if (cft == NULL || store == NULL)  { return nChanges; }

  // assert( self->nEntries == 1 );

  values  = store->values  + start;
  changes = store->changes + start;

  /* Read the cftype generated string/data into a buffer and then parse the
   * buffer to extract the simple stat.
   */
  read = cgroup_v1_fsRead( cft, cgd, s_readBuf, s_readBufSize );
  if (read < 1) { return nChanges; }

  /*
  printk(KERN_INFO "%s _blkio_extract_one():%u: %s%s: "
                            "read %ld bytes[ %s ]\n",
                      DEVICE_NAME, start, cgd->path, self->name,
                                   read, s_readBuf);
  // */

  if (kstrtou64( s_readBuf, 10, &newVal )) {
    // failed to parse the value
    printk(KERN_WARNING "%s _blkio_extract_one():%u:%s%s: "
                            "cannot parse number[ %s ]\n",
              DEVICE_NAME, start,
              cgd->path, self->name, s_readBuf);

  } else {
    // Determine if this is a diff and, if so, update the value.
    u64 oldVal = *values;

    if (!doDiff || newVal != oldVal) {
      *changes = TRUE;
      *values  = newVal;
      nChanges++;

    } else {
      *changes = FALSE;
    }

    /* Debug output {
    if (*changes) {
      printk(KERN_WARNING
              "%s _blkio_extract_one():%u:%s%s: %p:%p, "
              "doDiff[ %u ], [ %llu => %llu ]: DIFF    [ %u total ]\n",
                DEVICE_NAME, start,
                cgd->path, self->name,
                values, changes,
                doDiff, oldVal, newVal,
                nChanges);
    } else {
      printk(KERN_INFO
              "%s _blkio_extract_one():%u:%s%s: %p:%p, "
              "doDiff[ %u ], [ %llu => %llu ]: no-diff [ %u total ]\n",
                DEVICE_NAME, start,
                cgd->path, self->name,
                values, changes,
                doDiff, oldVal, newVal,
                nChanges);

    }
    // Debug output } */
  }

#endif

  return nChanges;
}

/**
 *  Extract data from a cftype that will generate a sequence of pairs.
 *  @method _blkio_extract_pairs
 *  @param  self      The cftype handler entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  For this extractor, both `start` and `self->nEntries` actually reference
 *  the target `bio_stats_t` entries:
 *    - `self->nEntries` indicates the number of entries that can be generated
 *      for each pair (1);
 *    - `start` is the starting index within `entry->store`;
 *
 *  The format of the retrieved sequence string is:
 *      %maj%:%min% u64
 *      ...
 *
 *  @return The number of detected changes {u32};
 */
static u32
_blkio_extract_pairs( cftype_handler_t* self,
                      cgroup_diff_t*    cgd,
                      u32               start,
                      bool              doDiff ) {

  u32             nChanges  = 0;

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)

  struct cftype*  cft       = self->cft;
  ssize_t         read      = 0;
  char*           cur       = s_readBuf;
  char*           end       = &s_readBuf[s_readBufSize];
  char*           key       = NULL;
  char*           val       = NULL;
  u32             consumed  = 0;
  char*           lastDev   = NULL; // Last '%maj%:%min%' device name
  blkio_store_t*  topStore  = NULL;
  bio_stats_t*    bios      = NULL;
  cgroup_store_t* bioStore  = NULL;
  u64*            values    = NULL; // into current `bios->store`;
  bool*           changes   = NULL; // into current `bios->store`;

  if (cft == NULL || cgd->store == NULL)  { return nChanges; }

  topStore = container_of( cgd->store, blkio_store_t, cgStore );

  // assert( self->nEntries == 1 );

  /* Read the cftype generated string/data into a buffer and then parse the
   * buffer to extract device-related stats.
   */
  read = cgroup_v1_fsRead( cft, cgd, s_readBuf, s_readBufSize );
  if (read < 1) { return nChanges; }

  /*
  printk(KERN_INFO "%s _blkio_extract_pairs():%u: %s%s: "
                            "read %ld bytes[ %s ]\n",
                      DEVICE_NAME, start, cgd->path, self->name,
                                   read, s_readBuf);
  // */

  end = &s_readBuf[ read ];

  // While we have a key/num pair ...
  while ( (consumed = lkb_getKeyNum( cur, end-cur, &key, &val )) > 0 ) {
    /* ... locate, create or use the previous device entry for the
     *      '%maj%:%min%' key
     */

    // key : %maj%:%min%
    if (lastDev == NULL || strncmp(key, lastDev, end-cur )) {
      /* New device
       *
       * See if we have an entry for this device.
       */
      u32 devId = _blkio_extract_dev( key );

      // :NOTE: On allocation failure, bios will be NULL
      bios = _bioList_get( topStore, devId, key );
      if (bios) {
        lastDev  = key;
        bioStore = bios->store;
        values   = bioStore->values  + start;
        changes  = bioStore->changes + start;
      }
    }

    if (bios) {
      // val : u64
      u64 oldVal  = *values;
      u64 newVal;

      if (kstrtou64( val, 10, &newVal )) {
        // failed to parse the value
        printk(KERN_WARNING "%s _blkio_extract_pairs():%u:"
                                " %s%s %-4s[ x%06x ]: cannot parse "
                                "number[ %s ] for type[ %s ]\n",
                  DEVICE_NAME, start,
                  cgd->path, self->name, bios->name, bios->id,
                  val, key);

      } else {
        // Determine if this is a diff and, if so, update the value.
        if (!doDiff || newVal != oldVal) {
          *changes = TRUE;
          *values  = newVal;
          bioStore->nChanges++;
          nChanges++;

        } else {
          *changes = FALSE;
        }

        /* Debug output {
        if (*changes) {
          printk(KERN_WARNING
                  "%s _blkio_extract_pairs():%u:%s%s %-4s[ x%06x ]: %p:%p, "
                  "doDiff[ %u ], [ %llu => %llu ]: DIFF    [ %u / %u total ]\n",
                    DEVICE_NAME, start,
                    cgd->path, self->name, bios->name, bios->id,
                    values, changes,
                    doDiff, oldVal, newVal,
                    bioStore->nChanges,
                    nChanges);
        } else {
          printk(KERN_INFO
                  "%s _blkio_extract_pairs():%u:%s%s %-4s[ x%06x ]: %p:%p, "
                  "doDiff[ %u ], [ %llu => %llu ]: no-diff [ %u / %u total ]\n",
                    DEVICE_NAME, start,
                    cgd->path, self->name, bios->name, bios->id,
                    values, changes,
                    doDiff, oldVal, newVal,
                    bioStore->nChanges,
                    nChanges);

        }
        // Debug output } */
      }

    } else {
      printk(KERN_ALERT
              "%s _blkio_extract_pairs():%u: No device entry found for "
                            "key[ %s ], val[ %s ]\n",
                DEVICE_NAME, start, key, val);
    }

    // Move beyond this key/value pair
    cur += consumed;
  }

#endif

  return nChanges;
}

/**
 *  Extract data from a cftype that will generate a sequence of triples.
 *  @method _blkio_extract_rwstats
 *  @param  self      The cftype handler entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  For this extractor, both `start` and `self->nEntries` actually reference
 *  the target `bio_stats_t` entries:
 *    - `self->nEntries` indicates the number of entries that can be generated
 *      for each device (6 : read, write, sync, async, discard, total);
 *    - `start` is the starting index within `entry->store`;
 *
 *  The format of the retrieved sequence string is:
 *      %maj%:%min% %type% u64
 *      ...
 *      Total u64
 *
 *  Valid %type% values are:
 *      Read, Write, Sync, Async, Discard, Total
 *
 *  @note   Generate values/changes in the order specified in `s_rwstats_label`
 *            0 : Read
 *            1 : Write
 *            2 : Sync
 *            3 : Async
 *            4 : Discard
 *            5 : Total
 *
 *  @return The number of detected changes {u32};
 */
static u32
_blkio_extract_rwstats( cftype_handler_t* self,
                        cgroup_diff_t*    cgd,
                        u32               start,
                        bool              doDiff ) {

  u32             nChanges  = 0;

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)

  struct cftype*  cft       = self->cft;
  ssize_t         read      = 0;
  char*           cur       = s_readBuf;
  char*           end       = &s_readBuf[s_readBufSize];
  char*           key       = NULL;
  char*           val       = NULL;
  u32             consumed  = 0;
  char*           lastDev   = NULL; // Last '%maj%:%min%' device name
  blkio_store_t*  topStore  = NULL;
  bio_stats_t*    bios      = NULL;
  cgroup_store_t* bioStore  = NULL;
  u64*            values    = NULL; // into current `bios->store`;
  bool*           changes   = NULL; // into current `bios->store`;

  if (cft == NULL || cgd->store == NULL)  { return nChanges; }

  topStore = container_of( cgd->store, blkio_store_t, cgStore );

  // assert( self->nEntries == s_rwstats_labelCnt );

  /* Read the cftype generated string/data into a buffer and then parse the
   * buffer to extract device-related stats.
   */
  read = cgroup_v1_fsRead( cft, cgd, s_readBuf, s_readBufSize );
  if (read < 1) { return nChanges; }

  /*
  printk(KERN_INFO "%s _blkio_extract_rwstats():%u: %s%s: "
                            "read %ld bytes[ %s ]\n",
                      DEVICE_NAME, start, cgd->path, self->name,
                                   read, s_readBuf);
  // */

  end = &s_readBuf[ read ];

  // While we have a key/value pair ...
  while ( (consumed = lkb_getKeyValue( cur, end-cur, &key, &val )) > 0 ) {
    if (! strncmp(key, "Total", 5)) {
      // End of list
      /* A triple list is terminated with a final line of:
       *    Total u64
       */
      break;
    }

    /* ... locate, create or use the previous device entry for the
     *      '%maj%:%min%' %type% u64
     */
    if (lastDev == NULL || strncmp(key, lastDev, end-cur )) {
      /* New device
       *
       * See if we have an entry for this device.
       */
      u32 devId = _blkio_extract_dev( key );

      // :NOTE: On allocation failure, bios will be NULL
      bios = _bioList_get( topStore, devId, key );
      if (bios) {
        lastDev  = key;
        bioStore = bios->store;
        values   = bioStore->values  + start;
        changes  = bioStore->changes + start;
      }
    }

    if (bios) {
      char* type    = val;

      // Find the separator between the type and following numeric value.
      u32 subConsume  = lkb_getKeyNum( type, end-type, &type, &val );
      consumed += subConsume;


      if (subConsume < 1) {
        printk(KERN_WARNING "%s _blkio_extract_rwstats():%u: %s%s"
                                  ": cannot locate next key/num pair[ %s ]\n",
                            DEVICE_NAME, start,
                            cgd->path, self->name, type);
      } else {
        // Identify the `type` and use it to determine the value/changes index.
        u32 index;

        for (index = 0; index < s_rwstats_labelCnt; index++) {
          if (!strncasecmp( type, s_rwstats_label[index], RWSTATS_LABEL_MAX_LEN)) {
            break;
          }
        }

        if (index >= s_rwstats_labelCnt) {
          printk(KERN_WARNING "%s _blkio_extract_rwstats():%u: %s%s"
                                    ": unexpected type[ %s ]\n",
                              DEVICE_NAME, start,
                              cgd->path, self->name, type);
        } else {
          // val  : u64
          u64 oldVal  = values[ index ];
          u64 newVal;

          if (kstrtou64( val, 10, &newVal )) {
            // failed to parse the value
            printk(KERN_WARNING "%s _blkio_extract_rwstats():%u.%u:%-5s: "
                                    "%s%s %-4s[ x%06x ]: cannot parse "
                                    "number[ %s ]\n",
                      DEVICE_NAME, start, index, type,
                      cgd->path, self->name, bios->name, bios->id,
                      val);

          } else {
            // Determine if this is a diff and, if so, update the value.
            if (!doDiff || newVal != oldVal) {
              changes[ index ] = TRUE;
              values[ index ]  = newVal;
              bioStore->nChanges++;
              nChanges++;

            } else {
              changes[ index ] = FALSE;
            }

            /* Debug output {
            if (changes[ index ]) {
              printk(KERN_WARNING
                      "%s _blkio_extract_rwstats():%u.%u:%-5s: "
                      "%s%s %-4s[ x%06x ]: values[ %p ], changes[ %p ], "
                      "doDiff[ %u ], [ %llu => %llu ]: DIFF    "
                      "[ %u / %u total ]\n",
                        DEVICE_NAME, start, index, type,
                        cgd->path, self->name, bios->name, bios->id,
                        values, changes,
                        doDiff, oldVal, newVal,
                        bioStore->nChanges,
                        nChanges);
            } else {
              printk(KERN_INFO
                      "%s _blkio_extract_rwstats():%u.%u:%-5s: "
                      "%s%s %-4s[ x%06x ]: values[ %p ], changes[ %p ], "
                      "doDiff[ %u ], [ %llu => %llu ]: no-diff "
                      "[ %u / %u total ]\n",
                        DEVICE_NAME, start, index, type,
                        cgd->path, self->name, bios->name, bios->id,
                        values, changes,
                        doDiff, oldVal, newVal,
                        bioStore->nChanges,
                        nChanges);

            }
            // Debug output } */
          }
        }
      }

    } else {
      printk(KERN_ALERT
              "%s _blkio_extract_rwstats():%u: No device entry found for "
                            "key[ %s ], val[ %s ]\n",
                DEVICE_NAME, start, key, val);
    }

    // Move beyond this key/value pair
    cur += consumed;
  }

#endif

  return nChanges;
}

/**
 *  Write the JSON version of the identified block device pair if there is a
 *  difference.
 *  @method _blkio_json_pair
 *  @param  cgd       The top-level cgroup diff entry {cgroup_diff_t*};
 *  @param  handler   The cftype handler that generated the value/change
 *                    information {cftype_handler_t*};
 *  @param  values    The generated value(s) {u64*};
 *  @param  changes   The generated change indicator(s) {bool*};
 *
 *  @return The number of bytes written {int};
 *  @private
 */
static ssize_t
_blkio_json_pair( cgroup_diff_t*    cgd,
                  cftype_handler_t* handler,
                  u64*              values,
                  bool*             changes ) {
  ssize_t written = 0;

  // assert( handler->nEntries == 1 );

  if (! *changes) { return written; }

  // Output change for this entry, ALWAYS starting with ','
  written += lkb_write( ",", 1 );

  // Output the key name '%subsys-name%.%name%'
  written += lkb_write( "\"", 1 );
  written += json_write_raw_strn_val( CGROUP_SUBSYS_NAME(cgd->subsys),
                                      CGROUP_MAX_PATH );
  written += lkb_write( ".", 1 );
  written += json_write_raw_strn_val( handler->name, CGROUP_MAX_PATH );
  written += lkb_write( "\":", 2 );

  written += json_write_u64_val( *values );
  // */

  //written += json_write_u64( handler->name, *values );

  return written;
}

/**
 *  Write the JSON version of the identified block device rwstats if there are
 *  any difference.
 *  @method _blkio_json_rwstats
 *  @param  cgd       The top-level cgroup diff entry {cgroup_diff_t*};
 *  @param  handler   The cftype handler that generated the value/change
 *                    information {cftype_handler_t*};
 *  @param  values    The generated value(s) {u64*};
 *  @param  changes   The generated change indicator(s) {bool*};
 *
 *  If there are any changes in the generated values, output them as an
 *  object:
 *    %cftype-name%: {
 *      read    : u64,
 *      write   : u64,
 *      sync    : u64,
 *      async   : u64,
 *      discard : u64,
 *      total   : u64
 *    }
 *
 *  @note   This relies on `_blkio_extract_rwstats()` generating values/changes
 *          in the order specified in `s_rwstats_label`:
 *            0 : Read
 *            1 : Write
 *            2 : Sync
 *            3 : Async
 *            4 : Discard
 *            5 : Total
 *
 *  @return The number of bytes written {int};
 *  @private
 */
static ssize_t
_blkio_json_rwstats(  cgroup_diff_t*    cgd,
                      cftype_handler_t* handler,
                      u64*              values,
                      bool*             changes) {
  ssize_t written   = 0;
  bool    hasChange = FALSE;
  bool    needComma = FALSE;
  u32     idex;

  // assert( self->nEntries == s_rwstats_labelCnt );

  // First determine if any of the change indicators are set.
  for (idex = 0; idex < handler->nEntries && !hasChange; idex++) {
    if (changes[idex])  { hasChange = TRUE; break; }
  }

  if (! hasChange)  { return written; }

  // Output change for this rwstats entry, ALWAYS starting with ','
  written += lkb_write( ",", 1 );

  // Output the key name '%subsys-name%.%name%'
  written += lkb_write( "\"", 1 );
  written += json_write_raw_strn_val( CGROUP_SUBSYS_NAME(cgd->subsys),
                                      CGROUP_MAX_PATH );
  written += lkb_write( ".", 1 );
  written += json_write_raw_strn_val( handler->name, CGROUP_MAX_PATH );
  written += lkb_write( "\":{", 3 );

  needComma = FALSE;
  for (idex = 0; idex < s_rwstats_labelCnt; idex++) {
    if (! changes[idex])  { continue; }

    if (needComma)  { written += lkb_write( ",", 1 ); }

    written += json_write_u64( s_rwstats_label[idex], values[ idex ] );

    needComma = TRUE;
  }

  written += lkb_write( "}", 1 );

  return written;
}

/**
 *  Given a single block device entry, write the JSON version of differences.
 *  @method _blkio_json
 *  @param  cgd       The top-level cgroup diff entry {cgroup_diff_t*};
 *  @param  bioStore  The block io store {cgroup_store_t*};
 *  @param  bioEntry  The block device entry {bio_stats_t*};
 *  @param  handler   A reference to first block-device-related handler from
 *                    s_handler {cftype_handler_t*};
 *
 *  If there are changes, output the block device name as a key for an object
 *  with all diffs as key/value pairs within the object.
 *
 *  @note   This method ASSUMES `handlers` is an entry in an array terminated
 *          with a handler having a NULL `name` property;
 *
 *  @return The number of bytes written {int};
 *  @private
 */
static ssize_t
_blkio_json(  cgroup_diff_t*    cgd,
              cftype_handler_t* handler,
              cgroup_store_t*   bioStore,
              bio_stats_t*      bioEntry ) {

  ssize_t written   = 0;
  u32     hdex      = 0;
  u32     sdex      = 0;
  u64*    values    = NULL;
  bool*   changes   = NULL;

  if (bioStore == NULL || bioStore->nChanges < 1)  { return written; }

  // assert( bioStore->nEntries == s_devEntries );

  // Output the key name
  written += lkb_write( "{", 1 );

  // bio_stats_t { char name[BDEVNAME_SIZE]; ... }
  written += json_write_u32(  "id",   bioEntry->id );
  written += lkb_write( ",", 1 );
  written += json_write_strn( "name", bioEntry->name, sizeof(bioEntry->name) );

  values   = bioStore->values;
  changes  = bioStore->changes;

  /*
  printk(KERN_INFO "%s _blkio_json(): bioEntry[ %s : x%06x ]: "
                                "%u / %u changes\n",
          DEVICE_NAME, bioEntry->name, bioEntry->id,
          bioStore->nChanges, bioStore->nEntries);
  // */

  // Walk the set of handlers to provide context for each value/change.
  for (hdex = 0, sdex = 0;
        handler && handler->name && sdex < bioStore->nEntries;
          hdex++, handler++){

    if (! handler->cft) { continue; }

    /*
    printk(KERN_INFO "%s _blkio_json(): bioEntry[ %s : x%06x ]: "
                                "output for handler #%u[ %s ], "
                                "%u entries ...\n",
            DEVICE_NAME, bioEntry->name, bioEntry->id,
            hdex, handler->name, handler->nEntries);
    // */

    if (handler->nEntries == 1) {
      // Output a single pair generated by this handler
      written += _blkio_json_pair( cgd, handler, values, changes );

    } else {
      // Output the set generated by this handler
      written += _blkio_json_rwstats(cgd, handler, values, changes );
    }

    values  += handler->nEntries;
    changes += handler->nEntries;
    sdex    += handler->nEntries;
  }

  written += lkb_write( "}", 1 );

  return written;
}

/*****************************************************************************/

/**
 *  The mapping of cftype to handling information.
 *
 *  This array and associated data will be updated during module initalization
 *  to:
 *    - initialize the cftype pointer for each entry that can be supported;
 *    - unsupported entries will have a NULL `cft` property;
 *    - `s_topEntries` will be initialized to the total number of entries that
 *      may be generated by the active handlers;
 *    - `s_devEntries` will be initialized to the total number of entries that
 *      may be generated by the full set of available sequence cftypes;
 *    - `s_devFirst` will be initialized to the index of the first
 *      block-device-related entry;
 */
static cftype_handler_t   s_handler[] = {
  { .name= "weight",          .nEntries= 1, .extract= _blkio_extract_one},
  { .name= "leaf_weight",     .nEntries= 1, .extract= _blkio_extract_one},

  /* pairs  : key u64 {
   *
   *    These entries are simply used to route to the proper extractor.
   *
   *    All `nEntries` values will be initialized to 1 in `cgm_blkio_init()`
   *    to indicate the number of properties generated within each target
   *    `bio_stats_t` entry;
   */
  { .name= "weight_device",     .extract= _blkio_extract_pairs},
  { .name= "leaf_weight_device",.extract= _blkio_extract_pairs},
  { .name= "time",              .extract= _blkio_extract_pairs},
  { .name= "time_recursive",    .extract= _blkio_extract_pairs},

  { .name= "sectors",           .extract= _blkio_extract_pairs},
  { .name= "sectors_recursive", .extract= _blkio_extract_pairs},

  { .name= "avg_queue_size",    .extract= _blkio_extract_pairs},

  { .name= "group_wait_time",   .extract= _blkio_extract_pairs},
  { .name= "idle_time",         .extract= _blkio_extract_pairs},
  { .name= "empty_time",        .extract= _blkio_extract_pairs},
  { .name= "dequeue",           .extract= _blkio_extract_pairs},
  { .name= "unaccounted_time",  .extract= _blkio_extract_pairs},
  // pairs  : key u64 }

  /* triples: key bio_rwstats_t {
   *
   *    These entries are simply used to route to the proper extractor.
   *
   *    All `nEntries` values will be initialized to
   *    6 (read, write, sync, async, discard, total) in `cgm_blkio_init()` to
   *    indicate the number of properties generated within each target
   *    `bio_stats_t` entry;
   */
  { .name= "io_service_bytes",            .extract= _blkio_extract_rwstats},
  { .name= "io_serviced",                 .extract= _blkio_extract_rwstats},
  { .name= "io_service_time",             .extract= _blkio_extract_rwstats},
  { .name= "io_wait_time",                .extract= _blkio_extract_rwstats},
  { .name= "io_merged",                   .extract= _blkio_extract_rwstats},
  { .name= "io_queued",                   .extract= _blkio_extract_rwstats},
  { .name= "io_service_bytes_recursive",  .extract= _blkio_extract_rwstats},
  { .name= "io_serviced_recursive",       .extract= _blkio_extract_rwstats},
  { .name= "io_service_time_recursive",   .extract= _blkio_extract_rwstats},
  { .name= "io_wait_time_recursive",      .extract= _blkio_extract_rwstats},
  { .name= "io_merged_recursive",         .extract= _blkio_extract_rwstats},
  { .name= "io_queued_recursive",         .extract= _blkio_extract_rwstats},

  { .name= "throttle.io_service_bytes",   .extract= _blkio_extract_rwstats},
  { .name= "throttle.io_serviced",        .extract= _blkio_extract_rwstats},
  { .name= "throttle.read_bps_device",    .extract= _blkio_extract_rwstats},
  { .name= "throttle.write_bps_device",   .extract= _blkio_extract_rwstats},
  { .name= "throttle.read_iops_device",   .extract= _blkio_extract_rwstats},
  { .name= "throttle.write_iops_device",  .extract= _blkio_extract_rwstats},
  // triples: key bio_rwstats_t }

  // Empty entry terminator : 'name == NULL'
  { 0 },
};

static u32                s_handlerCnt  = COUNT( s_handler ) - 1;

/**
 *  Associate the provided entry with our mapping and extractor information,
 *  ensure space has been allocated for subsystem-specific data and diff
 *  indicators, and that the diff indicators have all been reset.
 *  @method _cgm_blkio_prepEntry
 *  @param  entry   The target entry {cgroup_diff_t*};
 *
 *  @return A success/failure indicator (0 == success) {int};
 *  @private
 */
static int
_cgm_blkio_prepEntry( cgroup_diff_t*  entry ) {

  if (entry->store == NULL) {
    /* Allocate our custom store, which includes a cgroup_store_t so we can
     * treat it as a sort-of sub-class;
     *
     *    blkio_store_t*  pBioStore   = ...;
     *    cgroup_store_t* pCgStore    = &pBioStore->cgStore;
     *    blkio_store_t*  pBioStore2  = container_of( pCgStore,
     *                                                blkio_store_t,
     *                                                cgStore );
     *    pBioStore == pBioStore2
     *
     *    sizeof( cgroup_store_t )  : 24-bytes
     *      nEntries  4-bytes (1 32-bit value)
     *      nChanges  4-bytes (1 32-bit value)
     *      values    8-bytes (1 64-bit pointer)
     *      changes   8-bytes (1 64-bit pointer)
     *
     *    sizeof( blkio_store_t )   : 32-bytes
     *      devices    8-bytes (1 64-bit pointer)
     *      cgStore   24-bytes
     *
     *    allocated size:
     *      blkio_store_t 32-bytes
     *      values        s_topEntries * 8-bytes
     *      changes       s_topEntries * 1-byte
     */
    u32             nBytes  = sizeof( blkio_store_t ) +
                                s_topEntries * (sizeof(u64) + sizeof(bool));
    blkio_store_t*  store   = NULL;

    store = kzalloc( nBytes, GFP_KERNEL );
    if (store == NULL) {
      printk(KERN_ALERT "%s _cgm_blkio_prepEntry(): "
                              "Cannot allocate %u bytes for 'store'\n",
                        DEVICE_NAME, nBytes);
      return -1;
    }

    s_bytesAlloc += nBytes;

    /*
    printk(KERN_INFO
              "%s _cgm_blkio_prepEntry(): "
                "Created new %u byte store for %u entries: %p ...\n",
                  DEVICE_NAME, nBytes, s_topEntries, store);
    // */

    // Initialize the block device stats list
    INIT_HLIST_HEAD( &store->devices );

    entry->store            = &store->cgStore;
    entry->store->nEntries  = s_topEntries;
    entry->store->values    = GET_STORE_VALUES(  entry->store );
    entry->store->changes   = GET_STORE_CHANGES( entry->store );

    entry->handlers         = s_handler;
    entry->nHandlers        = s_handlerCnt;

    /*
    printk(KERN_INFO
              "%s _cgm_blkio_prepEntry(): "
                "entry->store[ %p ], %u entries, "
                "values[ %p ], changes[ %p ]\n",
                  DEVICE_NAME,
                  entry->store, entry->store->nEntries,
                  entry->store->values, entry->store->changes);
    // */

  } else {

    /* Reset the change counters of the top-level store and all block-device
     * stores.
     */
    blkio_store_t*  store = container_of(entry->store, blkio_store_t, cgStore);
    bio_stats_t*    bioEntry;

    entry->store->nChanges = 0;

    rcu_read_lock();
      hlist_for_each_entry_rcu( bioEntry, &store->devices, bio_chain) {
        cgroup_store_t* bioStore  = bioEntry->store;

        if (bioStore == NULL) {
          // BUG
          printk(KERN_WARNING "%s _cgm_blkio_prepEntry(): "
                                  "bioEntry[ %s : x%06x ]: "
                                  "has an unexpected NULL 'store'!\n",
                         DEVICE_NAME, bioEntry->name, bioEntry->id);
          continue;
        }

        bioStore->nChanges = 0;
      }
    rcu_read_unlock();

  }

  return 0;
}

/*****************************************************************************
 * cgroup module methods {
 *
 */

/**
 *  Initialize this collection module.
 *  @method cgm_blkio_init
 *  @param  subsys  A pointer to the cgroup subsystem {struct cgroup_subsys*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_blkio_init( struct cgroup_subsys* subsys ) {
#ifdef  CONFIG_CGROUPS  // {
  u32             nActive = 0;
  CGROUP_CFT_SET* cfts;

  /*
  printk(KERN_INFO
            "%s cgm_blkio_init(): ssid[ %2d : %s ]\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ));
  // */

  /* Fill in the cftype `cft` entries identifying (and counting) the final set
   * of active entries.
   *
   *  :NOTE: Upon completion, any inactive entry will have a NULL `cft`
   *         property.
   *
   *         By "inactive" we mean an entry for which no match exists in the
   *         cftype set of the provided subsystem.
   */
  s_topEntries = 0;
  s_devEntries = 0;
  s_devFirst   = -1;
  list_for_each_entry( cfts, &CGROUP_SUBSYS_CFTSET(subsys), node ) {
    struct cftype* cft = CGROUP_SUBSYS_CFTSET_ITEM(cfts);

    while (cft && cft->name[0] != '\0') {
      cftype_handler_t* handler = cgroup_v1_findHandler( s_handler,
                                                         s_handlerCnt,
                                                         cft->name );
      if (handler != NULL && handler->cft == NULL) {
        u32 hdex  = INDEXOF( s_handler, handler );

        handler->cft = cft;

        nActive++;

        if (handler->extract == _blkio_extract_pairs) {
          /* This is a block device handler that will generate a single metric
           * per block device referenced in the sequence file.
           */
          handler->nEntries = 1;
          s_devEntries     += handler->nEntries;

          if (hdex < s_devFirst) { s_devFirst = hdex; }

        } else if (handler->extract == _blkio_extract_rwstats) {
          /* This is a block device handler that will generate multiple metrics
           * per block device referenced in the sequence file.
           *
           *    read, write, sync, async, discard, total
           */
          handler->nEntries = s_rwstats_labelCnt;
          s_devEntries     += handler->nEntries;

          if (hdex < s_devFirst) { s_devFirst = hdex; }

        } else {
          if (! handler->extract) {
            // Fall-back to the cftype extract helper
            handler->extract = cgroup_v1_cftype_extract;
          }
          // else assert( handler->extract == _blkio_extract_one )

          s_topEntries    += handler->nEntries;

        }

        /*
        printk(KERN_INFO "%s cgm_blkio_init(): "
                              "Activate '%s' handler #%u '%s' "
                              "for cftype[ %s:%s ], %u entries\n",
                           DEVICE_NAME,
                           (handler->extract == _blkio_extract_rwstats
                              ? "rwstats"
                              : (handler->extract == _blkio_extract_pairs
                                  ? "pairs"
                                  : "top") ),
                           hdex, handler->name,
                           subsys->name, cft->name,
                           handler->nEntries);
        // */

        if (! handler->json) {
          // Use the default json output helper
          handler->json = cgroup_v1_cftype_json;
        }

      } else if (handler != NULL) {
        // else, skip this cftype : generates no map entries
        printk(KERN_WARNING "%s cgm_blkio_init(): SKIP cftype[ %s ]: "
                            "already actived ...\n",
                             DEVICE_NAME, cft->name);

      } else {
        // else, skip this cftype : generates no map entries
        printk(KERN_WARNING "%s cgm_blkio_init(): SKIP cftype[ %s ]\n",
                             DEVICE_NAME, cft->name);
      }

      cft++;
    }
  }

  /*
  printk(KERN_INFO
            "%s cgm_blkio_init(): ssid[ %2d : %s ] : "
                          "%u / %u handlers, "
                          "%u top-level entries, "
                          "%u devEntries (starting at %u)\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ),
            nActive, s_handlerCnt,
            s_topEntries, s_devEntries, s_devFirst);
  // */

  // /* Memory metrics {
  {
    u32 nBytes_ss = sizeof( blkio_store_t ) +
                      s_topEntries * (sizeof(u64) + sizeof(bool));
    u32 nBytes_bd = sizeof( bio_stats_t ) +
                    sizeof( cgroup_store_t ) +
                    s_devEntries * (sizeof(u64) + sizeof(bool));

    printk(KERN_INFO
            "%s cgm_blkio_init(): ssid[ %2d : %s ] : "
                  "%u bytes / cgroup:subsystem + %u bytes / block device\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ),
            nBytes_ss, nBytes_bd);
  }
  // /* Memory metrics }

  s_isActive = TRUE;
#endif  // CONFIG_CGROUPS  }
}

/**
 *  Gather subsystem metrics identifying any differences from any prior data.
 *  @method gather
 *  @param  entry   The cgroup diff entry for the target cgroup/subsystem
 *                  {cgroup_diff_t*};
 *
 *  @note   This method will update:
 *            entry->changed
 *            entry->data[]
 *
 *  @return void
 *  @protected
 */
static void
cgm_blkio_gather( cgroup_diff_t*  entry ) {
  cgroup_store_t*   store   = entry->store;
  bool              doDiff  = (store != NULL);
  u32               hdex    = 0;
  u32               sdex    = 0;

  if (! s_isActive || _cgm_blkio_prepEntry( entry )) {
    // Preparation failed
    return;
  }

  // In case `entry->store` was initially created or moved
  store = entry->store;

  /* Normalize the subsystem data, writing to `entry->data` and, if there was
   * pre-existing `entry->data`, identifying differences in both
   * `entry->map[].changed` and `entry->changed`;
   *
   *  :NOTE:  Since this cgroup handler separates out cftypes into top-level and
   *          per-block-device, we need to iterate over ALL handlers to allow
   *          each to properly scatter the metrics it collects.
   */
  for (hdex = 0, sdex = 0; hdex < s_handlerCnt; hdex++) {
    cftype_handler_t* handler = &entry->handlers[ hdex ];

    if (hdex == s_devFirst) {
      /* All further entries reference block-device entries with values/changes
       * in `bio_stats_t` stores so the store index needs to be reset.
       */
      sdex = 0;
    }

    if (! handler->cft) { continue; }

    /*
    printk(KERN_INFO "%s cgm_blkio_gather(): path[ %s ]: "
                                "handler %d: %s ...\n",
                       DEVICE_NAME, entry->path, hdex, handler->name);
    // */

    // assert( mapEntry->extract != NULL );
    store->nChanges += handler->extract( handler, entry, sdex, doDiff );

    sdex += handler->nEntries;
  }

  /*
  printk(KERN_INFO "%s cgm_blkio_gather(): path[ %s ]: %u changes\n",
                     DEVICE_NAME, entry->path, store->nChanges);
  // */
}

/**
 *  Given a gathered cgroup data, write the JSON version of differences.
 *  @method cgm_blkio_json
 *  @param  cgd     The cgroup diff entry {cgroup_diff_t*};
 *
 *  @note The output handler is expected to include a leading ',' before any
 *        diff output. This will actually be handled by the handler json
 *        method.
 *
 *  @return The number of bytes written {int};
 */
ssize_t
cgm_blkio_json( cgroup_diff_t* cgd ) {
  ssize_t           written = 0;
  u32               hdex    = 0;
  u32               sdex    = 0;
  u32               nDevs   = 0;
  cgroup_store_t*   cgStore = cgd->store;
  blkio_store_t*    store   = NULL;
  bio_stats_t*      bioEntry;

  if (cgStore == NULL || cgStore->nChanges < 1) {
    return written;
  }

  store = container_of( cgStore, blkio_store_t, cgStore );

  /*
  printk(KERN_INFO
          "%s cgm_blkio_json(): %u / %u top-level entries changed ...\n",
            DEVICE_NAME, cgStore->nChanges, cgStore->nEntries);
  // */

  // Iterate over the handlers for the top-level items
  //  for (handler = cgd->handlers; handler && handler->name; handler++)
  for (hdex = 0, sdex = 0;
        hdex < cgd->nHandlers && sdex < cgStore->nEntries;
          hdex++) {
    cftype_handler_t* handler = &cgd->handlers[ hdex ];

    if (! handler->cft) { continue; }

    // assert( handler->json != NULL );
    written += handler->json( handler, cgd, sdex );
    sdex    += handler->nEntries;
  }

  /* Iterate over collected block-specific entries headed at store->devices
   * and output any entries that have changes.
   */
  written += lkb_write( ",", 1 );
  written += json_write_key( "devices" );
  written += lkb_write( "[", 1 );

  rcu_read_lock();
    hlist_for_each_entry_rcu( bioEntry, &store->devices, bio_chain) {
      cgroup_store_t* bioStore  = bioEntry->store;

      if (bioStore == NULL) {
        // BUG
        printk(KERN_WARNING "%s cgm_blkio_json(): bioEntry[ %s : x%06x ]: "
                                      "has an unexpected NULL 'store'!\n",
                       DEVICE_NAME, bioEntry->name, bioEntry->id);
        continue;
      }

      /*
      printk(KERN_INFO
              "%s cgm_blkio_json(): bioEntry[ %s : x%06x ]: %u changes\n",
                DEVICE_NAME, bioEntry->name, bioEntry->id, bioStore->nChanges);
      // */

      if (bioStore->nChanges > 0) {
        if (nDevs > 0) { written += lkb_write( ",", 1 ); }

        written += _blkio_json( cgd, &s_handler[s_devFirst],
                                bioStore, bioEntry );

        nDevs++;
      }
    }
  rcu_read_unlock();

  written += lkb_write( "]", 1 );

  return written;
}

/**
 *  Release any allocated data for the given entry.
 *  @method cgm_blkio_release_rcu
 *  @param  entry   The cgroup diff entry for the target cgroup/subsystem
 *                  {cgroup_diff_t*};
 *
 *  @note   This method will be called from an RCU deletion callback
 *          (__cgroupHash_del_rcu()), which holds rcu_read_lock() so we
 *          cannot do anything that relies on obtaining the RCU lock ourselves
 *          (e.g. synchronize_rcu()).
 *
 *  @return void
 *  @protected
 */
static void
cgm_blkio_release_rcu( cgroup_diff_t* entry ) {
  // Free any data allocated and pointed to from within `data`
  if (entry->store)  {
    blkio_store_t*  store   = container_of( entry->store,
                                            blkio_store_t,
                                            cgStore );
    u32             nBytes  = sizeof( *store ) +
                                s_topEntries * (sizeof(u64) + sizeof(bool));

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
    _bioList_clear_rcu( store );
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD

    kfree( store );
    entry->store = NULL;

    s_bytesFreed += nBytes;
  }
}

/**
 *  Finalize, cleaning up any allocated resources.
 *  @method fini
 *
 *  @return void
 *  @protected
 */
static void
cgm_blkio_fini( void ) {

  if (s_bytesAlloc - s_bytesFreed != 0) {
    printk(KERN_WARNING "%s cgm_blkio_fini()  : "
                        "%7llu bytes allocated, %7llu freed : %llu LEAKED\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed,
                                     s_bytesAlloc - s_bytesFreed);
  } else {
    printk(KERN_INFO    "%s cgm_blkio_fini()  : "
                        "%7llu bytes allocated, %7llu freed\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed);
  }

  s_isActive = FALSE;
}

/* cgroup module methods }
 *****************************************************************************/

/**
 *  Expose this as a cgroup module.
 */
cgroup_module_t cgroup_module_blkio = {
  .name     = "blkio",
  .init     = cgm_blkio_init,
  .gather   = cgm_blkio_gather,
  .json     = cgm_blkio_json,
  .release  = cgm_blkio_release_rcu,
  .fini     = cgm_blkio_fini,
};
