/**
 *  cgroup v1 collector:  pids
 *
 *  This collector provides access to the `pids` cgroup subsystem metrics
 *  available via `/sys/fs/cgroup/pids/ * /pids.*`:
 *    - current     read_s64();
 *    - max         via sequence-file (s64 or 'max');
 *
 *  :NOTE:  The following cgroup sequence-files are not currently collected:
 *            - events
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 *
 *=============================================================================
 *    kernel/cgroup_pids.c
 *      v4.9
 *        "max"
 *          .seq_show = pids_max_show(      struct seq_file *sf, void *v)
 *            s64 or 'max' where 'max' == PIDS_MAX (PID_MAX_LIMIT + 1ULL)
 *
 *        "current"
 *          .read_s64 = pids_current_read(  struct cgroup_subsys_state *css,
 *                                          struct cftype *cft)
 *
 *        "events"
 *          .seq_show = pids_events_show(   struct seq_file *sf, void *v)
 *
 *=============================================================================
 *    kernel/cgroup_pids.c
 *      v3.10
 *        "max"
 *          .read_seq_string  = pids_max_show(    struct cgroup *cgroup,
 *                                                struct cftype *cft,
 *                                                struct seq_file *sf)
 *            s64 or 'max' where 'max' == PIDS_MAX (PID_MAX_LIMIT + 1ULL)
 *
 *        "current"
 *          .read_s64         = pids_current_read(struct cgroup *cgroup,
 *                                                struct cftype *cft)
 */
#include "../../json.h"

#include "../cgroup_v1.h"

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
/* A shared read buffer for cftype generated string/data for 'pids.max'.
 *
 *  The data will be either an s64 or the string 'max'
 *    Min 64-bit signed  : −9,223,372,036,854,775,808 == 20 characters
 *    Max 64-bit signed  :  9,223,372,036,854,775,807 == 19 characters
 */
static char     s_readBuf[21]   = {0};
static u32      s_readBufSize   = sizeof(s_readBuf);
#endif  // CONFIG_CGROUPS && USE_CGROUP_FSREAD

/*****************************************************************************/

/**
 *  The extractor for the 'pids.max' sequence.
 *  @method _pidsMax_extract
 *  @param  self      The cftype handler entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup data {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  A cftype extractor handles a single cftype and will fill `self->nEntries`
 *  values/change-indicators in `cgd`.
 *
 *  @note   This sequence will output EITHER a signed 64-bit value OR the
 *          string 'max'.
 *
 *  @return The number of detected changes {u32};
 */
static u32
_pidsMax_extract( cftype_handler_t* self,
                  cgroup_diff_t*    cgd,
                  u32               start,
                  bool              doDiff ) {

  u32             nChanges  = 0;

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
  cgroup_store_t* store     = cgd->store;
  struct cftype*  cft       = self->cft;
  ssize_t         read      = 0;

  /*
  printk(KERN_INFO "%s _pidsMax_extract():%02d: name[ %s ] ...\n",
                   DEVICE_NAME, start, cft->name);
  // */

  if (s_readBuf != NULL) {
    // Read the cgroup data
    read = cgroup_v1_fsRead( cft, cgd, s_readBuf, s_readBufSize );
  }

  /*
  printk(KERN_INFO "%s _pidsMax_extract():%02d: "
                                "name[ %s ]: read %ld bytes[ %s ]\n",
                   DEVICE_NAME, mapIndex, cft->name, read, s_readBuf);
  // */

  if (read > 0) {
    /*  Process the incoming data:
     *    'max' OR an s64
     */
    u64*  values    = store->values  + start;
    bool* changes   = store->changes + start;

    //  Remove the terminating new-line (ASSUMING it is the last character)
    s_readBuf[ read-1 ] = '\0';

    if (!doDiff || memcmp( values, s_readBuf, read )) {

      if (!strncmp( s_readBuf, "max", sizeof(u64) )) {
        // Appears to be the string 'max' -- copy up to 8 bytes (sizeof(u64))
        u64 asStr;

        memcpy( &asStr, s_readBuf, sizeof(u64) );

        *values  = asStr;
        *changes = TRUE;
        nChanges++;

      } else {
        // See if we can parse this as a signed 64-bit value
        s64 oldVal  = (s64)*values;
        s64 newVal;

        if (kstrtos64( s_readBuf, 10, &newVal )) {
          printk(KERN_WARNING "%s _pudsMax_extract(): name[ %s ], "
                               "val[ %s ]: cannot parse number\n",
                               DEVICE_NAME, cft->name, s_readBuf);

        } else if (!doDiff || newVal != oldVal) {

          *changes = TRUE;
          *values  = (u64)newVal;
          nChanges++;

        } else {
          *changes = FALSE;

        }
      }

    } else {
      *changes = FALSE;
    }
  }
#endif

  return nChanges;
}

/**
 *  The JSON output handler for the `pids.max` cftype sequence.
 *  @method _pidsMax_json
 *  @param  self      The cftype map entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup diff state {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *
 *  A JSON output handler will output `self->nEntries` from the values
 *  area as identified by the parallel change-indicators within `cgd`.
 *
 *  @note   Output either the string 'max' or a signed 64-bit value.
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 */
static ssize_t
_pidsMax_json(  cftype_handler_t* self,
                cgroup_diff_t*    cgd,
                u32               start ) {

  ssize_t         written = 0;
  cgroup_store_t* store   = cgd->store;
  u64*            values  = NULL;
  bool*           changes = NULL;
  char*           strVal;

  if (self->cft == NULL || store == NULL || store->nChanges < 1) {
    return written;
  }

  // Grab pointers to the `values` and `changes` arrays from the store
  values  = store->values  + start;
  changes = store->changes + start;

  if (! *changes) { return written; }

  // :XXX: We are required to always include a ',' before any output
  written += lkb_write( ",", 1 );

  strVal = (char*)values;
  if ( !strncmp( strVal, "max", 3 )) {
    // Output the string 'max'
    written += json_write_strn( "pids.max", "max", 3 );

  } else {
    // Output as a signed 64-bit value
    written += json_write_s64( "pids.max", (s64)*values );
  }

  return written;
}

/*****************************************************************************/

/**
 *  The mapping of cftype to handling information.
 *
 *  This array and associated data will be updated during module initalization
 *  to:
 *    - initialize the cftype pointer for each entry that can be supported;
 *    - unsupported entries will have a NULL `cft` property;
 *    - `s_entries` will be initialized to the total number of entries that may
 *      be generated by the active handlers;
 */
static cftype_handler_t   s_handler[] = {
  { .name= "current",    .nEntries= 1 },

  // Sequence-related properties
  { .name= "max",        .nEntries= 1, .extract= _pidsMax_extract,
                                       .json   = _pidsMax_json },

  // :TODO: "events" sequence (v4.9)

  // Empty entry terminator : 'name == NULL'
  { 0 },
};
static u32                s_handlerCnt  = COUNT( s_handler ) - 1;
static u32                s_entries     = 0;

/**
 *  Associate the provided entry with our mapping and extractor information and
 *  ensure space has been allocated for subsystem-specific data and diff
 *  indicators.
 *  @method _cgm_pids_prepEntry
 *  @param  entry   The target entry {cgroup_diff_t*};
 *
 *  @return A success/failure indicator (0 == success) {int};
 *  @private
 */
static int
_cgm_pids_prepEntry( cgroup_diff_t*  entry ) {

  if (entry->store == NULL) {
    // Create a new subsystem-specific store
    entry->store = cgroup_v1_makeStore( s_entries );
    if (entry->store == NULL) {
      printk(KERN_ALERT "%s _cgm_pids_prepEntry(): Cannot create 'store'\n",
                        DEVICE_NAME);
      return -1;
    }

    // Include handler information
    entry->handlers  = s_handler;
    entry->nHandlers = s_handlerCnt;

  } else {
    // Reset the change counter
    entry->store->nChanges = 0;

  }

  return 0;
}

/*****************************************************************************
 * cgroup module methods {
 *
 */

/**
 *  Initialize this collection module.
 *  @method cgm_pids_init
 *  @param  subsys  A pointer to the cgroup subsystem {struct cgroup_subsys*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_pids_init( struct cgroup_subsys* subsys ) {
#ifdef  CONFIG_CGROUPS  // {
  u32             nActive = 0;
  CGROUP_CFT_SET* cfts;

  /*
  printk(KERN_INFO "%s cgm_pids_init(): ssid[ %2d : %s ]\n",
                   DEVICE_NAME,
                   CGROUP_SUBSYS_ID(subsys),
                   CGROUP_SUBSYS_NAME( subsys ));
  // */

  /* Fill in the cftype `cft` entries identifying (and counting) the final set
   * of active entries.
   *
   *  :NOTE: Upon completion, any inactive entry will have a NULL `cft`
   *         property.
   *
   *         By "inactive" we mean an entry for which no match exists in the
   *         cftype set of the provided subsystem.
   */
  s_entries = 0;
  list_for_each_entry( cfts, &CGROUP_SUBSYS_CFTSET(subsys), node ) {
    struct cftype* cft = CGROUP_SUBSYS_CFTSET_ITEM(cfts);

    while (cft && cft->name[0] != '\0') {

      cftype_handler_t* handler = cgroup_v1_findHandler( s_handler,
                                                         s_handlerCnt,
                                                         cft->name );
      if (handler != NULL) {
        handler->cft = cft;

        nActive++;
        s_entries += handler->nEntries;

        if (! handler->extract) {
          // Use the default extract helper
          handler->extract = cgroup_v1_cftype_extract;
        }
        if (! handler->json) {
          // Use the default json output helper
          handler->json = cgroup_v1_cftype_json;
        }

      } else {
        // else, skip this cftype : generates no map entries
        printk(KERN_WARNING "%s cgm_pids_init(): SKIP cftype[ %s ]\n",
                             DEVICE_NAME, cft->name);
      }

      cft++;
    }
  }

  // /* Memory metrics {
  {
    u32 nBytes_ss = sizeof( cgroup_store_t ) +
                      s_entries * (sizeof(u64) + sizeof(bool));

    printk(KERN_INFO
            "%s cgm_pids_init(): ssid[ %2d : %s ] : "
                  "%u bytes / cgroup:subsystem\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ),
            nBytes_ss);
  }
  // /* Memory metrics }

#endif  // CONFIG_CGROUPS  }
}

/**
 *  Gather subsystem metrics identifying any differences from any prior data.
 *  @method cgm_pids_gather
 *  @param  entry   The cgroup diff entry for the target cgroup/subsystem
 *                  {cgroup_diff_t*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_pids_gather( cgroup_diff_t*  entry ) {
  cgroup_store_t*   store   = entry->store;
  bool              doDiff  = (store != NULL);
  u32               hdex    = 0;
  u32               sdex    = 0;

  if (_cgm_pids_prepEntry( entry )) {
    // Preparation failed
    return;
  }

  // In case `entry->store` was initially created or moved
  store = entry->store;

  /* Normalize the subsystem data, writing to `entry->data` and, if there was
   * pre-existing `entry->data`, identifying differences in both
   * `entry->map[].changed` and `entry->changed`;
   */
  for (hdex = 0, sdex = 0;
        hdex < entry->nHandlers && sdex < store->nEntries;
          hdex++) {
    cftype_handler_t* handler = &entry->handlers[ hdex ];

    if (! handler->cft) { continue; }

    // assert( mapEntry->extract != NULL );
    store->nChanges += handler->extract( handler, entry, sdex, doDiff );

    sdex += handler->nEntries;
  }
}
/* cgroup module methods }
 *****************************************************************************/

/**
 *  Expose this as a cgroup module.
 */
cgroup_module_t cgroup_module_pids = {
  .name     = "pids",
  .init     = cgm_pids_init,
  .gather   = cgm_pids_gather,
};
