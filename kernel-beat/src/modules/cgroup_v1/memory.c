/**
 *  cgroup v1 collector:  memory
 *
 *  This collector provides access to the `memory` cgroup subsystem metrics
 *  available via `/sys/fs/cgroup/memory/memory.*`:
 *    - memsw.usage_in_bytes          read_u64();
 *    - memsw.max_usage_in_bytes      read_u64();
 *    - memsw.limit_in_bytes          read_u64();
 *    - memsw.failcnt                 read_u64();
 *    - usage_in_bytes                read_u64();
 *    - max_usage_in_bytes            read_u64();
 *    - limit_in_bytes                read_u64();
 *    - soft_limit_in_bytes           read_u64();
 *    - failcnt                       read_u64();
 *    - kmem.limit_in_bytes           read_u64();
 *    - kmem.usage_in_bytes           read_u64();
 *    - kmem.failcnt                  read_u64();
 *    - kmem.max_usage_in_bytes       read_u64();
 *    - kmem.tcp.limit_in_bytes       read_u64();
 *    - kmem.tcp.usage_in_bytes       read_u64();
 *    - kmem.tcp.failcnt              read_u64();
 *    - kmem.tcp.max_usage_in_bytes   read_u64();
 *    - swap.current                  read_u64();
 *    - use_hierarchy                 read_u64();
 *    - swappiness                    read_u64();
 *    - move_charge_at_immigrate      read_u64();
 *
 *  :NOTE:  The following cgroup sequence-files are not currently collected:
 *            - swap.max
 *            - stat
 *                cache, rss, rss_huge, mapped_file, dirty, writeback,
 *                pgpgin, pgpgout, pgfault, pgmajfault,
 *                inactive_anon, active_anon, inactive_file, active_file,
 *                unevictable, hierarchical_memory_limit,
 *
 *                total_cache, total_rss, total_rss_huge, total_mapped_file,
 *                total_dirty, total_writeback,
 *                total_pgpgin, total_pgpgout, total_pgfault, total_pgmajfault,
 *                total_inactive_anon, total_active_anon,
 *                total_inactive_file, total_active_file,
 *                total_unevictable
 *
 *            - oom_control
 *                oom_kill_disable, under_oom
 *
 *            - pressure_level
 *                *** CANNOT READ ***
 *
 *            - numa_stat
 *                total=%v% N0=%v%
 *                file=%v% N0=%v%
 *                anon=%v% N0=%v%
 *                unevictable=%v% N0=%v%
 *                hierarchical_total=%v% N0=%v%
 *                hierarchical_file=%v% N0=%v%
 *                hierarchical_anon=%v% N0=%v%
 *                hierarchical_unevictable=%v% N0=%v%
 *
 *            - kmem.slabinfo
 *                slabinfo - version: 2.1
 *                # name            <active_objs> <num_objs> <objsize> \
 *                                    <objperslab> <pagesperslab> : tunables \
 *                                    <limit> <batchcount> <sharedfactor> : \
 *                                    slabdata <active_slabs> <num_slabs> \
 *                                    <sharedavail>
 *
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 *
 *=============================================================================
 *    mm/memcontrol.c
 *      v4.9
 *        "memsw.usage_in_bytes"        : MEMFILE_PRIVATE(_MEMSWAP, RES_USAGE)
 *        "memsw.max_usage_in_bytes"    : MEMFILE_PRIVATE(_MEMSWAP,
 *                                                                RES_MAX_USAGE)
 *        "memsw.limit_in_bytes"        : MEMFILE_PRIVATE(_MEMSWAP, RES_LIMIT),
 *        "memsw.failcnt"               : MEMFILE_PRIVATE(_MEMSWAP, RES_FAILCNT)
 *        "usage_in_bytes"              : MEMFILE_PRIVATE(_MEM, RES_USAGE)
 *        "max_usage_in_bytes"          : MEMFILE_PRIVATE(_MEM, RES_MAX_USAGE)
 *        "limit_in_bytes"              : MEMFILE_PRIVATE(_MEM, RES_LIMIT)
 *        "soft_limit_in_bytes"         : MEMFILE_PRIVATE(_MEM, RES_SOFT_LIMIT)
 *        "failcnt"                     : MEMFILE_PRIVATE(_MEM, RES_FAILCNT)
 *        "kmem.limit_in_bytes"         : MEMFILE_PRIVATE(_KMEM, RES_LIMIT)
 *        "kmem.usage_in_bytes"         : MEMFILE_PRIVATE(_KMEM, RES_USAGE)
 *        "kmem.failcnt"                : MEMFILE_PRIVATE(_KMEM, RES_FAILCNT)
 *        "kmem.max_usage_in_bytes"     : MEMFILE_PRIVATE(_KMEM, RES_MAX_USAGE)
 *        "kmem.tcp.limit_in_bytes"     : MEMFILE_PRIVATE(_TCP, RES_LIMIT)
 *        "kmem.tcp.usage_in_bytes"     : MEMFILE_PRIVATE(_TCP, RES_USAGE)
 *        "kmem.tcp.failcnt"            : MEMFILE_PRIVATE(_TCP, RES_FAILCNT)
 *        "kmem.tcp.max_usage_in_bytes" : MEMFILE_PRIVATE(_TCP, RES_MAX_USAGE)
 *          .read_u64 = mem_cgroup_read_u64(  struct cgroup_subsys_state *css,
 *                                            struct cftype *cft)
 *
 *        "swap.current"
 *          .read_u64 = swap_current_read(    struct cgroup_subsys_state *css,
 *                                            struct cftype *cft)
 *
 *        "swap.max"
 *          .seq_show = swap_max_show(  struct seq_file *m, void *v)
 *
 *        "use_hierarchy"
 *          .read_u64 = mem_cgroup_hierarchy_read(
 *                                            struct cgroup_subsys_state *css,
 *                                            struct cftype *cft)
 *
 *        "swappiness"
 *          .read_u64 = mem_cgroup_swappiness_read(
 *                                            struct cgroup_subsys_state *css,
 *                                            struct cftype *cft)
 *
 *        "move_charge_at_immigrate"
 *          .read_u64 = mem_cgroup_move_charge_read(
 *                                            struct cgroup_subsys_state *css,
 *                                            struct cftype *cft)
 *
 *        "stat"
 *          .seq_show = memcg_stat_show(  struct seq_file *m, void *v)
 *              cache, rss, rss_huge, mapped_file, dirty, writeback,
 *              pgpgin, pgpgout, pgfault, pgmajfault,
 *              inactive_anon, active_anon, inactive_file, active_file,
 *              unevictable, hierarchical_memory_limit,
 *
 *              total_cache, total_rss, total_rss_huge, total_mapped_file,
 *              total_dirty, total_writeback,
 *              total_pgpgin, total_pgpgout, total_pgfault, total_pgmajfault,
 *              total_inactive_anon, total_active_anon,
 *              total_inactive_file, total_active_file,
 *              total_unevictable
 *
 *        "oom_control" : MEMFILE_PRIVATE(_OOM_TYPE, OOM_CONTROL)
 *          .seq_show = mem_cgroup_oom_control_read(  struct seq_file *sf,
 *                                                    void *v)
 *              oom_kill_disable, under_oom
 *
 *        "pressure_level"
 *              *** CANNOT READ ***
 *
 *        "numa_stat"
 *          .seq_show = memcg_numa_stat_show( struct seq_file *m, void *v)
 *              total= N0=
 *              file= N0=
 *              anon= N0=
 *              unevictable= N0=
 *              hierarchical_total= N0=
 *              hierarchical_file= N0=
 *              hierarchical_anon= N0=
 *              hierarchical_unevictable= N0=
 *
 *        "kmem.slabinfo"
 *          .seq_show = memcg_slab_show(      struct seq_file *m, void *p)
 *              slabinfo - version: 2.1
 *              # name            <active_objs> <num_objs> <objsize> <objperslab> <pagesperslab> : tunables <limit> <batchcount> <sharedfactor> : slabdata <active_slabs> <num_slabs> <sharedavail>
 *
 *=============================================================================
 *    mm/memcontrol.c
 *      v3.10
 *        "memsw.usage_in_bytes"    : MEMFILE_PRIVATE(_MEMSWAP, RES_USAGE),
 *        "memsw.max_usage_in_bytes": MEMFILE_PRIVATE(_MEMSWAP, RES_MAX_USAGE),
 *        "usage_in_bytes"          : MEMFILE_PRIVATE(_MEM, RES_USAGE),
 *        "max_usage_in_bytes"      : MEMFILE_PRIVATE(_MEM, RES_MAX_USAGE),
 *        "limit_in_bytes"          : MEMFILE_PRIVATE(_MEM, RES_LIMIT),
 *        "soft_limit_in_bytes"     : MEMFILE_PRIVATE(_MEM, RES_SOFT_LIMIT),
 *        "failcnt"                 : MEMFILE_PRIVATE(_MEM, RES_FAILCNT),
 *        "kmem.limit_in_bytes"     : MEMFILE_PRIVATE(_KMEM, RES_LIMIT),
 *        "kmem.usage_in_bytes"     : MEMFILE_PRIVATE(_KMEM, RES_USAGE),
 *        "kmem.failcnt"            : MEMFILE_PRIVATE(_KMEM, RES_FAILCNT),
 *        "kmem.max_usage_in_bytes" : MEMFILE_PRIVATE(_KMEM, RES_MAX_USAGE),
 *        "memsw.usage_in_bytes"    : MEMFILE_PRIVATE(_MEMSWAP, RES_USAGE),
 *        "memsw.limit_in_bytes"    : MEMFILE_PRIVATE(_MEMSWAP, RES_LIMIT),
 *        "memsw.failcnt"           : MEMFILE_PRIVATE(_MEMSWAP, RES_FAILCNT),
 *          .read     = mem_cgroup_read(                  struct cgroup *cont,
 *                                                        struct cftype *cft,
 *                                                        struct file *file,
 *                                                        char __user *buf,
 *                                                        size_t nbytes,
 *                                                        loff_t *ppos)
 *
 *        "stat"
 *          .read_seq_string = memcg_stat_show(           struct cgroup *cont,
 *                                                        struct cftype *cft,
 *                                                        struct seq_file *m)
 *
 *        "use_hierarchy"
 *          .read_u64         = mem_cgroup_hierarchy_read(struct cgroup *cont,
 *                                                        struct cftype *cft)
 *
 *        "swappiness"
 *          .read_u64         = mem_cgroup_swappiness_read( struct cgroup *cgrp,
 *                                                          struct cftype *cft)
 *
 *        "move_charge_at_immigrate"
 *          .read_u64         = mem_cgroup_move_charge_read(struct cgroup *cgrp,
 *                                                          struct cftype *cft)
 *
 *        "oom_control"       : MEMFILE_PRIVATE(_OOM_TYPE, OOM_CONTROL),
 *          .read_map         = mem_cgroup_oom_control_read(
 *                                                  struct cgroup *cgrp,
 *                                                  struct cftype *cft,
 *                                                  struct cgroup_map_cb *cb)
 *
 *        "numa_stat"
 *          .read_seq_string  = memcg_numa_stat_show( struct cgroup *cont,
 *                                                    struct cftype *cft,
 *                                                    struct seq_file *m)
 *
 *        "kmem.slabinfo"
 *          .read_seq_string  = mem_cgroup_slabinfo_read( struct cgroup *cont,
 *                                                        struct cftype *cft,
 *                                                        struct seq_file *m)
 */
#include "../cgroup_v1.h"

#if     LINUX_VERSION_CODE < KERNEL_VERSION(3,15,0) // {
/**
 *  Prior to v4.9 (presumably around 3.15 with other cgroup changes), several
 *  cgroup properties are only available using a sequence-file-based read().
 *  For this, use a common buffer capable of storing a string-based u64 value.
 *
 *  A u64 could be a string as large as 20-digits ['18446744073709552000']
 *  with a NULL/new-line terminator.
 */
static char     s_readBuf[21] = {0};
static u32      s_readBufSize = sizeof( s_readBuf );
#endif  // LINUX_VERSION_CODE  }

/*****************************************************************************/

/**
 *  An extractor to normalize the change from using a sequence-file-based read()
 *  and direct read_u64() for many cgroup properties.
 *  @method _memNorm_extract
 *  @param  self      The cftype handler entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup data {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  @return The number of detected changes {u32};
 */
static u32
_memNorm_extract( cftype_handler_t* self,
                  cgroup_diff_t*    cgd,
                  u32               start,
                  bool              doDiff ) {

#if     LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0) // {
  /* v3.15+ uses a direct read_u64() so call the `cgroup_v1_cftype_extract()`
   * helper;
   */
  return cgroup_v1_cftype_extract( self, cgd, start, doDiff );

# else   // LINUX_VERSION_CODE < 3.15.0 }{
  // v3.15- uses a sequence-based read()
  u32             nChanges  = 0;

  // defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD) {
# if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
  cgroup_store_t* store     = cgd->store;
  struct cftype*  cft       = self->cft;
  ssize_t         read      = 0;

  if (cft == NULL || store == NULL)  { return nChanges; }

  /*
  printk(KERN_WARNING "%s _memNorm_extract(): name[ %s : %s ], start[ %u ]...\n",
                   DEVICE_NAME, cft->name, self->name, start);
  // */

  if (s_readBuf != NULL) {
    // Read the cgroup data
    read = cgroup_v1_fsRead( cft, cgd, s_readBuf, s_readBufSize );
  }

  /*
  printk(KERN_INFO "%s _memNorm_extract(): "
                                "name[ %s : %s ]: read %ld bytes[ %s ]\n",
                   DEVICE_NAME, cft->name, self->name, read, s_readBuf);
  // */

  if (read > 0) {
    u64*  values  = store->values  + start;
    bool* changes = store->changes + start;
    char* cur     = s_readBuf;
    u64   newVal;

    // Parse the generated u64 string.
    if (kstrtou64( cur, 10, &newVal )) {
      printk(KERN_WARNING "%s _perCpu_extract():%u: %s%s "
                                  "str[ %s ]: cannot parse number\n",
                           DEVICE_NAME, start, cgd->path, self->name, cur);
    } else {
      u64 oldVal  = *values;

      if ( oldVal != newVal ) {
        // Difference
        *changes = TRUE;
        *values  = newVal;
        nChanges++;

      } else {
        *changes = FALSE;
      }

      /*
      printk(KERN_INFO "%s _perCpu_extract():%u.%u: %s%s "
                                  "[ %llu => %llu ]: %s [ %u total ]\n",
                          DEVICE_NAME, start, idex,
                                       cgd->path, self->name,
                                       oldVal, newVal,
                                       (*changes ? "DIFF" : "no-diff"),
                                       nChanges);
      // */
    }
  }
# endif
  // defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD) }

  return nChanges;

# endif  // LINUX_VERSION_CODE  }
}

/*****************************************************************************/
/**
 *  The mapping of cftype to handling information.
 *
 *  This array and associated data will be updated during module initalization
 *  to:
 *    - initialize the cftype pointer for each entry that can be supported;
 *    - unsupported entries will have a NULL `cft` property;
 *    - `s_entries` will be initialized to the total number of entries that may
 *      be generated by the active handlers;
 */
static cftype_handler_t   s_handler[] = {
  // v3.10 uses read(), v4.9 uses read_u64() {
  { .name= "failcnt",                     .nEntries= 1,
                                          .extract = _memNorm_extract },

  { .name= "kmem.failcnt",                .nEntries= 1,
                                          .extract = _memNorm_extract },
  { .name= "kmem.limit_in_bytes",         .nEntries= 1,
                                          .extract = _memNorm_extract },
  { .name= "kmem.max_usage_in_bytes",     .nEntries= 1,
                                          .extract = _memNorm_extract },
  { .name= "kmem.usage_in_bytes",         .nEntries= 1,
                                          .extract = _memNorm_extract },

  { .name= "memsw.failcnt",               .nEntries= 1,
                                          .extract = _memNorm_extract },
  { .name= "memsw.limit_in_bytes",        .nEntries= 1,
                                          .extract = _memNorm_extract },
  { .name= "memsw.max_usage_in_bytes",    .nEntries= 1,
                                          .extract = _memNorm_extract },
  { .name= "memsw.usage_in_bytes",        .nEntries= 1,
                                          .extract = _memNorm_extract },

  { .name= "limit_in_bytes",              .nEntries= 1,
                                          .extract = _memNorm_extract },
  { .name= "max_usage_in_bytes",          .nEntries= 1,
                                          .extract = _memNorm_extract },
  { .name= "usage_in_bytes",              .nEntries= 1,
                                          .extract = _memNorm_extract },

  { .name= "soft_limit_in_bytes",         .nEntries= 1,
                                          .extract = _memNorm_extract },
  // v3.10 uses read(), v4.9 uses read_u64() }

  { .name= "move_charge_at_immigrate",    .nEntries= 1 },
  { .name= "swappiness",                  .nEntries= 1 },
  { .name= "use_hierarchy",               .nEntries= 1 },

  // Linux v4.9+ {
  { .name= "kmem.tcp.failcnt",            .nEntries= 1 },
  { .name= "kmem.tcp.limit_in_bytes",     .nEntries= 1 },
  { .name= "kmem.tcp.max_usage_in_bytes", .nEntries= 1 },
  { .name= "kmem.tcp.usage_in_bytes",     .nEntries= 1 },

  { .name= "swap.current",                .nEntries= 1 },
  // Linux v4.9+ }

  /* :TODO: Sequence-related properties
   *    "swap.max"                  // v4.9
   *    "stat"
   *    "oom_control"
   *    "pressure_level"            // v4.9
   *    "numa_stat"
   *    "kmem.slabinfo"
   */

  // Empty entry terminator : 'name == NULL'
  { 0 },
};
static u32                s_handlerCnt  = COUNT( s_handler ) - 1;
static u32                s_entries     = 0;

/**
 *  Associate the provided entry with our handler information and ensure space
 *  has been allocated for subsystem-specific data.
 *  @method _cgm_memory_prepEntry
 *  @param  entry   The target entry {cgroup_diff_t*};
 *
 *  @return A success/failure indicator (0 == success) {int};
 *  @private
 */
static int
_cgm_memory_prepEntry( cgroup_diff_t*  entry ) {

  if (entry->store == NULL) {
    // Create a new subsystem-specific store
    entry->store = cgroup_v1_makeStore( s_entries );
    if (entry->store == NULL) {
      printk(KERN_ALERT "%s _cgm_memory_prepEntry(): Cannot create 'store'\n",
                        DEVICE_NAME);
      return -1;
    }

    // Include handler information
    entry->handlers  = s_handler;
    entry->nHandlers = s_handlerCnt;

  } else {
    // Reset the change counter
    entry->store->nChanges = 0;

  }

  return 0;
}

/*****************************************************************************
 * cgroup module methods {
 *
 */

/**
 *  Initialize this collection module.
 *  @method cgm_memory_init
 *  @param  subsys  A pointer to the cgroup subsystem {struct cgroup_subsys*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_memory_init( struct cgroup_subsys* subsys ) {
#ifdef  CONFIG_CGROUPS  // {
  u32             nActive = 0;
  CGROUP_CFT_SET* cfts;

  /*
  printk(KERN_INFO "%s cgm_memory_init(): ssid[ %2d : %s ]\n",
                   DEVICE_NAME,
                   CGROUP_SUBSYS_ID(subsys),
                   CGROUP_SUBSYS_NAME( subsys ));
  // */

  /* Fill in the cftype `cft` entries identifying (and counting) the final set
   * of active entries.
   *
   *  :NOTE: Upon completion, any inactive entry will have a NULL `cft`
   *         property.
   *
   *         By "inactive" we mean an entry for which no match exists in the
   *         cftype set of the provided subsystem.
   */
  s_entries = 0;
  list_for_each_entry( cfts, &CGROUP_SUBSYS_CFTSET(subsys), node ) {
    struct cftype* cft = CGROUP_SUBSYS_CFTSET_ITEM(cfts);

    while (cft && cft->name[0] != '\0') {

      cftype_handler_t* handler = cgroup_v1_findHandler( s_handler,
                                                         s_handlerCnt,
                                                         cft->name );
      if (handler != NULL) {
        handler->cft = cft;

        nActive++;
        s_entries += handler->nEntries;

        if (! handler->extract) {
          // Use the default extract helper
          handler->extract = cgroup_v1_cftype_extract;
        }
        if (! handler->json) {
          // Use the default json output helper
          handler->json = cgroup_v1_cftype_json;
        }

      } else {
        // else, skip this cftype : generates no map entries
        printk(KERN_WARNING "%s cgm_memory_init(): SKIP cftype[ %s ]\n",
                             DEVICE_NAME, cft->name);
      }

      cft++;
    }
  }

  // /* Memory metrics {
  {
    u32 nBytes_ss = sizeof( cgroup_store_t ) +
                      s_entries * (sizeof(u64) + sizeof(bool));

    printk(KERN_INFO
            "%s cgm_memory_init(): ssid[ %2d : %s ] : "
                  "%u bytes / cgroup:subsystem\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ),
            nBytes_ss);
  }
  // /* Memory metrics }

#endif  // CONFIG_CGROUPS  }
}

/**
 *  Gather subsystem metrics identifying any differences from any prior data.
 *  @method gather
 *  @param  entry   The cgroup diff entry for the target cgroup/subsystem
 *                  {cgroup_diff_t*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_memory_gather( cgroup_diff_t*  entry ) {
  cgroup_store_t*   store   = entry->store;
  bool              doDiff  = (store != NULL);
  u32               hdex    = 0;
  u32               sdex    = 0;

  if (_cgm_memory_prepEntry( entry )) {
    // Preparation failed
    return;
  }

  // In case `entry->store` was initially created or moved
  store = entry->store;

  /* Normalize the subsystem data, writing to `entry->data` and, if there was
   * pre-existing `entry->data`, identifying differences in both
   * `entry->map[].changed` and `entry->changed`;
   */
  for (hdex = 0, sdex = 0;
        hdex < entry->nHandlers && sdex < store->nEntries;
          hdex++) {
    cftype_handler_t* handler = &entry->handlers[ hdex ];

    if (! handler->cft) { continue; }

    // assert( mapEntry->extract != NULL );
    store->nChanges += handler->extract( handler, entry, sdex, doDiff );

    sdex += handler->nEntries;
  }
}
/* cgroup module methods }
 *****************************************************************************/

/**
 *  Expose this as a cgroup module.
 */
cgroup_module_t cgroup_module_memory = {
  .name     = "memory",
  .init     = cgm_memory_init,
  .gather   = cgm_memory_gather,
};
