/**
 *  cgroup v1 collector:  cpuacct
 *
 *  This collector provides access to the `cpuacct` (aka 'cpu,cpuacct') cgroup
 *  subsystem metrics available via `/sys/fs/cgroup/cpuacct/cpuacct.*`:
 *    - usage             read_u64()
 *    - usage_user        read_u64()
 *    - usage_sys         read_u64()
 *    - usage_percpu      via sequence-file;
 *    - usage_percpu_user via sequence-file;
 *    - usage_percpu_sys  via sequence-file;
 *
 *  :NOTE:  The following cgroup sequence-files are not currently collected
 *          as (I believe) they are represented in the above collected metrics:
 *            - cpuacct.usage_all
 *                user   %time%
 *                system %time%
 *            - cpuacct.stat
 *                cpu user system
 *                %id% %time% %time%
 *                ...
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 */
#ifndef _CGROUP_CPUACCT_H_
#define _CGROUP_CPUACCT_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_cpuacct;

#endif  // _CGROUP_CPUACCT_H_
