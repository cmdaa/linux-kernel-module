/**
 *  cgroup v1 collector:  net_prio
 *
 *  This collector provides access to the `net_prio` (aka 'net_cls,net_prio')
 *  cgroup subsystem metrics available via `/sys/fs/cgroup/net_prio/net_prio.*`:
 *    - prioidx     read_u64();
 *    - ifpriomap   via sequence-file indexed by interface name
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 */
#ifndef _CGROUP_NETPRIO_H_
#define _CGROUP_NETPRIO_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_netprio;

#endif  // _CGROUP_NETPRIO_H_
