/**
 *  cgroup v1 collector:  net_cls
 *
 *  This collector provides access to the `net_cls` (aka 'net_cls,net_prio')
 *  cgroup subsystem metrics available via `/sys/fs/cgroup/net_cls/net_cls.*`:
 *    - classid     read_u64();
 */
#ifndef _CGROUP_NETCLS_H_
#define _CGROUP_NETCLS_H_

/**
 *  A module definition.
 */
extern cgroup_module_t  cgroup_module_netcls;

#endif  // _CGROUP_NETCLS_H_
