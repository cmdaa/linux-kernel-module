/**
 *  cgroup metric interface for the `devices` cgroup subsystem.
 *
 *  :TODO:  Implement. This will be fairly complex since all data is in
 *          a sequence-file that will need to be parsed. This will likely
 *          be handled similar to `blkio` consolidating by major/minor number.
 *
 *          Records have the form:
 *            %type% %dev-major%:%dev-minor% %access%
 *
 *            %type%  : 'a' = DEV_ALL
 *                      'c' = DEV_CHAR
 *                      'b' = DEV_BLOCK
 *                      'X' = UNKNOWN
 *
 *            %access%: 'r' = ACC_READ
 *                      'w' = ACC_WRITE
 *                      'm' = ACC_MKNOD
 *
 *=============================================================================
 *    security/device_cgroup.c
 *      v4.9
 *        "list"    : DEVCG_LIST
 *          .seq_show = devcgroup_seq_show(struct seq_file *m, void *v)
 *
 *=============================================================================
 *    security/device_cgroup.c
 *      v3.10
 *        "list"    : DEVCG_LIST
 *          .read_seq_string  = devcgroup_seq_read( struct cgroup *cgroup,
 *                                                  struct cftype *cft,
 *                                                  struct seq_file *m)
 */
