/**
 *  cgroup v1 collector:  cpuacct
 *
 *  This collector provides access to the `cpuacct` (aka 'cpu,cpuacct') cgroup
 *  subsystem metrics available via `/sys/fs/cgroup/cpuacct/cpuacct.*`:
 *    - usage             read_u64()
 *    - usage_user        read_u64()
 *    - usage_sys         read_u64()
 *    - usage_percpu      via sequence-file;
 *    - usage_percpu_user via sequence-file;
 *    - usage_percpu_sys  via sequence-file;
 *
 *  :NOTE:  The following cgroup sequence-files are not currently collected
 *          as (I believe) they are represented in the above collected metrics:
 *            - cpuacct.usage_all
 *                user   %time%
 *                system %time%
 *            - cpuacct.stat
 *                cpu user system
 *                %id% %time% %time%
 *                ...
 *
 *  :NOTE:  Access to sequence-file-based metrics may be restricted by
 *          excluding the USE_CGROUP_FSREAD #define.
 *
 *=============================================================================
 *    kernel/sched/cpuacct.c
 *      v4.9
 *        "usage"
 *          .read_u64 = cpuusage_read(        struct cgroup_subsys_state *css,
 *                                            struct cftype *cft)
 *
 *        "usage_user"
 *          .read_u64  = cpuusage_user_read(  struct cgroup_subsys_state *css,
 *                                            struct cftype *cft)
 *
 *        "usage_sys"
 *          .read_u64  = cpuusage_sys_read(   struct cgroup_subsys_state *css,
 *                                            struct cftype *cft)
 *
 *        "usage_percpu"
 *          .seq_show  = cpuacct_percpu_seq_show(       struct seq_file *m,
 *                                                      void *V)
 *
 *        "usage_percpu_user"
 *          .seq_show  = cpuacct_percpu_user_seq_show(  struct seq_file *m,
 *                                                      void *V)
 *
 *        "usage_percpu_sys"
 *          .seq_show  = cpuacct_percpu_sys_seq_show(   struct seq_file *m,
 *                                                      void *V)
 *
 *        "usage_all"
 *          .seq_show  = cpuacct_all_seq_show(          struct seq_file *m,
 *                                                      void *V)
 *
 *        "stat"
 *          .seq_show  = cpuacct_stats_show(            struct seq_file *sf,
 *                                                      void *V)
 *
 *=============================================================================
 *    kernel/sched/cpuacct.c
 *      v3.10
 *        "usage"
 *          .read_u64        = cpuusage_read(          struct cgroup *cgrp,
 *                                                     struct cftype *cft)
 *        "usage_percpu"
 *          .read_seq_string = cpuacct_percpu_seq_read(struct cgroup *cgroup,
 *                                                     struct cftype *cft,
 *                                                     struct seq_file *m)
 *        "stat"
 *          .read_map        = cpuacct_stats_show(     struct cgroup *cgrp,
 *                                                     struct cftype *cft,
 *                                                     struct cgroup_map_cb *cb)
 */
#include "../../json.h"

#include "../cgroup_v1.h"

#include <linux/slab.h>         // kzalloc() / krealloc() / kfree()

// Track memory allocation
static u64  s_bytesAlloc = 0;
static u64  s_bytesFreed = 0;

/* A shared buffer for reading and parsing the cftype sequence data.
 *
 *  :NOTE:  Since a u64 could be a string as large as 20-digits
 *          ['18446744073709552000'] with a space between each entry,
 *          the buffer we're about to read could be as large as:
 *            s_nCpu * 21 bytes long
 *
 *          For a small system, this isn't a big deal, but if the number of
 *          CPUs is large, it becomes a larger issue.
 */
static u32      s_nCpu        = 0;
static u32      s_readBufSize = 0;
static char*    s_readBuf     = NULL;

/*****************************************************************************/

/**
 *  The extractor callback for per-cpu sequences.
 *  @method _perCpu_extract
 *  @param  self      The cftype handler entry containing this method
 *                    {cftype_handler_t*};
 *  @param  cgd       The cgroup data {cgroup_diff_t*};
 *  @param  start     The starting index in the store state {u32};
 *  @param  doDiff    If true, prior data is available so perform a diff
 *                    before writing {bool};
 *
 *  @return The number of detected changes {u32};
 */
static u32
_perCpu_extract(  cftype_handler_t* self,
                  cgroup_diff_t*    cgd,
                  u32               start,
                  bool              doDiff ) {

  u32             nChanges  = 0;

#if defined(CONFIG_CGROUPS) && defined(USE_CGROUP_FSREAD)
  cgroup_store_t* store     = cgd->store;
  struct cftype*  cft       = self->cft;
  ssize_t         read      = 0;

  if (cft == NULL || store == NULL)  { return nChanges; }

  /*
  printk(KERN_WARNING "%s _perCpu_extract(): name[ %s : %s ], start[ %u ]...\n",
                   DEVICE_NAME, cft->name, self->name, start);
  // */

  if (s_readBuf != NULL) {
    // Read the cgroup data
    read = cgroup_v1_fsRead( cft, cgd, s_readBuf, s_readBufSize );
  }

  /*
  printk(KERN_INFO "%s _perCpu_extract(): "
                                "name[ %s : %s ]: read %ld bytes[ %s ]\n",
                   DEVICE_NAME, cft->name, self->name, read, s_readBuf);
  // */

  if (read > 0) {
    u64*  values  = store->values  + start;
    bool* changes = store->changes + start;
    char* cur     = s_readBuf;
    char* end     = &s_readBuf[read];
    u32   idex    = 0;

    /* Parse the generated string, 1/cpu:
     *   u64 u64 ...
     */
    for (idex = 0; idex < s_nCpu && cur < end; idex++) {
      char* ws      = lkb_strnws( cur, end - cur );
      u64   oldVal  = values[ idex ];
      u64   newVal  = 0;

      if (ws) { *ws = '\0'; }
      if (kstrtou64( cur, 10, &newVal )) {
        printk(KERN_WARNING "%s _perCpu_extract():%u.%u: %s%s "
                                    "str[ %s ]: cannot parse number\n",
                             DEVICE_NAME, start, idex,
                                          cgd->path, self->name,
                                          cur);
      } else {
        if ( oldVal != newVal ) {
          // Difference
          changes[ idex ] = TRUE;
          values[ idex ]  = newVal;
          nChanges++;

        } else {
          changes[ idex ] = FALSE;
        }

        /*
        printk(KERN_INFO "%s _perCpu_extract():%u.%u: %s%s "
                                    "[ %llu => %llu ]: %s [ %u total ]\n",
                            DEVICE_NAME, start, idex,
                                         cgd->path, self->name,
                                         oldVal, newVal,
                                         (changes[ idex ] ? "DIFF" : "no-diff"),
                                         nChanges);
        // */
      }

      cur = (ws ? ws + 1 : end);
    }
  }
#endif

  return nChanges;
}

/*****************************************************************************/

/**
 *  The mapping of cftype to handling information.
 *
 *  This array and associated data will be updated during module initalization
 *  to:
 *    - initialize the cftype pointer for each entry that can be supported;
 *    - unsupported entries will have a NULL `cft` property;
 *    - `s_entries` will be initialized to the total number of entries that may
 *      be generated by the active handlers;
 *    - per-cpu handlers will have `nEntries` properly initialized to the
 *      number of cpus;
 */
static cftype_handler_t   s_handler[] = {
  { .name= "usage",               .nEntries= 1 },
  { .name= "usage_user",          .nEntries= 1 },
  { .name= "usage_sys",           .nEntries= 1 },

  /* _perCpu_extract()
   *    :NOTE: `nEntries` will be properly set in `cgm_cpuacct_init()`;
   */
  { .name = "usage_percpu",       .nEntries= 1, .extract= _perCpu_extract },
  { .name = "usage_percpu_user",  .nEntries= 1, .extract= _perCpu_extract },
  { .name = "usage_percpu_sys",   .nEntries= 1, .extract= _perCpu_extract },

  // Empty entry terminator : 'name == NULL'
  { 0 },
};
static int                s_handlerCnt    = COUNT(s_handler) - 1;
static int                s_entries       = 0;

/**
 *  Associate the provided entry with our mapping and extractor information and
 *  ensure space has been allocated for subsystem-specific data and diff
 *  indicators.
 *  @method _cgm_cpuacct_prepEntry
 *  @param  entry   The target entry {cgroup_diff_t*};
 *
 *  @return A success/failure indicator (0 == success) {int};
 *  @private
 */
static int
_cgm_cpuacct_prepEntry( cgroup_diff_t*  entry ) {

  if (entry->store == NULL) {
    // Create a new subsystem-specific store
    entry->store = cgroup_v1_makeStore( s_entries );
    if (entry->store == NULL) {
      printk(KERN_ALERT "%s _cgm_cpu_prepEntry(): Cannot create 'store'\n",
                        DEVICE_NAME);
      return -1;
    }

    entry->handlers  = s_handler;
    entry->nHandlers = s_handlerCnt;

  } else {
    // Reset the change counter
    entry->store->nChanges = 0;

  }

  return 0;
}

/*****************************************************************************
 * cgroup module methods {
 *
 */

/**
 *  Initialize this collection module.
 *  @method cgm_cpuacct_init
 *  @param  subsys  A pointer to the cgroup subsystem {struct cgroup_subsys*};
 *
 *  @return void
 *  @protected
 */
static void
cgm_cpuacct_init( struct cgroup_subsys* subsys ) {
#ifdef  CONFIG_CGROUPS  // {
  u32             nActive = 0;
  CGROUP_CFT_SET* cfts;

  // Cache the number of cpus
  s_nCpu = num_present_cpus(); // nr_cpu_ids

  /*
  printk(KERN_INFO "%s cgm_cpuacct_init(): ssid[ %2d : %s ]\n",
                   DEVICE_NAME,
                   CGROUP_SUBSYS_ID(subsys),
                   CGROUP_SUBSYS_NAME( subsys ));
  // */

# ifdef USE_CGROUP_FSREAD // {
  /* Allocate a shared buffer for reading and parsing the cftype generated
   * string/data.
   *
   * The generated string/data will be a single u64 per cpu.
   *
   *  :NOTE:  Since a u64 could be a string as large as 20-digits
   *          ['18446744073709552000'] with a space between each entry,
   *          the buffer we're about to read could be as large as:
   *            s_nCpu * 21 bytes long
   *
   *          For a small system, this isn't a big deal, but if the number of
   *          CPUs is large, it becomes a larger issue.
   */
  s_readBufSize = s_nCpu * 21;
  s_readBuf     = kzalloc( s_readBufSize, GFP_KERNEL );
  if (s_readBuf == NULL) {
    printk(KERN_ALERT "%s cgm_cpuacct_init(): "
                            "Cannot allocate %u bytes for 's_readBuf'\n",
                      DEVICE_NAME, s_readBufSize);
    s_readBufSize = 0;

  } else {
    s_bytesAlloc += s_readBufSize;
  }
# endif // USE_CGROUP_FSREAD }

  /* Fill in the cftype `cft` entries identifying (and counting) the final set
   * of active entries.
   *
   *  :NOTE: Upon completion, any inactive entry will have a NULL `cft`
   *         property.
   *
   *         By "inactive" we mean an entry for which no match exists in the
   *         cftype set of the provided subsystem.
   */
  s_entries = 0;
  list_for_each_entry( cfts, &CGROUP_SUBSYS_CFTSET(subsys), node ) {
    struct cftype* cft = CGROUP_SUBSYS_CFTSET_ITEM(cfts);

    while (cft && cft->name[0] != '\0') {

      cftype_handler_t* handler = cgroup_v1_findHandler( s_handler,
                                                         s_handlerCnt,
                                                         cft->name );
      if (handler != NULL) {
        handler->cft = cft;

        nActive++;

        if (! handler->extract) {
          // Use the default extract helper
          handler->extract = cgroup_v1_cftype_extract;

        } else if (handler->extract == _perCpu_extract) {
          // Set `nEntries` to the proper value
          handler->nEntries = s_nCpu;
        }

        if (! handler->json) {
          // Use the default json output helper
          handler->json = cgroup_v1_cftype_json;
        }

        s_entries += handler->nEntries;

        /*
        printk(KERN_INFO "%s cgm_cpuacct_init(): "
                              "Activate handler '%s', %u entries\n",
                           DEVICE_NAME, handler->name, handler->nEntries);
        // */


      } else {
        // else, skip this cftype : generates no map entries
        printk(KERN_WARNING "%s cgm_cpuacct_init(): SKIP cftype[ %s ]\n",
                             DEVICE_NAME, cft->name);
      }

      cft++;
    }
  }

  // /* Memory metrics {
  {
                      // usage, usage_user, usage_sys
    u32 nBytes_cpu  = 3 * (sizeof(u64) + sizeof(bool));
    u32 nBytes_ss   = sizeof( cgroup_store_t ) +
                        ( s_entries * (sizeof(u64) + sizeof(bool)) ) +
                        ( s_nCpu    * nBytes_cpu );

    printk(KERN_INFO
            "%s cgm_cpuacct_init(): ssid[ %2d : %s ] : "
                  "%u bytes / cgroup:subsystem (%u bytes / cpu with %u cpus)\n",
            DEVICE_NAME,
            CGROUP_SUBSYS_ID(subsys),
            CGROUP_SUBSYS_NAME( subsys ),
            nBytes_ss, nBytes_cpu, s_nCpu);
  }
  // /* Memory metrics }

#endif  // CONFIG_CGROUPS  }
}

/**
 *  Gather subsystem metrics identifying any differences from any prior data.
 *  @method gather
 *  @param  entry   The cgroup data entry for the target cgroup/subsystem
 *                  {cgroup_diff_t*};
 *
 *  @note   This method will update:
 *            entry->changed
 *            entry->diff[]
 *            entry->data[]
 *
 *  @return void
 *  @protected
 */
static void
cgm_cpuacct_gather( cgroup_diff_t*  entry ) {
  cgroup_store_t* store   = entry->store;
  bool            doDiff  = (store != NULL);
  u32             hdex    = 0;
  u32             sdex    = 0;

  if (_cgm_cpuacct_prepEntry( entry )) {
    // Preparation failed
    return;
  }

  // In case `entry->store` was initially created or moved
  store = entry->store;

  /*
  printk(KERN_INFO "%s cgm_cpuacct_gather(): path[ %s ]: "
                              "%u handlers, %u entries ...\n",
                     DEVICE_NAME, entry->path,
                                  entry->nHandlers, store->nEntries);
  // */

  /* Normalize the subsystem data, writing to `entry->data` and, if there was
   * pre-existing `entry->data`, identifying differences in both
   * `entry->map[].changed` and `entry->changed`;
   */
  for (hdex = 0, sdex = 0;
        hdex < entry->nHandlers && sdex < store->nEntries;
          hdex++) {
    cftype_handler_t* handler = &entry->handlers[ hdex ];

    if (! handler->cft) { continue; }

    /*
    printk(KERN_WARNING "%s cgm_cpu_gather(): path[ %s ]: invoke '%s', "
                                "%u entries starting at %u ...\n",
                       DEVICE_NAME, entry->path,
                                    handler->name, handler->nEntries, sdex);
    // */

    // assert( mapEntry->extract != NULL );
    store->nChanges += handler->extract( handler, entry, sdex, doDiff );

    sdex += handler->nEntries;
  }
}

/**
 *  Finalize, cleaning up any allocated resources.
 *  @method fini
 *
 *  @return void
 *  @protected
 */
static void
cgm_cpuacct_fini( void ) {
  if (s_readBuf != NULL) {
    kfree( s_readBuf );
    s_bytesFreed += s_readBufSize;

    s_readBuf     = NULL;
    s_readBufSize = 0;
  }

  if (s_bytesAlloc - s_bytesFreed != 0) {
    printk(KERN_WARNING "%s cgm_cpuacct_fini(): "
                        "%7llu bytes allocated, %7llu freed : %llu LEAKED\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed,
                                     s_bytesAlloc - s_bytesFreed);
  } else {
    printk(KERN_INFO    "%s cgm_cpuacct_fini(): "
                        "%7llu bytes allocated, %7llu freed\n",
                        DEVICE_NAME, s_bytesAlloc, s_bytesFreed);
  }

}

/* cgroup module methods }
 *****************************************************************************/

/**
 *  Expose this as a cgroup module.
 */
cgroup_module_t cgroup_module_cpuacct = {
  .name     = "cpuacct",
  .init     = cgm_cpuacct_init,
  .gather   = cgm_cpuacct_gather,
  //.release  = cgm_cpuacct_release,
  .fini     = cgm_cpuacct_fini,
};
