/**
 *  Module: uptime
 *
 *  Beat-driven module that collects system uptime metrics.
 *
 *  This module presents metrics similar to those available via `/proc/uptime`;
 */
#include "../lkb.h"

#include <linux/sched.h>        // get_avenrun(), avenrun, FSHIFT, nr_threads
#include <uapi/linux/sysinfo.h> // SI_LOAD_SHIFT

#include "../map.h"
#include "../json.h"

#include "uptime.h"

// :NOTE: If additional fields are added, `s_map` MUST also be changed.
typedef struct {
  u32 seconds;  // in seconds
} uptime_t;

/**
 *  The mapping from `uptime_t` to member name, type, offset, and size.
 *
 *  @note For proper reporting, all fields from `uptime_t` MUST be identified.
 */
static map_t  s_map[] = {
  {"seconds",  MAP_TYPE_U32, OFFSETOF(uptime_t, seconds)},
};
static int      s_mapCnt    = COUNT(s_map);

#ifdef  USE_LKB_DIFF
static bool     s_haveLast  = FALSE;
static uptime_t s_uptimeLast;
#endif // USE_LKB_DIFF

/*****************************************************************************
 * Private methods {
 *
 */

/**
 *  Gather system uptime metrics.
 *  @method _uptime_gather
 *  @param  data    A pointer to the uptime data to fill {uptime_t*};
 *
 *  @return void
 *  @private
 */
static void
_uptime_gather( uptime_t* data ) {
  /* From kernel/sys.c : do_sysinfo()
   *  get_monotonic_boottime( &tp );
   *
   *  uptime = tp.tv_sec + (tp.tv_nsec ? 1 : 0);
   *
   * From fs/proc/uptime.c  : uptime_proc_show()
   *  get_monotonic_boottime( &uptime );
   *
   *  "%lu.%02lu", uptime.tv_sec, uptime.tv_nsec / (NSEC_PER_SEC / 100)
   */
  struct timespec tp;

  get_monotonic_boottime( &tp );

  data->seconds = (u32)( tp.tv_sec + (tp.tv_nsec ? 1 : 0) );
}

/* Private methods }
 *****************************************************************************
 * Module methods {
 *
 */

/**
 *  Initialize access to any non-exported kernel functions.
 *  @method lkb_uptime_init
 *
 *  @return void
 *  @protected
 */
static void
lkb_uptime_init( void ) {

  printk(KERN_INFO "%s uptime_init(): %lu bytes / beat\n",
                   DEVICE_NAME, sizeof(uptime_t));
}

/**
 *  Gather and write system uptime metrics as JSON to the circular buffer.
 *  @method lkb_uptime_json
 *
 *  @return The number of bytes written to the circular buffer {ssize_t};
 *  @protected
 */
static ssize_t
lkb_uptime_json( void ) {
  ssize_t   written   = 0;
  bool      needComma = 0;
  int       idex;
  uptime_t  uptime;
#ifdef  MEASURE_PERFORMANCE
  u64     start     = lkb_timestamp();
  u64     end;
#endif  // MEASURE_PERFORMANCE

  memset( &uptime, 0, sizeof(uptime) );

  _uptime_gather( &uptime );

  /* Write as JSON the uptime measurements which have changed since the last
   * beat.
   */
  written += lkb_write( "{", 1 );

  for (idex = 0; idex < s_mapCnt; idex++) {
    map_t*  entry     = &s_map[idex];
    void*   cur       = (void*)&uptime;
    bool    doOutput  = (
#ifdef  USE_LKB_DIFF
        !s_haveLast || mapDiff( entry, cur, (void*)&s_uptimeLast )
#else  // USE_LKB_DIFF
        TRUE
#endif // USE_LKB_DIFF
    );

    if ( doOutput ) {
      if (needComma)  { written += lkb_write( ",", 1 ); }

      written  += json_write_map( entry, cur );
      needComma = 1;
    }
  }

  written += lkb_write( "}", 1 );

#ifdef  USE_LKB_DIFF
  memcpy( &s_uptimeLast, &uptime, sizeof(s_uptimeLast) );
  s_haveLast = TRUE;
#endif // USE_LKB_DIFF

#ifdef  MEASURE_PERFORMANCE
  end = lkb_timestamp();
  printk(KERN_INFO "%s uptime_json()   : %6ld bytes in %7llu ns, beat %u\n",
                   DEVICE_NAME, written, end - start, lkb.beat);
#endif  // MEASURE_PERFORMANCE

  return written;
}

/**
 *  Reset any state for the (re)start of collections.
 *  @method lkb_uptime_reset
 *
 *  @return void
 *  @protected
 */
static void
lkb_uptime_reset( void ) {
#ifdef  USE_LKB_DIFF
  s_haveLast = FALSE;
#endif  // USE_LKB_DIFF
}
/* Module methods }
 *****************************************************************************/

/**
 *  Expose this as an lkb module.
 */
Module_t module_uptime = {
  .name   = "uptime",
  .init   = lkb_uptime_init,
  .json   = lkb_uptime_json,
  .reset  = lkb_uptime_reset,
};
