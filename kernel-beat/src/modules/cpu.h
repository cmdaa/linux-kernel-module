/**
 *  Module: cpu
 *
 *  Beat-driven module that collects CPU metrics.
 */
#ifndef _CPU_H_
#define _CPU_H_

/**
 *  A module definition.
 */
extern Module_t module_cpu;

#endif  // _CPU_H_
