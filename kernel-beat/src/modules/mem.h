/**
 *  Module: mem
 *
 *  Beat-driven module that collects memory usage metrics.
 *
 *  This module:
 *    - presents metrics similar to those available via `/proc/meminfo`;
 *
 *    - makes use of non-public, though exported, kernel symbols:
 *      - si_swapinfo()
 *      - total_swapcache_pages()
 *      - vm_commit_limit()
 *
 *      - vm_committed_as
 *      - hstates
 *      - default_hstate_idx
 */
#ifndef _MEM_H_
#define _MEM_H_

/**
 *  A module definition.
 */
extern Module_t module_mem;

#endif  // _MEM_H_
