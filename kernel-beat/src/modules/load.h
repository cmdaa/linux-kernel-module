/**
 *  Module: load
 *
 *  Beat-driven module that collects system load metrics.
 *
 *  This module:
 *    - presents metrics similar to those available via `/proc/loadavg`;
 *
 *    - makes use of non-public, though exported, kernel symbols:
 *      - get_avenrun()
 */
#ifndef _LOAD_H_
#define _LOAD_H_

/**
 *  A module definition.
 */
extern Module_t module_load;

#endif  // _LOAD_H_
