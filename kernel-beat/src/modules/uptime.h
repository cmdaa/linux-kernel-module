/**
 *  Module: uptime
 *
 *  Beat-driven module that collects system uptime metrics.
 *
 *  This module presents metrics similar to those available via `/proc/uptime`;
 */
#ifndef _UPTIME_H_
#define _UPTIME_H_

/**
 *  A module definition.
 */
extern Module_t module_uptime;

#endif  // _UPTIME_H_
