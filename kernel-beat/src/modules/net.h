/**
 *  Module: networks
 *
 *  Beat-driven module that collects metrics for each available network
 *  interface.
 *
 *  This module:
 *    - presents metrics similar to those available via `/proc/net/dev`;
 *
 *    - makes use of a hash table to monitor the set of active network
 *      interfaces;
 */
#ifndef _NET_H_
#define _NET_H_

/**
 *  A definition for the per-network module.
 */
extern Module_t module_net;

#endif  // _NET_H_
