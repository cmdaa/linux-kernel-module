/**
 *  Kernel-level tcp sender.
 *
 */
#ifndef _TCP_H_
#define _TCP_H_

#include <linux/uio.h>  // struct kvec

#include "lkb.h"

/**
 *  The maximum number of retries if the client dis-connects.
 *  @const  MAX_TCP_RETRIES
 */
#define MAX_TCP_RETRIES 10

/**
 *  Create/open a new kernel-level TCP socket.
 *  @method tcp_open_socket
 *  @param  dst       The destination information {net_failover_t*};
 *
 *  @return The new socket {struct socket*};
 */
extern struct socket* tcp_open_socket( net_failover_t* dst );

/**
 *  Close and release an existing kernel-level TCP socket.
 *  @method tcp_close_socket
 *  @param  dst       The destination information {net_failover_t*};
 *
 *  @return void
 */
extern void tcp_close_socket( net_failover_t* dst );

/**
 *  Send the given set of data, described via io vectors, over the provided
 *  socket to the specified address.
 *  @method tcp_send_iov
 *  @param  dst       The destination information {net_failover_t*};
 *  @param  iov       The io vectors {struct iov*};
 *  @param  iovCount  The number of io vectors {size_t};
 *
 *  @return The number of bytes sent (< 0 on error) {ssize_t};
 */
extern ssize_t tcp_send_iov(  net_failover_t* dst,
                              struct kvec*    iov,
                              size_t          iovCount);

/**
 *  Send the given data over the provided socket to the specified address.
 *  @method tcp_send
 *  @param  dst       The destination information {net_failover_t*};
 *  @param  buf       The data to send {void*};
 *  @param  bufLen    The length (in bytes) of `buf` {size_t};
 *
 *  @return The number of bytes sent (< 0 on error) {ssize_t};
 */
extern ssize_t tcp_send(  net_failover_t* dst,
                          void*           buf,
                          size_t          bufLen);

#endif  // _TCP_H_
