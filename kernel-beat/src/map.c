/**
 *  Provides a mechanism for naming and accessing fields within a structure.
 *  This is used for identifying and reporting differences in metrics between
 *  beats.
 *
 */
#include "map.h"
#include <linux/string.h>   // strnlen()

/**
 *  Given a map entry and two pointers, determine if the value of the
 *  referenced structure member is different between the two structures.
 *  @method mapDiff
 *  @param  entry     The map entry {map_t*};
 *  @param  s1        A pointer to the first  structure {void*};
 *  @param  s2        A pointer to the second structure {void*};
 *
 *  @return TRUE | FALSE {bool};
 */
bool
mapDiff( map_t* entry, void* s1, void* s2 ) {
  bool  res = FALSE;
  void* p1  = s1 + entry->offset;
  void* p2  = s2 + entry->offset;

  switch( entry->type ) {
    case MAP_TYPE_S8:  res = (*((s8*)p1)  != *((s8*)p2));                break;
    case MAP_TYPE_U8:  res = (*((u8*)p1)  != *((u8*)p2));                break;
    case MAP_TYPE_S16: res = (*((s16*)p1) != *((s16*)p2));               break;
    case MAP_TYPE_U16: res = (*((u16*)p1) != *((u16*)p2));               break;
    case MAP_TYPE_S32: res = (*((s32*)p1) != *((s32*)p2));               break;
    case MAP_TYPE_U32: res = (*((u32*)p1) != *((u32*)p2));               break;
    case MAP_TYPE_S64: res = (*((s64*)p1) != *((s64*)p2));               break;
    case MAP_TYPE_U64: res = (*((u64*)p1) != *((u64*)p2));               break;

    case MAP_TYPE_STR:
      // res = strncmp((char*)p1, (char*)p2, entry->size); break;
      // Fall-through to use memcmp()

    case MAP_TYPE_SPC:
      // IFF there is a size specified, perform a diff using memcmp()
      if (entry->size > 0) {
        res = (memcmp( p1, p2, entry->size ) ? TRUE : FALSE);
        break;
      }
      // If there is no size, fall-through to the default

    default:           res = (bool)-1;                                   break;
  }

  return res;
}

/**
 *  Given a map entry and pointer to mapped data, write the incoming value
 *  to the mapped data.
 *  @method mapWrite
 *  @param  entry     The map entry {map_t*};
 *  @param  dst       A pointer to the destination {void*};
 *  @param  src       A pointer to the soure {void*};
 *
 *  @return TRUE | FALSE {bool};
 */
bool
mapWrite( map_t* entry, void* dst, void* src ) {
  bool  res = TRUE;
  void* ptr = dst + entry->offset;

  switch( entry->type ) {
    case MAP_TYPE_STR: strncpy((char*)dst, (char*)src, entry->size);  break;
    case MAP_TYPE_S8:  (*((s8*)ptr)  = *((s8*)src));                  break;
    case MAP_TYPE_U8:  (*((u8*)ptr)  = *((u8*)src));                  break;
    case MAP_TYPE_S16: (*((s16*)ptr) = *((s16*)src));                 break;
    case MAP_TYPE_U16: (*((u16*)ptr) = *((u16*)src));                 break;
    case MAP_TYPE_S32: (*((s32*)ptr) = *((s32*)src));                 break;
    case MAP_TYPE_U32: (*((u32*)ptr) = *((u32*)src));                 break;
    case MAP_TYPE_S64: (*((s64*)ptr) = *((s64*)src));                 break;
    case MAP_TYPE_U64: (*((u64*)ptr) = *((u64*)src));                 break;

    default:           res = FALSE;                                   break;
  }

  return res;
}

/**
 *  Given a map entry and pointer to mapped data, read the target value.
 *  @method mapRead
 *  @param  entry     The map entry {map_t*};
 *  @param  dst       A pointer to the destination {void*};
 *  @param  src       A pointer to the soure {void*};
 *
 *  @return TRUE | FALSE {bool};
 */
bool
mapRead( map_t* entry, void* dst, void* src ) {
  bool  res = TRUE;

  switch( entry->type ) {
    case MAP_TYPE_STR: strncpy((char*)dst, (char*)src, entry->size);  break;
    case MAP_TYPE_S8:  (*((s8*)dst)  = *((s8*)src));                  break;
    case MAP_TYPE_U8:  (*((u8*)dst)  = *((u8*)src));                  break;
    case MAP_TYPE_S16: (*((s16*)dst) = *((s16*)src));                 break;
    case MAP_TYPE_U16: (*((u16*)dst) = *((u16*)src));                 break;
    case MAP_TYPE_S32: (*((s32*)dst) = *((s32*)src));                 break;
    case MAP_TYPE_U32: (*((u32*)dst) = *((u32*)src));                 break;
    case MAP_TYPE_S64: (*((s64*)dst) = *((s64*)src));                 break;
    case MAP_TYPE_U64: (*((u64*)dst) = *((u64*)src));                 break;

    default:           res = FALSE;                                   break;
  }

  return res;
}

/**
 *  Given a map entry, pointer to mapped data and a pointer to a new value,
 *  determine if the new value differs, write the new value and return the
 *  determination.
 *  @method mapDiffAndWrite
 *  @param  entry     The map entry {map_t*};
 *  @param  dst       A pointer to the destination {void*};
 *  @param  neVal     A pointer to the new value {void*};
 *
 *  @return TRUE | FALSE {bool};
 */
bool
mapDiffAndWrite( map_t* entry, void* dst, void* newVal ) {
  bool  isDiff  = FALSE;
  void* ptr     = dst + entry->offset;

  switch( entry->type ) {
    case MAP_TYPE_STR:
      isDiff = strncmp((char*)ptr, (char*)newVal, entry->size);
      //strncpy((char*)dst, (char*)newVal, entry->size);
      break;

    case MAP_TYPE_S8:
      isDiff = (*((s8*)ptr)  != *((s8*)newVal));
      //(*((s8*)ptr)  = *((s8*)src));
      break;

    case MAP_TYPE_U8:
      isDiff = (*((u8*)ptr)  != *((u8*)newVal));
      //(*((u8*)ptr)  = *((u8*)src));
      break;

    case MAP_TYPE_S16:
      isDiff = (*((s16*)ptr) != *((s16*)newVal));
      //(*((s16*)ptr) = *((s16*)src));
      break;

    case MAP_TYPE_U16:
      isDiff = (*((u16*)ptr) != *((u16*)newVal));
      //(*((u16*)ptr) = *((u16*)src));
      break;

    case MAP_TYPE_S32:
      isDiff = (*((s32*)ptr) != *((s32*)newVal));
      //(*((s32*)ptr) = *((s32*)src));
      break;

    case MAP_TYPE_U32:
      isDiff = (*((u32*)ptr) != *((u32*)newVal));
      //(*((u32*)ptr) = *((u32*)src));
      break;

    case MAP_TYPE_S64:
      isDiff = (*((s64*)ptr) != *((s64*)newVal));
      //(*((s64*)ptr) = *((s64*)src));
      break;

    case MAP_TYPE_U64:
      isDiff = (*((u64*)ptr) != *((u64*)newVal));
      //(*((u64*)ptr) = *((u64*)src));
      break;

    default:
      break;
  }

  if (isDiff) {
    mapWrite( entry, dst, newVal );
  }

  return isDiff;
}
