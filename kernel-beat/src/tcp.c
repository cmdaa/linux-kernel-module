/**
 *  Kernel-level tcp sender.
 *
 */
#include "tcp.h"

#include <linux/net.h>
#include <uapi/linux/in.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0)
static struct net* s_init_net = NULL;
#endif  // LINUX_VERSION_CODE

/**
 *  Create/open a new kernel-level TCP socket.
 *  @method tcp_open_socket
 *  @param  dst       The destination information {net_failover_t*};
 *
 *  @return The new socket {struct socket*};
 */
struct socket*
tcp_open_socket( net_failover_t* dst ) {
  struct socket*      sock  = NULL;
  struct sockaddr_in  addr  = {
    .sin_family = AF_INET,
    .sin_port   = dst->port,
    .sin_addr   = { .s_addr= dst->host },
  };
  int                 res;

  if (dst->port < 1 || dst->host < 1) {
    /* Socket not established:
     *  EIO     5   // I/O error
     *  ENXIO   6   // No such device or address
     *  ENODEV  19  // No such device
     *  EINVAL  22  // Invalid argument
     *  EPIPE   32  // Broken pipe
     */
    return NULL; //-EIO;
  }

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,2,0)
  if (s_init_net == NULL) {
    s_init_net = lkb_resolve_one( "init_net" );

    printk(KERN_INFO "%s tcp_open_socket(): init_net[ %p ]\n",
                     DEVICE_NAME, s_init_net);
    if (s_init_net == NULL) {
      return sock;
    }
  }

  res = sock_create_kern( s_init_net, AF_INET, SOCK_STREAM, 0, &sock );

#else   // LINUX_VERSION_CODE < 4.2.0
  res = sock_create_kern( AF_INET, SOCK_STREAM, 0, &sock );

#endif  // LINUX_VERSION_CODE


  if (res != 0) {
    printk(KERN_ALERT "%s tcp_open_socket(): FAILED[ %d ]\n",
                      DEVICE_NAME, res);
  } else {
    // Attempt to connect
    res = sock->ops->connect( sock, (struct sockaddr*)&addr,
                                    sizeof(addr), O_RDWR );
    if (res == 0) {
      printk(KERN_INFO "%s tcp_open_socket(): connected\n", DEVICE_NAME);

    } else if (res == -EINPROGRESS) {
      printk(KERN_INFO "%s tcp_open_socket(): connecting ...\n",
                      DEVICE_NAME);

    } else {
      printk(KERN_ALERT "%s tcp_open_socket(): connect FAILED[ %d ]\n",
                      DEVICE_NAME, res);
    }
  }

  return sock;
}

/**
 *  Close and release an existing kernel-level TCP socket.
 *  @method tcp_close_socket
 *  @param  dst       The destination information {net_failover_t*};
 *
 *  @return void
 */
void
tcp_close_socket( net_failover_t* dst ) {
  int res;

  if (dst->sock == NULL) { return; }

  res = dst->sock->ops->release( dst->sock );
  if (res == 0) {
    printk(KERN_INFO "%s tcp_close_socket(): closed\n", DEVICE_NAME);

    dst->sock = NULL;

  } else {
    printk(KERN_ALERT "%s tcp_close_socket(): FAILED[ %d ]\n",
                      DEVICE_NAME, res);
  }
}

/**
 *  Send the given set of data, described via io vectors, over the provided
 *  socket to the specified address.
 *  @method tcp_send_iov
 *  @param  dst       The destination information {net_failover_t*};
 *  @param  iov       The io vectors {struct iov*};
 *  @param  iovCount  The number of io vectors {size_t};
 *
 *  @return The number of bytes sent (< 0 on error) {ssize_t};
 */
ssize_t
tcp_send_iov( net_failover_t* dst,
              struct kvec*    iov,
              size_t          iovCount)
{
  unsigned int          flags   = MSG_DONTWAIT; //MSG_CONFIRM;
  struct msghdr         msg     = {
    .msg_flags      = flags,
    .msg_name       = NULL, //(struct sockaddr*)&addr,
    .msg_namelen    = 0,    //sizeof( addr ),
  };
  size_t                nBytes  = 0;
  int                   retries = 0;
  ssize_t               res;
  int                   idex;

  if (dst->sock == NULL) {
    /* Socket not established:
     *  EIO     5   // I/O error
     *  ENXIO   6   // No such device or address
     *  ENODEV  19  // No such device
     *  EINVAL  22  // Invalid argument
     *  EPIPE   32  // Broken pipe
     */
    return 0; //-EIO;
  }

  // Count the number of bytes from all io vectors.
  for (idex = 0; idex < iovCount; idex++) {
    nBytes += iov[idex].iov_len;
  }

retry:
  res = kernel_sendmsg( dst->sock, &msg, iov, iovCount, nBytes );

  if (res == ECONNRESET || res == EPIPE || res == ENOTCONN) {
    // Disconnected -- attempt to re-connect and try this send again
    if (++retries > MAX_TCP_RETRIES) {
      printk(KERN_INFO "%s tcp_send_iov(): max retries[ %d ], "
                          "last send error: %ld\n",
                       DEVICE_NAME, retries, res);
      return res;
    }

    tcp_close_socket( dst );

    dst->sock = tcp_open_socket( dst );
    if (dst->sock == NULL) {
      printk(KERN_INFO "%s tcp_send_iov(): (re)open socket failed, "
                            "last send error: %ld\n",
                       DEVICE_NAME, res);
      return res;
    }

    goto retry;
  }

  return res;
}

/**
 *  Send the given data over the provided socket to the specified address.
 *  @method tcp_send
 *  @param  dst       The destination information {net_failover_t*};
 *  @param  buf       The data to send {void*};
 *  @param  bufLen    The length (in bytes) of `buf` {size_t};
 *
 *  @return The number of bytes sent (< 0 on error) {ssize_t};
 */
ssize_t
tcp_send( net_failover_t* dst,
          void*           buf,
          size_t          bufLen)
{
  struct kvec iov = { .iov_base=buf, .iov_len= bufLen };

  return tcp_send_iov( dst, &iov, 1 );
}
