/**
 *  Kernel-level udp sender.
 *
 */
#ifndef _UDP_H_
#define _UDP_H_

#include <linux/uio.h>  // struct kvec

#include "lkb.h"

/**
 *  Create/open a new kerenel-level UDP socket.
 *  @method udp_open_socket
 *  @param  dst       The destination information {net_failover_t*};
 *
 *  @return The new socket {struct socket*};
 */
extern struct socket* udp_open_socket( net_failover_t* dst );

/**
 *  Close and release an existing kernel-level UDP socket.
 *  @method udp_close_socket
 *  @param  dst       The destination information {net_failover_t*};
 *
 *  @return void
 */
extern void udp_close_socket( net_failover_t* dst );

/**
 *  Send the given set of data, described via io vectors, over the provided
 *  socket to the specified address.
 *  @method udp_send_iov
 *  @param  dst       The destination information {udp_failover_t*};
 *  @param  iov       The io vectors {struct iov*};
 *  @param  iovCount  The number of io vectors {size_t};
 *
 *  @return The number of bytes sent (< 0 on error) {ssize_t};
 */
extern ssize_t udp_send_iov(  udp_failover_t* dst,
                              struct kvec*    iov,
                              size_t          iovCount);

/**
 *  Send the given data over the provided socket to the specified address.
 *  @method udp_send
 *  @param  dst       The destination information {udp_failover_t*};
 *  @param  buf       The data to send {void*};
 *  @param  bufLen    The length (in bytes) of `buf` {size_t};
 *
 *  @return The number of bytes sent (< 0 on error) {ssize_t};
 */
extern ssize_t udp_send(  udp_failover_t* dst,
                          void*           buf,
                          size_t          bufLen);

#endif  // _UDP_H_
