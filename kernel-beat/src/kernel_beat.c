/**
 *  This is the top-level loadable module providing:
 *    - the readable `/dev/kernel_beat` character device backed by the
 *      lkb-provided circular buffer;
 *    - `sysctl` support for externally controllable variables:
 *      - kernel.beat.interval      The beat interval (seconds);
 *      - kernel.beat.sync          The beat on which a full sync is performed;
 *      - kernel.beat.net_failover  The `ip:port` to which JSON blocks will be
 *                                  sent if the reader falls behind;
 *
 */
#include "lkb.h"

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/sched.h>    // TASK_INTERRUPTIBLE
#include <linux/sysctl.h>   // ctl_table
#include <linux/device.h>   // struct class, struct device
#include <linux/inet.h>     // in_aton()
#include <linux/poll.h>     // poll_table

#include "tcp.h"

/* :NOTE: In order to use several required kernel symbols, the license MUST be
 *        listed as GPL
 */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("D. Elmo Peele");
MODULE_DESCRIPTION("Gather beat-type metrics.");
MODULE_VERSION("0.16");

/****************************************************************************
 * Custom handler for net_failover {
 */

/**
 *  Custom handler for the 'net_failover' sysctl variable.
 *  @method _proc_net
 *  @param  table     The control table {struct ctl_table*};
 *  @param  write     Is this a write? {int};
 *  @param  buffer    The user-level buffer to read/write {void __user*{;
 *  @param  lenp      The size of `buffer` {size_t*};
 *  @param  ppos      The offset into `buffer` {loff_t};
 *
 *  The expected format of the `net_failover` sysctl variable is:
 *      '%host-ip%:%port%'
 *
 *  @return Results (0 == success) {int};
 *  @private
 */
static int _proc_net(struct ctl_table *table, int write,
                     void __user *buffer, size_t *lenp, loff_t *ppos) {

  char backup[ LKB_NET_STR_LEN ];
  int  ret;

  memcpy( backup, lkb.netFailover.str, LKB_NET_STR_LEN );

  ret = proc_dostring( table, write, buffer, lenp, ppos );

  if (write && ret == 0) {
    // Parse the string into "%host:%port%" and interpret the parts
    size_t  maxlen    = table->maxlen;
    size_t  len       = min( *lenp, maxlen );
    char*   pStart    = (char*)(table->data);
    char*   pCur      = pStart;
    char*   pColon    = NULL;
    u32     hostAddr  = 0;
    u16     port      = 0;

    /*
    printk(KERN_INFO
              "%s _proc_net(): write: %lu byte buffer[ %s ] ...\n",
                DEVICE_NAME, len, pStart);
    // */

    while ((pCur - pStart) < len) {

      if (*pCur == ':' && pColon == NULL) {
        /* %host%:%port%
         *  inet_aton() => in_aton()
         *  inet_pton() => in4_pton()
         */
        *pCur = '\0';

        /*
        printk(KERN_INFO
                "%s _proc_net(): Found colon[ %s ], hostAddr[ %x ] ...\n",
                DEVICE_NAME, pStart, hostAddr);
        // */

        hostAddr = in_aton( pStart );
        pColon   = pCur;
        *pCur    = ':';

        break;
      }

      pCur++;
    }

    // Parse the port
    if (pColon != NULL) {
      if (kstrtou16( pColon+1, 10, &port )) {
        // Failed to parse the port number
        printk(KERN_WARNING
                  "%s _proc_net(): Unable to parse port number [ %s ]\n",
                    DEVICE_NAME, pColon+1);

        // Replace the original
        memcpy( lkb.netFailover.str, backup, LKB_NET_STR_LEN );

        return -EINVAL;
      }

      lkb.netFailover.host = hostAddr;
      lkb.netFailover.port = htons( port );

      printk(KERN_INFO
              "%s _proc_net(): set netFailover: '%s' => "
                                                  "host[ %x ], port[ %u ]\n",
              DEVICE_NAME, pStart, hostAddr, port);

      if (! lkb.netFailover.sock) {
        // Initialize our NET failover socket
        //lkb.netFailover.sock = udp_open_socket( &lkb.netFailover );
        lkb.netFailover.sock = tcp_open_socket( &lkb.netFailover );
      }

      // Attempt to send an initial "ready" message
      if (lkb.netFailover.sock) {
        char*   msg     = "{\"status\":\"ready\"}\n";
        size_t  msgLen  = strnlen( msg, 24 );
        //ssize_t sent    = udp_send( &lkb.netFailover, msg, msgLen );
        ssize_t sent    = tcp_send( &lkb.netFailover, msg, msgLen );

        if (sent > 0) {
          printk(KERN_INFO
                  "%s _proc_net(): Sent %ld / %lu byte 'ready'\n",
                  DEVICE_NAME, sent, msgLen);

        } else {
          printk(KERN_INFO
                  "%s _proc_net(): FAILED to send %lu byte 'ready': %ld\n",
                  DEVICE_NAME, msgLen, sent);

        }
      }

    } else if (! strncmp( pStart, "none", len )) {

      if (lkb.netFailover.sock) {
        // Send a "close" message
        char*   msg     = "{\"status\":\"close\"}\n";
        size_t  msgLen  = strnlen( msg, 24 );
        //ssize_t sent    = udp_send( &lkb.netFailover, msg, msgLen );
        ssize_t sent    = tcp_send( &lkb.netFailover, msg, msgLen );

        if (sent > 0) {
          printk(KERN_INFO
                  "%s _proc_net(): Sent %ld / %lu byte 'close'\n",
                  DEVICE_NAME, sent, msgLen);

        } else {
          printk(KERN_INFO
                  "%s _proc_net(): FAILED to send %lu byte 'close': %ld\n",
                  DEVICE_NAME, msgLen, sent);

        }

        tcp_close_socket( &lkb.netFailover );
      }

      // Reset to NO net failover
      memset( &lkb.netFailover, 0, sizeof(lkb.netFailover) );

      printk(KERN_INFO
              "%s _proc_net(): set netFailover: 'none'\n", DEVICE_NAME);
    } else {
      // Invalid format : MISSING port #
      printk(KERN_WARNING
                "%s _proc_net(): Missing port [ %s ]\n",
                  DEVICE_NAME, pStart);

      // Replace the original
      memcpy( lkb.netFailover.str, backup, LKB_NET_STR_LEN );

      return -EINVAL;
    }
  }

  return ret;
}
/* Custom handler for net_failover }
 ****************************************************************************/

/**
 *  Template for a sysctl variable entry.
 */
#define SYSCTL_VAR_ENTRY(name, var) { \
  .procname     = (name),             \
  .data         = (void*)&(var),      \
  .maxlen       = sizeof(var),        \
  .mode         = 0644,               \
  .proc_handler = proc_dointvec,      \
}

#define SYSCTL_NET_ENTRY(name, var) { \
  .procname     = (name),             \
  .data         = (void*)&(var),      \
  .maxlen       = sizeof(var),        \
  .mode         = 0644,               \
  .proc_handler = _proc_net,          \
}

// kernel_beat sysctl variables
static struct ctl_table s_sysctlVars[]  = {
  SYSCTL_VAR_ENTRY("interval",      lkb.interval),
  SYSCTL_VAR_ENTRY("sync",          lkb.syncBeat),

  SYSCTL_NET_ENTRY("net_failover",  lkb.netFailover.str),
  {},
};

// kernel_beat sysctl path
static struct ctl_path  s_sysctlPath[]  = {
  { .procname = "kernel" },
  { .procname = "beat" },
  {},
};

/**
 *  Read from our device (lkb circular buffer).
 *  @method device_read
 *  @param  flip    The file structure {struct file*};
 *  @param  buffer  The user buffer {char*};
 *  @param  len     The maximum amount to read {size_t};
 *  @param  offset  The offset at which to begin reading {loff_t};
 *
 *  @return The number of bytes read {ssize_t};
 */
static ssize_t
device_read(struct file *flip, char *buffer, size_t len, loff_t *offset) {
  ssize_t available  = 0;
  ssize_t bytes_read = 0;
  ssize_t to_read    = 0;

  /*
  printk(KERN_INFO "%s device_read( len[ %lu ], offset[ %lld ] ) ...\n",
                   DEVICE_NAME, len, *offset);
  // */

  // Wait for data to become available
  down( &lkb.semBuf );
  do {
    int res;

    up( &lkb.semBuf );

    /* wait_event_killable()      : TASK_KILLABLE
     * wait_event_interruptible() : TASK_INTERRUPTIBLE
     *    responds to user-level process interrupts (e.g. SIGINT)
     */
    res = wait_event_interruptible( lkb.waitBuf,
                                    ((available = lkb_count()) > *offset) );
    if (res != 0) {
      // Interrupted / killed
      return res;
    }

    down( &lkb.semBuf );
  } while( available < 1 );

  // Compute how much we can read, skipping over any requested offset.
  if (*offset > 0) {
    printk(KERN_INFO "%s device_read(): seek[ %lld ] ...\n",
                     DEVICE_NAME, *offset);
    lkb_read_seek( *offset );
  }

  available = lkb_count();
  to_read   = (available < len ? available : len);

  /*
  printk(KERN_INFO "%s device_read(): read[ %lu of %lu ] ...\n",
                   DEVICE_NAME, to_read, len);
  // */

  /* Write from our circular buffer directly to the user-space buffer updating
   * the read position of our circular buffer.
   */
  bytes_read = lkb_read_to_user( buffer, to_read );

  up( &lkb.semBuf );

  if (bytes_read < 0) {
    return -EFAULT;
  }

  // Update the offset (to 0 since the read moves the tail pointer)
  //*offset += bytes_read;
  *offset = 0;

  return bytes_read;
}

/**
 *  Wrtie to our device (read-only).
 *  @method device_write
 *  @param  flip    The file structure {struct file*};
 *  @param  buffer  The user buffer {char*};
 *  @param  len     The number of bytes to write {size_t};
 *  @param  offset  The offset at which to begin writing {loff_t};
 *
 *  @return The number of bytes written {ssize_t};
 */
static ssize_t
device_write(struct file *flip, const char *buffer, size_t len, loff_t *offset){
  // This is a read-only device
  printk(KERN_ALERT "This operation is not supported.\n");
  return -EINVAL;
}

/**
 *  Open our device.
 *  @method device_open
 *  @param  inode   The inode of our device {struct inode};
 *  @param  file    The file structure {struct file*}
 *
 *  @return The open status {int};
 *          - on success, 0
 *          - on failure, < 0
 */
static int
device_open(struct inode *inode, struct file *file) {
  // If device is open, return busy
  down( &lkb.semCount );
  if (lkb.openCount) {
    up( &lkb.semCount );
    return -EBUSY;
  }

  lkb.openCount++;
  up( &lkb.semCount );

  printk(KERN_INFO "%s device_open(): openCount[ %u ]\n",
         DEVICE_NAME, lkb.openCount);

  try_module_get( THIS_MODULE );

  // Start the beat, gathering initial metrics and starting a periodic timer
  lkb_start_beat();

  return 0;
}

/**
 *  Perform poll request
 *  @method device_poll
 *  @param  file    The file structure {struct file*};
 *  @param  poll    The poll table {poll_table};
 *
 *  The returned mask is comprised of the bits:
 *      POLLIN      This bit must be set if the device can be read without
 *                  blocking;
 *      POLLRDNORM  This bit must be set if "normal" data is available for
 *                  reading.
 *                  === A readable device returns (POLLIN | POLLRDNORM). ===
 *
 *      POLLRDBAND  This bit indicates that out-of-band data is available for
 *                  reading from the device. It is currently used only in one
 *                  place in the Linux kernel (the DECnet code) and is not
 *                  generally applicable to device drivers.
 *      POLLPRI     High-priority data (out-of-band) can be read without
 *                  blocking.  This bit causes select to report that an
 *                  exception condition occurred on the file, because select
 *                  reports out-of-band data as an exception condition.
 *      POLLHUP     When a process reading this device sees end-of-file, the
 *                  driver must set POLLHUP (hang-up). A process calling select
 *                  is told that the device is readable, as dictated by the
 *                  select functionality.
 *      POLLERR     An error condition has occurred on the device. When poll is
 *                  invoked, the device is reported as both readable and
 *                  writable, since both read and write return an error code
 *                  without blocking.
 *      POLLOUT     This bit is set in the return value if the device can be
 *                  written to without blocking.
 *      POLLWRNORM  This bit has the same meaning as POLLOUT, and sometimes it
 *                  actually is the same number. A writable device returns
 *                  (POLLOUT | POLLWRNORM).
 *      POLLWRBAND  Like POLLRDBAND, this bit means that data with nonzero
 *                  priority can be written to the device. Only the datagram
 *                  implementation of poll uses this bit, since a datagram can
 *                  transmit out-of-band data.
 *
 *  @return A bit mask describing the operations (if any) that could be
 *          immediately performed without blocking {unsigned int};
 */
static unsigned int
device_poll(struct file *file, poll_table* wait) {
  ssize_t       available = 0;
  unsigned int  mask      = 0;

  down( &lkb.semBuf );

  /* Add our `waitBuf`, used to signal changes to the circular buffer, to the
   * poll `wait`.
   */
  poll_wait(file, &lkb.waitBuf, wait);

  // Grab the current count of available data
  available = lkb_count();

  up( &lkb.semBuf );

  if (available > 0) {
     // Data is avaialble to read -- readable
     mask |= POLLIN | POLLRDNORM;
  }
  return mask;
}

/**
 *  Close/release our device.
 *  @method device_release
 *  @param  inode   The inode of our device {struct inode};
 *  @param  file    The file structure {struct file*}
 *
 *  @return The open status {int};
 *          - on success, 0
 *          - on failure, < 0
 */
static int
device_release(struct inode *inode, struct file *file) {
  /* Decrement the open counter and usage count. Without this, the module would
   * not unload.
   */
  down( &lkb.semCount );
  lkb.openCount--;
  up( &lkb.semCount );

  printk(KERN_INFO "%s device_release(): openCount[ %u ]\n",
         DEVICE_NAME, lkb.openCount);

  // Stop the beat, releasing collection resources.
  lkb_stop_beat( FALSE /* allow reset */ );

  module_put( THIS_MODULE );
  return 0;
}

/* This structure points to all of the device functions */
static struct file_operations file_ops = {
  read:     device_read,
  write:    device_write,
  open:     device_open,
  poll:     device_poll,
  release:  device_release,
};

/**
 *  Initialize our module during load.
 *  @method kernel_beat_init
 *
 *  @return The open status {int};
 *          - on success, 0
 *          - on failure, < 0
 */
static int
__init kernel_beat_init(void) {
  // Initialize our circular buffer, semaphores, and wait queue.
  int res = lkb_init();
  if (res < 0) {
    printk(KERN_ALERT "%s initialization failed: %d\n",
           DEVICE_NAME, lkb.major);
    return res;
  }

  // Register character device
  lkb.major = register_chrdev( lkb.major, DEVICE_NAME, &file_ops );
  if (lkb.major < 0) {
    printk(KERN_ALERT "Could not register device %s: %d\n",
           DEVICE_NAME, lkb.major);
    lkb_cleanup();
    return lkb.major;

  }

  // Register the device class
  lkb.charClass = (void*)class_create( THIS_MODULE, DEVICE_CLASS );
  if (IS_ERR(lkb.charClass)) {
    printk(KERN_ALERT "%s class creation failed\n", DEVICE_NAME);

  } else {
    lkb.charDevice = (void*)device_create( lkb.charClass,
                                           NULL,
                                           MKDEV( lkb.major, 0),
                                           NULL,
                                           DEVICE_NAME );
    if (IS_ERR(lkb.charDevice)) {
      printk(KERN_ALERT "%s device creation failed\n", DEVICE_NAME);
      class_destroy( lkb.charClass );
      lkb.charClass = NULL;
    }
  }

  // Register sysctl paths
  lkb.sysctl = (void*)register_sysctl_paths( s_sysctlPath, s_sysctlVars );
  if (!lkb.sysctl) {
    printk(KERN_ALERT "%s sysctl registration failed\n", DEVICE_NAME);
  }

  /* :NOTE: This output string 'kernel_beat loaded... #' is used by the
   *        Makefile to identify the assigned major number in order to create
   *        the proper `/dev/kernel_beat` device for testing.
   */
  printk(KERN_INFO "%s loaded with device major number %d\n",
         DEVICE_NAME, lkb.major);

  printk(KERN_INFO "%s circular buffer available read[ %lu : %lu ], "
                                               "write[ %lu : %lu ]\n",
         DEVICE_NAME, lkb_count(), lkb_count_to_end(),
                      lkb_space(), lkb_space_to_end() );

  return 0;
}

/**
 *  Cleanup our module for unloading.
 *  @method kernel_beat_exit
 *
 *  @return void
 */
static void
__exit kernel_beat_exit(void) {
  // :NOTE: lkb_cleanup() zeros `lkb` so grab a copy of the registerd major #
  struct ctl_table_header*  hdr         = (struct ctl_table_header*)lkb.sysctl;
  void*                     charClass   = lkb.charClass;
  int                       major       = lkb.major;

  if (hdr) {
    unregister_sysctl_table( hdr );
  }

  if (lkb.netFailover.sock) {
    // Send a "shutdown" message
    char*   msg     = "{\"status\":\"shutdown\"}\n";
    size_t  msgLen  = strnlen( msg, 24 );
    //ssize_t sent    = udp_send( &lkb.netFailover, msg, msgLen );
    ssize_t sent    = tcp_send( &lkb.netFailover, msg, msgLen );

    if (sent > 0) {
      printk(KERN_INFO
              "%s kernel_beat_exit(): Sent %ld / %lu byte 'shutdown'\n",
              DEVICE_NAME, sent, msgLen);

    } else {
      printk(KERN_INFO
              "%s kernel_beat_exit(): FAILED to send %lu byte 'shutdown': %ld\n",
              DEVICE_NAME, msgLen, sent);

    }

    tcp_close_socket( &lkb.netFailover );
  }

  // Cleanup our circular buffer, semaphores, wait queue, and periodic timer.
  lkb_cleanup();

  if (charClass) {
    device_destroy( charClass, MKDEV(major, 0) );

    class_unregister( charClass );

    class_destroy( charClass );
  }

  // Unregister the character device.
  unregister_chrdev( major, DEVICE_NAME );

  printk(KERN_INFO "%s unloaded\n", DEVICE_NAME);
}

/* Register module functions */
module_init( kernel_beat_init );
module_exit( kernel_beat_exit );
