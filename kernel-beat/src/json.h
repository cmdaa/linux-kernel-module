/**
 *  Provides simple JSON-encoding directly into the circular buffer for basic
 *  data types.
 *
 *  Writes to the circular buffer are via `lkb_write()`.
 *
 */
#ifndef _JSON_H_
#define _JSON_H_

#include <linux/kernel.h>
#include "map.h"

#define JSON_MAX_KEY  MAP_KEY_MAX
#define JSON_MAX_STR  1024

/* Dynamic-type output based upon offset and data type:
 *
 *  typedef struct {
 *    char    name[16];
 *    size_t  offset;
 *    void    (*as)(void*);
 *  } Map_t;
 *
 *  void as_str( void* p ) { printf("%s", (char*)p); }
 *  void as_u32( void* p ) { printf("%u", (*(u32*)p)); }
 *  ...
 *
 *  // OFFSETOF() === offsetof() from stddef.h
 *  #define OFFSETOF(s,m) ((size_t) &(((s *)0)->m))
 *
 * ==============================================
 * Usage:
 *
 *  typedef struct {
 *    char  name[32];
 *    u32   pid;
 *  } proc_t;
 *
 *  static Map_t key_val[] = {
 *    {"name", OFFSETOF(proc_t, name), as_str)},
 *    {"pid",  OFFSETOF(proc_t, pid),  as_u32)},
 *    ...
 *  };
 *
 *  // Dumping key/value pairs for the `proc_t` pointed by by `proc`
 *  proc_t* proc  = ...;
 *  for (int idex = 0; idex < sizeof(key_val)/sizeof(key_val[0]); idex++) {
 *    Map_t*  entry = &key_val[idex];
 *    void*   ptr   = ((void*)proc) + entry->offset;
 *
 *    printf("\"%s\":", entry->name); (*entry->as)(ptr); printf("\n");
 *  }
 */

/**
 *  Write a raw, unquoted string value as JSON to the circular buffer.
 *  @method json_write_raw_strn_val
 *  @param  val   The value {const char*};
 *  @param  max   The maximum number of characters from `val` {int};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_raw_strn_val( const char* val, int max );

/**
 *  Write the provided key/string.
 *  @method json_write_key
 *  @param  key   The key/name {const char*};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_key( const char* key );

/**
 *  Write the string value as JSON to the circular buffer.
 *  @method json_write_strn_val
 *  @param  val   The value {const char*};
 *  @param  max   The maximum number of characters from `val` {int};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_strn_val( const char* val, int max );

/**
 *  Write the unsigned 64-bit value as JSON to the lkb circular buffer.
 *  @method json_write_u64_val
 *  @param  val   The value {u64};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_u64_val( u64 val );

/**
 *  Write the signed 64-bit value as JSON to the lkb circular buffer.
 *  @method json_write_s64_val
 *  @param  val   The value {s64};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_s64_val( s64 val );

/**
 *  Write the unsigned 32-bit value as JSON to the lkb circular buffer.
 *  @method json_write_u32_val
 *  @param  val   The value {u32};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_u32_val( u32 val );

/**
 *  Write the signed 32-bit value as JSON to the lkb circular buffer.
 *  @method json_write_s32_val
 *  @param  val   The value {s32};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_s32_val( s32 val );

/**
 *  Write the unsigned 16-bit value as JSON to the lkb circular buffer.
 *  @method json_write_u16_val
 *  @param  val   The value {u16};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_u16_val( u16 val );

/**
 *  Write the signed 16-bit value as JSON to the lkb circular buffer.
 *  @method json_write_s16_val
 *  @param  val   The value {s16};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_s16_val( s16 val );

/**
 *  Write the unsigned 8-bit value as JSON to the lkb circular buffer.
 *  @method json_write_u8_val
 *  @param  val   The value {u8};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_u8_val( u8 val );

/**
 *  Write the unsigned 64-bit key/value as JSON to the circular buffer.
 *  @method json_write_u64
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {u64};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_u64( const char* key, u64 val );

/**
 *  Write the signed 64-bit key/value as JSON to the circular buffer.
 *  @method json_write_s64
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {s64};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_s64( const char* key, s64 val );

/**
 *  Write the unsigned 32-bit key/value as JSON to the circular buffer.
 *  @method json_write_u32
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {u32};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_u32( const char* key, u32 val );

/**
 *  Write the signed 32-bit key/value as JSON to the circular buffer.
 *  @method json_write_s32
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {s32};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_s32( const char* key, s32 val );

/**
 *  Write the unsigned 16-bit key/value as JSON to the circular buffer.
 *  @method json_write_u16
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {u16};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_u16( const char* key, u16 val );

/**
 *  Write the signed 16-bit key/value as JSON to the circular buffer.
 *  @method json_write_s16
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {s16};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_s16( const char* key, s16 val );

/**
 *  Write the unsigned 8-bit key/value as JSON to the circular buffer.
 *  @method json_write_u8
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {u8};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_u8( const char* key, u8 val );

/**
 *  Write the signed 8-bit key/value as JSON to the circular buffer.
 *  @method json_write_s8
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {s8};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_s8( const char* key, s8 val );

/**
 *  Write the string key/value as JSON to the circular buffer.
 *  @method json_write_strn
 *  @param  key   The key/name {const char*};
 *  @param  val   The value {const char*};
 *  @param  max   The maximum number of characters from `val` {int};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_strn( const char* key, const char* val, int max );

/**
 *  Given a map entry and pointer to a structure, write the JSON version of
 *  referenced value.
 *  @method json_write_map
 *  @param  entry   The map entry {map_t*};
 *  @param  pstruct A pointer to the containing structure {void*};
 *
 *  @return The number of bytes written {int};
 */
extern ssize_t json_write_map( map_t* entry, void* pstruct );

#endif  // _JSON_H_
