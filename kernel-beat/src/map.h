/**
 *  Provides a mechanism for naming and accessing fields within a structure.
 *  This is used for identifying and reporting differences in metrics between
 *  beats.
 *
 */
#ifndef _MAP_H_
#define _MAP_H_

#include <linux/types.h>

#ifndef TRUE
#define TRUE  (bool)1
#define FALSE (bool)0
#endif  // TRUE

/**
 *  Compute the byte offset of a structure member.
 *  @method OFFSETOF
 *  @param  s     The target structure;
 *  @param  m     The target member;
 *
 *  @return The byte offset of the member {size_t};
 */
#define OFFSETOF(s,m) ((size_t) &(((s*)0)->m))

/**
 *  Compute the size of a structure member.
 *  @method SIZEOF
 *  @param  s     The target structure;
 *  @param  m     The target member;
 *
 *  @return The byte size of the member {size_t};
 */
#define SIZEOF(s,m)   ((size_t) sizeof(((s *)0)->m))

/**
 *  Compute the number of elements in an array of structures.
 *  @method COUNT
 *  @param  a     The target array of structures;
 *
 *  @return The number of elements {size_t};
 */
#define COUNT(a)      ((size_t) sizeof(a) / sizeof((a)[0]))

/**
 *  Compute the index of the given element in an array of structures.
 *  @method INDEXOF
 *  @param  _a    The target array of structures;
 *  @param  _e    The target element within the array;
 *
 *  @return The index of the element {u32};
 */
#define INDEXOF(_a,_e)  (u32)(((void*)(_e) - (void*)(_a)) / sizeof(_a[0]))

/**
 *  Given a map entry, pointer to the beginning of the data area, and type,
 *  return the offset value cast to a pointer to the provided type.
 *  @method MAP_DEREF
 *  @param  _m  The map entry {map_t*};
 *  @param  _p  A pointer to the data source {void*};
 *  @param  _t  The desired type;
 *
 *  @return A pointer to the target entry cast as a pointer to the desired type;
 */
#define MAP_DEREF(_m, _p, _t) (_t*)((void*)(_p) + (_m)->offset)

/**
 *  The maximum size of a map key/name.
 */
#define MAP_KEY_MAX 64

/**
 *  Map types
 */
#define MAP_TYPE_STR 0
#define MAP_TYPE_S8  1
#define MAP_TYPE_U8  2
#define MAP_TYPE_S16 3
#define MAP_TYPE_U16 4
#define MAP_TYPE_S32 5
#define MAP_TYPE_U32 6
#define MAP_TYPE_S64 7
#define MAP_TYPE_U64 8
#define MAP_TYPE_SPC 255 // Special
typedef u8  map_type_t;

/**
 *  Key name, type, offset, and (optional) size.
 */
typedef struct {
  char*       name;
  map_type_t  type;
  size_t      offset;
  size_t      size;
  bool        changed;
} map_t;

// MUST match both mapWrite() and mapDiffAndWrite();
typedef bool  (*mapWriter_t)( map_t* entry, void* dst, void* src );

/**
 *  Given a map entry and two pointers, determine if the value of the
 *  referenced structure member is different between the two structures.
 *  @method mapDiff
 *  @param  entry     The map entry {map_t*};
 *  @param  s1        A pointer to the first  structure {void*};
 *  @param  s2        A pointer to the second structure {void*};
 *
 *  @return TRUE | FALSE {bool};
 */
extern bool mapDiff( map_t* entry, void* p1, void* p2 );

/**
 *  Given a map entry and pointer to mapped data, write the incoming value
 *  to the mapped data.
 *  @method mapWrite
 *  @param  entry     The map entry {map_t*};
 *  @param  dst       A pointer to the destination {void*};
 *  @param  src       A pointer to the soure {void*};
 *
 *  @note   `src` is expected to point to the data type indicated by
 *          `entry->type`;
 *
 *  @return TRUE | FALSE {bool};
 */
extern bool mapWrite( map_t* entry, void* dst, void* src );

/**
 *  Given a map entry and pointer to mapped data, read the target value.
 *  @method mapRead
 *  @param  entry     The map entry {map_t*};
 *  @param  dst       A pointer to the destination {void*};
 *  @param  src       A pointer to the soure {void*};
 *
 *  @return TRUE | FALSE {bool};
 */
extern bool mapRead( map_t* entry, void* dst, void* src );

/**
 *  Given a map entry, pointer to mapped data and a pointer to a new value,
 *  determine if the new value differs, write the new value and return the
 *  determination.
 *  @method mapDiffAndWrite
 *  @param  entry     The map entry {map_t*};
 *  @param  dst       A pointer to the destination {void*};
 *  @param  neVal     A pointer to the new value {void*};
 *
 *  @return TRUE | FALSE {bool};
 */
extern bool mapDiffAndWrite( map_t* entry, void* dst, void* newVal );

#endif  // _MAP_H_
