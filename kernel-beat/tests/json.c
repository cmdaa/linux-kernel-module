#include <sys/types.h>  // ssize_t
#include <stdio.h>
#include <string.h>     // strnlen()

typedef unsigned char u8;

/**
 *  Write data to the buffer.
 *  @method lkb_write
 *  @param  buf     The data to write {u8*};
 *  @param  len     The number of bytes to write {ssize_t};
 *
 *  @note   `lkb.writeLock` must be held BEFORE calling this method.
 *
 *  @return The number of bytes written {ssize_t};
 */
ssize_t
lkb_write( u8* buf, ssize_t len ) {
  return fwrite( buf, 1, len, stdout );
}

/**
 *  Write a raw, unquoted string value as JSON to the circular buffer.
 *  @method json_write_raw_strn_val
 *  @param  val   The value {const char*};
 *  @param  max   The maximum number of characters from `val` {int};
 *
 *  @return The number of bytes written {int};
 */
ssize_t
json_write_raw_strn_val( const char* val, int max ) {
  size_t valLen   = strnlen( val, max );
  size_t written  = 0;
  size_t idex;

  /* Encode any special characters while writing this string.
   *
   * According to the JSON spec (https://www.json.org/json-en.html), a string
   * may contain "Any codepoint except " or control characters" and the only
   * valid escapes are:
   *   \"     "
   *   \'     '
   *   \/     /
   *   \b     backspace           (0x08)
   *   \t     horizontal-tab      (0x09)
   *   \n     new-line            (0x0a)
   *   \f     new-page/form-feed  (0x0c)
   *   \r     carriage-return     (0x0d)
   *   \u#### Unicode codepoint where '#' are hex digits
   *
   *  Control characters may be encoded as a Unicode codepoint.
   */
  for (idex = 0; idex < valLen; idex++) {
    char  strc  = val[idex];

    switch( strc ) {
      case '"':   // Escape this double quote
        written += lkb_write( "\\\"", 2 );
        break;

      case '\b':  // Backspace
        written += lkb_write( "\\b", 2 );
        break;

      case '\t':  // Tab
        written += lkb_write( "\\t", 2 );
        break;

      case '\n':  // Line feed
        written += lkb_write( "\\n", 2 );
        break;

      case '\f':  // Form feed
        written += lkb_write( "\\f", 2 );
        break;

      case '\r':  // Carriage return
        written += lkb_write( "\\r", 2 );
        break;

      case '\\':
        if (idex+1 < valLen) {
          // Ensure the next character is a valid escape sequence
          char  nextc = val[idex+1];

          switch( nextc ) {
            case 'u': {
              // \u is valid iff the following 4 digts are hex
              u8  isValid = 1;

              if ( idex+6 > valLen) {
                // Not enough characters
                isValid = 0;

              } else {
                // Ensure the 4 characters following \u are hex digits
                size_t  jdex;

                for ( jdex = idex+2; jdex < idex+6; jdex++) {
                  u8  cur = val[jdex];

                  if ( cur < '0' || cur > 'f'  ||
                      (cur > '9' && cur < 'A') ||
                      (cur > 'F' && cur < 'a') ) {
                    // NOT a hex digit
                    isValid = 0;
                    break;
                  }
                }
              }

              if (isValid != 1) {
                /* This is NOT a valid \u sequence.
                 *
                 * Treat it as a simple, escaped ASCII character 'u' squelching
                 * this escape character and continuing to the actual 'u' for
                 * normal processing.
                 */
                continue;
              }
            } // Valid Escape sequence -- Fall through

            case '"':
            //case '\'':  // Doesn't appear to be valid in reality
            case '/':
            case 'b':
            case 't':
            case 'n':
            case 'f':
            case 'r':
              /* Valid escape sequence.
               *
               * Write both characters of this valid escape sequence.
               */
              written += lkb_write( "\\",   1 );
              written += lkb_write( &nextc, 1 );

              // Skip the next character since we just wrote it
              idex++;
              continue;

            default:
              if (nextc >= 0x20 && nextc <= 0x7f) {
                /* The next character is in the ASCII range and should NOT be
                 * escaped.
                 *
                 * Squelch this escape character and continue on to the
                 * following character for normal processing, essentially
                 * unescaping the ASCII character.
                 */
                continue;
              }
              break;
          }
        }

        // NOT a valid escape sequence so simply escape this escape character.
        written += lkb_write( "\\\\", 2 );
        break;

      default:
        if ((u8)strc < 0x20) {
          // Control character (< 0x20) -- encode it using the '\u####' format.
          char   numBuf[5];
          size_t valLen   = snprintf( numBuf, sizeof(numBuf), "%04x",
                                                                (u8)strc );

          written += lkb_write( "\\u", 2 );
          written += lkb_write( numBuf, valLen );

        } else {
          // non-control codepoint
          written += lkb_write( &strc, 1 );
        }
        break;
    }
  }

  return written;
}

/*****************************************************************************
 * Main test harness
 *
 */
int
main( int argc, char** argv ) {
  char    str1[256] = {0};
  char    str2[512] = {0};
  char    str3[]    = { '\\', '"',
                        '\\', '\'',
                        '\\', '/',
                        '\\', 'b',
                        '\\', 't',
                        '\\', 'n',
                        '\\', 'f',
                        '\\', 'r',
                        '\\', '\\',
                        0 };
  char    str4[]    = "\\u0024, \\u00a2, \\u0939, \\u20ac, \\ud55c";
  char    str5[2048]= {0};
  ssize_t res;
  int     idex;
  u8      cdex;

  // Generate raw bytes for each character in the ASCII range (except 0)
  for (idex = 0; idex < sizeof(str1) - 1; idex++) {
    str1[idex] = (char)(idex+1);
  }

  // Generate escaped versions of printable ASCII characters
  for (idex = 0, cdex = 0x20; cdex < 0x80; idex += 2, cdex++) {
    str2[idex]   = '\\';
    str2[idex+1] = cdex;
  }

  // Generate unicode versions of printable ASCII characters
  for (idex = 0, cdex = 0x20; cdex < 0x80; idex += 7, cdex++) {
    u8  nibble1 = (cdex >> 4) & 0x0F;
    u8  nibble2 = (cdex       & 0x0F);
    u8  char1   = nibble1 + (nibble1 < 10 ? '0' : 'W');
    u8  char2   = nibble2 + (nibble2 < 10 ? '0' : 'W');

    str5[idex]   = '\\';
    str5[idex+1] = 'u';
    str5[idex+2] = '0';
    str5[idex+3] = '0';
    str5[idex+4] = char1;
    str5[idex+5] = char2;
    str5[idex+6] = ':';
  }

  fprintf(stdout, "{\n");

  fprintf(stdout, "  \"str1\": \"");
  (void)json_write_raw_strn_val( str1, sizeof(str1) );
  fprintf(stdout, "\"");

  fprintf(stdout, ",\n");
  fprintf(stdout, "  \"str2\": \"");
  (void)json_write_raw_strn_val( str2, sizeof(str2) );
  fprintf(stdout, "\"");

  fprintf(stdout, ",\n");
  fprintf(stdout, "  \"str3\": \"");
  (void)json_write_raw_strn_val( str3, sizeof(str3) );
  fprintf(stdout, "\"");

  fprintf(stdout, ",\n");
  fprintf(stdout, "  \"str4\": \"");
  (void)json_write_raw_strn_val( str4, strlen(str4) );
  fprintf(stdout, "\"");

  fprintf(stdout, ",\n");
  fprintf(stdout, "  \"str5\": \"");
  (void)json_write_raw_strn_val( str5, strlen(str5) );
  fprintf(stdout, "\"");

  fprintf(stdout, "\n}\n");
}
