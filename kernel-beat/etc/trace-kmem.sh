#!/bin/bash
#
# MUST be run as root
#   
ID=$(id -u)
if [[ $ID != 0 ]]; then
  echo "*** $(basename "$0") MUST be run as root"
  exit -1
fi

SRC="$(dirname "$(readlink -f $(dirname "$0"))")"
MOD=$(lsmod | grep kernel_beat)

# Clear any current trace buffer
echo > /sys/kernel/debug/tracing/trace

# Enable kmalloc/kfree tracing
# kmem:kmalloc
# kmem:kfree

cat <<EOF > /sys/kernel/debug/tracing/set_event
kmem:kmalloc
kmem:kfree
EOF
#echo 1 > /sys/kernel/debug/tracing/events/kmem/kmalloc/enable

# Exercise the module
pushd "${SRC}"                  > /dev/null
[[ -z "$MOD" ]] && make load    > /dev/null 2>&1
${SRC}/utils/read-beat -l 10    > /dev/null 2>&1
[[ -z "$MOD" ]] && make unload  > /dev/null 2>&1
popd                            > /dev/null

# Disable tracing
echo > /sys/kernel/debug/tracing/set_event
#echo 0 > /sys/kernel/debug/tracing/events/kmem/kmalloc/enable

# Present the trace
cat /sys/kernel/debug/tracing/trace
