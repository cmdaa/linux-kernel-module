#!/usr/bin/env node
/**
 *
 *  Given a KernelBeat performance measurement CSV generated via
 *  `extract-timings`, extract and normalize 'proc_mon' records:
 *    - rename columns:
 *        beat, module, jsonB, timeNs, timeMs, procs;
 *    - exclude timeNs;
 *    - align beat and procs with the next 'lkb_gather' record;
 *    - output columns:
 *        beat, jsonB, timeMs, procs
 */
const Readline  = require('readline');
const rl        = Readline.createInterface({
  input : process.stdin,
  //output: process.stdout,
});
const results   = {
  lineCnt   : 0,
  extracted : 0,
  warnings  : 0,
};
const recs      = [];


process.stdout.write('beat,jsonB,timeMs,procs\n');

/**
 *  Given a value, convert it to a floating point with fixed precision.
 *  @method fixedFloat
 *  @param  val               The target value {Number|String};
 *  @param  [precision = 4]   The target precision {Number};
 *
 *  @return The floating point value {Number};
 */
function fixedFloat( val, precision = 4 ) {
  const num = (typeof(val) === 'number' ? val : parseFloat( val ) );

  return parseFloat( num.toFixed( precision ) );
}

rl.on( 'line', line => {
  let [beat,module,jsonB,timeNs,timeMs,procs] = line.split(',');
  results.lineCnt++;

  const cnt     = results.lineCnt.toString().padStart(8);
  const extract = results.extracted.toString().padStart(8);
  const warn    = results.warnings.toString().padStart(8);
  process.stderr.write(
    `>>> Line ${cnt}, Extracted ${extract}, Warnings ${warn} ...\r`
  );

  if (results.lineCnt === 1) { return }

  // Convert numbers
  beat   = parseInt( beat );
  jsonB  = parseInt( jsonB );
  timeMs = fixedFloat( timeMs );
  procs  = parseInt( procs );

  switch( module ) {
    case 'proc_mon':
      results.extracted++;

      if (timeMs > 1000) {
        results.warnings++;

        console.error('\n=== Beat %d: used %s ms ...',
                      beat+1, timeMs);
        return;
      }

      recs.push( { jsonB, timeMs } );
      break;

    case 'lkb_gather': {
      // Update beat and process counts
      if (recs.length > 0) {
        const procBase = procs + recs.length;

        recs.forEach( (rec,idex) => {
          // beat, jsonB, timeMs, procs
          process.stdout.write(
            `${beat},${rec.jsonB},${rec.timeMs},${procBase - idex}\n`
          );
        });
      }
      recs.length = 0;
    } break;
  }
});

rl.on( 'close', () => {

  console.error('>>> %d lines processed, %d extracted, %d warnings%s',
                results.lineCnt,
                results.extracted,
                results.warnings,
                ' '.repeat(8));
});

// vi: ft=javascript
