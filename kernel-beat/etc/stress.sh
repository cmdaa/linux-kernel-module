#!/bin/bash
#
# Stress load for testing kernel_beat
#
trap "{ echo '>>> Exit'; exit 0; }" SIGINT

while [ 1 ]; do
    R="$(openssl rand -hex 1)"
    TS=$(date +'%s')

    if [[ ${R} < "08" ]]; then
        # DIV0 exception                5%
        printf "%10d: DIV0 exception ...\n" "$TS"
        ./error-div0 > /dev/null 2>&1

    elif [[ ${R} > "07" && ${R} < "16" ]]; then
        # NULL pointer dereference      5%
        printf "%10d: NULL pointer exception ...\n" "$TS"
        ./error-null > /dev/null 2>&1

    elif [[ ${R} > "15" && ${R} < "3d" ]]; then
        # Socket io                     15%
        printf "%10d: Socket IO : 30s ...\n" "$TS"
        stress-ng --sock 4 --timeout 30s

        #echo "ps ..."
        #ps -uax | awk '/sys/ {print $1}' > /dev/null 2>&1


    elif [[ ${R} > "3c" && ${R} < "7e" ]]; then
        # Memory stress                 25%
        printf "%10d: Memory stress : 30s ...\n" "$TS"
        stress-ng --vm 1 --bigheap 1 --cache 1 --timeout 30s

    elif [[ ${R} > "7d" && ${R} < "bf" ]]; then
        # Disk read                     25%
        printf "%10d: Disk stress : 30s ...\n" "$TS"
        stress-ng --iomix 2 --timeout 30s

        #echo "Disk stress : dd 1G ..."
        #dd ibs=1024 count=1048576 if=/dev/zero of=/dev/null status=none

    else    # > "be" && < "ff"
        # CPU stress                    25%
        printf "%10d: CPU stress : 30s ...\n" "$TS"
        stress-ng --cpu 4 --cpu-load 80 --timeout 30s

        #echo "CPU stress : dd | openssl ..."
        #dd ibs=1024 count=102400 if=/dev/urandom status=none | \
        #    openssl camellia256 -e -a -salt -k test -out /dev/null
    fi

done
