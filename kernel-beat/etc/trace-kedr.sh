#!/bin/bash
#
# Perform a memory trace of the kernel_beat module using kedr.
#
# kedr MUST be installed separately before running this script.
#
# You can find the sources and build instructions for kedr at:
#   https://github.com/euspectre/kedr/wiki/kedr_manual_getting_started
#
#
# This script MUST be run as root since module insertion, including
# starting/stopping kedr, requires root privileges.
#
ID=$(id -u)
if [[ $ID != 0 ]]; then
  echo "*** $(basename "$0") MUST be run as root"
  exit -1
fi

SRC="$(dirname "$(readlink -f $(dirname "$0"))")"
MOD=$(lsmod | grep kernel_beat)

KEDR=$(which kedr)
if [[ -z "$KEDR" ]]; then
  echo "*** kedr does not appear to be installed"
  exit -1
fi

# Clear the current dmesg buffer
dmesg --clear > /dev/null 2>&1

# Start kedr targeting our module
${KEDR} start 'kernel_beat' > /dev/null 2>&1

# Exercise the module
pushd "${SRC}"                  > /dev/null
[[ -z "$MOD" ]] && make load    > /dev/null 2>&1
${SRC}/utils/read-beat -l 10    > /dev/null 2>&1
[[ -z "$MOD" ]] && make unload  > /dev/null 2>&1
popd                            > /dev/null

# Stop kedr
${KEDR} stop > /dev/null 2>&1

# Present the results
dmesg --human | grep -E 'kernel_beat|leak_check'
