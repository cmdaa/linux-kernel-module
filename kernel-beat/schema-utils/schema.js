const Path    = require('path');
const Fs      = require('fs');
const Request = require('request');
const Ajv     = require('ajv');

// Processing state
const state       = {
  srcDir    : Path.resolve( Path.join( __dirname, '..' ) ),
  baseUri   : 'https://cmdaa.io/linux-kernel-module/kernel-beat/',
  baseSchema: 'schema.json',

  ajv       : new Ajv({ loadSchema: _loadSchemaUri }),

  root      : null,
};

module.exports = {
  state : state,
  ajv   : state.ajv,

  /**
   *  Load and validate the base schema.
   *  @method load
   *
   *  `state.root` will be set to the final validator upon completion.
   *
   *  @return A promise for results {Promise};
   *          - on success, the final validator {Function};
   *          - on failure, an error {Error};
   */
  load( silent = false ) {
    return _loadSchemaFile( state.baseSchema )
      .then( schema => {
        if (!silent) {
          console.error('>>> Schema [ %s ]: loaded', state.baseSchema);
        }

        return state.ajv.compileAsync( schema );
      })
      .then( validate => {
        state.root = validate;

        if (!silent) {
          console.error('>>> Schema [ %s ]: compiled', state.baseSchema);
        }

        return validate;
      });
  },
};

/*****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Load a schema from a local file.
 *  @method _loadSchemaFile
 *  @param  path    The path to the schema file {String};
 *
 *  @return A promise for results {Promise};
 *          - on success, the contents {String};
 *          - on failure, an error {Error};
 *  @private
 */
function _loadSchemaFile( path ) {
  return new Promise( (resolve, reject) => {
    /*
    console.error('=== _loadSchemaFile( %s ): ...', path);
    // */

    let data, schema, validate;

    try {
      data = Fs.readFileSync( Path.join( state.srcDir, path ), 'utf8' );

    } catch(ex) {
      console.error('*** Cannot read [ %s ]:', path, ex);

      return reject( ex );
    }

    try {
      schema = JSON.parse( data );

    } catch(ex) {
      console.error('*** Invalid JSON for [ %s ]:', path, ex);

      return reject( ex );
    }

    return resolve( schema );
  });
}

/**
 *  Load a schema from a URI.
 *  @method _loadSchemaUri
 *  @param  uri     The URI to load {String};
 *
 *  @return A promise for results {Promise};
 *          - on success, the contents {String};
 *          - on failure, an error {Error};
 *  @private
 */
function _loadSchemaUri( uri ) {
  if (uri.startsWith( state.baseUri )) {
    return _loadSchemaFile( uri.slice( state.baseUri.length ) );
  }

  return new Promise( (resolve, reject) => {
    /*
    console.error('=== _loadSchemaUri( %s ) ...', uri);
    // */

    Request({method: 'GET', json: true, uri: uri}, function(err, res, body) {
      if (err)  { return reject( err ) }
      if (res.statusCode >= 400) {
        return reject( new Error(`Loading error: ${res.statusCode}`) );
      }

      return body;
    });
  });

  /*
  return Request.json( uri ).then( res => {
    if (res.statusCode >= 400) {
      throw new Error(`Loading error: ${res.statusCode}`);
    }

    return res.body;
  });
  // */
}
/* Private helpers }
 *****************************************************************************/
