#!/usr/bin/env python
from __future__ import print_function

import os
import sys
import json
import jsonschema


gSrcDir     = os.path.dirname( os.path.dirname( os.path.abspath( __file__ ) ) )
gBaseUri    = 'https://cmdaa.io/linux-kernel-module/kernel-beat/'
gBaseSchema = 'schema.json'

def _json_file( path ): #{
  """
  Load the identified file/path as JSON data.

  Arguments:
    path (str):
      The path to the target file

  Returns:
    The retrieved file interpreted as JSON
  """
  with open( path ) as file: #{
    return json.load( file )
  #}
#}

class LocalResolver( jsonschema.RefResolver ): #{
  """
  A $ref resolver that interprets URLs that begin with `gBaseUri` as
  local files in the `gSrcDir` directory.
  """

  def resolve_local( self, uri ): #{
    """
    Resolve a local ``uri``

    If called directly, does not check the store first, but after retrieving
    the document at the specified URI it will be saved in the store if
    :attr:`cache_remote` is True.

    Arguments:
      uri (str):
        The URI to resolve

    Returns:
      The retrieved document
    """
    path     = uri.replace( gBaseUri, '' )
    fullPath = os.path.join( gSrcDir, path )

    #print("resolve_local( %s ) ..." % (path))

    result = _json_file( fullPath )

    if self.cache_remote: #{
      self.store[ uri ] = result
    #}
  #}

  def resolve_from_url( self, url ): #{
    """
    Resolve a schema from a ``uri``

    Arguments:
      uri (str):
        The URI to resolve

    Returns:
      The retrieved document
    """
    uri, fragment = jsonschema.compat.urldefrag(url)

    #print("resolve_from_url( %s ) ..." % (url))

    try: #{
      document = self.store[ uri ]

    except KeyError: #}{
      try: #{
        if uri.startswith( gBaseUri ): #{
          # The target is relative to `baseUri` so attempt a local load
          document = self.resolve_local( uri )

        else: #}{
          # The target appears to be remote...
          document = self.resolve_remote( uri )
        #}

      except Exception as exc: #}{
        raise exceptions.RefResolutionError(exc)

      #}
    #}

    return self.resolve_fragment( document, fragment )
  #}
#}

def eprint( *args, **kwargs ): #{
  """
  print a message to stderr with no line terminator (no '\n').
  """
  #print( *args, file=sys.stderr, end='', flush=True, **kwargs )
  print( *args, file=sys.stderr, end='', **kwargs )
#}

def _validate( validator, path ): #{
  """
  Given a schema validator and path to a data file, validate all
  lines (JSON blocks) from the file.
  """
  with open( path, 'r' ) as fp: #{
    lineNum = 0
    errors  = 0

    eprint(">>> %s: ..." % (path))
    for line in iter(fp.readline, ''): #{
      lineNum += 1

      eprint("\r>>> %s: %8d " % (path, lineNum))

      try: #{
        data = json.loads( line )

      except Exception as exc: #}{
        errors += 1
        eprint("*** invalid JSON: %s\n" % (path, exc))
        continue

      #}

      #print("=== data: %s", data )

      try: #{
        validator.validate( data )

        #eprint(">>> %s is valid", path)

      except Exception as exc: #}{
        errors += 1
        #eprint("*** invalid (via schema : %s): %s\n" % (path, exc))
        eprint("*** invalid (via schema): %s\n" % (path))
        eprint( exc )

      #}
    #}

    eprint("\r>>> %s: %8d : complete ( %d errors )\n" % (path, lineNum, errors))
  #}
#}

def run( arguments, stdout = sys.stdout, stderr = sys.stderr ): #{
  """
  Run the contents of the files specified in `arguments` through a JSON schema
  validator.
  """
  #print(">>> Loading base schema [ %s ] ...", gBaseSchema)

  schema   = _json_file( os.path.join( gSrcDir, gBaseSchema ) )

  #########################################################################
  # :XXX: For the jsonschema validator, having 'required' on the top level
  #       seems to require that the properties exist for sub-objects
  #       (e.g. 'load') which is NOT correct.
  #
  #       As a work-around, remove 'required' from the top-level.
  #
  schema.pop( 'required', None )
  #########################################################################

  # Create a validator that uses our local resolver
  resolver = LocalResolver.from_schema( schema )

  validator_cls = jsonschema.validators.validator_for( schema )
  validator_cls.check_schema( schema )

  validator = validator_cls( schema, resolver=resolver )

  print(">>> Schema [ %s ]: ready" % (gBaseSchema))

  # Validate the JSON from all specified files
  for arg in arguments: #{

    _validate( validator, arg )
  #}
#}

def main( args = sys.argv[1:] ): #{
  sys.exit( run( arguments = args ) )
#}

main()
