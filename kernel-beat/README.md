[KernelBeat](../docs/KernelBeat.md) sources and utilities.

### TL;DR;

To build, load, and test the module, perform the following from this sub-directory:
``` bash
make module utils
make load
./utils/read-beat
... JSON stream of KernelBeat messages ...
^C
make unload
```

In a separate terminal, you may monitor kernel log messages using the
`./etc/monitor-dmesg` script, which is a simple helper for:
``` bash
sudo dmesg --human --follow
```

Watch for log messages prefixed with `kernel_beat`.

---

### Available build targets

<table>
 <thead>
   <tr><th>target</th><th>notes</th></tr>
 </thead>
 <tbody>
  <tr>
   <td>module</td>
   <td>Build the <code>kernel_beat.ko</code> module;</td>
  </tr>
  <tr>
   <td>utils</td>
   <td>Build utilities in the <code>utils</code> sub-directory;</td>
  </tr>
  <tr>
   <td>clean</td>
   <td>Perform a shallow cleaning of the build area from artifacts;</td>
  </tr>
  <tr>
   <td>dist-clean</td>
   <td>
    Perform a deeper cleaning, including <code>utils</code> and
    <code>tags</code>;
   </td>
  </tr>
  <tr>
   <td>load</td>
   <td>
    Build and load the <code>kernel_beat.ko</code> module
    (without installing it in the system modules area);
   </td>
  </tr>
  <tr>
   <td>unload</td>
   <td>Unload the <code>kernel_beat.ko</code> module;</td>
  </tr>
  <tr>
   <td>unload-force</td>
   <td>
    Attempt to force the unload of the <code>kernel_beat.ko</code> module;
   </td>
  </tr>
  <tr>
   <td>module_install</td>
   <td>
    Build and install the kernel_beat.ko module in the system modules area
    (<code>/lib/modules/$(uname -r)/extra/</code>) and run <code>depmod</code>
    to ensure the module is loadable via <code>modprobe</code>;
   </td>
  </tr>
  <tr>
   <td>kernel_beat.dis</td>
   <td>Generate a disassembled version of <code>kernel_beat.ko</code>;</td>
  </tr>
  <tr>
   <td>tags</td>
   <td>Generate a tags file (via <code>ctags</code>);</td>
  </tr>
  <tr>
   <td>/dev/kernel_beat</td>
   <td>
    If <code>/dev/kernel_beat</code> does not exist, create it using the major
    number determined via the <code>MAJOR_NUM</code> rule;
   </td>
  </tr>
  <tr>
   <td>status</td>
   <td>Check if the <code>kernel_beat.ko</code> module is loaded;</td>
  </tr>
  <tr>
   <td>MAJOR_NUM</td>
   <td>
    Fetch the latest major number assigned to the kernel_beat device;<br />
    <br />
    <i><b>NOTE:</b> This requires that the kernel module has been loaded and
    the loading status is in the journal</i>;
   </td>
  </tr>
  <tr>
   <td>KERN_INFO</td>
   <td>Fetch the version of the current kernel (<code>uname -r</code>);</td>
  </tr>
 </tbody>
</table>


---

### Available compile flags

<table>
 <thead>
   <tr><th>flag</th><th>notes</th></tr>
 </thead>
 <tbody>
  <tr>
   <td>USE_LKB_DIFF</td>
   <td>Report only differences in metrics;</td>
  </tr>
  <tr>
   <td>USE_CGROUP_FSREAD</td>
   <td>
    If set, allow the use of cgroup_v1_fsRead() to access cgroup data that is
    not directly available. Access is obtained via filesystem-based reads to
    <code>/sys/fs/cgroup</code>. Note that this also involves parsing the
    retrieved data, most often as key/value pairs;
   </td>
  </tr>
  <tr>
   <td>MEASURE_PERFORMANCE</td>
   <td>Include printk reports of performance from each sub-module;</td>
  </tr>
  <tr>
   <td>MONITOR_PROCESS_CREATION</td>
   <td>
    If defined, process creation will be hooked but currently no reporting is
    done;
   </td>
  </tr>
  <tr>
   <td>USE_DIE_NOTIFIER</td>
   <td>
    Use die notifications to monitor unexpected process termination. This is
    the system used by kdebug for notifications of kernel faults.
    Unfortunately, for user-level processes, this NOT triggered for exceptions
    like <code>SIGSEGV</code>, although it is triggered for *SOME* exceptions
    (e.g. <code>SIGFPE</code> / divide-by-zero);
   </td>
  </tr>
  <tr>
   <td>USE_KERNEL_SOURCE</td>
   <td>
    If set, along with <code>-I$(KERN_BUILD)</code>, enable direct inclusion of
    non-exported, source-based kernel headers. This avoids a KLUDGE needed in
    <code>src/filesystem.c</code> for access to the non-exported
    <code>struct mount</code>;<br />
    <br />
    <i><b>:NOTE:</b> This should only be enabled if your build system has been
    properly configured so the <code>$(KERN_BUILD)</code> directory contains
    full, properly prepared kernel sources along with a copy of
    <code>Module.symvers</code> for the target system</i>;<br />
    <br />
    To properly prepare kernel sources:
    <pre>
cd $(KERN_BUILD)
make oldconfig
make modules_prepare
cp (%orig%)/Module.symvers $(KERN_BUILD)
    </pre>
   </td>
  </tr>
 </tbody>
</table>
