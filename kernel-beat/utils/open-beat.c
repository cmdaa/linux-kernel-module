/**
 *  Open /dev/kernel_beat but do not read, forcing the circular buffer to be
 *  flushed as it overflows.
 *
 *  Before running this, you may want to start a udp listener and set
 *  'kernel.beat.udp_failover' to reference it:
 *    sudo sysctl -w kernel.beat.udp_failover="ip-addr:port"
 */
#include <stdio.h>
#include <stdlib.h>   // exit()
#include <fcntl.h>    // open()
#include <unistd.h>   // close(), read(), sleep()
#include <errno.h>    // errno
#include <string.h>   // strerror()
#include <signal.h>   // signal()

#define DEV_NAME  "/dev/kernel_beat"

typedef unsigned char bool;
#define FALSE (bool)0
#define TRUE  (bool)1

/**
 *  The read state.
 */
struct {
  char*   path;
  int     fd;
} s_read  = { NULL, -1 };

/**
 *  Finalize read, closing the file handle, and exiting with the given exit
 *  code.
 *  @method _readFini
 *  @param  exitCode    The exit code {int};
 *
 *  @return void
 *  @private
 */
static void
_readFini( int exitCode ) {
  if (s_read.fd >= 0) {
    close( s_read.fd );
    s_read.fd = -1;
  }

  exit( exitCode );
}

/**
 *  On SIGINT, close and exit.
 *  @method _handleSigint
 *  @param  sig   The signal {int};
 *
 *  @return void
 *  @private
 */
static void
_handleSigint( int sig ) {
  _readFini( -1 );
}

/**
 *  Initialize our read state.
 *  @method _readInit
 *  @param  path    The path to the file to read {char*};
 *
 *  @return void
 *  @private
 */
static void
_readInit( char* path ) {
  signal( SIGINT, _handleSigint );

  s_read.path = path;
  s_read.fd   = open( path, O_RDONLY);

  if (s_read.fd < 0) {
    fprintf(stderr, "*** Cannot open %s: %s\n",
            s_read.path, strerror(errno));
    _readFini( -1 );
  }
}

int
main( int argc, char** argv ) {

  _readInit( DEV_NAME );

  // Sleep until interrupted...
  while ( TRUE ) {
    sleep( 30 );
    fprintf(stdout, ".");
    fflush(stdout);
  }

  _readFini( 0 );
}
