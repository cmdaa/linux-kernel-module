/**
 *  Perform a large memory allocation, defaulting to 6G.
 *
 *  Usage: test-alloc [#bytes]
 */
#include <stdio.h>
#include <stdlib.h>     // strtoul()
#include <string.h>     // memset()
#include <sys/types.h>

int
main( int argc, char** argv ) {
  size_t  GB      = 1024LL * 1024LL * 1024LL;
  size_t  nBytes  = 3LL * GB; // 3G
  void*   data    = NULL;
  size_t  idex;

  if (argc > 1) {
    nBytes = (size_t)strtoul( argv[2], NULL, 10 );
  }

  printf(">>> Attempt to allocate %llu bytes [ %4.4f GB ] ...\n",
          nBytes, ((double)nBytes / (double)GB) );

  data = malloc( nBytes );
  printf(">>> Allocated data\n");

  printf(">>> Manually touch all bytes ...\n");
  memset( data, 0, nBytes );
  /*
  for (idex = 0; idex < nBytes; idex++) {
    fprintf(stdout, "%llu ", idex);
    if (idex % 1024 == 0) { fprintf(stdout, "."); fflush(stdout); }

    data[ idex ] = (char)(idex & 0x0ff);
  }
  fprintf(stdout, " done\n");
  // */

  printf(">>> Free data\n");
  free( data );

  return 0;
}
