/**
 *  A simple UDP server that listens for incoming messages and outputs them to
 *  the terminal.
 *
 *  Usage: udp-server [port=3333]
 *
 */
#include <stdio.h>
#include <stdlib.h>     // EXIT_FAILURE, exit(), atoi()
#include <unistd.h>     // close()
#include <string.h>     // memset()
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

typedef unsigned char bool;
#define FALSE (bool)0
#define TRUE  (bool)1

#define DEFAULT_PORT  3333
#define MAXBUF        1U << 15  // 32,768 : UDP MAX (1U << 16) = 65,536

static int  s_sockfd    = -1;
static bool s_terminate = FALSE;

/**
 *  Close the server cleanly
 */
static void
_closeSocket() {
  if (s_sockfd < 0) { return; }

  close( s_sockfd );
  s_sockfd = -1;
}

/**
 *  Handle SIGINT to close the server cleanly
 */
static void
_handleSignal( int sig ) {
  printf("*** Caught signal %d\n", sig);

  s_terminate = TRUE;
  _closeSocket();
}

int
main( int argc, char** argv ) {
  unsigned short      port          = DEFAULT_PORT;
  char                buf[ MAXBUF ];
  struct sockaddr_in  addrSrv;
  struct sockaddr_in  addrCli;
  int                 addrLen;
  int                 nRead;

  if (argc > 1) {
    // Use the command-line-provided port number
    port = atoi( argv[1] );
  }

  // Create the listening UDP socket
  if ( (s_sockfd = socket( AF_INET, SOCK_DGRAM, 0 )) < 0 ) {
    perror("socket creation failed");
    exit( EXIT_FAILURE );
  }

  memset( &addrSrv, 0, sizeof(addrSrv) );

  // Initialize server address/port information
  addrSrv.sin_family      = AF_INET;
  addrSrv.sin_port        = htons( port );
  addrSrv.sin_addr.s_addr = INADDR_ANY;

  // Bind the socket to the server address
  if ( bind( s_sockfd, (const struct sockaddr*)&addrSrv, sizeof(addrSrv)) < 0){
    perror("bind failed");
    exit( EXIT_FAILURE );
  }


  signal( SIGINT, _handleSignal );

  printf(">>> Listening on port %d ...\n", port);

  do {
    addrLen = sizeof(addrCli);
    memset( &addrCli, 0, addrLen );

    if ( (nRead = recvfrom( s_sockfd, buf, MAXBUF, MSG_WAITALL,
                            (struct sockaddr*)&addrCli, &addrLen )) < 0) {
      break;
    }

    buf[ nRead ] = '\0';

    printf("From %s: %s\n", inet_ntoa( addrCli.sin_addr ), buf);

  } while ( !s_terminate );

  _closeSocket();
  return 0;
}
