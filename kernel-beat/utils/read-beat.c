/**
 *  Open /dev/kernel_beat and perform a continuous read, either by byte-based
 *  blocks (default) or for a given number of lines (JSON blocks).
 *
 *  Usage: read-beat [-l #lines]
 */
#include <stdio.h>
#include <stdlib.h>   // exit()
#include <fcntl.h>    // open()
#include <unistd.h>   // close(), read()
#include <errno.h>    // errno
#include <string.h>   // strerror()
#include <signal.h>   // signal()
#include <sys/stat.h> // fstat()

#define DEV_NAME  "/dev/kernel_beat"

typedef unsigned char bool;
#define FALSE (bool)0
#define TRUE  (bool)1

// A single-byte buffer for byte-based, line-oriented reading.
static char s_buf1[1];

/**
 *  The read state.
 */
struct {
  char*   path;
  int     fd;
  size_t  bufSize;
  char*   buf;      //  default to using the static, single-byte `s_buf1`
} s_read  = { NULL, -1, sizeof(s_buf1), s_buf1 };

/**
 *  Finalize read, freeing any buffer, closing the file handle, and exiting
 *  with the given exit code.
 *  @method _readFini
 *  @param  exitCode    The exit code {int};
 *
 *  @return void
 *  @private
 */
static void
_readFini( int exitCode ) {
  if (s_read.buf != NULL && s_read.buf != s_buf1) {
    free( s_read.buf );
    s_read.buf = NULL;
  }

  if (s_read.fd >= 0) {
    close( s_read.fd );
    s_read.fd = -1;
  }

  exit( exitCode );
}

/**
 *  On SIGINT, close and exit.
 *  @method _handleSigint
 *  @param  sig   The signal {int};
 *
 *  @return void
 *  @private
 */
static void
_handleSigint( int sig ) {
  _readFini( -1 );
}

/**
 *  Initialize our read state.
 *  @method _readInit
 *  @param  path    The path to the file to read {char*};
 *  @param  useBuf  If truthy, allocate a read buffer {bool};
 *
 *  @return void
 *  @private
 */
static void
_readInit( char* path, bool useBuf ) {
  signal( SIGINT, _handleSigint );

  s_read.path = path;
  s_read.fd   = open( path, O_RDONLY);

  if (s_read.fd < 0) {
    fprintf(stderr, "*** Cannot open %s: %s\n",
            s_read.path, strerror(errno));
    _readFini( -1 );
  }

  if (useBuf) {
    /* Allocate a multi-byte read buffer based upon the block size of the
     * target.
     */
    struct stat statBuf = {0};

    if (fstat( s_read.fd, &statBuf) < 0) {
      fprintf(stderr, "*** fstat of %s failed: %s\n",
              s_read.path, strerror(errno));
      _readFini( -1 );
    }
    //s_read.bufSize = (size_t)statBuf.st_blksize * 32; // align with cat
    s_read.bufSize = (size_t)statBuf.st_blksize;

    s_read.buf = malloc( s_read.bufSize );
    if (s_read.buf == NULL) {
      fprintf(stderr, "*** Cannot allocate buffer [ %u bytes ]: %s\n",
              s_read.bufSize, strerror(errno));
      _readFini( -1 );
    }
  }
}

/**
 *  Read blocks at a time.
 *  @method _readBlocks
 *
 *  @return void
 *  @private
 */
static void
_readBlocks() {
  ssize_t in;

  fprintf(stderr, ">>> %u bytes per block from %s ...\n",
          s_read.bufSize, s_read.path);

  while ( (in = read( s_read.fd, s_read.buf, s_read.bufSize )) >= 0) {
    fwrite( s_read.buf, in, 1, stdout );
    fflush( stdout );
  }
}

/**
 *  Read bytes at a time to identify lines.
 *  @method _readLines
 *  @param  lineLimit   The maximum number of lines to read {size_t};
 *
 *  @return void
 *  @private
 */
static void
_readLines( size_t lineLimit ) {
  size_t  lineCnt   = 0;
  ssize_t in;

  fprintf(stderr, ">>> Reads %u lines from %s ...\n", lineLimit, s_read.path);

  // assert( s_read.bufSize == 1 );
  while ( lineCnt < lineLimit &&
          (in = read( s_read.fd, s_read.buf, s_read.bufSize )) >= 0) {

    fwrite( s_read.buf, in, 1, stdout );
    if (s_read.buf[0] == '\n') {
      // New-line
      lineCnt++;
      fflush( stdout );
    }
  }
}

int
main( int argc, char** argv ) {
  size_t  lineLimit = 0;
  ssize_t in;

  if (argc > 1) {
    if (argc > 2 && !strncmp(argv[1], "-l", 2)) {
      lineLimit = atoi( argv[2] );

    } else {
      fprintf(stderr, "*** Usage: read-beat [-l #lines]\n");
      return -1;
    }
  }

  _readInit( DEV_NAME, (lineLimit < 1) );

  if (lineLimit < 1) {
    // Read large chunks at a time ignoring any line endings.
    _readBlocks();

  } else {
    // Read a single byte at a time, flushing a line at a time.
    _readLines( lineLimit );
  }

  _readFini( 0 );
}
