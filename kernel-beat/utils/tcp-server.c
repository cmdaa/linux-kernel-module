/**
 *  A simple TCP server that listens for incoming messages and outputs them to
 *  the terminal.
 *
 *  Usage: tcp-server [port=3333]
 *
 */
#include <stdio.h>
#include <stdlib.h>     // EXIT_FAILURE, exit(), atoi()
#include <unistd.h>     // close()
#include <string.h>     // memset()
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <pthread.h>

typedef unsigned char bool;
#define FALSE (bool)0
#define TRUE  (bool)1

#define DEFAULT_PORT  3333
#define MAX_CLIENTS   128
#define MAXBUF        1U << 15  // 32,768 : UDP MAX (1U << 16) = 65,536

static int  s_sockfd    = -1;
static bool s_terminate = FALSE;

/**
 *  Close the server cleanly
 */
static void
_closeSocket() {
  if (s_sockfd < 0) { return; }

  close( s_sockfd );
  s_sockfd = -1;
}

/**
 *  Shutdown, closing the server cleanly
 */
static void
_shutdown( int exitCode ) {
  s_terminate = TRUE;
  _closeSocket();

  exit( exitCode );
}

/**
 *  Handle SIGINT to close the server cleanly
 */
static void
_handleSignal( int sig ) {
  printf("*** Caught signal %d\n", sig);

  _shutdown( sig );
}

/**
 *  Handle a client connection, reading and outputting line-by-line
 */
static void*
_clientThread( void* arg ) {
  int   client_fd   = *((int*)arg);
  char  nextChar[2] = {0};

  do {
    int   flags = 0;  // MSG_OOB, MSG_PEEK, MSG_WAITALL
    int   nRead = recv( client_fd, nextChar, 1, flags );

    if (nRead == 0) { break; }
    if (nRead <  0) {
      perror("client recv failed");
      break;
    }

    printf("%s", nextChar);

  } while ( !s_terminate );

  printf(">>> %d: DONE\n", client_fd);

  close( client_fd );

  pthread_exit( NULL );
}

int
main( int argc, char** argv ) {
  unsigned short      port          = DEFAULT_PORT;
  int                 reuseAddr     = 1;
  int                 clientCnt     = 0;
  pthread_t           clientThread[ MAX_CLIENTS ];
  struct sockaddr_in  addrSrv;

  if (argc > 1) {
    // Use the command-line-provided port number
    port = atoi( argv[1] );
  }

  // Create the listening TCP socket
  if ( (s_sockfd = socket( AF_INET, SOCK_STREAM, 0 )) < 0 ) {
    perror("socket creation failed");
    exit( EXIT_FAILURE );
  }

  // Enable address re-use
  if (setsockopt( s_sockfd, SOL_SOCKET, SO_REUSEADDR,
                  (const void*)&reuseAddr, sizeof(reuseAddr)) != 0) {
    perror("enabling address re-use failed");
  }

  memset( &addrSrv, 0, sizeof(addrSrv) );

  // Initialize server address/port information
  addrSrv.sin_family      = AF_INET;
  addrSrv.sin_port        = htons( port );
  addrSrv.sin_addr.s_addr = htonl( INADDR_ANY );

  // Bind the socket to the server address
  if ( bind( s_sockfd, (const struct sockaddr*)&addrSrv, sizeof(addrSrv)) < 0){
    perror("bind failed");
    _shutdown( EXIT_FAILURE );
  }

  if ( listen( s_sockfd, MAX_CLIENTS )) {
    perror("listen failed");
    _shutdown( EXIT_FAILURE );
  }

  signal( SIGINT, _handleSignal );

  printf(">>> Listening on TCP port %d ...\n", port);

  do {
    struct sockaddr_in  addrCli;
    int                 addrLen     = sizeof( addrCli );
    int                 client_fd;

    memset( &addrCli, 0, addrLen );

    if ( (client_fd = accept( s_sockfd,
                              (struct sockaddr*)&addrCli, &addrLen )) < 0) {
      perror("cannot establish new connection");
      continue;
      /*
      _shutdown( EXIT_FAILURE );
      // */
    }

    printf(">>> %d: TCP connection from %s ...\n",
            client_fd, inet_ntoa( addrCli.sin_addr ));


    if (pthread_create( &clientThread[clientCnt], NULL,
                        _clientThread, &client_fd ) != 0 ) {

        perror("failed to create thread");
    }

    if (clientCnt >= MAX_CLIENTS) {
      // Wait for all current clients to complete
      int idex;
      for (idex = 0; idex < clientCnt; idex++) {
        pthread_join( clientThread[idex], NULL );
      }

      clientCnt = 0;
    }

  } while ( !s_terminate );

  _closeSocket();
  return 0;
}
