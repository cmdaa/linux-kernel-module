/**
 *  A simple TCP client that connects to the given server/port and sends a
 *  simple "Hello" message.
 *
 *  Usage: tcp-client [server[:port]=127.0.0.1:3333]
 *
 */
#include <stdio.h>
#include <stdlib.h>     // EXIT_FAILURE, exit(), atoi()
#include <unistd.h>     // close()
#include <string.h>     // memset()
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>

#define DEFAULT_ADDR    "127.0.0.1"
#define DEFAULT_PORT    3333

static int  s_sockfd    = -1;

/**
 *  Close the client cleanly
 */
static void
_closeSocket() {
  if (s_sockfd < 0) { return; }

  close( s_sockfd );
  s_sockfd = -1;
}

/**
 *  Shutdown, closing the server cleanly
 */
static void
_shutdown( int exitCode ) {
  _closeSocket();

  exit( exitCode );
}

/**
 *  Handle SIGINT to close the server cleanly
 */
static void
_handleSignal( int sig ) {
  printf("*** Caught signal %d\n", sig);

  _shutdown( sig );
}

int
main( int argc, char** argv ) {
  char*               servIp        = DEFAULT_ADDR;
  unsigned short      servPort      = DEFAULT_PORT;
  char*               buf           = "{\"greeting\":\"Hello\"}\n";
  struct sockaddr_in  servAddr;
  ssize_t             nSend;

  if (argc > 1) {
    // Use the command-line-provided server/port
    char* colon = strchr( argv[1], ':' );
    if (colon) {
      // Parse the port
      *colon   = '\0';
      servPort = atoi( colon + 1 );

    }

    // Parse the address
    servIp = argv[1];
  }

  // Create the TCP socket
  if ( (s_sockfd = socket( AF_INET, SOCK_STREAM, 0 )) < 0 ) {
    perror("socket creation failed");
    exit( EXIT_FAILURE );
  }

  signal( SIGINT, _handleSignal );

  // Initialize server address/port information
  memset( &servAddr, 0, sizeof(servAddr) );
  servAddr.sin_family      = AF_INET;
  servAddr.sin_port        = htons( servPort );
  servAddr.sin_addr.s_addr = inet_addr( servIp );

  printf(">>> Connecting to TCP %s:%u ...\n", servIp, servPort);
  if (connect( s_sockfd, (struct sockaddr*)&servAddr, sizeof(servAddr) ) != 0) {
    perror("connect failed");
    _shutdown( EXIT_FAILURE );
  }

  nSend = write( s_sockfd, buf, strlen(buf) );
  if (nSend > 0) {
    printf(">>> Sent %d bytes to %s:%u\n", nSend, servIp, servPort);

  } else {
    printf("*** Send to %s:%u FAILED: %s\n",
           servIp, servPort, strerror( errno ));
  }

  _closeSocket();
  return 0;
}
