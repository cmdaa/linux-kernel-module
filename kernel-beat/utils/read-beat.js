#!/usr/bin/env node
const Fs    = require('fs');

const path  = 'kernelbeat:///dev/kernel_beat';
const url   = new URL( path );

poll( url );

/**
 *  Open a file handler, read all current data and then poll for more.
 *  @method poll
 *  @param  url   The target url {String};
 *
 *  @return void
 */
function poll( url ) {
  console.log(">>> Opening poll for '%s' ...", url.pathname);
  Fs.open( url.pathname, 'r', (err, fd) => {
    if (err) {
      console.log('*** Error:', err);
      return;
    }

    let state = {
      fd    : fd,
      buf   : Buffer.alloc(4096),
      parts : [],
    };

    read_poll( state );
  });
}

/**
 *  Given a file handle, buffer, and pending parts, gather data line-by-line
 *  polling when there is no more data.
 *  @method read_poll
 *  @param  state         Processing state {Object};
 *  @param  state.fd      The file descriptor {Number};
 *  @param  state.buf     The current read buffer {Buffer};
 *  @param  state.parts   The pending read buffers {Array};
 *
 *  @return void
 */
function read_poll( state ) {
  Fs.read( state.fd, state.buf, 0, state.buf.length, null,
            (err, inBytes, inBuf) => {

    if (err) {
      console.log('*** ERROR:', err);
      Fs.close( state.fd );
      return;
    }

    if (inBytes < 1) {
      // EOF -- wait and then try again
      console.log('--- eof');
      return setTimeout( () => read_poll( state ), 1000 );
    }

    console.log('>>> Read %d/%d bytes ...', inBytes, inBuf.length);

    /* Split out any lines from this buffer, combined with any current pending
     * parts.
     */
    let offset = 0;
    let eol;
    while ( offset < inBytes &&
            (eol = inBuf.indexOf( 0xa, offset )) > offset ) {

      let part  = inBuf.slice( offset, eol );
      console.log('--- eol @%d [ %d ]', eol, part.length);

      state.parts.push( part );

      let msg = Buffer.concat( state.parts );

      console.log('line[ %d : %s ]', msg.length, msg);

      state.parts.length = 0;
      offset       = eol + 1;
    }

    if (offset < inBytes) {
      // Save the unprocessed end of this buffer as a pending part
      console.log('--- Push awaiting eol [ %d parts ] ...',
                  state.parts.length + 1);

      state.parts.push( Buffer.from( inBuf.slice( offset ) ) );
    }

    // Zero the current read buffer and read again
    state.buf.fill(0);
    return read_poll( state );
  });
}

/* :XXX: Reading as a stream doesn't work well in this case since upon reaching
 *       the current EOF, the stream will emit 'end' and essentially close the
 *       stream.  This will result in the character device being closed
 *       resulting in a state reset within KernelBeat.
 *
 *       To properly read and follow, we MUST keep the file handle open and
 *       simply wait for new data to arrive.
 */
function stream( url ) {
  const opts    = {
    flags     : 'r',
    encoding  : 'utf-8',
    autoClose : true,
    emitClose : true,
  };

  console.log(">>> Opening read stream for '%s' ...", url.pathname);
  var st = Fs.createReadStream( url.pathname, opts );
  st.on('error',   err => { console.log('*** Error:', err) } )
    .on('close',   ()  => { console.log('*** Closed') } )
    .on('end',     ()  => { console.log('*** End') } )
    .on('pause',   ()  => { console.log('=== pause') } )
    .on('resume',  ()  => { console.log('=== resume') } )
    .on('data',    buf => {
      console.log('=== data[ %s ]', buf);
    } )
  /*
    .on('readable',()  => {
      console.log('=== readable');
    
      let data;
      while (data = this.read) {
        console.log('=== readable: data[ %s ]', data);
      }
    } );
  // */

  setTimeout( () => { st.close() }, 5000 );
}
