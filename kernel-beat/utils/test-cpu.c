/**
 *  Spin, using a large amoung of cpu.
 *
 *  Usage: test-cpu [seconds]
 */
#include <stdio.h>
#include <stdlib.h>     // strtoul()
#include <sys/time.h>   // gettimeofday()
#include <sys/types.h>

int
main( int argc, char** argv ) {
  time_t          seconds = 10;
  time_t          elapsed = 0;
  time_t          last    = 0;
  struct timeval  start   = {0};
  struct timezone zone    = {0};

  gettimeofday( &start, &zone );

  if (argc > 1) {
    seconds = (time_t)strtoul( argv[1], NULL, 10 );
  }

  fprintf( stdout, ">>> Start @ %u.%u for %u seconds ...",
           start.tv_sec, start.tv_usec, seconds);
  fflush( stdout );
  while (elapsed < seconds) {
    struct timeval  time;
    gettimeofday( &time, &zone );

    elapsed = time.tv_sec - start.tv_sec;
    if (elapsed != last) {
      fprintf( stdout, "." );
      fflush(  stdout );
    }
    last = elapsed;
  }
  fprintf( stdout, " done\n" );

  return 0;
}
