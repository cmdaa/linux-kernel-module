/**
 *  Exit with a non-zero code.
 *
 */
#include <stdlib.h>

int
main( int argc, char** argv ) {
  exit( 42 );
}
