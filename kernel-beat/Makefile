###############################################################################
# Available targets:
#    module             Build the `kernel_beat.ko` module;
#    utils              Build utilities in the `utils` sub-directory;
#    clean              Perform a shallow cleaning of the build area from
#                       artifacts;
#    dist-clean         Perform a deeper cleaning, including `utils` and
#                       `tags`;
#
#    load               Build and load the `kernel_beat.ko` module
#                       (without installing it in the system modules area);
#    unload             Unload the `kernel_beat.ko` module;
#    unload-force       Attempt to force the unload of the `kernel_beat.ko`
#                       module;
#
#    module_install     Build and install the kernel_beat.ko module in the
#                       system modules area (/lib/modules/$(uname -r)/extra/)
#                       and run `depmod` to ensure the module is loadable via
#                       `modprobe`;
#
#    module_uninstall   Unload the module and remove it from the system modules
#                       area (/lib/moculdes/$(uname -r)/extra/) and run
#                       `depmod` to ensure the module is no longer included;
#
#    kernel_beat.dis    Generate a disassembled version of `kernel_beat.ko`;
#
#    tags               Generate a tags file (via ctags);
#
#    /dev/kernel_beat   If `/dev/kernel_beat` does not exist, create it
#                       using the major number determined via the `MAJOR_NUM`
#                       rule;
#                       :NOTE: The device SHOULD BE automatically created when
#                              the module successfully loads. If it isn't,
#                              there's something wrong...
#
#    status             Check if the `kernel_beat.ko` module is loaded;
#
#    MAJOR_NUM          Fetch the latest major number assigned to the
#                       kernel_beat device
#                       (NOTE: This requires that the kernel module has been
#                              loaded and the loading status is in the
#                              journal);
#
#    KERN_INFO          Fetch the version of the current kernel (uname -r);
#
###############################################################################
# Available compile flags:
#
#    USE_LKB_DIFF             Report only differences in metrics;
#
#    USE_CGROUP_FSREAD        If set, allow the use of cgroup_v1_fsRead() to
#                             access cgroup data that is not directly
#                             available. Access is obtained via
#                             filesystem-based reads to '/sys/fs/cgroup'. Note
#                             that this also involves parsing the retrieved
#                             data, most often as key/value pairs;
#
#
#    MEASURE_PERFORMANCE      Include printk reports of performance from each
#                             sub-module;
#
#    MONITOR_PROCESS_CREATION If defined, process creation will be hooked but
#                             currently no reporting is done;
#
#    USE_DIE_NOTIFIER         Use die notifications to monitor unexpected
#                             process termination. This is the system used by
#                             kdebug for notifications of kernel faults.
#                             Unfortunately, for user-level processes, this NOT
#                             triggered for exceptions like SIGSEGV, although
#                             it is triggered for SOME exceptions
#                             (e.g. SIGFPE / divide-by-zero);
#
#    USE_KERNEL_SOURCE        If set, along with `-I$(KERN_BUILD)`, enable
#                             direct inclusion of non-exported, source-based
#                             kernel headers. This avoids a KLUDGE needed in
#                             `src/filesystem.c` for access to the non-exported
#                             `struct mount`;
#
#                             :NOTE: This should only be enabled if your build
#                                    system has been properly configured so the
#                                    $(KERN_BUILD) directory contains full,
#                                    properly prepared kernel sources along
#                                    with a copy of `Module.symvers` for the
#                                    target system;
#
#                                    To properly prepare kernel sources:
#                                      cd $(KERN_BUILD)
#                                      make oldconfig
#                                      make modules_prepare
#                                      cp (%orig%)/Module.symvers \
#                                         $(KERN_BUILD)
#
#CFLAGS_MODULE:= -DUSE_LKB_DIFF -DUSE_CGROUP_FSREAD -DMEASURE_PERFORMANCE
CFLAGS_MODULE:= -DUSE_LKB_DIFF -DUSE_CGROUP_FSREAD

KERN_VERSION := $(shell uname -r)

###
# Debian        : /lib/modules/$(KERN_VERSION)/build
# Redhat/Centos : /usr/src/kernels/$(KERN_VERSION)
#
# :NOTE: For Redhat/Centos, KERN_VERSION used for builds is quite often NOT the
#        same as what is reported by `uname -r`, particularly from within a
#        docker environment.
#
KERN_BUILD       := /lib/modules/$(KERN_VERSION)/build

obj-m += kernel_beat.o

cgroup_objs      := ./src/modules/cgroup_v1.o \
                      ./src/modules/cgroup_v1/blkio.o \
                      ./src/modules/cgroup_v1/cpu.o \
                      ./src/modules/cgroup_v1/cpuacct.o \
                      ./src/modules/cgroup_v1/cpuset.o \
                      ./src/modules/cgroup_v1/freezer.o \
                      ./src/modules/cgroup_v1/memory.o \
                      ./src/modules/cgroup_v1/net_cls.o \
                      ./src/modules/cgroup_v1/net_prio.o \
                      ./src/modules/cgroup_v1/pids.o

module_objs      := ./src/modules/cpu.o \
                    ./src/modules/mem.o \
                    ./src/modules/load.o \
                    ./src/modules/uptime.o \
                    ./src/modules/proc.o \
                    ./src/modules/filesystem.o  \
                    ./src/modules/net.o \
                    ./src/modules/proc_mon.o  \
                    $(cgroup_objs)

kernel_beat-objs := ./src/lkb.o \
                    ./src/map.o \
                    ./src/json.o \
                    ./src/tcp.o \
                    ./src/kernel_beat.o \
                    $(module_objs)

#                   ./src/udp.o \

all: module kernel_beat.dis tags

# Create the kernel_beat.ko module
module: src/kernel_beat.c
	@make -C $(KERN_BUILD) M=$(PWD) modules

module_install: src/kernel_beat.c
	@sudo make -C $(KERN_BUILD) M=$(PWD) modules_install
	@sudo cp module-load.conf /etc/modules-load.d/kernel_beat.conf
	@sudo depmod
	@sudo modprobe kernel_beat

module_uninstall: unload-force
	@sudo rm -f /lib/modules/$(KERN_VERSION)/extra/kernel_beat.ko
	@sudo rm -f /etc/modules-load.d/kernel_beat.conf
	@sudo depmod

# Generate a disassembled version of the kernel_beat.ko module
kernel_beat.dis: kernel_beat.ko
	@echo "  OBJDUMP $@"
	@objdump -dSlr $< > $@

utils: .FORCE
	@make -C utils

tags: .FORCE
	@ctags -a --extra=+fq --c-kinds=+px --fields=+iaS --langmap=c:+.h \
		--exclude='.*.o.cmd' --recurse=yes src/

#		src/*.[ch] src/modules/*.[ch] src/modules/cgroup_v1/*.[ch]

version:
	@awk '/MODULE_VERSION/{print substr($$0, 17, length($$0)-19);}' src/kernel_beat.c

clean:
	@rm -f kernel_beat.dis
	@make -C $(KERN_BUILD) M=$(PWD) clean

dist-clean: clean
	@make -C utils clean
	@rm -f tags


################################################################################
/dev/kernel_beat:
	@sudo mknod /dev/kernel_beat c $(shell make -s MAJOR_NUM) 0

load:
	@sudo insmod kernel_beat.ko

unload:
	@sudo rmmod kernel_beat.ko

unload-force:
	@sudo rmmod -f kernel_beat.ko

status:
	@lsmod | grep 'kernel_beat'

# Fetch the latest major number assigned to the kernel_beat device
MAJOR_NUM:
	@sudo dmesg | awk 'BEGIN{num=-1} /kernel_beat loaded/{num=$$NF} END{print num}'

KERN_INFO:
	@echo "KERN_VERSION: $(KERN_VERSION)"

.FORCE:
