Researching the use of a linux kernel module for access to metrics similar to
what MetricBeat and AuditBeat provide while providing additional capabilities.

# Goals

- simplify the data flow while providing access to capabilities not provided by
  [MetricBeat](docs/MetricBeat.md) and AuditBeat;
- gain tracking of process creation and exit with summary stats on process
  termination possibly using [frtrace](docs/ftrace.md);


# Development Environment

Developing a linux kernel module requires either bare metal or a virtual
machine. For ease of development, we'll choose the virtual machine route.


## Build helpers

There are several helper scripts in [etc/](etc/) to aid building kernel_beat in
various environments:
- [etc/build.sh](etc/build.sh) : given a docker image
  (e.g. `centos:7`, `debian:9-slim`, `ubuntu:16.04`) identify the base os,
  start a container using the specified image and run the os-specific build
  script within that container;
- [etc/build-local.sh](etc/build-local.sh) : attempt to identify the base os of
  the local system and if possible, run the os-specific build script locally;
- [etc/chroot-build.sh](etc/chroot-build.sh) : perform a build within a chroot
  environment that attempts to mirror the system on which it is run. This
  script makes use of `etc/build-local.sh`. It may be used directly on a host
  system to allow a build without littering the system with unwanted packages:
  ```bash
  #
  # Generated build artifacts will be place in './builds'
  #
  sudo ./etc/chroot-build.sh
  ```
  It may also be from run within a container, if the container is given
  read-only access to the host's root filesystem. An appropriate container
  image may be built using `make builder` and may be run via:
  ```bash
  #
  # Run the chroot-build.sh script within a docker image placing the generated
  # build artifacts into '$(pwd)/builds'
  #
  # :NOTE: This is codified in etc/run-builder.sh
  #
  docker run --rm -it \
    --privileged \
    --volume /:/mnt/root:ro \
    --volume "$(pwd)/builds:/root/linux-kernel-module/builds" \
      cmdaa/kernel_beat-build:$(make -sC kernel-beat version)
  ```
- [etc/kernelbeat-init.sh](etc/kernelbeat-init.sh) : initialize the use of
  kernelbeat on the host:
  - accepts several optional environment variables:
    - `KB_VERSION`      Optional kernel-beat version number if it differs from
                        what is reported via: `make -C kernel-beat version`;
    - `KB_REPO_HOST`    Optional host name   for the kernel-beat repository
                          (if not provided, pull/push will be disabled);
    - `KB_REPO_PORT`    Optional port number for the kernel-beat repository
                          (if not provided, pull/push will be disabled);
    - `KB_REPO_TRIES`   Optional repository pull/push retry count [2];
    - `KB_REPO_TIMEOUT` Optional repository pull/push timeout (seconds) [30];
  - identify the minimal kernel version of the host (`KERN_VERSION`);
  - if a kernel-beat repository is identified (`KB_REPO_HOST` and `KB_REPO_PORT`),
    attempt to retrieve (HTTP/GET) the target version (`KB_VERSION`) for the
    identified minimal kernel version;
  - if the target version is not available, perform a chroot-based build (via
    `etc/chroot-build.sh`) and, if the kernel-beat repository is identifed
    attempt to push (HTTP/POST) the new version to the repository;
  - (re)load the `kernel_beat` module into the host kernel;

OS-specific build scripts:
- [etc/build-centos.sh](etc/build-centos.sh) : run within a Centos environment,
  install required build packages, build the kernel_beat module, place it
  within the [builds/](builds/) area, and clean up;
- [etc/build-debian.sh](etc/build-debian.sh) : run within a Debian environment,
  install required build packages, build the kernel_beat module, place it
  within the [builds/](builds/) area, and clean up;
- [etc/build-ubuntu.sh](etc/build-ubuntu.sh) : run within a Ubuntu environment,
  install required build packages, build the kernel_beat module, place it
  within the [builds/](builds/) area, and clean up;


## VirtualBox setup

Start by preparing networking.

- Create a NAT Network to allow access *out* as well to any other virtual
  machine on the same NAT network. This is done via `Preferences > Network`:
  > Test => 10.0.2.0/24, enable DHCP

- Create a Host Network adapter to allow direct access to the virtual machine
  from the host. This is done via `Network > Create`:
  > Host => 192.168.56.1/24, enable DHCP


**NOTE**: If the host network is added AFTER OS install, you will need to
          manually update the network configuration as follows:
``` bash
ip link     # => enp0s3 (NAT network), enp0s8 (Host network)
ip address  # => enp0s3 (10.0.2.5/24), enp0s8 (no address)
cat >> /etc/network/interfaces <<EOF

# The secondary (host) network interface
allow-hotplug enp0s8
iface enp0s8 inet dhcp
EOF

ifup enp0s8
```

## Virtual machine install

### Debian 9 (net install)

For ease of access to kernel sources we make use of Debian 9 and to reduce the
initial download, we chose the `net install` version.

Boot the machine with the ISO image attached as a CD and walk through the
installation process.

After Debian is installed:
``` bash
# Login as root
apt-get install sudo vim git ntp exuberant-ctags
usermod -aG sudo %user%

# Setup NTP to sync to the same source as the host
cat > /etc/ntp.conf <<EOF
server win-dc-01.infosec.tycho.ncsc.mil
EOF

systemctl restart ntp

# Setup timedatectl to sync to the same source as the host
#   win-dc-01.infosec.tycho.ncsc.mil
vim /etc/systemd/timesyncd.conf
[Time]
NTP=win-dc-01.infosec.tycho.ncsc.mil
:wq

timedatectl set-ntp true
timedatectl status

# Logout and back in as %user%
#
# Install the kernel module build environment
sudo apt-get install build-essential linux-headers-$(uname -r)

# Module build environment:
#   => /lib/modules/$(uname -r)/build
#
# Module Makefile (:NOTE: Must be 'Makefile')
#
#  obj-m   += lkm_example.o
#  KERN_SRC = /lib/modules/$(shell uname -r)/build
#  
#  all:
#  	make -C $(KERN_SRC) M=$(PWD) modules
#  
#  clean:
#  	make -C $(KERN_SRC) M=$(PWD) clean

# To install full kernel sources for reference (not needed to build modules)
sudo apt-get install linux-source

# Sources are in /usr/src/linux-source-*.tar.xz
#   Unbundle using --xz
tar -xv --xz -f /usr/src/linux-source-4.9.tar.xz 
```

Now, if you'd like, you can generate a ctags file for the kernel sources to
allow easier code navigation:
``` bash
cd linux-source-4.9
sudo make tags
```

**:NOTE:** In order to use kernel includes within a module (e.g. `#include
<fs/mount.h>` in `filesystem.c`), the full kernel sources must replace the
include-only sources linked to from `/lib/modules/$(uname -r)/build`.

For Debian, `build` is typically a soft-link to a header-only directory
(`/usr/src/linux-headers-$(uname -r)`).

For CentOS, `build` is typically a soft-link to a heard-only version of the
full kernel sources (`/usr/src/kernels/$(uname -r)`).

In either case, you may either change the soft-link or place the full kernel
sources in the target directory but you will need to manually copy over the
pre-built `Module.symvers`.

Once the source are in place, you can prepare them for use via:
``` bash
cd /lib/modules/$(uname -r)/build
make oldconfig
make modules_prepare

# :NOTE: `Module.symvers` will NOT be built. It will need to be copied from
#        the old headers area.
```

### CentOS 7 notes

Install into a VM with the following properties:
- 8G RAM
- 2 CPU
- 16M Graphics memory
- 25G VDI dynamic disk
- Network adapters:
  - 1 : NAT Network (Test)
  - 2 : Host-only network

Make sure you configure networking here, otherwise you'll have to do it
manually once the system is installed:
- enp0s3 (NAT network); ON (should automatically set DHCP for IPv4);
- enp0s8 (Host network); ON (should automatically set DHCP for IPv4);

Add your user with additional groups: wheel, sudo

After CentOS is installed:
``` bash
# Login as root
#
# Update
yum update -y

# Install personal development packages
yum install -y vim-enhanced git ctags-etags

# Install packages for installing the Virtual Box additions
#   perl gcc dkms kernel-devel kernel-headers make bzip2
#
yum install -y perl gcc dkms kernel-devel kernel-headers make bzip2

# Reboot
shutdown -r now

# Login as root
# Insert the Virtual Box additions CD, mount and install (FAILS!!!)
mount /dev/cdrom /mnt
cd /mnt
./VBoxLinuxAdditions.run --nox11

# Add source mount to /etc/fstab
cat <<EOF >> /etc/fstab
cmdaa   /home/depeele/src/cmdaa vboxsf  noauto,rw,nodev,uid=31734,gid=2162 0 0
EOF

# Make sure we can mount the sources
mount /home/depeele/src/cmdaa
```

#### Networking

Even setting the Host network adapter to ON does not seem to properly establish
the second network.

You'll need to manually configure it now with something like:
``` bash
ip link    # => enp0s3 (NAT network), enp0s8 (Host network)
ip address # => enp0s3 (no address),  enp0s8 (no address)

# Update ifcfg scripts to change:
#   ONBOOT=no   => ONBOOT=yes
#                  DHCP=yes
# /etc/sysconfig/network-scripts/ifcfg-enp0s3
# /etc/sysconfig/network-scripts/ifcfg-enp0s8

# Restart the interfaces
ifdown enp0s3 && ifup enp0s3
ifdown enp0s8 && ifup enp0s8

# Now, ip address should report something like:
ip address # => enp0s3 (10.0.2.6/24), enp0s8 (192.168.56.102/24)
```

#### Kernel build tools

``` bash
# Development packages for building kernel modules
yum install -y kernel-devel kernel-headers

# Kernel build  : /lib/modules/3.10.0-957.el7.x86_64
# Kernel sources: /usr/src/kernels/3.10.0-957.5.1.el7.x86_64

# Set the proper soft-links
cd /usr/src/kernels
ln -s 3.10.0-957.5.1.el7.x86_64 3.10.0-957.el7.x86_64
```

#### Kernel sources

CentOS 7 provides no easy access to kernel sources.

You'll need to install a large number of tools in order to run the `rpmbuild`
process required to gain access to the sources.

``` bash
# Install kernel build tools
yum install -y \
	asciidoc audit-libs-devel bash binutils binutils-devel bison bzip2 \
	diffutils elfutils-devel elfutils-libelf-devel findutils flex \
	gawk gcc gnupg gzip hmaccalc m4 make module-init-tools \
	net-tools newt-devel patch patchutils \
	perl perl-ExtUtils-Embed python python-devel \
	redhat-rpm-config rpm-build sh-utils tar xmlto zlib-devel
yum install -y \
	bc gettext java-devel ncurses-devel numactl-devel openssl \
	pciutils-devel pesign python-docutils
yum install -y rpm-build
# CentOS Linux release 7.6.1810 (Core) 
VERS=$(awk '/CentOS/ {print $4}' /etc/centos-release)
KVER=$(uname -r)

# Fetch the source RPM > ~/rpmbuild
rpm -i http://vault.centos.org/${VERS}/updates/Source/SPackages/kernel-3.10.0-957.1.3.el7.src.rpm

#rpm -i http://vault.centos.org/${VERS}/updates/Source/SPackages/kernel-3.10.0-957.5.1.el7.src.rpm

# Build the RPM sources to access the kernel sources
cd ~/rpmbuild/SPECS
rpmbuild -bp --target=$(uname -m) kernel.spec

# Sources in ~/rpmbuild/BUILD/kernel*/linux*/
cd ~/rpmbuild/BUILD/kernel*/linux*/
KERN_SRC=$(pwd)

make modules_prepare

#make M=relative/path/to/module/ modules #modules_install

# Copy sources to their expected location
cd ~/rpmbuild/BUILD/kernel-3.10.0-957.1.3.el7/linux-3.10.0-957.5.1.el7.x86_64
mkdir -p /usr/src/kernels/3.10.0-957.5.1.el7.x86_64

tar cbf 1024 - . | \
    (cd /usr/src/kernels/3.10.0-957.5.1.el7.x86_64 tar xvbf 1024 -)

cd /usr/src/kernels/3.10.0-957.1.3.el7.x86_64
make modules_prepare
```

### CentOS 8 notes

Install into a VM with the following properties:
- 8G RAM
- 2 CPU
- 16M Graphics memory
- 25G VDI dynamic disk
- Network adapters:
  - 1 : NAT Network (Test)
  - 2 : Host-only network

Make sure you configure networking here, otherwise you'll have to do it
manually once the system is installed:
- enp0s3 (NAT network); ON (should automatically set DHCP for IPv4);
- enp0s8 (Host network); ON (should automatically set DHCP for IPv4);

Download the CentOS 8 ISO (e.g. http://mirror.umd.edu/centos/8.3.2011/isos/x86_64/CentOS-8.3.2011-x86_64-boot.iso)

Set the CentOS 8 ISO image as the boot source, boot and install.

This will require several steps:
- turn ON the network;
- select the install source (e.g. http://mirror.centos.org/centos/8/BaseOS/x86_64/os/);
- select the install type (Base Environment: Custom, no additional software);
- set the root password;
- add your development user with admin privileges;

After CentOS is installed:
``` bash
# Login as root
#
# Determine the ip address (for ssh connectivity)
ip address

# Install personal development packages
dnf install -y vim tar git

# Install kernel development packages
dnf install -y \
      perl gcc kernel-devel kernel-headers make bzip2 elfutils-libelf-devel

# /lib/modules should have a `uname -r` sub-directory for module building
ls -l /lib/modules

# Optionally setup your user to sudo without a password
# from /etc/sudoers, uncomment:
# %wheel        ALL=(ALL)       NOPASSWD: ALL
# and comment the previous %wheel entry

# Logout from root
#
# Setup your local ssh with an entry for the VM using the ip address determined
# after logging in as root for the first time.
#     ~/.ssh/config
#     Host centos8
#       Hostname: 192.168.0.179
#
# ssh to setup your user's environment
#
ssh centos8
mkdir -m 700 .ssh

# scp over your ssh keys, bash, vim, and git dot files
# Setup to allow ssh (assuming your primary keys are 'id_rsa' & 'id_rsa.pub':
cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys

###
# Pull kernel-beat sources
#   (assuming gitlab has been seeded with your 'id_rsa.pub' ssh key).
#
mkdir -p ~/src/cmdaa
cd ~/src/cmdaa
git clone git@gitlab.com:cmdaa/linux-kernel-module.git

cd linux-kernel-module/kernel-beat
make

###
# If you need to kernel sources
#
OS_VERS=$(awk '/CentOS/ {print $4}' /etc/centos-release)
LK_VERS=$(uname -r | sed 's/.x86_64//')
#
#rpm -i http://vault.centos.org/${OS_VERS}/updates/Source/SPackages/kernel-${LK_VERS}.src.rpm
#   :NOT FOUND:
rpm -i https://vault.centos.org/8.3.2011/BaseOS/Source/SPackages/kernel-4.18.0-193.19.1.el8_2.src.rpm

# Need rpmbuild and kernel build packages
sudo dnf install -y \
  rpm-build \
  asciidoc \
	audit-libs-devel \
	bc \
	binutils-devel \
	bison \
	elfutils-devel \
	flex \
	kabi-dw \
	libcap-devel \
	libcap-ng-devel \
	llvm-toolset \
	ncurses-devel \
	nss-tools \
	numactl-devel \
	openssl-devel \
	pciutils-devel \
	perl-generators \
	pesign \
	python3-devel \
	python3-docutils \
	rsync

	#dwarves  (NOT FOUND) -- comment out of kernel.spec

# Build the RPM sources to access the kernel sources
cd ~/rpmbuild/SPECS
rpmbuild -bp --target=$(uname -m) --without=perf --without=doc kernel.spec

# Sources in ~/rpmbuild/BUILD/kernel*/linux*/
cd ~/rpmbuild/BUILD/kernel*/linux*/

#make modules_prepare
#
## Copy sources to their expected location
#cd ~/rpmbuild/BUILD/kernel-3.10.0-957.1.3.el7/linux-3.10.0-957.5.1.el7.x86_64
#mkdir -p /usr/src/kernels/3.10.0-957.5.1.el7.x86_64

###
# Access the older kernel version
#
rpm -i http://vault.centos.org/7.6.1810/updates/Source/SPackages/kernel-3.10.0-957.1.3.el7.src.rpm

cd ~/rpmbuild/SPECS
# Update kernel.spec to specify python
#   %global __python /usr/bin/python3
#
#   #BuildRequires: python-devel, newt-devel, perl(ExtUtils::Embed)
#   BuildRequires: newt-devel, perl(ExtUtils::Embed)

rpmbuild -bp --target=$(uname -m) \
  --without=perf --without=doc --without=bpftool \
    kernel.spec


sudo dnf install -y newt-devel platform-python-devel xmlto


```
