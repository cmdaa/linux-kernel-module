#!/bin/bash
#
# Run within a debian system to build kernel_beat.
#
# Usage: build-debian.sh
#
#
# To run within a debian docker image:
#
#   docker run --rm -i \
#     --volume  $(pwd):/root/linux-kernel-module \
#     --workdir /root/linux-kernel-module \
#       debian:9-slim \
#         ./etc/build-debian.sh
#
set -e

 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")

#
# Determine if this build is running within a docker container.
# (If so, we cannot rely on 'uname' to determine the target OS/kernel).
#
DOCKER_CGROUPS=( $(cat /proc/1/cgroup 2>/dev/null | awk '/docker\//') )
IN_DOCKER=${#DOCKER_CGROUPS[@]}

if [[ $IN_DOCKER -eq 0 ]]; then
  echo ">>> Native Debian build"
else
  echo ">>> Docker-based Debian build"
fi


##########################################################
# Ensure the required packages are installed
#
REQUIRED_PKGS=( 'build-essential' 'linux-headers-amd64' )

PKG_LIST="$(apt list --installed 2>/dev/null)"

# Determine which packages needs to be installed
INSTALL_PKGS=()
for PKG in ${REQUIRED_PKGS[@]}; do
  HAS_PKG="$(echo "$PKG_LIST" | awk '/^'$PKG'/')"
  if [[ -z "$HAS_PKG" ]]; then
    INSTALL_PKGS=( ${INSTALL_PKGS[@]} "$PKG" )
  fi
done

if [[ ${#INSTALL_PKGS[@]} -gt 0 ]]; then
  echo ">>> Install packages [ ${INSTALL_PKGS[@]} ]"
  echo ">>>   :NOTE: If is done within a chroot build and fails,"
  echo ">>>          the best bet is to install the required"
  echo ">>>          packages directly on the host node and run"
  echo ">>>          the build script again."
  apt-get install -y ${INSTALL_PKGS[@]}
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echo "*** Package install(s) failed, this build will likely fail as well"
  fi
fi

##########################################################
# Identify the build environment
#
KB_SRC="${ROOT}/kernel-beat"
KB_VERSION="$(make -sC $KB_SRC version)"

KERN_BUILD_DIR="/lib/modules"

if [[ $IN_DOCKER -eq 0 ]]; then
  #
  # Determine the target kernel version from `uname -r`
  #
  KERN_VERSION=$(uname -r)
else
  #
  # Determine the target kernel version from the newest directory in the kernel
  # build area.
  #
  KERN_VERSION=$(ls -t1 $KERN_BUILD_DIR | head -1)
fi

# :XXX: ASSUME the base system and architecture are proper reported even within
#       docker
KERN_NAME="$(uname -s | tr [:upper:] [:lower:])"
KERN_ARCH="$(uname -m | tr [:upper:] [:lower:])"


OS_NAME="$(awk -F= '/^ID=/{print $2}' /etc/os-release | \
                  sed 's/\"//g' | tr [:upper:] [:lower:])"
OS_VERSION_MIN="$(awk -F= '/^VERSION_ID=/{print $2}' /etc/os-release | \
                  sed 's/\"//g')"
# Debian has no /etc/system-release and so no easy way to determine the full os
# version
OS_VERSION_FULL="$OS_VERSION_MIN"

# Make the shared build functions available
. $DIR/build-funcs.sh

##########################################################
# Build, place, and clean the kernel_beat module
#
build_module "$KERN_BUILD_DIR/$KERN_VERSION/build" "$KB_SRC"

place_module

# Clean the build area
clean_module "$KERN_BUILD_DIR/$KERN_VERSION/build" "$KB_SRC"
