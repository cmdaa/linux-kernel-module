#!/bin/bash
#
# Build-related functions

##
# @method check_rc
# @param  rc    The return code to check {Number};
# @param  ...   The message to output if `rc` indicates an error {String};
#
# @note   If `rc` indicates an error, this method will exit and not return;
#
function check_rc() {
  local rc=$1

  if [ $rc -ne 0 ]; then
    shift
    echo "*** FAILURE[ $rc ]: $@"
    exit $rc
  fi
}

##
# Retrieve the basic OS name for the current system
# @method get_os_name
#
# @return The OS name (or exit)
#
function get_os_name() {
  local KERN_NAME OS_NAME

  KERN_NAME="$(uname -s | tr [:upper:] [:lower:])"
  if [[ $KERN_NAME != 'linux' ]]; then
    cat <<EOF

***
*** kernel_beat can only be compiled for linux-based kernels.
***
*** This system reports a '$KERN_NAME' kernel.
***

EOF
    exit -1
  fi

  if [[ ! -f /etc/os-release ]]; then
    cat <<EOF

***
*** This system has no /etc/os-release
*** so we cannot determine which os-specific build to run.
***

EOF
    exit -1
  fi

  OS_NAME="$(awk -F= '/^ID=/{print $2}' /etc/os-release | \
                  sed 's/\"//g' | tr [:upper:] [:lower:])"

  echo "$OS_NAME"
}

##
# Build the kernel_beat module
# @method build_module
# @param  kern_dir    The kernel build directory {String};
# @param  kb_src      The kernel_beat source build directory {String};
#
function build_module() {
  local KERN_DIR="$1"
  local   KB_SRC="$2"

  echo ">>> Build the kernel_beat module"
  if [[ ! -d "$KERN_DIR" ]]; then
    check_rc -1 "Missing kernel build directory '$KERN_DIR'"
  fi

  if [[ ! -d "$KB_SRC" ]]; then
    check_rc -1 "Missing kernel_beat source directory '$KB_SRC'"
  fi

  make -C "$KERN_DIR" M=$KB_SRC modules
  check_rc $? "kernel_beat module build failed"
}

##
# Clean the kernel_beat module
# @method clean_module
# @param  kern_dir    The kernel build directory {String};
# @param  kb_src      The kernel_beat source build directory {String};
#
function clean_module() {
  local KERN_DIR="$1"
  local   KB_SRC="$2"

  echo ">>> Clean the kernel_beat build"
  make -C "$KERN_DIR" M=$KB_SRC clean
  check_rc $? "kernel_beat module clean failed"
}

##
# Move the compiled kernel_beat module into the builds area
# @method place_module
#
# This method expects the following global environment variables:
#   KB_SRC              kernel_beat source directory
#   KB_VERSION          kernel_beat version
#
#   KERN_ARCH           kernel architecture     (e.g. x86_64)
#   KERN_NAME           kernel name             (e.g. linux)
#   KERN_VERSION        full kernel version
#   KERN_BUILD_DIR      kernel build directory
#
#   OS_NAME             name of the operating system (e.g. centos)
#   OS_VERSION_FULL     full os version
#   OS_VERSION_MIN      minimal os version
#
function place_module() {
  # Versioned kernel_beat file name
  local KB_NAME="kernel_beat-$KB_VERSION"

  # Relative path WITHIN 'builds' to the specific version directory
  local  SPEC_REL="specific/$KERN_VERSION"
  local SPEC_FILE="$SPEC_REL/$KB_NAME.ko"

  # Relative paths from the current working directory
  local SPEC_DIR="builds/$SPEC_REL"
  local  MIN_DIR="builds/$KERN_VERSION"

  [[ ! -d "$SPEC_DIR" ]] && mkdir -p "$SPEC_DIR"
  [[ ! -d "$MIN_DIR" ]]  && mkdir -p "$MIN_DIR"

  # Move the module to the specific builds version directory and compress
  echo ">>> Move the kernel_beat module to builds/$SPEC_FILE.gz"

  mv   -f "$KB_SRC/kernel_beat.ko" "builds/$SPEC_FILE"
  check_rc $? "Cannot move kernel_beat module into place"

  gzip -f "builds/$SPEC_FILE"
  check_rc $? "Cannot compress kernel_beat module"

  # Update the soft-link from the minimal version path
  echo ">>> Update minimal version soft-links in $MIN_DIR"
  ln -sf "../${SPEC_FILE}.gz" "$MIN_DIR/$(basename "$SPEC_FILE").gz"
  check_rc $? "Cannot establish kernel_beat module soft-link"

  ###########################################################################
  # Save information about this build environment
  #
  OS_TAG="${OS_NAME}-${OS_VERSION_FULL}"

  KERN_TAG="${KERN_NAME}-${KERN_VERSION}"

  ENV_FILE="${SPEC_DIR}/kernel_beat-${KB_VERSION}.env"

  echo ">>> Save build environment to $ENV_FILE"
  cat <<EOF > "$ENV_FILE"
###
# Kernel base info: uname -m
#                 : uname -s
#                 : uname -r
#                 : ${KERN_BUILD_DIR}
#
KERN_ARCH="${KERN_ARCH}"
KERN_NAME="${KERN_NAME}"
KERN_VERSION="${KERN_VERSION}"
KERN_BUILD_DIR="${KERN_BUILD_DIR}"

###
# OS base info    : /etc/os-release
#
OS_NAME="${OS_NAME}"
OS_VERSION_FULL="${OS_VERSION_FULL}"
OS_VERSION_MIN="${OS_VERSION_MIN}"

###
# OS full release : /etc/os-release
#
EOF

  awk '{
    if (length($0) < 1){print;next}
    printf("OS_RELEASE_%s\n",$0);
  }' /etc/os-release >> "$ENV_FILE"

  if [[ -f /etc/system-release ]]; then
    cat <<EOF >> "$ENV_FILE"
###
# System release  : /etc/system-release
#
SYS_RELEASE="$(cat /etc/system-release)"
SYS_VERSION="${OS_VERSION_FULL}"

EOF
  fi

  cat <<EOF >> "$ENV_FILE"
###
# KernelBeat version
#
KB_VERSION="${KB_VERSION}"

###
# Simplified tags
#
KB_TAG_OS="${KB_VERSION}-${OS_TAG}"

KB_TAG_KERN="${KB_VERSION}-${KERN_TAG}"

KB_TAG_MIN="${KB_VERSION}-${KERN_TAG}"
KB_TAG_FULL="${KB_VERSION}-${OS_TAG}-${KERN_TAG}"
EOF
}
