#!/bin/bash
#
# Enter a chroot environment.
#
ETC_DIR="$(realpath "$(dirname "$0")")"
LKM_DIR="$(dirname "$ETC_DIR")"

source $ETC_DIR/build-funcs.sh

OS_NAME="$(get_os_name)"

CHROOT_PATH="$(pwd)/chroot-$OS_NAME"

if [[ ! -d "$CHROOT_PATH" ]] ;then
  echo "***"
  echo "*** There is no '$CHROOT_PATH' subdirectory."
  echo "***"
  exit -1
fi

echo ">>> Mount chroot/proc and chroot/dev ..."
mount --bind /proc $CHROOT_PATH/proc
mount --bind /dev $CHROOT_PATH/dev

echo ">>> Enter chroot:"
chroot $CHROOT_PATH

echo ">>> Unmount chroot/proc and chroot/dev ..."
umount $CHROOT_PATH/proc
umount $CHROOT_PATH/dev
