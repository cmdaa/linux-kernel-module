#!/bin/bash
#
# Initialize the host node by installing the proper kernel_beat module.
#
# The kernel_beat module may be retrieved from an S3-based repository if the
# required S3 environment variables are provided. If the module cannot be
# retrieved, a chroot-based build will be attempted. Only if the module can be
# successfully retrieved or built will there be an install attempt.
#
# This script assumes:
#   - the following commands are available:
#       make
#       s4cmd
#       lsmod,rmmod,insmod
#
#   - if the following s3-based environment variables are provided,
#     pull-from/push-to an S3-based kernel-beat repository will be enabled:
#       [S3_SCHEME]       URL scheme [http]
#       S3_HOST           Hostname of the s3 repository
#       S3_PORT           Port number of the s3 repository
#       S3_BUCKET         The kernel-beat bucket within the s3 repository
#       S3_ACCESS_KEY     The access key for the s3 repository
#       S3_SECRET_KEY     The secret key for the s3 repository
#       [S3_INIT_TIMEOUT] On initialization, the timeout between connection
#                         check attempts (seconds) [5];
#       [S3_INIT_TRIES]   On initialization, the maximum number of attempts to
#                         connect to the S3 service before determining that it
#                         is not available [12];
#                           :NOTE: Using the defaults, initialization will wait
#                                  a maximum of 1 minute for the s3 service to
#                                  become available
#                                  (5 second timeout * 12 tries == 60 seconds);
#
#         s4cmd --endpoint=$S3_SCHEME://$S3_HOST:$S3_PORT
#           get s3://$S3_BUCKET/4.4.0-1122.x86_64/kernel_beat-0.15.ko.gz
#
#           put kernel_beat-0.15.ko.gz \
#                   s3://$S3_BUCKET/4.4.0-1122.x86_64/kernel_beat-0.15.ko.gz
#
#       Optionally, these environment variables COULD be written to an s3
#       configuration file of the form:
#         [default]
#         host_base       = ${S3_HOST}:${S3_PORT}
#         host_bucket     = ${S3_HOST}:${S3_PORT}
#
#         access_key      = ${S3_ACCESS_KEY}
#         secret_key      = ${S3_SECRET_KEY}
#
#       The target s3 bucket stores pre-compiled kernel_beat modules organized
#       withing the bucket as:
#         `$KERN_VERSION/kernel_beat-$KB_VERSION.ko.gz`
#
#   - this container is run with privileges to install a kernel module
#     and access the host's system information/configuration (--privileged);
#
#   - this container has read-only access to the host's root filesystem in case
#     a module build is required (--volume /:/mnt/root:ro);
#
ETC_DIR="$(realpath "$(dirname "$0")")"
LKM_DIR="$(dirname "$ETC_DIR")"

##
# @method log
# @param  message   The message to log with date/time stamp
#
# @return void
#
function log {
  echo "$(date '+%Y-%m-%d %H:%M:%S') - install-kernelbeat - $1"
}

##
# @method fail
#
# Terminate with a failure code
#
# @return void
#
function fail() {
    log "Stopping (fail)"
    exit 1
}

# If we don't catch these signals, this script will not respond at all to a
# regular shutdown request, therefore, we explicitly terminate if we
# receive these signals.
trap fail SIGTERM SIGQUIT

##
# If S3 is enabled, ensure that our bucket exists
# @method check_bucket
#
# Uses environment variables:
#   S3_ENDPOINT_URL (S3_SCHEME, S3_HOST, S3_PORT)
#     S3_BUCKET
#     S3_ACCESS_KEY
#     S3_SECRET_KEY
#
# @return An indication of success (0 == success, !0 == failure);
#
function check_bucket() {
  if [[ -z "$S3_ENDPOINT_URL" ]]; then
    return -1
  fi

  local S4CMD="s4cmd --endpoint-url=$S3_ENDPOINT_URL"

  local KB_URL="s3://$S3_BUCKET"

  # Wait for S3 to become available:
  #   Default: 12 tries, 5 seconds between attempts == 1 minute max wait
  ATTEMPT=0 ; LIMIT=${S3_INIT_TRIES:-12}
  while [ 1 ]; do
    ATTEMPT=$(( ATTEMPT + 1 ))

    $ETC_DIR/tcp-port-check.py 1 $S3_HOST $S3_PORT
    if [ $? -eq 0 ]; then
      log "S3 [ $S3_HOST $S3_PORT ] available [ $ATTEMPT attempt(s) ]"
      # connected!
      break;
    fi

    if [ $ATTEMPT -ge $LIMIT ]; then
      log "S3 [ $S3_HOST:$S3_PORT ] not accessible after $LIMIT attempts"
      return -1
    fi

    log "Waiting for S3 [ $S3_HOST $S3_PORT ] ..."
    sleep ${S3_INIT_TIMEOUT:-5}&
    wait $!
  done

  # Check if the target bucket is available
  log "S3 get/put : check bucket $KB_URL ..."

  $S4CMD ls $KB_URL
  if [ $? -ne 0 ]; then
    log "S3 get/put : create new bucket $KB_URL ..."

    $S4CMD mb $KB_URL
    rc=$?
    if [ $rc -ne 0 ]; then
      return $rc
    fi
  fi

  return 0
}

##
# Attempt to pull the kernel-beat module from the repository
# @method pull_module
#
# Uses environment variables:
#   KERN_VERSION
#   KB_VERSION
#   S3_ENDPOINT_URL (S3_SCHEME, S3_HOST, S3_PORT)
#     S3_BUCKET
#     S3_ACCESS_KEY
#     S3_SECRET_KEY
#
# @return An indication of success (0 == success, !0 == failure);
#
function pull_module() {
  if [[ -z "$S3_ENDPOINT_URL" ]]; then
    return -1
  fi

  local S4CMD="s4cmd --endpoint-url=$S3_ENDPOINT_URL"

  local KB_REPO_PATH="$KERN_VERSION/kernel_beat-$KB_VERSION.ko.gz"

  local KB_URL="s3://$S3_BUCKET/$KB_REPO_PATH"
  local KB_ZIP="kernel_beat.ko.gz"

  # S3 get/put : check bucket s3://kernel-beat ...
  # 2021-04-01 13:34 43986 s3://kernel-beat/4.4.0-1124.x86_64/kernel_beat-0.15.ko.gz
  # S3 get/put enabled [ http://cmdaa-minio:9000/kernel-beat ]
  #
  # Pull s3://kernel-beat/4.4.0-1124.x86_64/kernel_beat-0.15.ko.gz
  # s3://kernel-beat/4.4.0-1124.x86_64/kernel_beat-0.15.ko.gz => ./kernel_beat-0.15.ko.gz
  #
  # Decompress kernel_beat.ko.gz
  # gzip: kernel_beat.ko.gz: No such file or directory
  #
  # Cannot decompress kernel_beat.ko.gz


  log "Pull $KB_URL"

  $S4CMD get $KB_URL $KB_ZIP
  rc=$?
  if [ $rc -ne 0 ]; then
    log "Cannot get $KB_URL"
    return $rc
    #fail
  fi

  log "Decompress $KB_ZIP"
  gunzip "$KB_ZIP"
  rc=$?
  if [ $rc -ne 0 ]; then
    log "Cannot decompress $KB_ZIP"
    return $rc
    #fail
  fi

  return 0
}

##
# Attempt to push the kernel-beat module to the repository
# @method push_module
#
# Uses environment variables:
#   KERN_VERSION
#   KB_VERSION
#   S3_ENDPOINT_URL (S3_SCHEME, S3_HOST, S3_PORT)
#     S3_BUCKET
#     S3_ACCESS_KEY
#     S3_SECRET_KEY
#
# @return An indication of success (0 == success, !0 == failure);
#
function push_module() {
  if [[ -z "$S3_ENDPOINT_URL" ]]; then
    return -1
  fi

  local S4CMD="s4cmd --endpoint-url=$S3_ENDPOINT_URL"

  local KB_REPO_PATH="$KERN_VERSION/kernel_beat-$KB_VERSION.ko.gz"

  local KB_URL="s3://$S3_BUCKET/$KB_REPO_PATH"
  local KB_ZIP="$LKM_DIR/builds/$KB_REPO_PATH"

  log "Put $KB_URL"

  $S4CMD put "$KB_ZIP" "$KB_URL"
  rc=$?
  if [ $rc -ne 0 ]; then
    log "Cannot POST $KB_URL"
    return $rc
    #fail
  fi

  return 0
}

##
# Build the kernel-beat module
# @method build_module
#
# Uses environment variables:
#   KERN_VERSION
#   KB_VERSION
#
# @return An indication of success (0 == success, !0 == failure);
#
function build_module() {
  log "Build the kernel_beat module v$KB_VERSION for Linux $KERN_VERSION"

  # It is expected that the build script will place a compressed version of the
  # generated module in:
  #   $LKM_DIR/builds/$KERN_VERSION/kernel_beat-$KB_VERSION.ko.gz
  #
  $ETC_DIR/chroot-build.sh
  rc=$?
  if [ $rc -ne 0 ]; then
    log "Cannot build the kernel_beat module"
    return $rc
  fi

  local KB_ZIP="$LKM_DIR/builds/$KERN_VERSION/kernel_beat-$KB_VERSION.ko.gz"

  log "Decompress $KB_ZIP"
  gunzip -c "$KB_ZIP" > kernel_beat.ko
  rc=$?
  if [ $rc -ne 0 ]; then
    log "Cannot decompress $KB_ZIP"
    return $rc
    #fail
  fi

  return 0
}

##
# (Re)load the kernel-beat module
# @method load_module
#
# Uses environment variables:
#   KERN_VERSION
#   KB_VERSION
#
# @return An indication of success (0 == success, !0 == failure);
#
function load_module() {
  #
  # If the module is currently loaded, unload it now
  #
  if [ ! -z "$(lsmod | grep kernel_beat)" ]; then
    log "Unload the current kernel_beat module"
    rmmod kernel_beat
  fi

  log "Load kernel_beat v$KB_VERSION for linux $KERN_VERSION"
  insmod kernel_beat.ko

  return 0
}

#############################################################################
# main {
#
# Determine if the s3 repository is enabled.
#
if [[ ! -z "$S3_HOST"   &&
      ! -z "$S3_PORT"   &&
      ! -z "$S3_BUCKET" &&
      ! -z "$S3_ACCESS_KEY" &&
      ! -z "$S3_SECRET_KEY" ]]; then

  S3_ENDPOINT_URL="${S3_SCHEME:-http}://$S3_HOST:$S3_PORT"

  check_bucket
  if [[ $? -ne 0 ]]; then
    log "S3 get/put : FAILED to create bucket [ $S3_ENDPOINT_URL/$S3_BUCKET ]"

    unset S3_ENDPOINT_URL

  else
    log "S3 get/put enabled [ $S3_ENDPOINT_URL/$S3_BUCKET ]"
  fi
else
  log "S3 get/put disabled"
  #S3_ENDPOINT_URL=""

fi

###
# Identify the kernel-beat and kernel versions
#
KB_VERSION="$(make -sC $LKM_DIR/kernel-beat version)"

if [[ -z "$KB_VERSION" ]]; then
  log "ERROR: Cannot identify kernel-beat version"
  fail
fi

KERN_VERSION=$(uname -r)

###
# Attempt to pull the module from the repo
pull_module
if [[ $? -ne 0 ]]; then
  # Pull failed -- attempt to build the module
  build_module
  [[ $? -ne 0 ]] && fail

  # Attempt to push the new module to the repo
  #   :NOTE: Beyond logging, we don't care if the push succeeds
  push_module
fi

# (Re)load the kernel module
load_module
[[ $? -ne 0 ]] && fail

log "Complete"
exit 0
# main }
#############################################################################
