#!/bin/bash
#
# Run a build within a specific docker image.
#
# Usage: build.sh [os-image:tag] [header-type]
#
# :NOTE: header-type is only used for ubuntu images
#        since they have multiple types of kernel headers:
#           generic, virtual, oem, kvm
#
# Examples:
#     build.sh centos:7.3.1611
#
#     build.sh ubuntu:16.04
#     build.sh ubuntu:16.04 aws
#
#     build.sh debian:9-slim
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")

BUILD_IMAGE="${1:-centos:7.3.1611}"

   BASE_TYPE="$(echo "$BUILD_IMAGE" | sed -E 's/:.*$//')"
BUILD_SCRIPT="build-$BASE_TYPE.sh"

if [[ ! -f "$DIR/$BUILD_SCRIPT" ]] ;then
  echo "*** No build script for $BASE_TYPE ( $BUILD_IMAGE )"
  exit -1
fi

HEADER_TYPE=""
echo -n ">>> Building from $BUILD_IMAGE"

if [[ $# -gt 1 ]]; then
  HEADER_TYPE="${2:-generic}"

  echo -n " [ header-type $HEADER_TYPE ]"
fi
echo

docker run --rm -i \
  --volume  $ROOT:/root/linux-kernel-module \
  --workdir /root/linux-kernel-module \
    $BUILD_IMAGE \
      ./etc/$BUILD_SCRIPT $HEADER_TYPE
