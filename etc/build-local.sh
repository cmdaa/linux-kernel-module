#!/bin/bash
#
# Attmpt to run a local build.
#
# Usage: build-local.sh [header-type]
#
# :NOTE: header-type is only used for ubuntu images
#        since they have multiple types of kernel headers:
#           generic, virtual, oem, kvm
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")

source $DIR/build-funcs.sh

HEADER_TYPE="$1"

OS_NAME="$(get_os_name)"

BUILD_SCRIPT="build-$OS_NAME.sh"

if [[ ! -f "$DIR/$BUILD_SCRIPT" ]] ;then
  echo "*** No build script for $OS_NAME"
  exit -1
fi

echo ">>> Building for $OS_NAME: DIR[ $DIR ], BUILD_SCRIPT[ $BUILD_SCRIPT ]"

$DIR/$BUILD_SCRIPT $HEADER_TYPE
