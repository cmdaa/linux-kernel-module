#!/usr/bin/env python3
#
# A simple netcat-like python-based TCP port access check.
#
# Usage: tcp-port-check.py <timeout-secs> <host> <port>
#
import sys
import socket

if len(sys.argv) < 4: #{
  print('*** Usage: tcp-port-check.py <timeout-secs> <host> <port>')
  sys.exit(-1)
#}

timeout = int( sys.argv[1] )
host    = sys.argv[2]
port    = int( sys.argv[3] )

location = ( host, port )

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Timeout (seconds)
sock.settimeout( timeout )

try: #{
  rc = sock.connect_ex( location )

  #if rc == 0: #{
  #  print('>>> Connected')
  #else: #}{
  #  print('*** NOT Connected [ %s ]' % (rc))
  ##}

except Exception as ex: #}{
  #print('*** Exception:', ex )
  rc = -1
#}

sock.close()

sys.exit( rc )
