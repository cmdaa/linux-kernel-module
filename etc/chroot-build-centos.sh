#!/bin/bash
#
# Build kernel-beat from within a CentOS chroot environment that mirros the
# current host.
#
# This script requires root permissions.
#
# This script will:
#   - determine the kernel and operating system versions of the this host;
#   - fetch the raw CentOs RPMs for the operating system and kernel;
#   - use the raw RPMs to create a mirror chroot environment;
#   - run the 'build-local.sh' script from within the chroot environment to
#     build a kernel module for the host;
#   - copy the generated build artifacts out of the chroot
#     '/root/linux-kernel-module/builds' to 'builds';
#   - unmount the chroot environment;
#
ETC_DIR="$(realpath "$(dirname "$0")")"
LKM_DIR="$(dirname "$ETC_DIR")"

#CHROOT_PATH="$(pwd)/chroot-centos"
CHROOT_PATH="/mnt/chroot-centos"

source $ETC_DIR/build-funcs.sh

##
# Given a pre-populated package list contained in the '$PKG_LIST' file,
# attempt to locate and download the given package.
# @method get_pkg
# @param  pkg_base_name   The base search name for the target package;
#
# Expected globals:
#   OS_VERSION  the target OS version;
#   PKG_LIST    the path to a file containing the full package list (HTML);
#
# @returns  The path to the file containing the package;
function get_pkg() {
  local pkg_base_name=$1
  local pkg_full_name="$(awk '/'$pkg_base_name'/{print}' "$PKG_LIST" |
                          sed -E 's/^.*('$pkg_base_name'[^>]+)<.*$/\1/')"
  if [ -z "$pkg_full_name" ]; then
    echo "*** Cannot locate package [ $pkg_base_name ]"
    exit -1
  fi

  if [ ! -f "$pkg_full_name" ]; then
    curl -Lks -o "$pkg_full_name" \
      "https://vault.centos.org/$OS_VERSION/os/x86_64/Packages/$pkg_full_name"
    if [ $? -ne 0 -o ! -f $pkg_full_name ]; then
      echo "*** Cannot retrieve package [ $pkg_full_name ]"
      exit -1
    fi
  fi

  echo "$pkg_full_name"
}

##
# Cleanup chroot mounts
# @method cleanup_mounts
#
function cleanup_mounts() {
  echo -n ">>> Cleanup: umount proc ..."
  umount "$CHROOT_PATH/proc"
  if [ $? -ne 0 ]; then echo " FAILED"
  else             echo " done"
  fi

  echo -n ">>> Cleanup: umount dev ..."
  umount "$CHROOT_PATH/dev"
  if [ $? -ne 0 ]; then echo " FAILED"
  else             echo " done"
  fi
}

if [ ! -d "$CHROOT_PATH" ]; then
  # Build a CentOS chroot with a specific CentOS release and kernel:
  #
  #   https://geek.co.il/2010/03/14/how-to-build-a-chroot-jail-environment-for-centos
  #   http://yuminstall.org/how-to-install-centos-release-7-7-1908-0-el7-centos-x86_64-on-centos7/
  #
  echo ">>> Build $CHROOT_PATH ..."

  ###
  # Determine the CentOS version
  #
  echo -n ">>> Determine CentOS version ..."
  OS_VERSION="$(cat /etc/centos-release |
                  sed -E 's/^Cent[^0-9]+([0-9.]+).*$/\1/')"
  check_rc $? "cat"
  if [ -z "$OS_VERSION" ]; then
    echo
    echo "*** Cannot identify the CentOS version"
    exit -1
  fi
  echo " $OS_VERSION"

  ###
  # Using the CentOS version, locate a matching package in the CentOS valut
  #
  echo -n ">>> Fetch CentOS package list from the value ..."
  PKG_LIST="centos-$OS_VERSION.pkgs"
  curl -Lks -o "$PKG_LIST" \
    https://vault.centos.org/$OS_VERSION/os/x86_64/Packages/
  check_rc $? "curl"
  echo " done"

  echo -n ">>> Get the CentOS release package ..."
  OS_PKG="$(get_pkg 'centos-release')"
  check_rc $? "get_pkg"
  echo " $OS_PKG"

  echo -n ">>> Get the kernel-devel package ..."
  KD_PKG="$(get_pkg 'kernel-devel')"
  check_rc $? "get_pkg"
  echo " $KD_PKG"

  echo ">>> Generate the base 'chroot' ..."
  mkdir "$CHROOT_PATH"
  rpm -i --root="$CHROOT_PATH" --nodeps $OS_PKG
  check_rc $? "rpm"

  echo ">>> Populate 'chroot' with base packages ..."
  #yum --installroot="$CHROOT_PATH" install -y rpm-build yum
  yum --installroot="$CHROOT_PATH" install -y \
    rpm-build yum gcc make
  check_rc $? "yum"

  echo ">>> Force install $KD_PKG ..."
  rpm -i --root="$CHROOT_PATH" --nodeps $KD_PKG
  check_rc $? "rpm"

  echo -n ">>> Copy in linux-kerne-beat sources ..."
  mkdir -p "$CHROOT_PATH/root/linux-kernel-module"
  check_rc $? "mkdir"

  cp -R "$LKM_DIR/etc" "$LKM_DIR/kernel-beat" \
    "$CHROOT_PATH/root/linux-kernel-module"
  check_rc $? "cp"
  echo " done"

else
  echo ">>> Use the existing $CHROOT_PATH"
fi

echo -n ">>> Mount chroot/proc and chroot/dev ..."
mount --bind /proc $CHROOT_PATH/proc
check_rc $? "proc"

mount --bind /dev $CHROOT_PATH/dev
check_rc $? "def"
echo " done"

echo ">>> Enter chroot [ $CHROOT_PATH] and run the build ..."
chroot "$CHROOT_PATH" /root/linux-kernel-module/etc/build-local.sh
rc=$?
cleanup_mounts
check_rc $? "chroot build-local.sh"

echo ">>> Copy new builds out of chroot/builds ..."
tar -cC "$CHROOT_PATH" -b 8192 -f - builds | \
  tar -xvC "$LKM_DIR" -b 8192 -f -
check_rc $? "tar"

########################################################################
#curl -Lks -o centos-release-7-7.1908.0.el7.centos.x86_64.rpm \
#  https://vault.centos.org/7.7.1908/os/x86_64/Packages/centos-release-7-7.1908.0.el7.centos.x86_64.rpm
#
#mkdir chroot
#sudo rpm -i --root=$(pwd)/chroot --nodeps \
#  centos-release-7-7.1908.0.el7.centos.x86_64.rpm
#
#sudo yum --installroot=$(pwd)/chroot install -y rpm-build yum
#
#sudo mount --bind /proc $(pwd)/chroot/proc
#sudo mount --bind /dev $(pwd)/chroot/dev
#
#sudo chroot $(pwd)/chroot
#
## Unmount
#sudo umount $(pwd)/chroot/proc
#sudo umount $(pwd)/chroot/dev
#
###
#wget http://mirror.centos.org/centos/5/os/x86_64/CentOS/centos-release-5-4.el5.centos.1.x86_64.rpm
#
#and just install it forcefully:
#
#rpm -i --root=/var/tmp/chroot --nodeps centos-release-5-4.el5.centos.1.x86_64.rpm
#
#Finally we can call on YUM to install the rest of our system:
#
#yum --installroot=/var/tmp/chroot install -y rpm-build yum


