#!/bin/bash
#
# This script will:
#   - use or create '/mnt/root' as a read-only mirror of the host's root
#     filesystem;
#   - establish a temporary '/mnt/overlay/*' to create/mount a chroot
#     environment that mirrors the host;
#   - run the 'build-local.sh' script from within the chroot environment to
#     build a kernel module for the host;
#   - copy the generated build artifacts out of the temporary overlay to
#     '/root/linux-kernel-module/builds' which MAY be read-write mounted
#     to provide access to the build artifact outside of this container;
#   - unmount the chroot environment, and, if it was mounted by this script,
#     '/mnt/root';
#
# :NOTE: If '/mnt/root' does not exist, this script will temporarily mount
#        the root partition as a read-only bind mount to '/mnt/root'.
#
# :NOTE: If this script is run within a container, it expects that it will
#        have:
#         - access to the host system with read-only access to the host
#           system's root filesystem (/mnt/root);
#         - read-write access to a host-based build area to receive the build
#           artifacts;
#         - docker flags to provide this environment:
#             --privileged
#             --pid=host
#             --volume /:/mnt/root:ro
#             --volume <local-build-repo>:/root/linux-kernel-module/builds
#
# :NOTE: Newer versions of docker will NOT allow overlay over overlay so we
#        have to resort to creating a temporary 'tmpfs' filesystem and using it
#        to perform the overlay mount.
#
#        Ref: https://stackoverflow.com/questions/29550736/
#                 can-i-mount-docker-host-directory-as-copy-on-write-overlay
#
ETC_DIR="$(realpath "$(dirname "$0")")"
LKM_DIR="$(dirname "$ETC_DIR")"

UMOUNT_ROOT=0

function _check_rc() {
  local rc=$1

  if [ $rc -ne 0 ]; then
    shift
    echo "*** FAILURE[ $rc ]: $@"
    exit $rc
  fi
}

function cleanup() {
  local rc=${1:-0}

  ###
  # Unmount
  if [ -d /mnt/chroot ]; then
    echo -n ">>> Unmount /mnt/chroot ..."
    umount /mnt/chroot ; _check_rc $? "umount /mnt/chroot"
    rmdir  /mnt/chroot ; _check_rc $? "rmdir /mnt/chroot"
    echo " done"
  fi

  if [ -d /mnt/overlay ]; then
    echo -n ">>> Unmount /mnt/overlay ..."
    umount /mnt/overlay ; _check_rc $? "umount /mnt/overlay"
    rmdir  /mnt/overlay ; _check_rc $? "rmdir /mnt/overlay"
    echo " done"
  fi

  if [ -d /mnt/root -a $UMOUNT_ROOT -eq 1 ]; then
    echo -n ">>> Unmount /mnt/root ..."
    umount /mnt/root ; _check_rc $? "umount /mnt/root"
    rmdir  /mnt/root ; _check_rc $? "rmdir /mnt/root"
    echo " done"
  fi

  # Ensure a clean exit
  exit $rc
}

function check_rc() {
  local rc=$1

  if [ $rc -ne 0 ]; then
    shift
    echo "*** FAILURE[ $rc ]: $@"
    cleanup $rc
  fi

  echo -n "."
}


if [ ! -d /mnt/root ]; then
  # Temporarily read-only bind mount / to /mnt/root
  UMOUNT_ROOT=1
  echo -n ">>> Bind-mount / (read-only) to /mnt/root ..."
  mkdir /mnt/root ;                check_rc $? "mkdir /mnt/root"
  mount --bind -o ro / /mnt/root ; check_rc $? "read-only mount / to /mnt/root"
  echo " done"
fi

if [ ! -d /mnt/overlay ]; then
  echo -n ">>> Mount tmpfs to /mnt/overlay and initialize ..."
  mkdir /mnt/overlay ;                check_rc $? "mkdir /mnt/overlay"
  mount -t tmpfs tmpfs /mnt/overlay ; check_rc $? "mount tmpfs to /mnt/overlay"

  mkdir -p /mnt/overlay/work \
           /mnt/overlay/upper/dev \
           /mnt/overlay/upper/etc \
           /mnt/overlay/upper/root/linux-kernel-module
  check_rc $? "mkdir /mnt/overlay/{work,upper}"

  if [ -c /dev/urandom ]; then
    ####
    # Without /dev/urandom, trying to run 'yum' on Centos 7.7.1908 from within
    # the chroot results in:
    #   error: Failed to initialize NSS library
    #
    # The discussion at
    #   https://serverfault.com/questions/866294/error-failed-to-initialize-nss-library
    # suggests this is due to the use of the dynamic loader, which apparently
    # needs '/dev/urandom'.
    #
    # Looking further, /usr/lib64/python2.7/site-packages/rpm is a python
    # module that wraps a dynamic library. This appears to the be source of the
    # load issues.
    #
    UR_NUMS="$(ls -l /dev/urandom | sed 's/,//g' | 
                  awk '{printf("%s %s\n",$5,$6);}')"
    mknod /mnt/overlay/upper/dev/urandom c ${UR_NUMS}
    check_rc $? "mknod /mnt/overlay/upper/dev/urandom c ${UR_NUMS}"
  fi

  # Ensure we have a usable version of /etc/resolv.conf
  # (on Ubuntu, this is a soft-link to '/run/systemd/resolve/stub-resolv.conf'
  #  which is not accessible from within the chroot)
  cp -H /etc/resolv.conf /mnt/overlay/upper/etc
  check_rc $? "copy /dev/resolv.conf"

  # Copy in the linux-kernel-module etc and kernel-beat sources
  cp -R $LKM_DIR/etc $LKM_DIR/kernel-beat \
        /mnt/overlay/upper/root/linux-kernel-module
  check_rc $? "copy linux-kernel-module"

  echo " done"
fi

# Create the chroot as an overlay filesystem
echo -n ">>> Create chroot as an overlay ..."
if [ ! -d /mnt/chroot ]; then
  mkdir -p /mnt/chroot ; check_rc $? "mkdir /mnt/chroot"
  echo -n "."
fi

mount -t overlay overlay \
  -o lowerdir=/mnt/root,upperdir=/mnt/overlay/upper,workdir=/mnt/overlay/work \
    /mnt/chroot
check_rc $? "mount chroot as an overlay"
echo " done"

# Enter the chroot environment and run the build
#
# :NOTE: Since the build is run from '/' the final builds will be placed in
#        '/builds' => '/mnt/overlay/upper/builds'
#
echo ">>> Enter chroot and run the build ..."
chroot /mnt/chroot /root/linux-kernel-module/etc/build-local.sh
check_rc $? "chroot build"

echo ">>> Copy new builds out of the temporary mount ..."
tar -cC /mnt/overlay/upper -b 8192 -f - builds | \
  tar -xvC "$LKM_DIR" -b 8192 -f -
check_rc $? "tar"

# Ensure a clean exit
cleanup 0
