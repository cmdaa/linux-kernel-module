#!/bin/bash
#
# This script will:
#   - determine the host's OS;
#   - run the matching 'chroot-build-%OS%.sh' script to build a chroot
#     environment that mirros the host, and run the kernel build build, copying
#     out the module;
#
# :NOTE: If this script is run within a container, it expects that it will
#        have:
#         - read-write access to a host-based build area to receive the build
#           artifacts;
#         - docker flags to provide this environment:
#             --privileged
#             --pid=host
#             --volume <local-build-repo>:/root/linux-kernel-module/builds
#
ETC_DIR="$(realpath "$(dirname "$0")")"

source $ETC_DIR/build-funcs.sh

OS_NAME="$(get_os_name)"

BUILD_SCRIPT="$ETC_DIR/chroot-build-${OS_NAME}.sh"

if [[ ! -f "$DIR/$BUILD_SCRIPT" ]] ;then
  echo "*** No build script for $OS_NAME"
  exit -1
fi

# Enter the chroot environment and run the build
#
# :NOTE: Since the build is run from '/' the final builds will be placed in
#        '/builds' => '/mnt/overlay/upper/builds'
#
$BUILD_SCRIPT

# Ensure a clean exit
exit 0
