#!/bin/bash
#
# Run within an ubuntu system to build kernel_beat.
#
# Usage: build-ubuntu.sh [header-type]
#
# :NOTE: header-type is only used for ubuntu images
#        since they have multiple types of kernel headers:
#           generic, virtual, oem, kvm
#             v
#             linux-headers-generic
#
#        even with specific kernel versions:
#           4.15.0-1095-aws
#             v
#             linux-headers-4.15.0-1095-aws
#
#
# To run within an ubuntu docker image:
#
#   docker run --rm -i \
#     --volume  $(pwd):/root/linux-kernel-module \
#     --workdir /root/linux-kernel-module \
#       ubuntu:16.04 \
#         ./etc/build-ubuntu.sh $HEADER_TYPE
#
set -e

 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")

#
# Determine if this build is running within a docker container.
# (If so, we cannot rely on 'uname' to determine the target OS/kernel).
#
DOCKER_CGROUPS=( $(cat /proc/1/cgroup 2>/dev/null | awk '/docker\//') )
IN_DOCKER=${#DOCKER_CGROUPS[@]}

if [[ $IN_DOCKER -eq 0 ]]; then
  echo ">>> Native Ubuntu build"
else
  echo ">>> Docker-based Ubuntu build"
fi


if [[ $# -gt 0 ]]; then
  HEADER_TYPE="$1"

  #
  # When determining the kernel build version, look in the kernel build area
  # instead of using 'uname -r'.
  #
  KERN_VERSION_FROM_UNAME=0

elif [[ $IN_DOCKER -eq 0 ]]; then
  #
  # Attempt to determine the proper header types.
  #
  # :NOTE: This will only work properly if we are NOT within a docker
  #        container since from within docker, 'uname' reports inforamtion
  #        about the system running docker and not the container.
  #
  # The release *should* have the form:
  #   4.4.0-1122-aws
  #              ^^^- kernel header type
  #         ^^^^----- kernel build
  #   ^^^^^---------- kernel version
  #
  KERN_VERSION=$(uname -r)
  HEADER_TYPE=$(echo "$KERN_VERSION" | sed 's/-/./g' | \
                  awk -F. '{print $NF}')

  # When determining the kernel build version, use 'uname -r'
  KERN_VERSION_FROM_UNAME=1

fi

if [[ -z "$HEADER_TYPE" ]]; then
  # Fall back to 'generic'
  HEADER_TYPE="generic"
fi

echo ">>> Building for ubuntu, headers '${HEADER_TYPE}'"

##########################################################
# Ensure the required packages are installed
#
REQUIRED_PKGS=( 'build-essential' "linux-headers-${HEADER_TYPE}" )

PKG_LIST="$(apt list --installed 2>/dev/null)"

# Determine which packages needs to be installed
INSTALL_PKGS=()
for PKG in ${REQUIRED_PKGS[@]}; do
  HAS_PKG="$(echo "$PKG_LIST" | awk '/^'$PKG'/')"
  if [[ -z "$HAS_PKG" ]]; then
    INSTALL_PKGS=( ${INSTALL_PKGS[@]} "$PKG" )
  fi
done

if [[ ${#INSTALL_PKGS[@]} -gt 0 ]]; then
  echo ">>> Install packages [ ${INSTALL_PKGS[@]} ]"
  echo ">>>   :NOTE: If is done within a chroot build and fails,"
  echo ">>>          the best bet is to install the required"
  echo ">>>          packages directly on the host node and run"
  echo ">>>          the build script again."
  apt-get install -y ${INSTALL_PKGS[@]}
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echo "*** Package install(s) failed, this build will likely fail as well"
  fi
fi

##########################################################
# Identify the build environment
#
KB_SRC="${ROOT}/kernel-beat"
KB_VERSION="$(make -sC $KB_SRC version)"

KERN_BUILD_DIR="/lib/modules"
if [[ $KERN_VERSION_FROM_UNAME -eq 1 ]]; then
  #
  # Determine the target kernel version from `uname -r`
  #
  KERN_VERSION=$(uname -r)
else
  #
  # Determine the target kernel version from the newest directory in the kernel
  # build area.
  #
  KERN_VERSION=$(ls -t1 $KERN_BUILD_DIR | head -1)
fi

# :XXX: ASSUME the base system and architecture are proper reported even within
#       docker
KERN_NAME="$(uname -s | tr [:upper:] [:lower:])"
KERN_ARCH="$(uname -m | tr [:upper:] [:lower:])"

OS_NAME="$(awk -F= '/^ID=/{print $2}' /etc/os-release | \
                  sed 's/\"//g' | tr [:upper:] [:lower:])"
OS_VERSION_MIN="$(awk -F= '/^VERSION_ID=/{print $2}' /etc/os-release | \
                  sed 's/\"//g')"
OS_VERSION_FULL="$(awk -F= '/^VERSION=/{print $2}' /etc/os-release | \
                    sed 's/\"//g' | awk '{print $1}')"

# Make the shared build functions available
. $DIR/build-funcs.sh

##########################################################
# Build, place, and clean the kernel_beat module
#
build_module "$KERN_BUILD_DIR/$KERN_VERSION/build" "$KB_SRC"

place_module

# Clean the build area
clean_module "$KERN_BUILD_DIR/$KERN_VERSION/build" "$KB_SRC"
