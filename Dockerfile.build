# This image should be run with:
#   --privileged
#   --pid=host
#   # Read-only access to the host's root filesyste
#   --volume /:/mnt/root:ro
#   # To receive the build artifacts
#   --volume $(pwd)/builds:/root/linux-kernel-module/builds
#
# e.g.
#   docker run --rm -it --privileged --pid=host \
#     --volume /:/mnt/root:ro \
#     --volume "$(pwd)/builds:/root/linux-kernel-module/builds"
#
FROM ubuntu:20.04

ADD etc                   /root/linux-kernel-module/etc
ADD kernel-beat/Makefile  /root/linux-kernel-module/kernel-beat/Makefile
ADD kernel-beat/src       /root/linux-kernel-module/kernel-beat/src

#
# Packages to allow this image to also be used, via the
# `etc/kernelbeat-init.sh` script, to determine if a build is needed and
# query/install the module:
#   make      the determine the kernel_beat version
#               :NOTE: for this step, we *could* just pull out the awk that
#                      retrieves the version instead of running `make`;
#   s4cmd     enable this image to be used to:
#               1. attempt to pull a pre-compiled module from the repository
#                  before a build is required;
#               2. push a newly compiled module to the repository;
#   kmod      enable query and installation of the module;
#
RUN apt update && \
    DEBIAN_FRONTEND=noninteractive \
      apt install --no-upgrade -y make s4cmd kmod

VOLUME  [ "/mnt/root", "/root/linux-kernel-module/builds" ]

WORKDIR "/root"

CMD [ "/root/linux-kernel-module/etc/chroot-build.sh" ]
