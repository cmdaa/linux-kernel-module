BUILD_IMAGE := cmdaa/kernel_beat-build
BUILD_TAG   := $(shell make -sC kernel-beat version)

###
builder:
	docker build \
	  -f Dockerfile.build \
	  -t $(BUILD_IMAGE):$(BUILD_TAG) \
	    .

push:
	docker tag $(BUILD_IMAGE):$(BUILD_TAG) $(BUILD_IMAGE):latest
	docker push $(BUILD_IMAGE):$(BUILD_TAG)
	docker push $(BUILD_IMAGE):latest

###
run-sh:
	docker run --rm -it \
	  --volume /:/mnt/root:ro \
	  --volume "$(shell pwd)/builds:/root/linux-kernel-module/builds" \
	  $(BUILD_IMAGE):$(BUILD_TAG) \
	    /bin/bash

run-build:
	docker run --rm -it \
	  --privileged \
	  --volume /:/mnt/root:ro \
	  --volume "$(shell pwd)/builds:/root/linux-kernel-module/builds" \
	  $(BUILD_IMAGE):$(BUILD_TAG)

info:
	@echo "BUILD_IMAGE[ $(BUILD_IMAGE) ]"
	@echo "BUILD_TAG  [ $(BUILD_TAG) ]"
